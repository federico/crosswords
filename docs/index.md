Development guide for Crosswords
================================

Build instructions
------------------

* [Compiling on MacOS](macos-build.md)

Coding style
------------

* [Style Guide](style-guide.md)


Design documents
----------------

FIXME: We may want to split these by topic.

* [Background shapes for cells](cell-shapebg.md)
* [CSS in Crosswords](css.md)
* [Edit Autosolve Dialog](edit-autofill-dialog.md)
* [Game State](game-state.md)
* [Grid Layout](grid-layout.md)
* [Crossword Quirks](language-quirks.md)
* [Overlays on the grid](overlays.md)
* [PlayXwordColumn](play-xword-column.md)
* [Puzzle Downloader](puzzle-downloader.md)
* [Puzzle Set Resource](puzzle-set-resource.md)
* [Puzzle Sets](puzzle-sets.md)
* [The PuzzleStack](puzzle-stack.md)
* [Immutable XwordState](stateless.md)
* [Word List](word-list.md)
* [Word Solver](word-solver.md)
* [Crossword State](xword-state.md)
* [Play State Modes refactor](xword-state-modes.md)

