# Crossword Quirks
## Problem Statement

One challenge we've run into is per-language (and per-puzzle)
quirks. As an example, Dutch has an "IJ" digraph that appears in
puzzles as a character pair. Another example is the existence of rebus
puzzles in english. To handle that, we need to provide alternate
behaviors for crosswords.

Quirks is our way of encapsulating the game behavior in a single
place. It is primarily used by an XwordState to adjust the behavior of
a grid, but can be used outside of a play state to adjust the outer
chrome.

## Assumptions

To best handle that, we have a quirk GObject that can be included with
a puzzle state. It's optional – no quirks means standard behavior. But
it's a way to change the behavior on a per-puzzle basis. In addition,
it's possible to have quirks based on the environment of the crossword
game, so a quirks object without an associated crossword is possible

## List of quirks

* **IJ_DIGRAPH:** Dutch support for IJ digraph
* **GUESS_ADVANCE:** User setting for how to advance the cursor after a guess
