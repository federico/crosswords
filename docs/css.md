# CSS in Crosswords

## PlayCell

PlayCell can have the following class styles:

* **null**: An empty cell
* **normal**: A normal cell. Rendered with a light background
* **block**: A block cell. Rendered with a dark background
* **initial-val**: A cell that's not editable because it has an initial val.
* **error**: A cell that's revealed to show an error
* **highlighted**: A cell that's part of highlight row
* **focused**: A cell that has the current focus

