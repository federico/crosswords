# Style Guide

If in doubt, follow the GTK+ style.

## Random notes
* Declare all variables at the top of the block.
* Always use `g_autofree` and `g_autoptr` where possible.
* Do one g_autofree per line, and make sure it's initialized to NULL.

## FIXME notes

If you're going to leave a fixme in the code, please put the area
that's being worked on in parens after the comment. That make it easy
to grep for all areas of a common domain that may need fixing. As an
example:

``` /* FIXME(css): This color is hardcoded and should be loaded from the .css file */```

Common domains:
* refactor: a reminder to clean up a messy area of code
* optimization: a reminder that a chunk of code is unnecessarily slow/big

We don't use TODO or have a TODO.md file. Please file bugs for all
other issues.
