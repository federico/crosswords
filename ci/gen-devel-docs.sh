#!/bin/sh

set -eu

mkdir -p public/devel-docs
sphinx-build docs public/devel-docs
