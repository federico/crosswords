
![GNOME Crosswords](data/images/hero.png)<br>
_A Crossword player and editor for GNOME._


**GNOME Crosswords** is a crossword player and editor.

Features:
*   Uses .ipuz files internally and supports a significant chunk of
    the .ipuz spec for crosswords
*   Supports cryptic, barred, acrostic, and rebus-style puzzles
*   Loads .ipuz, puz, and .jpz files from disk
*   Supports standalone puzzle collections of crosswords with multiple
    ways of playing them
*   External puzzle set downloaders can be used to download puzzles
*   Extensive styling support for crosswords. Square, black and white
    crosswords are traditional, but it can also take advantage of
    color and shapes
*   Reveal button to find mistakes in the puzzle
*   Hint button to suggest possible answers using [Peter Broda's Wordlist](https://peterbroda.me/crosswords/wordlist/?fbclid=IwAR04CeR_nhEW5M7CoK6Pyc3lxtzAlD9i9nk6pYadGXWtWN9pTBNWvHCE2hk)
*   Puzzle checksums for puzzles that don't include an answer
*   Zoomable interface to support multiple screen sizes
*   Respects the Desktop-wide dark-mode preference
*   Language-specific quirks

The current version is 0.3.6. You can read about the latest changes in
the [NEWS](NEWS.md) file.

![Example Puzzle](data/images/a-dogs-day.png)<br>
_Example crossword_

### Installation

The game is available for most Linux distros at
[flathub](https://flathub.org/apps/details/org.gnome.Crosswords).

It is also available within Fedora: `sudo dnf install crosswords` will
install it.

### GNOME Crossword Editor

This program was originally intended to be a standalone game. However,
it grew a separate crossword editor after we realized that having a
standalone tool was needed to ship higher quality crosswords. It is
also a really interesting project to work on.

The editor is still under development and is somewhat rudimentary in
nature. However, it can be used to create basic crosswords with grids
and clues. It has a pattern solver and grid autofill dialog for
filling in hard-to-finish corners, and will make suggestions of words
when creating the grid.

## Crosswords
This game won't be fun at all without good puzzles! If you are
interested in joining me in building a great set of crosswords for
people to play, [let me know](mailto:jrb@gnome.org)! I'd love to add a
bunch more puzzles to the game and showcase other people's works.

We support standalone puzzle sets, so it should be possible to bundle
up a bunch of puzzles together under a common theme and distribute
them as a standalone.

We save crosswords as [ipuz](http://ipuz.org) files loaded by
[libipuz](https://gitlab.gnome.org/jrb/libipuz). This is a great, free
file format.

_ipuz is a trademark of Puzzazz, Inc., used with permission_

## Community

We have a budding community of folks working on the game, solver, and
puzzles on #crosswords on gimp matrix. Feel free to drop by and say
hi! We're happy to talk about anything crossword related.

You can join the chat room
[here](https://matrix.to/#/#crosswords:gnome.org).

## Build instructions

This builds on most modern Linux distributions and MacOS ([build
hints](docs/macos-build.md)). It was developed primarily on Fedora and
OpenSuse. It has dependencies on GTK 4.8 and libadwaita-1.2, which
will be built submodules if not installed.

The easiest way to build and test Crosswords from source is by building a flatpak. This command will build and install it

```shell
$ flatpak-builder  --force-clean _flatpak/ org.gnome.Crosswords.Devel.json  --user --install
$ # To run the output:
$ flatpak run org.gnome.Crosswords.Devel
```

### Local build

To build it on Linux, use standard meson tools:

```shell
$ meson setup _build
$ ninja -C _build
```

**Note:** The meson in Ubuntu 21.10 isn't recent enough to build this app. You can
install a local version of meson that's sufficiently new by running `pip3
install --user meson`.


### .puz files (Optional)

In order to use the convertor to load other crossword types, you need
to install some python dependencies. The easiest way to do this is to
use pip:

```shell
$ pip install -r requirements.txt
```

## Running Crosswords

Running it out of the `builddir` is a little more involved as it
requires some environment variables set to work. To make this simple
and avoid a full system installation, a `run` script is included.

Use it as follows:

```shell
$ cd _build/

$ # To run the game
$ ./run ./src/crosswords

$ # To run the crossword editor
$ ./run ./src/crossword-editor

$ # To use the convertor
$ ./run ./tools/ipuz-convertor -i puzzle.puz -o /path/to/puzzle.ipuz
```

## Test it!

Bugs should be reported to the our
[bugtracking system](https://gitlab.gnome.org/jrb/crosswords/issues)
on GNOME gitlab.

## Development

Please see the [Development guide](https://jrb.pages.gitlab.gnome.org/crosswords/devel-docs/index.html).
