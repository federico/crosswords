/* crosswords-misc.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <locale.h>
#include <glib/gi18n-lib.h>

#include "crosswords-misc.h"
#include "grid-layout.h"
#include "play-grid.h"
#include "svg.h"

GdkPixbuf *
xwd_pixbuf_load_from_gresource (GResource  *resource,
                                const char *resource_path)
{
  GBytes *bytes;
  g_autoptr (GError) error = NULL;
  g_autoptr (GdkPixbufLoader) loader = NULL;
  GdkPixbuf *pixbuf;

  if (resource == NULL)
    bytes = g_resources_lookup_data (resource_path,
                                     G_RESOURCE_LOOKUP_FLAGS_NONE,
                                     NULL);
  else
    bytes = g_resource_lookup_data (resource, resource_path,
                                    G_RESOURCE_LOOKUP_FLAGS_NONE,
                                    NULL);
  loader = gdk_pixbuf_loader_new ();
  gdk_pixbuf_loader_write_bytes (loader, bytes, &error);
  gdk_pixbuf_loader_close (loader, NULL);

  pixbuf = gdk_pixbuf_loader_get_pixbuf (loader);
  if (pixbuf)
    g_object_ref (G_OBJECT (pixbuf));

  return pixbuf;
}

GtkWidget *
xwd_image_new_from_gresource (GResource  *resource,
                              const char *resource_path)
{
  GtkWidget *image;
  GdkPixbuf *pixbuf = NULL;

  pixbuf = xwd_pixbuf_load_from_gresource (resource, resource_path);
  if (pixbuf != NULL)
    {
      image = gtk_image_new ();
      gtk_image_set_from_pixbuf (GTK_IMAGE (image), pixbuf);
      g_object_unref (pixbuf);
      return image;
    }

  return NULL;
}


IPuzPuzzle *
xwd_load_puzzle_from_resource (GResource  *resource,
                               const char *resource_path)
{
  g_autoptr (GInputStream) stream = NULL;
  IPuzPuzzle *puzzle;

  if (resource)
    stream = g_resource_open_stream (resource, resource_path, 0, NULL);
  else
    stream = g_resources_open_stream (resource_path, 0, NULL);
  puzzle = ipuz_puzzle_new_from_stream (stream, NULL, NULL);

  return puzzle;
}


GdkPixbuf *
xwd_thumbnail_puzzle (IPuzPuzzle *puzzle)
{
  GridLayout *layout;
  XwordState *state;
  GdkPixbuf *pixbuf;
  GString *s;
  g_autoptr (GInputStream) stream = NULL;

  state = xword_state_new (IPUZ_CROSSWORD (puzzle), NULL, XWORD_STATE_VIEW);
  layout = grid_layout_new (state);
  LayoutConfig config = {
    .border_size = 1,
    .base_size = 3,
  };
  s = svg_from_layout (layout, &config);

  stream = g_memory_input_stream_new_from_data (s->str, s->len, NULL);
  pixbuf = gdk_pixbuf_new_from_stream (stream, NULL, NULL);

  g_string_free (s, TRUE);
  grid_layout_free (layout);
  xword_state_free (state);

  return pixbuf;
}

guint
utf8_n_clusters (const gchar  *text)
{
  if (text == NULL || *text == '\0')
    return 0;

  /* FIXME(i18n): Actually write this thing. I'm faking it to get the code working
   */
  return (guint) g_utf8_strlen (text, -1);
}

/**
 * utf8_get_next_cluster:
 * @text: a valid utf8 string
 * @end: an end pointer to the next utf8 character in text
 *
 * Returns a newly allocated string of the next text cluster in
 * text. #end is set to be the start of the next cluster in the
 * string.
 *
 * Returns: a newly allocated utf8 string
 **/
gchar *
utf8_get_next_cluster (const gchar  *text,
                       const gchar **end)
{
  const gchar *ptr;
  gchar *retval;
  gunichar c;

  if (text == NULL || *text == '\0')
    {
      *end = text;
      return NULL;
    }

  /* Move forward through any spaces or control characters until we find an uppercase character */
  ptr = text;
  c = g_utf8_get_char (ptr);
  while (ptr[0] && g_unichar_isspace (c))
    {
      ptr = g_utf8_next_char (ptr);
      c = g_utf8_get_char (ptr);
    }

  text = ptr;
  if (!text[0])
    {
      *end = text;
      return NULL;
   }

  /* Go through subsequent characters appending any marks */
  do
    {
      ptr = g_utf8_next_char (ptr);
      c = g_utf8_get_char (ptr);
    }
  while (ptr[0] && g_unichar_ismark (c));

  retval = g_strndup (text, ptr - text);
  *end = ptr;

  return retval;
}

static void
zoom_level_css_clear_or_set (GtkWidget   *widget,
                             const gchar *css_prefix,
                             const gchar *css_class,
                             gboolean     set_css)
{
  g_autofree gchar *real_css_class = NULL;
  gboolean has_css;

  if (css_prefix)
    real_css_class = g_strdup_printf ("%s%s", css_prefix, css_class);
  else
    real_css_class = g_strdup (css_class);

  has_css = gtk_widget_has_css_class (widget, real_css_class);

  /* GTK will blindly update the css class regardless of what's
   * set. Since we are calling this every keypress, we only remove/set
   * the css_class if it's different. */
  if (!has_css && set_css)
    gtk_widget_add_css_class (widget, real_css_class);
  else if (has_css && !set_css)
    gtk_widget_remove_css_class (widget, real_css_class);
}

void
set_zoom_level_css_class (GtkWidget   *widget,
                          const gchar *css_prefix,
                          ZoomLevel    zoom_level)
{
  g_return_if_fail (GTK_IS_WIDGET (widget));

  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-xsmall",
                               (zoom_level == ZOOM_XSMALL));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-small",
                               (zoom_level == ZOOM_SMALL));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-normal",
                               (zoom_level == ZOOM_NORMAL));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-large",
                               (zoom_level == ZOOM_LARGE));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-xlarge",
                               (zoom_level == ZOOM_XLARGE));
}



/* This is cut-and-pasted from libgnome-desktop, which is GPLv2+. It
 * is license compatible. Hopefully it doesn't change that often, but
 * we don't use anything else from the library. */

static char *
construct_language_name (const char *language,
                         const char *territory,
                         const char *codeset,
                         const char *modifier)
{
  char *name;

  g_assert (language != NULL && language[0] != 0);
  g_assert (territory == NULL || territory[0] != 0);
  g_assert (codeset == NULL || codeset[0] != 0);
  g_assert (modifier == NULL || modifier[0] != 0);

  name = g_strdup_printf ("%s%s%s%s%s%s%s",
                          language,
                          territory != NULL? "_" : "",
                          territory != NULL? territory : "",
                          codeset != NULL? "." : "",
                          codeset != NULL? codeset : "",
                          modifier != NULL? "@" : "",
                          modifier != NULL? modifier : "");

  return name;
}


static char *
normalize_codeset (const char *codeset)
{
  if (codeset == NULL)
    return NULL;

  if (g_str_equal (codeset, "UTF-8") ||
      g_str_equal (codeset, "utf8"))
    return g_strdup ("UTF-8");

  return g_strdup (codeset);
}

static gboolean
language_name_is_valid (const char *language_name)
{
  locale_t locale;

  if (language_name == NULL)
    return FALSE;

  locale = newlocale (LC_MESSAGES_MASK, language_name, (locale_t) 0);
  if (locale != (locale_t) 0)
    {
      freelocale (locale);
      return TRUE;
    }

  return FALSE;
}

gboolean
crosswords_parse_locale (const char *locale,
                         char      **language_codep,
                         char      **country_codep,
                         char      **codesetp,
                         char      **modifierp)
{
  static GRegex *re = NULL;
  GMatchInfo *match_info;
  gboolean    res;
  gboolean    retval;

  match_info = NULL;
  retval = FALSE;

  if (re == NULL)
    {
      GError *error = NULL;
      re = g_regex_new ("^(?P<language>[^_.@[:space:]]+)"
                        "(_(?P<territory>[[:upper:]]+))?"
                        "(\\.(?P<codeset>[-_0-9a-zA-Z]+))?"
                        "(@(?P<modifier>[[:ascii:]]+))?$",
                        0, 0, &error);
      if (re == NULL)
        {
          g_warning ("%s", error->message);
          g_error_free (error);
          goto out;
        }
    }

  if (!g_regex_match (re, locale, 0, &match_info) ||
      g_match_info_is_partial_match (match_info))
    {
      g_warning ("locale '%s' isn't valid", locale);
      goto out;
    }

  res = g_match_info_matches (match_info);
  if (! res)
    {
      g_warning ("Unable to parse locale: %s", locale);
      goto out;
    }

  retval = TRUE;

  if (language_codep != NULL)
    *language_codep = g_match_info_fetch_named (match_info, "language");

  if (country_codep != NULL)
    {
      *country_codep = g_match_info_fetch_named (match_info, "territory");

      if (*country_codep != NULL && *country_codep[0] == '\0')
        {
          g_free (*country_codep);
          *country_codep = NULL;
        }
    }

  if (codesetp != NULL)
    {
      *codesetp = g_match_info_fetch_named (match_info, "codeset");

      if (*codesetp != NULL && *codesetp[0] == '\0')
        {
          g_free (*codesetp);
          *codesetp = NULL;
        }
    }

  if (modifierp != NULL)
    {
      *modifierp = g_match_info_fetch_named (match_info, "modifier");

      if (*modifierp != NULL && *modifierp[0] == '\0')
        {
          g_free (*modifierp);
          *modifierp = NULL;
        }
    }

  if (codesetp != NULL && *codesetp != NULL)
    {
      g_autofree gchar *normalized_codeset = NULL;
      g_autofree gchar *normalized_name = NULL;

      normalized_codeset = normalize_codeset (*codesetp);
      normalized_name = construct_language_name (language_codep ? *language_codep : NULL,
                                                 country_codep ? *country_codep : NULL,
                                                 normalized_codeset,
                                                 modifierp ? *modifierp : NULL);

      if (language_name_is_valid (normalized_name))
        {
          g_free (*codesetp);
          *codesetp = g_steal_pointer (&normalized_codeset);
        }
    }

 out:
  g_match_info_free (match_info);

  return retval;
}
