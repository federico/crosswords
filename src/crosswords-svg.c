#include "crosswords-config.h"

#include "crosswords-svg.h"

struct _CrosswordsSvg
{
  GtkWidget parent_instance;

  RsvgHandle *handle;
  gdouble width;
  gdouble height;
};

static void crosswords_svg_init (CrosswordsSvg *csvg);
static void crosswords_svg_class_init (CrosswordsSvgClass *klass);

G_DEFINE_TYPE (CrosswordsSvg, crosswords_svg, GTK_TYPE_WIDGET);

static void
measure (GtkWidget      *widget,
         GtkOrientation  orientation,
         int             for_size,
         int            *minimum,
         int            *natural,
         int            *minimum_baseline,
         int            *natural_baseline)
{
  CrosswordsSvg *csvg = CROSSWORDS_SVG (widget);
  int size = 0;

  switch (orientation)
    {
    case GTK_ORIENTATION_HORIZONTAL:
      size = ceil (csvg->width);
      break;

    case GTK_ORIENTATION_VERTICAL:
      size = ceil (csvg->height);
      break;

    default:
      g_assert_not_reached ();
    }

  *minimum = size;
  *natural = size;
  *minimum_baseline = -1;
  *natural_baseline = -1;
}

static void
snapshot (GtkWidget   *widget,
          GtkSnapshot *snapshot)
{
  CrosswordsSvg *csvg = CROSSWORDS_SVG (widget);

  if (!csvg->handle)
    {
      return;
    }

  cairo_t *cr = gtk_snapshot_append_cairo (snapshot, &GRAPHENE_RECT_INIT (0, 0, csvg->width, csvg->height));
  GError *error = NULL;
  RsvgRectangle viewport = {
    .x = 0,
    .y = 0,
    .width = csvg->width,
    .height = csvg->height,
  };

  if (!rsvg_handle_render_document (csvg->handle, cr, &viewport, &error))
    {
      g_warning ("could not render SVG: %s", error->message);
      g_error_free (error);
    }
}

static void
finalize (GObject *object)
{
  CrosswordsSvg *csvg = CROSSWORDS_SVG (object);

  if (csvg->handle)
    {
      g_clear_object (&csvg->handle);
    }


  G_OBJECT_CLASS (crosswords_svg_parent_class)->finalize (object);
}

static void
crosswords_svg_init (CrosswordsSvg *csvg)
{
  csvg->handle = NULL;
  csvg->width = 0;
  csvg->height = 0;
}

static void
crosswords_svg_class_init (CrosswordsSvgClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = finalize;

  widget_class->measure = measure;
  widget_class->snapshot = snapshot;
}

/**
 * crosswords_svg_new:
 *
 * Creates a new, empty SVG widget.  You can later
 * set an SVG in it with crosswords_svg_set_document().
 *
 * Returns: a new #CrosswordsSvg widget.
 */
GtkWidget *
crosswords_svg_new (void)
{
  return g_object_new (CROSSWORDS_TYPE_SVG, NULL);
}

/**
 * crosswords_svg_set_document:
 * @csvg: Widget to modify.
 * @handle: (nullable) (transfer none) Loaded handle to an SVG document.
 *
 * Sets an SVG document on a @csvg.  The widget's size requisition
 * will correspond to the document's intrinsic size in pixels.  It is
 * a programming error to set a @handle that has no intrinsic size in
 * absolute units that can be resolved to pixels; the program will
 * abort in that case.
 */
void
crosswords_svg_set_document (CrosswordsSvg *csvg,
                             RsvgHandle    *handle)
{
  gdouble width = 0.0;
  gdouble height = 0.0;

  if (handle)
    {
      if (!rsvg_handle_get_intrinsic_size_in_pixels (handle, &width, &height))
        {
          g_error ("SVG has no intrinsic size in absolute units; fix it!");
        }

      handle = g_object_ref (handle);
    }

  if (csvg->handle)
    {
      g_object_unref (csvg->handle);
    }

  csvg->handle = handle;
  csvg->width = width;
  csvg->height = height;
  gtk_widget_queue_resize (GTK_WIDGET (csvg));
}

