#include <glib.h>
#include <locale.h>
#include "grid-layout.h"
#include "play-grid.h"

G_GNUC_WARN_UNUSED_RESULT static XwordState *
load_state (const char *filename)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IPuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  XwordState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = xword_state_new (IPUZ_CROSSWORD (puzzle), NULL, XWORD_STATE_SOLVE);
  g_assert (state != NULL);

  g_object_unref (puzzle);
  g_free (path);

  return state;
}

#define ITERATIONS 100

static void
grid_bench (void)
{
  XwordState *state = load_state ("tests/missing-pets.ipuz");
  gdouble total_elapsed = 0.0;
  guint i;

  GtkWidget *window = gtk_window_new ();
  gtk_widget_show (window);

  for (i = 0; i < ITERATIONS; i++) {
    GTimer *timer = g_timer_new ();
    g_timer_start (timer);

    GtkWidget *grid = play_grid_new ();
    gtk_widget_show (grid);
    gtk_window_set_child (GTK_WINDOW (window), grid);
    play_grid_update_state (PLAY_GRID (grid), state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));

    g_timer_stop (timer);
    total_elapsed += g_timer_elapsed (timer, NULL);
    g_timer_reset (timer);
      
    gtk_window_set_child (GTK_WINDOW (window), NULL);
  }

  gdouble average = total_elapsed / ITERATIONS;

  printf ("%u iterations; average per iteration: %f sec\n", ITERATIONS, average);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);
  gtk_init ();

  grid_bench ();
  return 0;
}
