/* edit-app.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <adwaita.h>
#include "charset-entry-buffer.h"
#include "crosswords-credits.h"
#include "crosswords-init.h"
#include "edit-app.h"
#include "edit-autofill-dialog.h"
#include "edit-save-changes-dialog.h"
#include "edit-basics.h"
#include "edit-clues.h"
#include "edit-metadata.h"
#include "edit-puzzle-stack.h"
#include "edit-window.h"
#include "edit-xword.h"


struct _EditApp
{
  AdwApplication parent;
};

static void edit_app_init                       (EditApp        *app);
static void edit_app_class_init                 (EditAppClass   *klass);
static void edit_app_activate                   (GApplication   *app);
static void edit_app_open                       (GApplication   *app,
                                                 GFile         **files,
                                                 int             n_files,
                                                 const char     *hint);
static void edit_app_startup                    (GApplication   *app);
static void edit_app_window_added               (GtkApplication *application,
                                                 GtkWindow      *window);
static void edit_application_actions_new_window (GSimpleAction  *action,
                                                 GVariant       *param,
                                                 gpointer        user_data);
static void edit_application_actions_quit       (GSimpleAction  *action,
                                                 GVariant       *param,
                                                 gpointer        user_data);
static void edit_application_actions_about      (GSimpleAction  *action,
                                                 GVariant       *param,
                                                 gpointer        user_data);


G_DEFINE_TYPE(EditApp, edit_app, ADW_TYPE_APPLICATION);


static void
edit_app_init (EditApp *app)
{
  /* Nothing to do*/
}

static void
edit_app_class_init (EditAppClass *klass)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (klass);
  GtkApplicationClass *gtk_application_class = GTK_APPLICATION_CLASS (klass);

  application_class->activate = edit_app_activate;
  application_class->open = edit_app_open;
  application_class->startup = edit_app_startup;

  gtk_application_class->window_added = edit_app_window_added;
}


static void
edit_app_activate (GApplication *app)
{
  GtkWindow *window;

  g_assert (EDIT_IS_APP (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (EDIT_TYPE_WINDOW,
                           "application", app,
                           NULL);

  gtk_window_present (window);
}

static void
edit_app_open (GApplication  *app,
               GFile        **files,
               int            n_files,
               const char    *hint)
{
  g_assert (EDIT_IS_APP (app));

  /* FIXME(refactor): clean up the crosswords-init / edit-init mess */
  g_type_ensure (EDIT_TYPE_XWORD);
  /* Do we want to dedup files? Or worry about it opening the same file
   * twice? Probably best to just do what we say and make sure we can handle
   * multiple instances of the same file at the same time.
   */
  for (int i = 0; i < n_files; i++)
    {
      EditWindow *window;
      g_autofree gchar *uri = NULL;

      uri = g_file_get_uri (files[i]);

      window = g_object_new (EDIT_TYPE_WINDOW,
                             "application", app,
                             NULL);
      edit_window_load_uri (window, uri);
      gtk_window_present (GTK_WINDOW (window));
    }
}

static void
edit_app_startup (GApplication *app)
{
  static const GActionEntry actions[] = {
    { "new-window", edit_application_actions_new_window, NULL, NULL, NULL, {0,0,0}},
    { "quit", edit_application_actions_quit, NULL, NULL, NULL, {0,0,0}},
    { "about", edit_application_actions_about, NULL, NULL, NULL, {0,0,0}},
  };
  static const gchar *new_window_accels[] = { "<Control>N", NULL };
  static const gchar *quit_accels[] = { "<Primary>Q", NULL };

  G_APPLICATION_CLASS (edit_app_parent_class)->startup (app);

  crosswords_init ();
  /* FIXME(refactor): clean up the crosswords-init / edit-init mess */
  g_type_ensure (CHARSET_TYPE_ENTRY_BUFFER);
  g_type_ensure (EDIT_TYPE_AUTOFILL_DIALOG);
  g_type_ensure (EDIT_TYPE_BASICS);
  g_type_ensure (EDIT_TYPE_XWORD);
  g_type_ensure (EDIT_TYPE_CLUES);
  g_type_ensure (EDIT_TYPE_METADATA);
  g_type_ensure (EDIT_TYPE_PUZZLE_STACK);

  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.new-window", new_window_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.quit", quit_accels);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   actions,
                                   G_N_ELEMENTS (actions),
                                   app);

}

static void
edit_app_window_added (GtkApplication *application,
                       GtkWindow      *window)
{
  g_assert (EDIT_IS_APP (application));
  g_assert (GTK_IS_WINDOW (window));

#ifdef DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (window), "devel");
#endif

  GTK_APPLICATION_CLASS (edit_app_parent_class)->window_added (application, window);
}

/* Actions */

static void
edit_application_actions_new_window (GSimpleAction  *action,
                                       GVariant       *param,
                                       gpointer        user_data)
{
  GtkApplication *app;
  EditWindow *edit_window;

  app = GTK_APPLICATION (user_data);

  edit_window = g_object_new (EDIT_TYPE_WINDOW,
                              "application", app,
                              NULL);

  gtk_window_present (GTK_WINDOW (edit_window));
}

static void
edit_application_actions_quit (GSimpleAction *action,
                               GVariant      *param,
                               gpointer       user_data)
{
  GtkApplication *app;
  GList *app_list;
  GList *save_info_list = NULL;

  app = GTK_APPLICATION (user_data);

  for (app_list = gtk_application_get_windows (app);
       app_list;
       app_list = app_list->next)
    {
      g_autofree gchar *title = NULL;
      g_autofree gchar *path = NULL;

      if (edit_window_get_unsaved_changes (EDIT_WINDOW (app_list->data),
                                           &title, &path))
        {
          save_info_list = edit_save_info_list_add (save_info_list,
                                                    GTK_WIDGET (app_list->data),
                                                    title, path);
        }
    }

  if (save_info_list == NULL)
    {
      g_application_quit (G_APPLICATION (app));
    }
  else
    {
      edit_save_changes_dialog_run (gtk_application_get_active_window (app),
                                    save_info_list,
                                    G_APPLICATION (app));
    }
}

static void
edit_application_actions_about (GSimpleAction *action,
                                GVariant      *param,
                                gpointer       user_data)
{
  EditApp *app;
  AdwAboutWindow *dialog;
  GtkWindow *window;

  app = EDIT_APP (user_data);
  g_assert (EDIT_IS_APP (app));

  dialog = ADW_ABOUT_WINDOW (adw_about_window_new ());
  adw_about_window_set_application_name (dialog, _("Crossword Editor"));
  adw_about_window_set_application_icon (dialog, PACKAGE_ICON_NAME);
  adw_about_window_set_developers (dialog, crosswords_authors);
  /* Translators: Put your name here so that it will be included in the about dialog */
  adw_about_window_set_translator_credits (dialog, _("translator-credits"));
  adw_about_window_set_designers (dialog, crosswords_designers);
  adw_about_window_set_version (dialog, PACKAGE_VERSION);
  adw_about_window_set_copyright (dialog, "© 2021-2022 Jonathan Blandford");
  adw_about_window_set_license_type (dialog, GTK_LICENSE_GPL_3_0);
  adw_about_window_set_website (dialog, PACKAGE_WEBSITE);
  adw_about_window_set_issue_url (dialog, "https://gitlab.gnome.org/jrb/crosswords/-/issues/new");

  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);

  gtk_window_present (GTK_WINDOW (dialog));
}


/* Public methods */

EditApp *
edit_app_new (void)
{
  return g_object_new (EDIT_TYPE_APP,
                       "application-id", EDIT_APP_ID,
                       "flags", G_APPLICATION_HANDLES_OPEN,
                       "register-session", TRUE,
                       NULL);
}
