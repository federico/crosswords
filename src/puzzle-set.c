/* puzzle-set.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>

#include "play-clue-row.h"
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "puzzle-set.h"
#include "puzzle-set-resource.h"
#include "play-xword.h"

enum
{
  PUZZLES_START,
  CHANGE_PHASE,
  PUZZLES_DONE,
  REVEAL_CANCELED,
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SET_TYPE,
  PROP_ID,
  PROP_SHORT_NAME,
  PROP_LONG_NAME,
  PROP_LOCALE,
  PROP_LANGUAGE,
  PROP_DISABLED,
  N_PROPS
};

static GList *global_puzzle_sets = NULL;

static guint puzzle_set_signals[LAST_SIGNAL] = { 0 };
static GParamSpec *obj_props[N_PROPS] = {NULL, };


typedef struct
{
  PuzzleSetType set_type;
  gchar *id;
  gchar *short_name;
  gchar *long_name;
  gchar *locale;
  gchar *language;
  gboolean disabled;
  guint reveal_toggled_cb_id;
  PuzzlePhase phase;
} PuzzleSetPrivate;


static void         puzzle_set_init                (PuzzleSet      *self);
static void         puzzle_set_class_init          (PuzzleSetClass *klass);
static void         puzzle_set_set_property        (GObject        *object,
                                                    guint           prop_id,
                                                    const GValue   *value,
                                                    GParamSpec     *pspec);
static void         puzzle_set_get_property        (GObject        *object,
                                                    guint           prop_id,
                                                    GValue         *value,
                                                    GParamSpec     *pspec);
static void         puzzle_set_finalize            (GObject        *object);

static void         puzzle_set_real_puzzles_start  (PuzzleSet      *puzzle_set);
static void         puzzle_set_real_change_phase   (PuzzleSet      *puzzle_set,
                                                    PuzzlePhase     phase);
static void         puzzle_set_real_puzzles_done   (PuzzleSet      *puzzle_set);
static GtkWidget   *puzzle_set_real_get_widget     (PuzzleSet      *puzzle_set,
                                                    PuzzlePhase     phase);
static IPuzPuzzle  *puzzle_set_real_get_puzzle     (PuzzleSet      *puzzle_set,
                                                    PuzzlePhase     phase);
static const gchar *puzzle_set_real_get_title      (PuzzleSet      *puzzle_set,
                                                    PuzzlePhase     phase);
static gboolean     puzzle_set_real_use_picker     (PuzzleSet      *puzzle_set);
static void         puzzle_set_real_reset_puzzle   (PuzzleSet      *puzzle_set);
static void         puzzle_set_real_reveal_toggled (PuzzleSet      *puzzle_set,
                                                    gboolean        toggled);
static void         puzzle_set_real_show_hint      (PuzzleSet      *puzzle_set);
static const gchar *puzzle_set_real_get_uri        (PuzzleSet      *puzzle_set,
                                                    PuzzlePhase     phase);
static void         puzzle_set_real_load_uri       (PuzzleSet      *puzzle_set,
                                                    const gchar    *uri);


G_DEFINE_TYPE_WITH_PRIVATE (PuzzleSet, puzzle_set, G_TYPE_OBJECT);


static
void puzzle_set_init (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (self));

  priv->phase = PUZZLE_PHASE_MAIN;
}

static void
puzzle_set_class_init (PuzzleSetClass *klass)
{
  GObjectClass *object_class;

  object_class = (GObjectClass *) klass;

  object_class->set_property = puzzle_set_set_property;
  object_class->get_property = puzzle_set_get_property;
  object_class->finalize = puzzle_set_finalize;

  /* Navigation signals */
  klass->puzzles_start = puzzle_set_real_puzzles_start;
  klass->change_phase = puzzle_set_real_change_phase;
  klass->puzzles_done = puzzle_set_real_puzzles_done;

  /* Display virtual functions */
  klass->get_widget = puzzle_set_real_get_widget;
  klass->get_puzzle = puzzle_set_real_get_puzzle;
  klass->get_title = puzzle_set_real_get_title;
  klass->use_picker = puzzle_set_real_use_picker;
  klass->reset_puzzle = puzzle_set_real_reset_puzzle;
  klass->reveal_toggled = puzzle_set_real_reveal_toggled;
  klass->show_hint = puzzle_set_real_show_hint;
  klass->get_uri = puzzle_set_real_get_uri;
  klass->load_uri = puzzle_set_real_load_uri;

  puzzle_set_signals [PUZZLES_START] =
    g_signal_new ("puzzles-start",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, puzzles_start),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  puzzle_set_signals [CHANGE_PHASE] =
    g_signal_new ("change-phase",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, change_phase),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  PUZZLE_TYPE_PHASE);

  puzzle_set_signals [PUZZLES_DONE] =
    g_signal_new ("puzzles-done",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, puzzles_done),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  puzzle_set_signals [REVEAL_CANCELED] =
    g_signal_new ("reveal-canceled",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, reveal_canceled),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_SET_TYPE] =
    g_param_spec_enum ("set-type",
                       "Set Type",
                       "Type of Puzzle Set",
                       PUZZLE_TYPE_SET_TYPE,
                       PUZZLE_SET_TYPE_RESOURCE,
                       G_PARAM_READWRITE);

  obj_props[PROP_ID] =
    g_param_spec_string ("id",
                         "id",
                         "ID for the Puzzle Set",
                         NULL,
                         G_PARAM_READWRITE);

  obj_props[PROP_SHORT_NAME] =
    g_param_spec_string ("short-name",
                         "Short Name",
                         "Short name for the Puzzle Set",
                         NULL,
                         G_PARAM_READWRITE);

  obj_props[PROP_LONG_NAME] =
    g_param_spec_string ("long-name",
                         "Long Name",
                         "Long name for the Puzzle Set",
                         NULL,
                         G_PARAM_READWRITE);

  obj_props[PROP_LOCALE] =
    g_param_spec_string ("locale",
                         "Locale",
                         "Locale for the Puzzle Set",
                         NULL,
                         G_PARAM_READWRITE);

  obj_props[PROP_LANGUAGE] =
    g_param_spec_string ("language",
                         "Language",
                         "Language for the Puzzle Set",
                         NULL,
                         G_PARAM_READABLE);

  obj_props[PROP_DISABLED] =
    g_param_spec_boolean ("disabled",
                          "Disabled",
                          "Whether the Puzzle Set is available to be installed",
                          FALSE,
                          G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
puzzle_set_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (object));

  switch (prop_id)
    {
    case PROP_SET_TYPE:
      priv->set_type = g_value_get_enum (value);
      break;
    case PROP_ID:
      g_clear_pointer (&priv->id, g_free);
      priv->id = g_value_dup_string (value);
      break;
    case PROP_SHORT_NAME:
      g_clear_pointer (&priv->short_name, g_free);
      priv->short_name = g_value_dup_string (value);
      break;
    case PROP_LONG_NAME:
      g_clear_pointer (&priv->long_name, g_free);
      priv->long_name = g_value_dup_string (value);
      break;
    case PROP_LOCALE:
      g_clear_pointer (&priv->locale, g_free);
      g_clear_pointer (&priv->language, g_free);
      priv->locale = g_value_dup_string (value);
      crosswords_parse_locale (priv->locale,
                               &priv->language,
                               NULL, NULL, NULL);
      g_object_notify_by_pspec (object, obj_props[PROP_LANGUAGE]);
      break;
    case PROP_LANGUAGE:
      g_clear_pointer (&priv->language, g_free);
      priv->language = g_value_dup_string (value);
      break;
    case PROP_DISABLED:
      priv->disabled = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (object));

  switch (prop_id)
    {
    case PROP_SET_TYPE:
      g_value_set_enum (value, priv->set_type);
      break;
    case PROP_ID:
      g_value_set_string (value, priv->id);
      break;
    case PROP_SHORT_NAME:
      g_value_set_string (value, priv->short_name);
      break;
    case PROP_LONG_NAME:
      g_value_set_string (value, priv->long_name);
      break;
    case PROP_LOCALE:
      g_value_set_string (value, priv->locale);
      break;
    case PROP_LANGUAGE:
      g_value_set_string (value, priv->language);
      break;
    case PROP_DISABLED:
      g_value_set_boolean (value, priv->disabled);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_finalize (GObject *object)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (object));

  g_free (priv->id);
  g_free (priv->short_name);
  g_free (priv->long_name);
  g_free (priv->locale);
  g_free (priv->language);

  G_OBJECT_CLASS (puzzle_set_parent_class)->finalize (object);
}

static void
puzzle_set_real_puzzles_start (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;
  GtkWidget *game;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (self));

  game = puzzle_set_get_widget (self, PUZZLE_PHASE_GAME);

  if (game != NULL &&
      PLAY_IS_XWORD (game))
    {
      priv->reveal_toggled_cb_id =
        g_signal_connect_swapped (game, "reveal-canceled",
                                  G_CALLBACK (puzzle_set_reveal_canceled),
                                  self);
    }
}


static void
puzzle_set_real_change_phase (PuzzleSet   *self,
                              PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;
  GtkWidget *game;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (self));
  game = puzzle_set_get_widget (self, PUZZLE_PHASE_GAME);

  if (PLAY_IS_XWORD (game))
    play_xword_set_reveal_mode (PLAY_XWORD (game), XWORD_REVEAL_NONE);

  priv->phase = phase;
}

PuzzlePhase
puzzle_set_get_phase (PuzzleSet *puzzle_set)
{
 PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (puzzle_set));

  return priv->phase;
}

static void
puzzle_set_real_puzzles_done (PuzzleSet *self)
{
  /* Pass */
}

static GtkWidget *
puzzle_set_real_get_widget (PuzzleSet   *self,
                            PuzzlePhase  phase)
{
  return NULL;
}

IPuzPuzzle *
puzzle_set_real_get_puzzle (PuzzleSet   *puzzle_set,
                            PuzzlePhase  phase)
{
  return NULL;
}

static const gchar *
puzzle_set_real_get_title (PuzzleSet   *self,
                           PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (self));

  return priv->short_name;
}


static gboolean
puzzle_set_real_use_picker (PuzzleSet *self)
{
  return (puzzle_set_get_widget (self, PUZZLE_PHASE_PICKER) != NULL);
}

static void
puzzle_set_real_reset_puzzle (PuzzleSet *puzzle_set)
{
  GtkWidget *game;

  game = puzzle_set_get_widget (puzzle_set, PUZZLE_PHASE_GAME);

  /* We have a default handler for PLAY_XWORD */
  if (game != NULL &&
      PLAY_IS_XWORD (game))
    {
      play_xword_clear_puzzle (PLAY_XWORD (game));
    }
}

static void
puzzle_set_real_reveal_toggled (PuzzleSet *puzzle_set,
                                gboolean   toggled)
{
  GtkWidget *game;

  game = puzzle_set_get_widget (puzzle_set, PUZZLE_PHASE_GAME);

  /* We have a default handler for PLAY_XWORD */
  if (game != NULL &&
      PLAY_IS_XWORD (game))
    {
      play_xword_set_reveal_mode (PLAY_XWORD (game),
                                  toggled ? XWORD_REVEAL_ERRORS_BOARD : XWORD_REVEAL_NONE);
    }
}

static void
puzzle_set_real_show_hint (PuzzleSet *puzzle_set)
{
  GtkWidget *game;

  game = puzzle_set_get_widget (puzzle_set, PUZZLE_PHASE_GAME);

  /* We have a default handler for PLAY_XWORD */
  if (game != NULL &&
      PLAY_IS_XWORD (game))
    {
      play_xword_show_hint (PLAY_XWORD (game));
    }
}

static const gchar *
puzzle_set_real_get_uri (PuzzleSet   *puzzle_set,
                         PuzzlePhase  phase)
{
  return NULL;
}

static void
puzzle_set_real_load_uri (PuzzleSet   *puzzle_set,
                          const gchar *uri)
{
  /* pass */
}

/* Public Functions */

PuzzleSetType
puzzle_set_get_puzzle_type (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), PUZZLE_SET_TYPE_RESOURCE);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->set_type;
}

const gchar *
puzzle_set_get_id (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->id;
}

const gchar *
puzzle_set_get_short_name (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->short_name;
}

const gchar *
puzzle_set_get_long_name (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->long_name;
}

const gchar *
puzzle_set_get_locale (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->locale;
}

const gchar *
puzzle_set_get_language (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->language;
}

gboolean
puzzle_set_get_disabled (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), TRUE);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->disabled;
}

void
puzzle_set_puzzles_start (PuzzleSet *puzzle_set)
{
  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[PUZZLES_START], 0);
}

void
puzzle_set_change_phase (PuzzleSet   *puzzle_set,
                         PuzzlePhase  phase)
{
  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[CHANGE_PHASE], 0, phase);
}

void
puzzle_set_puzzles_done (PuzzleSet *puzzle_set)
{

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[PUZZLES_DONE], 0);
}

void
puzzle_set_reveal_canceled (PuzzleSet *puzzle_set)
{

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[REVEAL_CANCELED], 0);
}


GtkWidget *
puzzle_set_get_widget (PuzzleSet   *puzzle_set,
                       PuzzlePhase  phase)
{
  PuzzleSetClass *klass;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);
  g_return_val_if_fail (phase != PUZZLE_PHASE_MAIN, NULL);

  klass = (PuzzleSetClass *)
    G_OBJECT_GET_CLASS (puzzle_set);

  return (klass)->get_widget (puzzle_set, phase);
}

IPuzPuzzle *
puzzle_set_get_puzzle (PuzzleSet   *puzzle_set,
                       PuzzlePhase  phase)
{
  PuzzleSetClass *klass;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  klass = (PuzzleSetClass *)
    G_OBJECT_GET_CLASS (puzzle_set);

  return (klass)->get_puzzle (puzzle_set, phase);
}

const gchar *
puzzle_set_get_title (PuzzleSet   *puzzle_set,
                      PuzzlePhase  phase)
{
  PuzzleSetClass *klass;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);
  g_return_val_if_fail (phase != PUZZLE_PHASE_MAIN, NULL);

  klass = (PuzzleSetClass *)
    G_OBJECT_GET_CLASS (puzzle_set);

  return (klass)->get_title (puzzle_set, phase);
}

gboolean
puzzle_set_use_picker (PuzzleSet *puzzle_set)
{
  PuzzleSetClass *klass;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), FALSE);

  klass = (PuzzleSetClass *) G_OBJECT_GET_CLASS (puzzle_set);

  return (klass)->use_picker (puzzle_set);
}

void
puzzle_set_reset_puzzle (PuzzleSet *puzzle_set)
{
  PuzzleSetClass *klass;

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  klass = (PuzzleSetClass *) G_OBJECT_GET_CLASS (puzzle_set);

  (klass)->reset_puzzle (puzzle_set);
}

void
puzzle_set_reveal_toggled (PuzzleSet *puzzle_set,
                           gboolean   reveal)
{
  PuzzleSetClass *klass;

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  klass = (PuzzleSetClass *) G_OBJECT_GET_CLASS (puzzle_set);

  (klass)->reveal_toggled (puzzle_set, reveal);
}

void
puzzle_set_show_hint (PuzzleSet *puzzle_set)
{
  PuzzleSetClass *klass;

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  klass = (PuzzleSetClass *) G_OBJECT_GET_CLASS (puzzle_set);

  (klass)->show_hint (puzzle_set);
}

const gchar *
puzzle_set_get_uri (PuzzleSet   *puzzle_set,
                    PuzzlePhase  phase)
{
  PuzzleSetClass *klass;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  klass = (PuzzleSetClass *) G_OBJECT_GET_CLASS (puzzle_set);

  return (klass)->get_uri (puzzle_set, phase);
}

void
puzzle_set_load_uri (PuzzleSet   *puzzle_set,
                     const gchar *uri)
{
  PuzzleSetClass *klass;

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  klass = (PuzzleSetClass *) G_OBJECT_GET_CLASS (puzzle_set);

  (klass)->load_uri (puzzle_set, uri);
}

/* Non-class functions */

static GList *
crosswords_load_puzzle_sets_from_dir (const gchar *dir_name,
                                      GList       *list)
{
  const char *name;
  GDir *data_dir;

  data_dir = g_dir_open (dir_name, 0, NULL);
  /* Ignore directories that don't exist. This is fine, because we're
   * walking through $XDG_DATA_DIRS */
  if (data_dir == NULL)
    return list;

  while ((name = g_dir_read_name (data_dir)) != NULL)
    {
      g_autoptr (GError) err = NULL;
      g_autofree gchar *file_name = NULL;
      GResource *resource;
      PuzzleSet *puzzle_set;

      file_name = g_build_filename (dir_name, name, NULL);
      /* Recurse to subdirectories */
      /* FIXME(cleanup): Should we look for loops? */
      if (g_file_test (file_name, G_FILE_TEST_IS_DIR))
        {
          list = crosswords_load_puzzle_sets_from_dir (file_name, list);
          continue;
        }

      if (! g_str_has_suffix (name, ".gresource"))
        continue;

      resource = g_resource_load (file_name, &err);
      if (err)
        {
          g_warning ("Failed to load resource %s\nError %s", file_name, err->message);
          continue;
        }

      puzzle_set = puzzle_set_resource_new (resource);
      g_resource_unref (resource);

      /* Can we create a meaningful puzzle_set from this resource? */
      if (puzzle_set == NULL)
        continue;

      list = g_list_prepend (list, puzzle_set);
    }
  g_dir_close (data_dir);

  return list;
}

void
puzzle_sets_init (void)
{
  const gchar *const *datadirs;
  const gchar *PUZZLE_SET_PATH = NULL;

  /* FIXME: Make this reloadable in the future, so we can catch new puzzle_sets */
  if (global_puzzle_sets != NULL)
    return;

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/puzzle-sets", NULL);
      global_puzzle_sets = crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/extensions/puzzle-sets", NULL);
      global_puzzle_sets = crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  if (g_get_user_data_dir ())
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (g_get_user_data_dir (), "crosswords/puzzle-sets", NULL);
      global_puzzle_sets = crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  /* We also look in $PUZZLE_SET_PATH for puzzles. This is mostly for
   * running out of the builddir with a local install. It is set in
   * the 'run' script in this case.  */
  PUZZLE_SET_PATH = g_getenv ("PUZZLE_SET_PATH");
  if (PUZZLE_SET_PATH && PUZZLE_SET_PATH[0])
    {
      g_auto (GStrv) puzzle_set_paths = NULL;

      puzzle_set_paths = g_strsplit (PUZZLE_SET_PATH, ":", -1);
      for (guint i = 0; puzzle_set_paths[i]; i++)
        global_puzzle_sets = crosswords_load_puzzle_sets_from_dir (puzzle_set_paths[i], global_puzzle_sets);
    }

  if (global_puzzle_sets == NULL)
    {
      /* We should always have the "uri" puzzle-set at a minimum, and
       * we won't work without it. This implies a broken
       * installation. Erroring out. */
      g_warning (_("No puzzle sets were found. Use $PUZZLE_SET_PATH to set a directory to search."));
      g_warning (_("When running out of a local build, use the `run` script to set the environment correctly."));
      g_assert_not_reached ();
    }
}
