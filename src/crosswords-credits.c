/* crosswords-credits.c
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-credits.h"


/* Add additional contributors here: */

const char *crosswords_authors[] = {
  "Jonathan Blandford <jrb@gnome.org>",
  "Federico Mena-Quintero",
  "Philip Goto",
  "Davide Cavalca",
  NULL
};

const char *crosswords_designers[] = {
  "Sam Hewitt",
  NULL
};

const char *crosswords_puzzles[] = {
  "Jonathan Blandford",
  "Rosanna Yuen",
  NULL
};

/* Keep this in sync with data/org.gnome.Crosswords.metainfo.xml.in.in */

const char *crosswords_release_notes = N_("\
<p>\
  This release adds the following improvements:\
</p>\
<ul>\
<li> Adaptive layout to handle a wide variety of sizes</li>\
<li> Cleaner appearance for the game board</li>\
<li> Extra-small size to fit on smaller screen</li>\
<li> Window size restoration on restart</li>\
<li> Mime types for ipuz/jpz/puz files</li>\
<li> Load jpz/puz files from the command line</li>\
<li> Improvements to cell shape contrast and color</li>\
<li> New setting: switch-on-move</li>\
<li> Translations: new Italian translation</li>\
</ul>\
<p>\
Better spec compliance:\
</p>\
<ul>\
<li> Complex enumerations</li>\
<li> HTML support for clues and metadata</li>\
<li> Compliant placement of intro/notes fields</li>\
</ul>\
<p>\
Crossword convertor improvements:\
</p>\
<ul>\
<li>.puz convertor imports circles and rebus puzzles</li>\
<li>new .jpz convertor</li>\
<li>Both convertors create enumerations and html text to be closer to the ipuz spec</li>\
<li>Code refactor to allow additional convertors to be easily added</li>\
</ul>\
");
