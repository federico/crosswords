/* play-clue-row.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-misc.h"
#include "play-clue-row.h"
#include <libipuz/libipuz.h>


enum
{
  PROP_0,
  PROP_STANDALONE,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };


struct _PlayClueRow
{
  GtkWidget  parent_instance;
  IPuzClueId clue_id;

  GtkWidget *clue_label;
  GtkWidget *num_label;
  gboolean standalone;
};


static void play_clue_row_init         (PlayClueRow      *self);
static void play_clue_row_class_init   (PlayClueRowClass *klass);
static void play_clue_row_set_property (GObject          *object,
                                        guint             prop_id,
                                        const GValue     *value,
                                        GParamSpec       *pspec);
static void play_clue_row_get_property (GObject          *object,
                                        guint             prop_id,
                                        GValue           *value,
                                        GParamSpec       *pspec);
static void play_clue_row_dispose      (GObject          *object);


G_DEFINE_TYPE (PlayClueRow, play_clue_row, GTK_TYPE_WIDGET);


static void
play_clue_row_init (PlayClueRow *self)
{
  self->clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;

  self->num_label = gtk_label_new (NULL);
  gtk_label_set_xalign (GTK_LABEL (self->num_label), 0.0);
  gtk_label_set_yalign (GTK_LABEL (self->num_label), 0.0);
  gtk_label_set_use_markup (GTK_LABEL (self->num_label), TRUE);
  gtk_widget_set_name (self->num_label, "num-label");

  self->clue_label = gtk_label_new (NULL);
  gtk_label_set_wrap (GTK_LABEL (self->clue_label), TRUE);
  gtk_label_set_width_chars (GTK_LABEL (self->clue_label), 30);
  gtk_label_set_xalign (GTK_LABEL (self->clue_label), 0.0);
  gtk_label_set_yalign (GTK_LABEL (self->clue_label), 0.0);
  gtk_widget_set_name (self->clue_label, "clue-label");

  gtk_widget_insert_after (self->num_label, GTK_WIDGET (self), NULL);
  gtk_widget_insert_after (self->clue_label, GTK_WIDGET (self), self->num_label);
}

static void
play_clue_row_class_init (PlayClueRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = play_clue_row_set_property;
  object_class->get_property = play_clue_row_get_property;
  object_class->dispose = play_clue_row_dispose;

  obj_props[PROP_STANDALONE] = g_param_spec_boolean ("standalone",
                                                    "Standalone",
                                                    "Whether to render the clue separate from a list",
                                                    FALSE,
                                                    G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (GTK_WIDGET_CLASS (klass), "clue-row");
  gtk_widget_class_set_layout_manager_type (GTK_WIDGET_CLASS (klass),
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
play_clue_row_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  PlayClueRow *self = PLAY_CLUE_ROW (object);

  switch (prop_id)
    {
    case PROP_STANDALONE:
      play_clue_row_set_standalone (self, g_value_get_boolean (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_clue_row_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  PlayClueRow *self = PLAY_CLUE_ROW (object);

  switch (prop_id)
    {
    case PROP_STANDALONE:
      g_value_set_boolean (value, self->standalone);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static void
play_clue_row_dispose (GObject *object)
{
  GtkWidget *child;

  g_return_if_fail (object != NULL);


  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (play_clue_row_parent_class)->dispose (object);
}

/* Public Members */

GtkWidget *
play_clue_row_new (GtkSizeGroup *size_group)
{
  PlayClueRow *clue_row;

  g_return_val_if_fail (GTK_IS_SIZE_GROUP (size_group), NULL);

  clue_row = g_object_new (PLAY_TYPE_CLUE_ROW, NULL);
  gtk_size_group_add_widget (size_group, clue_row->num_label);

  return GTK_WIDGET (clue_row);
}

void
play_clue_row_set_standalone (PlayClueRow *clue_row,
                              gboolean     standalone)
{
  g_return_if_fail (PLAY_IS_CLUE_ROW (clue_row));

  standalone = !!standalone;

  if (standalone == clue_row->standalone)
    return;

  clue_row->standalone = standalone;

  if (standalone)
    gtk_widget_add_css_class (GTK_WIDGET (clue_row),
                                "standalone");
  else
    gtk_widget_remove_css_class (GTK_WIDGET (clue_row),
                                 "standalone");
}

void
play_clue_row_update (PlayClueRow *clue_row,
                      XwordState  *state,
                      IPuzClue    *clue,
                      IPuzClueId   clue_id,
                      gboolean     showenumerations,
                      ZoomLevel    zoom_level)
{
  g_autofree gchar *clue_string = NULL;
  g_autofree gchar *num_text = NULL;
  const gchar *clue_text;
  gboolean correct = FALSE;

  g_return_if_fail (PLAY_IS_CLUE_ROW (clue_row));
  g_return_if_fail (clue != NULL);

  clue_row->clue_id = clue_id;

  /* Set the clue number/label */

  /* Note, the space character at the end of the numbering
   * text. That's an EM SPACE (U+2003). That's so we can have
   * the space between clues and clue numbers honor the curent
   * font. */

  if (ipuz_clue_get_number (clue) > 0)
    num_text = g_strdup_printf ("<b>%d</b> ", ipuz_clue_get_number (clue));
  else if (ipuz_clue_get_label (clue) != NULL)
    num_text = g_markup_printf_escaped ("<b>%s</b> ", ipuz_clue_get_label (clue));

  if (num_text)
    {
      set_zoom_level_css_class (clue_row->num_label, NULL, zoom_level);
      gtk_label_set_markup (GTK_LABEL (clue_row->num_label), num_text);
    }

  /* Handle the is-title case. This will make us wrap more
   * aggressively with small puzzles */
  if (clue_row->standalone)
    gtk_label_set_width_chars (GTK_LABEL (clue_row->clue_label), -1);
  else
    gtk_label_set_width_chars (GTK_LABEL (clue_row->clue_label), 30);

  /* The clue label */
  /* Clear the guessed label */
  gtk_widget_remove_css_class (clue_row->num_label, "guessed");
  gtk_widget_remove_css_class (clue_row->clue_label, "guessed");
  gtk_widget_remove_css_class (GTK_WIDGET (clue_row), "guessed-error");

  clue_text = ipuz_clue_get_clue_text (clue);
  if (clue_text == NULL)
    /* This is what we display for an empty clue row */
    clue_text = _("&lt;empty>");
  if (showenumerations)
    {
      g_autoptr (IPuzEnumeration) enumeration = NULL;

      enumeration = ipuz_clue_get_enumeration (clue);
      if (ipuz_enumeration_get_display (enumeration))
        clue_string = g_strdup_printf ("%s  (%s)",
                                       clue_text,
                                       ipuz_enumeration_get_display (enumeration));
    }
  else
    {
      clue_string = g_strdup (clue_text);
    }

  set_zoom_level_css_class (clue_row->clue_label, NULL, zoom_level);

  /* set up the guessed and guessed/reveal states on the css */
  if (state->mode == XWORD_STATE_SOLVE &&
      ipuz_crossword_clue_guessed (state->xword, clue, &correct))
    {
      if (state->reveal_mode != XWORD_REVEAL_NONE && !correct)
        gtk_widget_add_css_class (GTK_WIDGET (clue_row), "guessed-error");

      gtk_widget_add_css_class (clue_row->num_label, "guessed");
      gtk_widget_add_css_class (clue_row->clue_label, "guessed");
    }
  gtk_label_set_markup (GTK_LABEL (clue_row->clue_label), clue_string);
}

IPuzClueId
play_clue_row_get_clue_id (PlayClueRow *clue_row)
{
  IPuzClueId none = { .direction = IPUZ_CLUE_DIRECTION_NONE, .index = 0 };

  g_return_val_if_fail (PLAY_IS_CLUE_ROW (clue_row), none);

  return clue_row->clue_id;
}
