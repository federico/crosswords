/* puzzle-downloader-dialog.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS


typedef enum
{
  DOWNLOADER_TYPE_FILE,
  DOWNLOADER_TYPE_AUTO,
  DOWNLOADER_TYPE_DATE,
  DOWNLOADER_TYPE_URL,
  DOWNLOADER_TYPE_NUMBER,
  DOWNLOADER_TYPE_ENTRY
} DownloaderType;

#define PUZZLE_TYPE_DOWNLOADER_DIALOG (puzzle_downloader_dialog_get_type())
G_DECLARE_FINAL_TYPE (PuzzleDownloaderDialog, puzzle_downloader_dialog, PUZZLE, DOWNLOADER_DIALOG, GtkDialog);


GtkDialog *   puzzle_downloader_dialog_new               (DownloaderType          type,
                                                          GtkWindow              *parent_window,
                                                          const gchar            *primary_text,
                                                          const gchar            *secondary_text,
                                                          const gchar            *uri,
                                                          const gchar            *link_text);
void          puzzle_downloader_dialog_set_date_limits   (PuzzleDownloaderDialog *dialog,
                                                          GDate                  *value,
                                                          GDate                  *lower,
                                                          GDate                  *upper);
void          puzzle_downloader_dialog_set_number_limits (PuzzleDownloaderDialog *dialog,
                                                          gint                    value,
                                                          gint                    lower,
                                                          gint                    upper);
void          puzzle_downloader_dialog_set_url_base      (PuzzleDownloaderDialog *dialog,
                                                          const gchar            *string);
gint          puzzle_downloader_dialog_get_number        (PuzzleDownloaderDialog *dialog);
GDateTime    *puzzle_downloader_dialog_get_date          (PuzzleDownloaderDialog *dialog);
gchar        *puzzle_downloader_dialog_get_url           (PuzzleDownloaderDialog *dialog);
gchar        *puzzle_downloader_dialog_get_entry         (PuzzleDownloaderDialog *dialog);


G_END_DECLS
