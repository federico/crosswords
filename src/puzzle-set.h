/* puzzle-set.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

G_BEGIN_DECLS

typedef enum
{
  PUZZLE_SET_TYPE_RESOURCE,
  /* Extend this later with more granularity */
} PuzzleSetType;


typedef enum
{
  PUZZLE_PHASE_MAIN,
  PUZZLE_PHASE_PICKER,
  PUZZLE_PHASE_GAME,
} PuzzlePhase;

#define PUZZLE_TYPE_SET (puzzle_set_get_type())
G_DECLARE_DERIVABLE_TYPE (PuzzleSet, puzzle_set, PUZZLE, SET, GObject);


struct _PuzzleSetClass
{
  GObjectClass parent_class;

  /* Signals */
  void         (* puzzles_start)   (PuzzleSet   *puzzle_set);
  void         (* change_phase)    (PuzzleSet   *puzzle_set,
                                    PuzzlePhase  phase);
  void         (* puzzles_done)    (PuzzleSet   *puzzle_set);
  void         (* reveal_canceled) (PuzzleSet   *puzzle_set);

  /* Virtual function */
  GtkWidget   *(* get_widget)      (PuzzleSet   *puzzle_set,
                                    PuzzlePhase  phase);
  IPuzPuzzle  *(* get_puzzle)      (PuzzleSet   *puzzle_set,
                                    PuzzlePhase  phase);
  const gchar *(* get_title)       (PuzzleSet   *puzzle_set,
                                    PuzzlePhase  phase);
  gboolean     (* use_picker)      (PuzzleSet   *puzzle_set);
  void         (* reset_puzzle)    (PuzzleSet   *puzzle_set);
  void         (* reveal_toggled)  (PuzzleSet   *puzzle_set,
                                    gboolean     reveal);
  void         (* show_hint)       (PuzzleSet   *puzzle_set);
  const gchar *(* get_uri)         (PuzzleSet   *puzzle_set,
                                    PuzzlePhase  phase);
  void         (* load_uri)        (PuzzleSet   *puzzle_set,
                                    const gchar *uri);
};

PuzzleSetType  puzzle_set_get_puzzle_type   (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_id            (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_short_name    (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_long_name     (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_locale        (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_language      (PuzzleSet   *puzzle_set);
gboolean       puzzle_set_get_disabled      (PuzzleSet   *puzzle_set);
void           puzzle_set_puzzles_start     (PuzzleSet   *puzzle_set);
void           puzzle_set_change_phase      (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
PuzzlePhase    puzzle_set_get_phase         (PuzzleSet   *puzzle_set);
void           puzzle_set_puzzles_done      (PuzzleSet   *puzzle_set);
void           puzzle_set_reveal_canceled   (PuzzleSet   *puzzle_set);
GtkWidget     *puzzle_set_get_widget        (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
IPuzPuzzle    *puzzle_set_get_puzzle        (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
const gchar   *puzzle_set_get_title         (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
gboolean       puzzle_set_use_picker        (PuzzleSet   *puzzle_set);
void           puzzle_set_reset_puzzle      (PuzzleSet   *puzzle_set);
void           puzzle_set_reveal_toggled    (PuzzleSet   *puzzle_set,
                                             gboolean     reveal);
void           puzzle_set_show_hint         (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_uri           (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
void           puzzle_set_load_uri          (PuzzleSet   *puzzle_set,
                                             const gchar *uri);

/* Useful functions */
void           puzzle_sets_init             (void);
GListModel    *crosswords_load_puzzle_sets  (void);


G_END_DECLS
