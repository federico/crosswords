/* edit-clues.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-clue-entry.h"
#include "edit-clues.h"
#include "play-grid.h"


struct _EditClues
{
  GtkWidget    parent_instance;

  PuzzleStack *puzzle_stack;
  gulong       changed_id;

  XwordState  *clues_state;
  XwordState  *answer_state;

  /* Template widgets */
  GtkWidget   *clues_grid;
  GtkWidget   *across_toggle;
  GtkWidget   *down_toggle;
  GtkWidget   *answer_grid;
  GtkWidget   *clue_text_view;
  GtkWidget   *label_entry;
  GtkWidget   *enumeration_entry;
};


static void        edit_clues_init                (EditClues       *self);
static void        edit_clues_class_init          (EditCluesClass  *klass);
static void        edit_clues_dispose             (GObject         *object);
static void        edit_clues_actions_toggle      (EditClues       *self,
                                                   const gchar     *action_name,
                                                   GVariant        *param);
static XwordState *edit_clues_create_answer_state (EditClues       *self);
static void        clues_grid_do_command_cb       (PlayGrid        *grid,
                                                   XwordCmdKind     kind,
                                                   EditClues       *edit_clues);
static void        clues_grid_cell_selected_cb    (PlayGrid        *grid,
                                                   guint            row,
                                                   guint            column,
                                                   EditClues       *edit_clues);
static void        direction_toggled_cb           (EditClues       *edit_clues,
                                                   GtkWidget       *toggle);
static void        prev_clicked_cb                (EditClues       *edit_clues);
static void        next_clicked_cb                (EditClues       *edit_clues);
static void        answer_grid_do_command_cb      (PlayGrid        *grid,
                                                   XwordCmdKind     kind,
                                                   EditClues       *edit_clues);
static void        answer_grid_cell_selected_cb   (PlayGrid        *grid,
                                                   guint            row,
                                                   guint            column,
                                                   EditClues       *edit_clues);
static void        use_clue_clicked_cb            (EditClues       *edit_clues);
static void        edit_clues_update_states       (EditClues       *edit_clues);


G_DEFINE_TYPE (EditClues, edit_clues, GTK_TYPE_WIDGET);


static void
edit_clues_init (EditClues *self)
{
  GtkLayoutManager *box_layout;

  gtk_widget_init_template (GTK_WIDGET (self));

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_HORIZONTAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 50);
  gtk_widget_set_hexpand (GTK_WIDGET (self), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (self), TRUE);
}

static void
edit_clues_class_init (EditCluesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_clues_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clues.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClues, clues_grid);
  gtk_widget_class_bind_template_child (widget_class, EditClues, across_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditClues, down_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditClues, answer_grid);
  gtk_widget_class_bind_template_child (widget_class, EditClues, clue_text_view);
  gtk_widget_class_bind_template_child (widget_class, EditClues, label_entry);
  gtk_widget_class_bind_template_child (widget_class, EditClues, enumeration_entry);

  gtk_widget_class_bind_template_callback (widget_class, clues_grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, clues_grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, direction_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, prev_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, next_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, use_clue_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, answer_grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, answer_grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_clues_commit_changes);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_t, GDK_CONTROL_MASK, "stage.toggle", NULL);

  gtk_widget_class_install_action (widget_class, "stage.toggle", NULL,
                                   (GtkWidgetActionActivateFunc) edit_clues_actions_toggle);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_clues_dispose (GObject *object)
{
  EditClues *self;
  GtkWidget *child;

  self = EDIT_CLUES (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);
  g_clear_pointer (&self->clues_state, xword_state_free);
  g_clear_pointer (&self->answer_state, xword_state_free);

  G_OBJECT_CLASS (edit_clues_parent_class)->dispose (object);
}

static void
edit_clues_actions_toggle (EditClues   *self,
                           const gchar *action_name,
                           GVariant    *param)
{

  g_assert (EDIT_IS_CLUES (self));

  if (self->clues_state == NULL || self->clues_state->xword == NULL)
    return;

  /* Let's try toggling the cursor */
  self->clues_state =
    xword_state_replace (self->clues_state,
                         xword_state_do_command (self->clues_state,
                                                 XWORD_CMD_KIND_SWITCH));
  self->answer_state = xword_state_replace (self->answer_state, edit_clues_create_answer_state (self));
  edit_clues_update_states (self);
}


static XwordState *
edit_clues_create_answer_state (EditClues *self)
{
  XwordState *new_state;
  g_autofree gchar *str = NULL;
  const gchar *ptr;
  const gchar *end;
  g_autoptr (IPuzCrossword) xword = NULL;
  guint i = 0;

  if (self->clues_state == NULL)
    return NULL;

  if (self->clues_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return NULL;

  str = ipuz_crossword_get_clue_string_by_id (self->clues_state->xword,
                                              self->clues_state->clue);
  if (str == NULL || !str[0])
    return NULL;

  xword = ipuz_crossword_new ();
  ipuz_crossword_set_size (xword, utf8_n_clusters (str), 1);
  ptr = str;
  while (ptr[0])
    {
      IPuzCell *cell;
      g_autofree gchar *cluster = NULL;
      IPuzCellCoord coord = {
        .row = 0,
        .column = i
      };

      cluster = utf8_get_next_cluster (ptr, &end);
      ptr = end;

      /* FIXME (magichars): Centralize this */
      if (g_strcmp0 (cluster, "?") != 0)
        {
          cell = ipuz_crossword_get_cell (xword, coord);
          ipuz_cell_set_solution (cell, cluster);
        }
      i++;
    }

  new_state = xword_state_new (xword, NULL, XWORD_STATE_SELECT);

  return new_state;
}

static void
edit_clues_puzzle_stack_changed_new (EditClues   *self,
                                     PuzzleStack *puzzle_stack)
{
  IPuzPuzzle *puzzle;
  PuzzleStackChangeType change_type;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  change_type = puzzle_stack_get_change_type (puzzle_stack);

  if (puzzle == NULL)
    {
      g_clear_pointer (&self->clues_state, xword_state_free);
      g_clear_pointer (&self->answer_state, xword_state_free);
    }
  else /* We have a puzzle to work with */
    {
      /* We always create a new state and reset the state when we get a new puzzle */
      if (change_type == STACK_CHANGE_PUZZLE)
        {
          self->clues_state = xword_state_replace (self->clues_state, xword_state_new (IPUZ_CROSSWORD (puzzle), NULL, XWORD_STATE_EDIT_BROWSE));
        }
      else
        {
          /* We may have a similar looking puzzle, but it's a new-to-us puzzle
           * so we update the state as best as we can */
          if (self->clues_state->xword != IPUZ_CROSSWORD (puzzle))
            {
              self->clues_state = xword_state_replace (self->clues_state, xword_state_dehydrate (self->clues_state));
              self->clues_state = xword_state_replace (self->clues_state, xword_state_hydrate (self->clues_state,
                                                                                               IPUZ_CROSSWORD (puzzle),
                                                                                               NULL));
            }
        }
      self->answer_state = xword_state_replace (self->answer_state, edit_clues_create_answer_state (self));
    }

  if (self->clues_state)
    {
      XwordState *dehydrated_state;
      dehydrated_state = xword_state_dehydrate (self->clues_state);
      puzzle_stack_set_data (puzzle_stack,
                             "edit-clues",
                             dehydrated_state,
                             (GDestroyNotify) xword_state_free);
    }
}

static void
edit_clues_puzzle_stack_changed_load (EditClues   *self,
                                      PuzzleStack *puzzle_stack)
{
  IPuzPuzzle *puzzle;
  XwordState *dehydrated_state;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  dehydrated_state = puzzle_stack_get_data (puzzle_stack, "edit-clues");

  if (dehydrated_state)
    {
      self->clues_state = xword_state_replace (self->clues_state, xword_state_hydrate (dehydrated_state,
                                                                                       IPUZ_CROSSWORD (puzzle),
                                                                                       NULL));
    }
  else
    {
      if (self->clues_state)
        g_clear_pointer (&self->clues_state, xword_state_free);
    }
}


static void
edit_clues_puzzle_stack_changed (EditClues             *self,
                                 gboolean               new_frame,
                                 PuzzleStackChangeType  change_type,
                                 PuzzleStack           *puzzle_stack)
{
  if (new_frame)
    {
      edit_clues_puzzle_stack_changed_new (self, puzzle_stack);
    }
  else
    {
      edit_clues_puzzle_stack_changed_load (self, puzzle_stack);
    }

  edit_clues_update_states (self);
}

static void
clues_grid_do_command_cb (PlayGrid        *grid,
                          XwordCmdKind     kind,
                          EditClues       *clues)
{
  clues->clues_state = xword_state_replace (clues->clues_state,
                                            xword_state_do_command (clues->clues_state, kind));
  clues->answer_state = xword_state_replace (clues->answer_state, edit_clues_create_answer_state (clues));
  edit_clues_update_states (clues);
}

static void
clues_grid_cell_selected_cb    (PlayGrid       *grid,
                                guint           row,
                                guint           column,
                                EditClues      *clues)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  clues->clues_state = xword_state_replace (clues->clues_state, xword_state_cell_selected (clues->clues_state, coord));
  clues->answer_state = xword_state_replace (clues->answer_state, edit_clues_create_answer_state (clues));
  edit_clues_update_states (clues);
}

static void
direction_toggled_cb (EditClues *edit_clues,
                      GtkWidget *toggle)
{
  IPuzClueDirection direction;

  g_assert (EDIT_IS_CLUES (edit_clues));
  if (edit_clues->clues_state == NULL || edit_clues->clues_state->xword == NULL)
    return;

  if (toggle == edit_clues->across_toggle)
    direction = IPUZ_CLUE_DIRECTION_ACROSS;
  else if (toggle == edit_clues->down_toggle)
    direction = IPUZ_CLUE_DIRECTION_DOWN;
  else
    g_assert_not_reached ();

  if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)))
    return;

  if (edit_clues->clues_state->clue.direction == direction)
    return;

  /* Let's try toggling the cursor */
  edit_clues->clues_state =
    xword_state_replace (edit_clues->clues_state,
                         xword_state_do_command (edit_clues->clues_state,
                                                 XWORD_CMD_KIND_SWITCH));

  /* If not, let's just select the first one */
  if (edit_clues->clues_state->clue.direction != direction)
    {
      IPuzClue *clue;
      IPuzClueId clue_id = {
        .direction = direction,
        .index = 0
      };

      clue = ipuz_crossword_get_clue_by_id (edit_clues->clues_state->xword, clue_id);
      edit_clues->clues_state =
        xword_state_replace (edit_clues->clues_state,
                             xword_state_clue_selected (edit_clues->clues_state,
                                                        clue));
    }

  edit_clues->answer_state = xword_state_replace (edit_clues->answer_state, edit_clues_create_answer_state (edit_clues));
  edit_clues_update_states (edit_clues);
}

static void
prev_clicked_cb (EditClues *edit_clues)
{
  g_assert (EDIT_IS_CLUES (edit_clues));
  if (edit_clues->clues_state == NULL || edit_clues->clues_state->xword == NULL)
    return;

  edit_clues->clues_state = xword_state_replace (edit_clues->clues_state,
                                                 xword_state_do_command (edit_clues->clues_state,
                                                                         XWORD_CMD_KIND_BACK_CLUE));
  edit_clues->answer_state = xword_state_replace (edit_clues->answer_state, edit_clues_create_answer_state (edit_clues));
  edit_clues_update_states (edit_clues);
}

static void
next_clicked_cb (EditClues *edit_clues)
{
  g_assert (EDIT_IS_CLUES (edit_clues));
  if (edit_clues->clues_state == NULL || edit_clues->clues_state->xword == NULL)
    return;

  edit_clues->clues_state = xword_state_replace (edit_clues->clues_state,
                                                 xword_state_do_command (edit_clues->clues_state,
                                                                         XWORD_CMD_KIND_FORWARD_CLUE));
  edit_clues->answer_state = xword_state_replace (edit_clues->answer_state, edit_clues_create_answer_state (edit_clues));
  edit_clues_update_states (edit_clues);
}

static void
answer_grid_do_command_cb (PlayGrid        *grid,
                           XwordCmdKind     kind,
                           EditClues       *clues)
{
}

static void
answer_grid_cell_selected_cb (PlayGrid  *grid,
                              guint      row,
                              guint      column,
                              EditClues *clues)
{
}

static void
use_clue_clicked_cb (EditClues *edit_clues)
{
  GtkTextBuffer *text_buffer;
  IPuzClue *clue;
  g_autofree gchar *clue_text = NULL;
  GtkTextIter start_iter;
  GtkTextIter end_iter;

  g_assert (EDIT_IS_CLUES (edit_clues));
  if (edit_clues->clues_state == NULL || edit_clues->clues_state->xword == NULL)
    return;

  /* Update the clue */
  clue = ipuz_crossword_get_clue_by_id (edit_clues->clues_state->xword,
                                        edit_clues->clues_state->clue);

  /* clue text */
  text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (edit_clues->clue_text_view));
  gtk_text_buffer_get_start_iter (text_buffer, &start_iter);
  gtk_text_buffer_get_end_iter (text_buffer, &end_iter);

  clue_text = gtk_text_buffer_get_text (text_buffer,
                                        &start_iter, &end_iter,
                                        FALSE);
  ipuz_clue_set_clue_text (clue, clue_text);

  next_clicked_cb (edit_clues);
  puzzle_stack_push_change (edit_clues->puzzle_stack,
                            STACK_CHANGE_STATE,
                            IPUZ_PUZZLE (edit_clues->clues_state->xword));
}

static void
edit_clues_update_state (EditClues *clues)
{
  GtkTextBuffer *text_buffer;
  gboolean has_across_clues;
  gboolean has_down_clues;
  IPuzClue *clue;
  const gchar *label;
  g_autoptr (IPuzEnumeration) enumeration = NULL;
  const gchar *clue_text;

  g_assert (EDIT_IS_CLUES (clues));

  if (clues->clues_state == NULL || clues->clues_state->xword == NULL)
    /* FIXME: display a different UI for a NULL puzzle? */
    return;

  /* Update the across/down toggle */
  has_across_clues = (ipuz_crossword_get_n_clues (clues->clues_state->xword,
                                                  IPUZ_CLUE_DIRECTION_ACROSS) > 0);
  has_down_clues = (ipuz_crossword_get_n_clues (clues->clues_state->xword,
                                                IPUZ_CLUE_DIRECTION_DOWN) > 0);
  gtk_widget_set_sensitive (clues->across_toggle, has_across_clues);
  gtk_widget_set_sensitive (clues->down_toggle, has_down_clues);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (clues->across_toggle),
                                (clues->clues_state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (clues->down_toggle),
                                (clues->clues_state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN));

  /* Update the clue */
  clue = ipuz_crossword_get_clue_by_id (clues->clues_state->xword,
                                        clues->clues_state->clue);

  /* clue text */
  text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (clues->clue_text_view));
  clue_text = ipuz_clue_get_clue_text (clue);
  gtk_text_buffer_set_text (text_buffer,
                            (clue_text ? clue_text : ""),
                            -1);

  /* label */
  label = ipuz_clue_get_label (clue);
  gtk_editable_set_text (GTK_EDITABLE (clues->label_entry),
                         (label ? label : ""));

  /* enumeration */
  enumeration = ipuz_clue_get_enumeration (clue);
  if (enumeration)
    gtk_editable_set_text (GTK_EDITABLE (clues->enumeration_entry),
                           ipuz_enumeration_get_display (enumeration));
}

/* called just when the values on the grid changes */
static void
edit_clues_update_states (EditClues *clues)
{
  LayoutConfig answer_layout_config = {
    .border_size = 4,
    .base_size = 20,
  };
  if (clues->clues_state == NULL)
    return;

  play_grid_update_state (PLAY_GRID (clues->clues_grid), clues->clues_state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  play_grid_update_state (PLAY_GRID (clues->answer_grid), clues->answer_state, answer_layout_config);
  edit_clues_update_state (clues);
}

/* Public functions */

void
edit_clues_set_puzzle_stack (EditClues   *edit_clues,
                             PuzzleStack *puzzle_stack)
{
  g_return_if_fail (EDIT_IS_CLUES (edit_clues));

  if (puzzle_stack)
    g_object_ref (puzzle_stack);

  g_clear_signal_handler (&edit_clues->changed_id,
                          edit_clues->puzzle_stack);
  g_clear_object (&edit_clues->puzzle_stack);

  edit_clues->puzzle_stack = puzzle_stack;
  edit_clues->changed_id = g_signal_connect_swapped (edit_clues->puzzle_stack,
                                                     "changed",
                                                     G_CALLBACK (edit_clues_puzzle_stack_changed),
                                                     edit_clues);

  /* Force a changed signal to get the inital setup done */
  edit_clues_puzzle_stack_changed (edit_clues, TRUE, STACK_CHANGE_PUZZLE, puzzle_stack);
}

/* If we have a clue half-written, commit it to the stack. This is
 * called during stage changes and when saving to disk
 */
void
edit_clues_commit_changes (EditClues *edit_clues)
{
  GtkTextBuffer *text_buffer;
  IPuzClue *clue;
  g_autoptr (IPuzEnumeration) enumeration = NULL;
  g_autofree gchar *clue_text = NULL;
  const gchar *puzzle_clue_text;
  const gchar *label_text;
  const gchar *enumeration_text;
  GtkTextIter start_iter, end_iter;
  gboolean push_change = FALSE;
  PuzzleStackChangeType change_type = STACK_CHANGE_STATE;

  g_return_if_fail (EDIT_IS_CLUES (edit_clues));

  if (edit_clues->clues_state == NULL || edit_clues->clues_state->xword == NULL)
    return;

  /* Get the clue we are editing */
  clue = ipuz_crossword_get_clue_by_id (edit_clues->clues_state->xword,
                                        edit_clues->clues_state->clue);

  /* clue text */
  puzzle_clue_text = ipuz_clue_get_clue_text (clue);
  if (puzzle_clue_text == NULL)
    puzzle_clue_text = "";

  text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (edit_clues->clue_text_view));
  gtk_text_buffer_get_start_iter (text_buffer, &start_iter);
  gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
  clue_text = gtk_text_buffer_get_text (text_buffer,
                                        &start_iter, &end_iter,
                                        FALSE);
  if (g_strcmp0 (puzzle_clue_text, clue_text))
    {
      ipuz_clue_set_clue_text (clue, clue_text);
      push_change = TRUE;
      change_type = STACK_CHANGE_STATE;
    }

  /* label text */
  puzzle_clue_text = ipuz_clue_get_label (clue);
  if (puzzle_clue_text == NULL)
    puzzle_clue_text = "";
  label_text = gtk_editable_get_text (GTK_EDITABLE (edit_clues->label_entry));
  if (g_strcmp0 (puzzle_clue_text, label_text))
    {
      ipuz_clue_set_label (clue, label_text);
      push_change = TRUE;
      change_type = STACK_CHANGE_PUZZLE;
    }

  /* enumeration text */
  enumeration_text = gtk_editable_get_text (GTK_EDITABLE (edit_clues->enumeration_entry));
  enumeration = ipuz_clue_get_enumeration (clue);
  if (enumeration != NULL &&
      g_strcmp0 (ipuz_enumeration_get_src (enumeration), enumeration_text))
    {
      g_autoptr (IPuzEnumeration) new_enumeration = NULL;
      new_enumeration = ipuz_enumeration_new (enumeration_text, IPUZ_VERBOSITY_STANDARD);
      ipuz_clue_set_enumeration (clue, new_enumeration);
      push_change = TRUE;
      change_type = STACK_CHANGE_STATE;
    }


  /* update the puzzle */
  if (push_change)
    puzzle_stack_push_change (edit_clues->puzzle_stack,
                              change_type,
                              IPUZ_PUZZLE (edit_clues->clues_state->xword));
}
