/* play-grid.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "crosswords-enums.h"
#include "crosswords-svg.h"
#include "play-border.h"
#include "play-cell.h"
#include "grid-layout.h"
#include "play-grid.h"
#include "play-marshaller.h"
#include "svg.h"


enum
{
  PROP_0,
  PROP_RESIZABLE,
  PROP_MIN_BASE_SIZE,
  PROP_MAX_BASE_SIZE,
  N_PROPS,
};

enum
{
  GUESS,
  GUESS_AT_CELL,
  DO_COMMAND,
  CELL_SELECTED,
  N_SIGNALS
};


static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };

struct _PlayGrid
{
  GtkWidget  parent_instance;
  gboolean   resizable;
  guint min_base_size;
  guint max_base_size;

  GtkWidget *overlay;
  GtkWidget *vbox;

  /* SvgWidget overlaid on the vbox */
  GtkWidget *decorations;

  /* See grid-layout.h for the meaning of board_rows vs. grid_rows / etc. */
  guint      board_rows;
  guint      board_columns;
  guint      grid_rows;
  guint      grid_columns;

  /* 2D array of PlayCell* or PlayBorder*, stored as grid_rows*grid_columns elements, row
   * major, rowstride=grid_columns */
  GPtrArray *children;

  /* Array with one GtkBox per grid_row */
  GPtrArray *rows;

  /* Layout configuration for the current set of widgets */
  GridLayout *layout;
  LayoutConfig config;

  /* Whether we are populated yet.  The fields above are valid only if loaded == TRUE. */
  gboolean   loaded;
};


static void               play_grid_init             (PlayGrid       *self);
static void               play_grid_class_init       (PlayGridClass  *klass);
static void               play_grid_dispose          (GObject        *object);
static void               play_grid_set_property     (GObject        *object,
                                                      guint           prop_id,
                                                      const GValue   *value,
                                                      GParamSpec     *pspec);
static void               play_grid_get_property     (GObject        *object,
                                                      guint           prop_id,
                                                      GValue         *value,
                                                      GParamSpec     *pspec);
static void               play_grid_size_allocate    (GtkWidget      *widget,
                                                      int             width,
                                                      int             height,
                                                      int             baseline);
static void               play_grid_measure          (GtkWidget      *widget,
                                                      GtkOrientation  orientation,
                                                      int             for_size,
                                                      int            *minimum,
                                                      int            *natural,
                                                      int            *minimum_baseline,
                                                      int            *natural_baseline);
static GtkSizeRequestMode play_grid_get_request_mode (GtkWidget      *widget);


G_DEFINE_TYPE (PlayGrid, play_grid, GTK_TYPE_WIDGET);

static void
play_grid_init (PlayGrid *self)
{
  gtk_widget_set_can_focus (GTK_WIDGET (self), TRUE);
  self->overlay = gtk_overlay_new ();
  self->vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_overlay_set_child (GTK_OVERLAY (self->overlay), self->vbox);
  gtk_widget_insert_before (self->overlay, GTK_WIDGET (self), NULL);

  self->decorations = crosswords_svg_new ();
  gtk_widget_set_can_target (self->decorations, FALSE);
  gtk_overlay_add_overlay (GTK_OVERLAY (self->overlay), self->decorations);

  /* Keep these values in sync with layout_config_at_zoom_level(), as
   * well as class_init() */
  self->min_base_size = 7;
  self->max_base_size = 36;
  self->resizable = FALSE;

  self->board_rows = 0;
  self->board_columns = 0;
  self->grid_rows = 0;
  self->grid_columns = 0;
  self->children = NULL;
  self->rows = NULL;
  self->config = layout_config_default (IPUZ_PUZZLE_CROSSWORD);
  self->loaded = FALSE;
}

static void
play_grid_class_init (PlayGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = play_grid_set_property;
  object_class->get_property = play_grid_get_property;
  object_class->dispose = play_grid_dispose;

  widget_class->size_allocate = play_grid_size_allocate;
  widget_class->measure = play_grid_measure;
  widget_class->get_request_mode = play_grid_get_request_mode;

  obj_signals [GUESS] =
    g_signal_new ("guess",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [GUESS_AT_CELL] =
    g_signal_new ("guess-at-cell",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  G_TYPE_STRING,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CELL_SELECTED] =
    g_signal_new ("cell-selected",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [DO_COMMAND] =
    g_signal_new ("do-command",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  XWORD_TYPE_CMD_KIND);

  obj_props[PROP_RESIZABLE] = g_param_spec_boolean ("resizable",
                                                    "Resizable",
                                                    "Whether the grid fills it's size",
                                                    FALSE,
                                                    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  obj_props[PROP_MIN_BASE_SIZE] = g_param_spec_uint ("min-base-size",
                                                     "Minimum base size",
                                                     "Smallest possible base size",
                                                     MIN_CELL_BASE_SIZE,
                                                     MAX_CELL_BASE_SIZE,
                                                     7,
                                                     G_PARAM_READWRITE);
  obj_props[PROP_MAX_BASE_SIZE] = g_param_spec_uint ("max-base-size",
                                                     "Maximum base size",
                                                     "Largest possible base_size",
                                                     MIN_CELL_BASE_SIZE,
                                                     MAX_CELL_BASE_SIZE,
                                                     48,
                                                     G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

}


static void
play_grid_clear (PlayGrid *grid)
{
  if (grid->loaded)
    {
      for (guint i = 0; i < grid->grid_rows; i++)
        {
          GtkWidget **row = (GtkWidget **) &g_ptr_array_index (grid->rows, i);
          gtk_widget_unparent (*row);
          *row = NULL;
        }

      g_ptr_array_free (grid->rows, TRUE);
      g_ptr_array_free (grid->children, TRUE);

      grid->board_rows = 0;
      grid->board_columns = 0;
      grid->grid_rows = 0;
      grid->grid_columns = 0;
      grid->children = NULL;
      grid->rows = NULL;
      grid->loaded = FALSE;
    }
}

static void
play_grid_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PlayGrid *self = PLAY_GRID (object);

  switch (prop_id)
    {
    case PROP_RESIZABLE:
      self->resizable = g_value_get_boolean (value);
      break;
    case PROP_MIN_BASE_SIZE:
      self->min_base_size = g_value_get_uint (value);
      break;
    case PROP_MAX_BASE_SIZE:
      self->max_base_size = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_grid_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PlayGrid *self;

  g_return_if_fail (object != NULL);

  self = PLAY_GRID (object);

  switch (prop_id)
    {
    case PROP_RESIZABLE:
      g_value_set_boolean (value, self->resizable);
      break;
    case PROP_MIN_BASE_SIZE:
      g_value_set_boolean (value, self->min_base_size);
      break;
    case PROP_MAX_BASE_SIZE:
      g_value_set_boolean (value, self->max_base_size);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static void
play_grid_dispose (GObject *object)
{
  PlayGrid *self;
  GtkWidget *child;

  self = PLAY_GRID (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->layout, grid_layout_free);
  G_OBJECT_CLASS (play_grid_parent_class)->dispose (object);
}

static void
play_grid_size_allocate (GtkWidget *widget,
                         int        width,
                         int        height,
                         int        baseline)
{
  PlayGrid *self;
  GtkWidget *child;
  LayoutConfig config;
  gboolean valid = FALSE;

  self = PLAY_GRID (widget);

  config = layout_config_within_bounds (self->min_base_size,
                                        self->max_base_size,
                                        width, height,
                                        self->board_columns,
                                        self->board_rows,
                                        &valid);

  if (valid)
    {
      int child_min = 0;
      int child_nat = 0;
      int child_min_baseline = -1;
      int child_nat_baseline = -1;
      play_grid_set_layout_config (self, config);

      /* GTK will complain if we just allocate the overlay without remeasuring it */
      gtk_widget_measure (self->overlay, GTK_ORIENTATION_VERTICAL, -1,
                          &child_min, &child_nat,
                          &child_min_baseline, &child_nat_baseline);
      gtk_widget_measure (self->overlay, GTK_ORIENTATION_HORIZONTAL, -1,
                          &child_min, &child_nat,
                          &child_min_baseline, &child_nat_baseline);
    }

  for (child = gtk_widget_get_first_child (widget);
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      if (gtk_widget_should_layout (child))
        gtk_widget_allocate (child, width, height, baseline, NULL);
    }
}

static void
play_grid_measure (GtkWidget      *widget,
                   GtkOrientation  orientation,
                   int             for_size,
                   int            *minimum,
                   int            *natural,
                   int            *minimum_baseline,
                   int            *natural_baseline)
{
  PlayGrid *self;
  GtkWidget *child;
  gint new_width = 0;
  gint new_height = 0;

  self = PLAY_GRID (widget);

  /* This is just the overlay widget, though we keep the loop just in
   * case the layout changes again. */
  for (child = gtk_widget_get_first_child (widget);
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      if (gtk_widget_should_layout (child))
        {
          int child_min = 0;
          int child_nat = 0;
          int child_min_baseline = -1;
          int child_nat_baseline = -1;

          gtk_widget_measure (child, orientation, for_size,
                              &child_min, &child_nat,
                              &child_min_baseline, &child_nat_baseline);

          *minimum = MAX (*minimum, child_min);
          *natural = MAX (*natural, child_nat);

          if (child_min_baseline > -1)
            *minimum_baseline = MAX (*minimum_baseline, child_min_baseline);
          if (child_nat_baseline > -1)
            *natural_baseline = MAX (*natural_baseline, child_nat_baseline);
        }
    }

  /* If we're resizable, forget all that measuring */
  if (! self->resizable)
    return;

  if (for_size == -1)
    {
      layout_config_size_at_base_size (self->min_base_size,
                                       self->board_columns,
                                       self->board_rows,
                                       &new_width, &new_height);

    }
  else
    {
      LayoutConfig config;
      gboolean valid;

      if (orientation == GTK_ORIENTATION_HORIZONTAL)
        {
          config = layout_config_within_bounds (self->min_base_size,
                                                self->max_base_size,
                                                -1, for_size,
                                                self->board_columns,
                                                self->board_rows,
                                                &valid);
        }
      else if (orientation == GTK_ORIENTATION_VERTICAL)
        {
          config = layout_config_within_bounds (self->min_base_size,
                                                self->max_base_size,
                                                for_size, -1,
                                                self->board_columns,
                                                self->board_rows,
                                                &valid);

        }
      layout_config_size_at_base_size (config.base_size,
                                       self->board_columns,
                                       self->board_rows,
                                       &new_width, &new_height);
    }
  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
      *minimum = new_width;
      *natural = new_width;
    }
  else if (orientation == GTK_ORIENTATION_VERTICAL)
    {
      *minimum = new_height;
      *natural = new_height;
    }
}

static GtkSizeRequestMode
play_grid_get_request_mode (GtkWidget *widget)
{
  PlayGrid *self;

  self = PLAY_GRID (widget);

  if (self->resizable)
    return GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH;

  return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
guess_cb (GObject     *cell,
          const gchar *guess,
          PlayGrid    *grid)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (grid), obj_signals [GUESS], 0, guess);
}

static void
guess_at_cell_cb (GObject     *cell,
                  const gchar *guess,
                  guint        row,
                  guint        column,
                  PlayGrid    *grid)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (grid), obj_signals [GUESS_AT_CELL], 0, guess, row, column);
}

static void
do_command_cb (GObject         *cell,
               XwordCmdKind     kind,
               PlayGrid        *grid)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (grid), obj_signals [DO_COMMAND], 0, kind);
}

static void
cell_selected_cb (GObject *cell,
                  guint row,
                  guint column,
                  PlayGrid *grid)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (grid), obj_signals [CELL_SELECTED], 0, row, column);
}

static GtkWidget *
create_cell_widget (PlayGrid      *grid,
                    IPuzCellCoord  coord,
                    LayoutConfig   config,
                    gboolean       editable)
{
  GtkWidget *cell = play_cell_new (coord, config, editable);
  g_signal_connect (G_OBJECT (cell), "guess", G_CALLBACK (guess_cb), grid);
  g_signal_connect (G_OBJECT (cell), "guess-at-cell", G_CALLBACK (guess_at_cell_cb), grid);
  g_signal_connect (G_OBJECT (cell), "do-command", G_CALLBACK (do_command_cb), grid);
  g_signal_connect (G_OBJECT (cell), "cell-selected", G_CALLBACK (cell_selected_cb), grid);
  return cell;
}

static GtkWidget *
create_border_widget (PlayGrid *grid, PlayBorderKind kind, LayoutConfig config)
{
  GtkWidget *border = play_border_new (kind, config);
  return border;
}

/* Public Functions */

GtkWidget *
play_grid_new (void)
{
  GObject *object;

  object = g_object_new (PLAY_TYPE_GRID, NULL);

  return GTK_WIDGET (object);
}

static PlayBorderKind
layout_item_kind_to_border_kind (LayoutItemKind kind)
{
  switch (kind)
    {
    case LAYOUT_ITEM_KIND_INTERSECTION:
      return PLAY_BORDER_KIND_INTERSECTION;

    case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL:
      return PLAY_BORDER_KIND_HORIZONTAL;

    case LAYOUT_ITEM_KIND_BORDER_VERTICAL:
      return PLAY_BORDER_KIND_VERTICAL;

    default:
      g_assert_not_reached ();
    }
}

static void
set_decorations (PlayGrid *grid, LayoutConfig config)
{
  GString *svg = svg_overlays_from_layout (grid->layout, &config);
  GError *error = NULL;
  RsvgHandle *handle = rsvg_handle_new_from_data ((guint8 *) svg->str, svg->len, &error);

  g_assert (handle != NULL);
  g_assert_no_error (error);

  crosswords_svg_set_document (CROSSWORDS_SVG (grid->decorations), handle);
  g_object_unref (handle);
}

static void
create_widgets (PlayGrid       *grid,
                GridLayout     *layout,
                IPuzCrossword  *xword,
                XwordStateMode  mode,
                LayoutConfig    config)
{
  guint grid_rows, grid_columns;
  guint grid_row, grid_column;

  g_assert (!grid->loaded);

  grid_rows = layout->grid_rows;
  grid_columns = layout->grid_columns;

  grid->board_rows = layout->board_rows;
  grid->board_columns = layout->board_columns;
  grid->grid_rows = grid_rows;
  grid->grid_columns = grid_columns;
  grid->children = g_ptr_array_sized_new (grid_rows * grid_columns);
  grid->rows = g_ptr_array_sized_new (grid_rows);

  for (grid_row = 0; grid_row < grid_rows; grid_row++)
    {
      GtkWidget *row = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
      g_ptr_array_add (grid->rows, row);

      for (grid_column = 0; grid_column < grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItemKind kind = grid_layout_get_kind (layout, grid_coord);

          switch (kind) {
          case LAYOUT_ITEM_KIND_INTERSECTION:
          case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL:
          case LAYOUT_ITEM_KIND_BORDER_VERTICAL: {
            PlayBorderKind border_kind = layout_item_kind_to_border_kind (kind);
            GtkWidget *border = create_border_widget (grid, border_kind, config);
            gtk_box_append (GTK_BOX (row), border);
            g_ptr_array_add (grid->children, border);
            break;
          }
          case LAYOUT_ITEM_KIND_CELL:
          case LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL:{
            IPuzCellCoord coord = { .row = grid_row / 2, .column = grid_column / 2 };
            GtkWidget *cell;
            gboolean editable;

            editable = (mode == XWORD_STATE_SOLVE || mode == XWORD_STATE_EDIT);
            cell = create_cell_widget (grid, coord, config, editable);
            gtk_box_append (GTK_BOX (row), cell);
            g_ptr_array_add (grid->children, cell);
            break;
          }

          default:
            g_assert_not_reached ();
            return;
          }
        }

      gtk_widget_insert_before (row, grid->vbox, NULL);
    }

  grid->loaded = TRUE;
}

static void
update_widgets (PlayGrid       *grid,
                GridLayout     *layout,
                IPuzCrossword  *xword,
                XwordStateMode  mode)
{
  IPuzBoard *board;
  guint grid_row, grid_column;

  board = ipuz_crossword_get_board (xword);

  g_assert (grid->board_rows == layout->board_rows);
  g_assert (grid->board_columns == layout->board_columns);
  g_assert (grid->grid_rows == layout->grid_rows);
  g_assert (grid->grid_columns == layout->grid_columns);

  for (grid_row = 0; grid_row < grid->grid_rows; grid_row++)
    {
      for (grid_column = 0; grid_column < grid->grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItemKind kind = grid_layout_get_kind (layout, grid_coord);
          GtkWidget *child = g_ptr_array_index (grid->children, grid_row * grid->grid_columns + grid_column);

          switch (kind) {
          case LAYOUT_ITEM_KIND_INTERSECTION: {
            LayoutIntersection item = grid_layout_get_intersection (layout, grid_coord);
            PlayBorder *border = PLAY_BORDER (child);
            play_border_update_state (border, item.filled, item.css_class, item.bg_color_set, item.bg_color);
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL: {
            LayoutBorderHorizontal item = grid_layout_get_border_horizontal (layout, grid_coord);
            PlayBorder *border = PLAY_BORDER (child);
            play_border_update_state (border, item.filled, item.css_class, item.bg_color_set, item.bg_color);
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_VERTICAL: {
            LayoutBorderVertical item = grid_layout_get_border_vertical (layout, grid_coord);
            PlayBorder *border = PLAY_BORDER (child);
            play_border_update_state (border, item.filled, item.css_class, item.bg_color_set, item.bg_color);
            break;
          }

          case LAYOUT_ITEM_KIND_CELL: {
            PlayCell *cell = PLAY_CELL (child);
            IPuzCellCoord coord = { .row = grid_row / 2, .column = grid_column / 2 };
            LayoutCell cell_layout = grid_layout_get_cell (layout, grid_coord);
            IPuzCell *puz_cell = ipuz_board_get_cell (board, coord);
            play_cell_load_state (cell, puz_cell, &cell_layout);
            break;
          }
          case LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL: {
            PlayCell *cell = PLAY_CELL (child);
            IPuzCellCoord coord = { .row = grid_row / 2, .column = grid_column / 2 };
            LayoutClueBlockCell clue_block_layout = grid_layout_get_clue_block_cell (layout, grid_coord);
            IPuzCell *puz_cell = ipuz_board_get_cell (board, coord);
            play_cell_load_clue_block_state (cell, puz_cell, &clue_block_layout);
            break;
          }
          default:
            g_assert_not_reached ();
            return;
          }
        }
    }
}

static gboolean
should_reload (PlayGrid     *grid,
               GridLayout   *layout,
               LayoutConfig  new_config)
{
  return (!grid->loaded
          || grid->board_rows != layout->board_rows
          || grid->board_columns != layout->board_columns
          || !layout_config_equal (&grid->config, &new_config));
}


void
play_grid_update_state (PlayGrid     *self,
                        XwordState   *state,
                        LayoutConfig  config)
{
  g_return_if_fail (PLAY_IS_GRID (self));

  g_clear_pointer (&self->layout, grid_layout_free);
  self->layout = grid_layout_new (state);

  /* as an optimization, we keep old widgets around if the board
   * hasn't changed configuration */
  if (should_reload (self, self->layout, config))
    {
      play_grid_clear (self);
      create_widgets (self, self->layout, state->xword, state->mode, config);
      self->config = config;
    }

  set_decorations (self, config);
  update_widgets (self, self->layout, state->xword, state->mode);
}


void
play_grid_focus_cell (PlayGrid      *grid,
                      IPuzCellCoord  coord)
{
  g_return_if_fail (PLAY_IS_GRID (grid));

  g_assert (grid->children != NULL);
  g_assert (grid->board_rows > 0);
  g_assert (grid->board_columns > 0);

  g_assert (coord.row < grid->board_rows);
  g_assert (coord.column < grid->board_columns);

  guint grid_row = 2 * coord.row + 1;
  guint grid_column = 2 * coord.column + 1;

  GtkWidget *cell = g_ptr_array_index (grid->children, grid_row * grid->grid_columns + grid_column);
  gtk_widget_grab_focus (cell);
}

void
play_grid_set_layout_config (PlayGrid     *grid,
                             LayoutConfig  config)
{
  guint grid_row, grid_column;

  g_return_if_fail (PLAY_IS_GRID (grid));

  if (layout_config_equal (&config, &grid->config))
    return;

  grid->config = config;

  set_decorations (grid, config);
  for (grid_row = 0; grid_row < grid->grid_rows; grid_row++)
    {
      for (grid_column = 0; grid_column < grid->grid_columns; grid_column++)
        {
          GtkWidget *child = g_ptr_array_index (grid->children, grid_row * grid->grid_columns + grid_column);

          if (PLAY_IS_CELL (child))
            play_cell_update_config (PLAY_CELL (child), config);
          else if (PLAY_IS_BORDER (child))
            play_border_update_config (PLAY_BORDER (child), config);
        }
    }
}
