/* charset.c - Maintain a character set for a crossword puzzle
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>
#include "charset.h"

struct Charset
{
  /* Keys are GUINT_TO_POINTER(gunichar), values are ignored and always NULL */
  GTree *tree;
};


static gint
charset_compare_cb (gconstpointer a, gconstpointer b)
{
  gunichar ca = GPOINTER_TO_UINT (a);
  gunichar cb = GPOINTER_TO_UINT (b);

  if (ca < cb)
    return -1;
  else if (ca > cb)
    return 1;
  else
    return 0;
}

/**
 * charset_new:
 *
 * Returns: an empty character set.  Use `charset_add()` to populate it.
 */
Charset *
charset_new (void)
{
  Charset *charset = g_new0 (Charset, 1);
  charset->tree = g_tree_new (charset_compare_cb);
  return charset;
}

/** 
 * charset_free:
 * @charset: the character set to free.
 *
 * Frees a character set.
 */
void
charset_free (Charset *charset)
{
  g_tree_unref (charset->tree);
  g_free (charset);
}


/**
 * charset_add:
 * @charset: the character set to populate.
 * @str: string with chatacters to add to the charset.
 *
 * Adds each unicode code point from @str into the @charset.  The @charset will then
 * discard duplicates.
 */
void
charset_add (Charset *charset, const char *str)
{
  const char *p;

  for (p = str; p[0]; p = g_utf8_next_char (p))
    {
      gunichar c;
      gpointer key;

      c = g_utf8_get_char (p);

      key = GUINT_TO_POINTER(c);
      g_tree_replace (charset->tree, key, NULL);
    }
}

typedef struct {
  gunichar needle;
  gsize index;
} IndexTraverse;

static gboolean
index_traverse_cb (gpointer key, gpointer value, gpointer data)
{
  gunichar ch = GPOINTER_TO_UINT (key);
  IndexTraverse *traverse = data;

  if (ch == traverse->needle)
    {
      return TRUE;
    }
  else
    {
      traverse->index += 1;
      return FALSE;
    }
}

/**
 * charset_index:
 * @charset: the character set to search in.
 * @c: character to search for.
 *
 * Returns the index of the character @c in the @charset, or -1 if it does not exist.
 */
gint
charset_index (const Charset *charset, gunichar c)
{
  IndexTraverse traverse = {
    .needle = c,
    .index = 0,
  };

  g_tree_foreach (charset->tree, index_traverse_cb, &traverse);

  if (traverse.index < charset_num_characters (charset))
    {
      return (gint) traverse.index;
    }
  else
    {
      return -1;
    }
}

/**
 * charset_num_characters:
 * @charset: the character set to query.
 *
 * Returns: the number of characters stored in the @charset.  This is a constant-time operation.
 */
gsize
charset_num_characters (const Charset *charset)
{
  return g_tree_nnodes (charset->tree);
}

typedef struct {
  GString *string;
} SerializeTraverse;

static gboolean
serialize_traverse_cb (gpointer key, gpointer value, gpointer data)
{
  gunichar ch = GPOINTER_TO_UINT (key);
  SerializeTraverse *traverse = data;

  g_string_append_unichar (traverse->string, ch);
  return FALSE;
}

/**
 * charset_serialize:
 * @charset: the character set to serialize.
 *
 * Concatenates all the unique characters stored in a @charset, in the order in which they
 * would be returned by charset_index().
 *
 * Returns: a string with all the characters from the character set.
 */
char *
charset_serialize (const Charset *charset)
{
  SerializeTraverse traverse = {
    .string = g_string_new (NULL),
  };

  g_tree_foreach (charset->tree, serialize_traverse_cb, &traverse);
  return g_string_free (traverse.string, FALSE);
}

/**
 * charset_deserialize:
 * @str: String serialization of a character set, as returned by charset_serialize().
 *
 * Creates a new character set by deserializing from a string.
 *
 * Returns: a new character set.
 */
Charset *
charset_deserialize (const char *str)
{
  Charset *charset = charset_new ();
  charset_add (charset, str);
  return charset;
}

#ifdef TESTING
static void
charset_handles_ascii (void)
{
  g_autoptr (Charset) charset = charset_new ();
  charset_add (charset, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  g_assert_cmpint (charset_num_characters (charset), ==, 26);
  g_assert_cmpint (charset_index (charset, 'A'), ==, 0);
  g_assert_cmpint (charset_index (charset, 'M'), ==, 12);
  g_assert_cmpint (charset_index (charset, 'Z'), ==, 25);
  g_assert_cmpint (charset_index (charset, '7'), ==, -1);

  g_autofree char *serialized = charset_serialize (charset);
  g_assert_cmpstr (serialized, ==, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
}

static void
charset_handles_non_ascii (void)
{
  g_autoptr (Charset) charset = charset_new ();
  charset_add (charset, "ÁRBOL");
  charset_add (charset, "BLÅHAJ");
  charset_add (charset, "BORLA");
  charset_add (charset, "TRALALA");

  g_assert_cmpint (charset_num_characters (charset), ==, 10);
  g_autofree char *serialized = charset_serialize (charset);

  /* Characters are sorted by Unicode code point, not logically.  I guess that doesn't matter? */
  g_assert_cmpstr (serialized, ==, "ABHJLORTÁÅ");

  g_assert_cmpint (charset_index (charset, 'X'), ==, -1);
}

static void
charset_serialization_roundtrip (void)
{
  g_autoptr (Charset) charset = charset_new ();
  charset_add (charset, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ");

  g_autofree char *ser = charset_serialize (charset);
  g_autoptr (Charset) deserialized = charset_deserialize (ser);

  g_autofree char *roundtrip = charset_serialize (deserialized);
  g_assert_cmpstr (ser, ==, roundtrip);
  g_assert_cmpstr (roundtrip, ==, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ");
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/charset/handles_ascii", charset_handles_ascii);
  g_test_add_func ("/charset/handles_non_ascii", charset_handles_non_ascii);
  g_test_add_func ("/charset/serialization_roundtrip", charset_serialization_roundtrip);

  return g_test_run ();
}
#endif
