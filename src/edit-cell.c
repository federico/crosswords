/* edit-cell.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "edit-cell.h"


enum {
  CELL_CHANGED,
  N_SIGNALS
};
static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditCell
{
  GtkWidget parent_instance;
  IPuzCrossword *xword;

  GtkWidget *cell_type_combo;
};

static void edit_cell_init       (EditCell      *self);
static void edit_cell_class_init (EditCellClass *klass);
static void edit_cell_dispose    (GObject       *object);

static void cell_type_combo_changed (GtkComboBox *combo,
                                     EditCell    *edit);

G_DEFINE_TYPE (EditCell, edit_cell, GTK_TYPE_WIDGET);

static void
edit_cell_init (EditCell *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (self->cell_type_combo, "changed", G_CALLBACK (cell_type_combo_changed), self);
}

static void
edit_cell_class_init (EditCellClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_cell_dispose;

  obj_signals [CELL_CHANGED] =
  g_signal_new ("cell-changed",
                EDIT_TYPE_CELL,
                G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                0,
                NULL, NULL,
                NULL,
                G_TYPE_NONE, 0);


  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-cell.ui");
  gtk_widget_class_bind_template_child (widget_class, EditCell, cell_type_combo);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_cell_dispose (GObject *object)
{
  EditCell *edit;
  GtkWidget *child;

  edit = EDIT_CELL (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&edit->xword, g_object_unref);

  G_OBJECT_CLASS (edit_cell_parent_class)->dispose (object);
}


GtkWidget *
edit_cell_new (void)
{
  return (GtkWidget *) g_object_new (EDIT_TYPE_CELL, NULL);
}


static void
cell_type_combo_changed (GtkComboBox *combo,
                         EditCell    *edit)
{
  IPuzCellCellType cell_type;
  IPuzCell *puz_cell;
  IPuzCellCoord coord = { .row = 0, .column = 0 };

  cell_type = (IPuzCellCellType) gtk_combo_box_get_active (combo);
  puz_cell = ipuz_crossword_get_cell (edit->xword, coord);
  ipuz_cell_set_cell_type (puz_cell, cell_type);

  g_signal_emit (G_OBJECT (edit), obj_signals[CELL_CHANGED], 0);
}

static void
cell_type_combo_load_state (EditCell *edit,
                            IPuzCell *puz_cell)
{
  IPuzCellCellType type;

  type = ipuz_cell_get_cell_type (puz_cell);

  gtk_combo_box_set_active (GTK_COMBO_BOX (edit->cell_type_combo), (gint) type);
}

void
edit_cell_load_state (EditCell      *edit,
                      IPuzCrossword *xword)
{
  IPuzCell *puz_cell;
  IPuzCellCoord coord = { .row = 0, .column = 0 };

  g_return_if_fail (EDIT_IS_CELL (edit));
  g_return_if_fail (xword != NULL);

  g_object_ref (xword);
  if (edit->xword)
    g_object_unref (edit->xword);

  edit->xword = xword;
  puz_cell = ipuz_crossword_get_cell (edit->xword, coord);

  cell_type_combo_load_state (edit, puz_cell);
}


