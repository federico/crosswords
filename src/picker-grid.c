/* picker-grid.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "picker-grid.h"
#include "puzzle-button.h"
#include "puzzle-set.h"


struct _PickerGrid
{
  PuzzlePicker parent_instance;

  GKeyFile *key_file;
  guint width;
  guint height;
  gboolean locked;
  gboolean show_progress;

  /* Bound widgets */
  GtkWidget *toast_overlay;
  GtkWidget *header_label;
  GtkWidget *button_grid;
};


static void picker_grid_init         (PickerGrid      *self);
static void picker_grid_class_init   (PickerGridClass *klass);
static void picker_grid_dispose      (GObject         *object);
static void picker_grid_load         (PuzzlePicker    *self);
static void picker_grid_update_state (PuzzlePicker    *self);


G_DEFINE_TYPE (PickerGrid, picker_grid, PUZZLE_TYPE_PICKER);


static void
picker_grid_init (PickerGrid *self)
{
  GtkLayoutManager *box_layout;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->locked = FALSE;
  self->show_progress = TRUE;
  self->width = 0;
  self->height = 0;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 16);
}

static void
picker_grid_class_init (PickerGridClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  PuzzlePickerClass *picker_class = PUZZLE_PICKER_CLASS (klass);

  object_class->dispose = picker_grid_dispose;
  picker_class->load = picker_grid_load;
  picker_class->update_state = picker_grid_update_state;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/picker-grid.ui");
  gtk_widget_class_bind_template_child (widget_class, PickerGrid, toast_overlay);
  gtk_widget_class_bind_template_child (widget_class, PickerGrid, header_label);
  gtk_widget_class_bind_template_child (widget_class, PickerGrid, button_grid);

  /* Ironically, we're a box not a grid */
  gtk_widget_class_set_layout_manager_type (widget_class,
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
picker_grid_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (picker_grid_parent_class)->dispose (object);
}

/* Load from the keyfile */

static void
picker_grid_load_header (PickerGrid *self)
{
  g_autoptr (GKeyFile) key_file = NULL;
  g_autofree gchar *header = NULL;
  g_autofree gchar *header_face = NULL;
  PangoFontDescription *font_desc;
  PangoAttribute *attr;
  PangoAttrList *attrs;

  g_object_get (self, "key-file", &key_file, NULL);

  header = g_key_file_get_string (key_file, "Picker Grid", "Header", NULL);
  header_face = g_key_file_get_string (key_file, "Picker Grid", "HeaderFace", NULL);

  gtk_label_set_text (GTK_LABEL (self->header_label), header);

  /* Set the label style */
  /* FIXME(CSS): set some of this from CSS */
  attrs = pango_attr_list_new ();
  font_desc = pango_font_description_new ();
  pango_font_description_set_size (font_desc, 48 * PANGO_SCALE);
  pango_font_description_set_weight (font_desc, PANGO_WEIGHT_HEAVY);
  if (header_face)
    pango_font_description_set_family (font_desc, header_face);
  attr = pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);
  pango_attr_list_insert (attrs, attr);
  gtk_label_set_attributes (GTK_LABEL (self->header_label), attrs);
  pango_attr_list_unref (attrs);
}


static void
button_clicked_cb (PuzzleButton     *button,
                   guint             puzzle_id,
                   PuzzleButtonMode  mode,
                   PickerGrid       *picker_grid)
{
  IPuzPuzzle *puzzle;
  puzzle = puzzle_picker_get_puzzle (PUZZLE_PICKER (picker_grid),
                                     puzzle_id -1);
  g_assert (puzzle != NULL);

  if (mode == PUZZLE_BUTTON_MODE_SOLVED)
    {
      puzzle_picker_clear_puzzle (PUZZLE_PICKER (picker_grid),
                                  puzzle,
                                  _("Clear this puzzle?"),
                                  _("You have completed this puzzle. Do you want replay it? You will have to solve it again to make progress.\n\nYou can also view the completed puzzle."),
                                  TRUE, TRUE);
    }
  else
    {
      puzzle_picker_puzzle_selected (PUZZLE_PICKER (picker_grid), puzzle);
    }
}


/* We don't really work at more than an 8x8 puzzle size */
#define MAX_PICKER_GRID_SIZE 8

static void
picker_grid_load_buttons (PickerGrid *self)
{
  g_autoptr (GKeyFile) key_file = NULL;
  guint row, column;
  g_autoptr (GtkSizeGroup) size_group = NULL;

  g_object_get (self, "key-file", &key_file, NULL);
  /* constrain ourselves to 8x8. Any larger won't make sense visually */
  self->width = g_key_file_get_uint64 (key_file, "Picker Grid", "Width", NULL);
  self->height = g_key_file_get_uint64 (key_file, "Picker Grid", "Height", NULL);

  self->width = MIN (MAX_PICKER_GRID_SIZE, self->width);
  self->height = MIN (MAX_PICKER_GRID_SIZE, self->height);
  size_group = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);
  
  for (row = 0; row < self->height; row++)
    for (column = 0; column < self->width; column++)
      {
        guint puzzle_id;
        g_autofree gchar *section = NULL;
        g_autofree gchar *thumbnail = NULL;
        g_autoptr (GdkPixbuf) thumb_pixbuf = NULL;
        IPuzPuzzle *puzzle;
        GtkWidget *button;

        puzzle_id = (row * self->width + column + 1);
        section = g_strdup_printf ("Puzzle%u", puzzle_id);
        
        puzzle = puzzle_picker_get_puzzle (PUZZLE_PICKER (self), puzzle_id - 1);
        button = puzzle_button_new (puzzle, FALSE);

        g_object_set (button,
                      "puzzle-id", puzzle_id,
                      "show-progress", self->show_progress,
                      "progress-fraction", 0.0,
                      "progress-size-group", size_group,
                      NULL);
        gtk_widget_set_size_request (button, 200, 200);

        thumbnail = g_key_file_get_string (key_file, section, "Thumbnail", NULL);
        if (thumbnail)
          {
            g_autofree gchar *thumbnail_path = NULL;

            thumbnail_path = g_build_filename (PUZZLE_SET_PREFIX,
                                               puzzle_picker_get_id (PUZZLE_PICKER (self)),
                                               thumbnail,
                                               NULL);
            thumb_pixbuf = xwd_pixbuf_load_from_gresource (puzzle_picker_get_resource (PUZZLE_PICKER (self)),
                                                           thumbnail_path);
            g_object_set (button,
                          "thumb-pixbuf", thumb_pixbuf,
                          NULL);
          }

        g_signal_connect (G_OBJECT (button), "puzzle-clicked",
                          G_CALLBACK (button_clicked_cb), self);
        gtk_grid_attach (GTK_GRID (self->button_grid),
                         button, column, row,
                         1, 1);
      }
}

static void
picker_grid_load (PuzzlePicker *self)
{
  g_autoptr (GKeyFile) key_file = NULL;

  PUZZLE_PICKER_CLASS (picker_grid_parent_class)->load (self);

  g_object_get (self, "key-file", &key_file, NULL);

  PICKER_GRID (self)->locked = g_key_file_get_boolean (key_file, "Picker Grid", "Locked", NULL);
  PICKER_GRID (self)->show_progress = g_key_file_get_boolean (key_file, "Picker Grid", "ShowProgress", NULL);

  picker_grid_load_header (PICKER_GRID (self));
  picker_grid_load_buttons (PICKER_GRID (self));
}

/* Public Functions */


static gboolean
calculate_button_locked_status (PickerGrid *grid,
                                guint       column,
                                guint       row,
                                gfloat      percent)
{
  IPuzPuzzle *left_puzzle = NULL;
  IPuzPuzzle *above_puzzle = NULL;

  if (percent > 0.0)
    return FALSE;

  /* Magic values */
  if (column > 0)
    left_puzzle = puzzle_picker_get_puzzle (PUZZLE_PICKER (grid), row * grid->width + (column-1));

  if (row > 0)
    above_puzzle = puzzle_picker_get_puzzle (PUZZLE_PICKER (grid), (row - 1) *grid->width + column);

  /* The top left button is always unlocked */
  if (row == 0 && column == 0)
    return FALSE;
  else if (left_puzzle && ipuz_crossword_game_won (IPUZ_CROSSWORD (left_puzzle)))
    return FALSE;
  else if (above_puzzle && ipuz_crossword_game_won (IPUZ_CROSSWORD (above_puzzle)))
    return FALSE;

  return TRUE;
}

static void
picker_grid_update_state (PuzzlePicker *self)
{
  PickerGrid *grid;
  guint row, column;
  gboolean all_won = TRUE;

  PUZZLE_PICKER_CLASS (picker_grid_parent_class)->update_state (self);

  grid = PICKER_GRID (self);
  
  for (row = 0; row < grid->height; row++)
    for (column = 0; column < grid->width; column++)
      {
        GtkWidget *button;
        IPuzPuzzle *puzzle;
        IPuzGuesses *guesses;
        PuzzleButtonMode mode;
        gfloat percent;

        button = gtk_grid_get_child_at (GTK_GRID (grid->button_grid), column, row);
        puzzle = puzzle_picker_get_puzzle (self, row * grid->width + column);
        guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
        percent = ipuz_guesses_get_percent (guesses);

        mode = PUZZLE_BUTTON_MODE_NORMAL;
        if (calculate_button_locked_status (grid, column, row, percent))
          mode = PUZZLE_BUTTON_MODE_LOCKED;
        else if (ipuz_crossword_game_won (IPUZ_CROSSWORD (puzzle)))
          mode = PUZZLE_BUTTON_MODE_SOLVED;


        all_won = all_won && (mode == PUZZLE_BUTTON_MODE_SOLVED);
        g_object_set (G_OBJECT (button),
                      "mode", mode,
                      "progress-fraction", percent,
                      "show-progress", (mode == PUZZLE_BUTTON_MODE_NORMAL),
                      NULL);
      }

  if (all_won)
    {
      adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (grid->toast_overlay),
                                   adw_toast_new (_("Congratulations — you have completed this puzzle set! Thank you for being\nan early tester. If you have feedback, please send it to: <a href=\"mailto:jrb@gnome.org\"><i>jrb@gnome.org</i></a>")));
    }
}
