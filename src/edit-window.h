/* edit-window.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <adwaita.h>
#include <libipuz/libipuz.h>

G_BEGIN_DECLS

#define EDIT_TYPE_WINDOW (edit_window_get_type())
G_DECLARE_FINAL_TYPE (EditWindow, edit_window, EDIT, WINDOW, AdwApplicationWindow);


gboolean edit_window_get_unsaved_changes (EditWindow   *edit_window,
                                          gchar       **title,
                                          gchar       **path);
void     edit_window_save                (EditWindow   *edit_window,
                                          const gchar *uri);
void     edit_window_load_uri            (EditWindow   *edit_window,
                                          const gchar  *uri);

G_END_DECLS
