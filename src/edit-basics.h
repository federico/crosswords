/* edit-basics.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "puzzle-stack.h"

G_BEGIN_DECLS


#define EDIT_TYPE_BASICS (edit_basics_get_type())
G_DECLARE_FINAL_TYPE (EditBasics, edit_basics, EDIT, BASICS, GtkWidget);


void edit_basics_lock_and_write   (EditBasics  *basics);
void edit_basics_set_puzzle_stack (EditBasics  *basics,
                                   PuzzleStack *puzzle_stack);



G_END_DECLS
