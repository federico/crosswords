/* word-solver.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "word-solver.h"
#include "word-list.h"


enum {
  FINISHED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _WordSolver
{
  GObject parent_object;
  GTask *task;
  GCancellable *cancellable;
  WordSolverState state;

  IPuzCrossword *xword;
  CellArray *cell_array;
  WordArray *word_array;
  IPuzGuesses *overlay;

  gint max_solutions;
  gint min_priority;
  gboolean clear_on_finish;

  /* These are the only structures modifiable by tasks */
  gint count; /* Only use with g_atomic_int_* */
  GMutex solutions_mutex; /* For accessing solutions */
  GArray *solutions;
};


static void         word_solver_init       (WordSolver           *self);
static void         word_solver_class_init (WordSolverClass      *klass);
static void         word_solver_dispose    (GObject            *object);
static void         word_solver_finalize   (GObject            *object);


G_DEFINE_TYPE (WordSolver, word_solver, G_TYPE_OBJECT);


static void
word_solver_init (WordSolver *self)
{
  self->max_solutions = -1;
  self->min_priority = 50;
  self->state = WORD_SOLVER_READY;
  g_mutex_init (&self->solutions_mutex);
}

static void
word_solver_class_init (WordSolverClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_solver_dispose;
  object_class->finalize = word_solver_finalize;

  obj_signals [FINISHED] =
    g_signal_new ("finished",
                  WORD_TYPE_SOLVER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
word_solver_dispose (GObject *object)
{
  WordSolver *self;

  self = WORD_SOLVER (object);

  g_clear_object (&self->task);
  g_clear_object (&self->xword);
  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->cell_array, cell_array_unref);
  g_clear_pointer (&self->word_array, word_array_unref);

  if (self->overlay)
    {
      ipuz_guesses_unref (self->overlay);
      self->overlay = NULL;
    }

  if (self->solutions)
    {
      g_array_free (self->solutions, TRUE);
      self->solutions = NULL;
    }

  G_OBJECT_CLASS (word_solver_parent_class)->dispose (object);
}

static void
word_solver_finalize (GObject *object)
{
  WordSolver *self;

  self = WORD_SOLVER (object);

  g_mutex_clear (&self->solutions_mutex);

  G_OBJECT_CLASS (word_solver_parent_class)->finalize (object);
}

WordSolver *
word_solver_new (void)
{
  return g_object_new (WORD_TYPE_SOLVER, NULL);
}


static gboolean
word_solver_first_open (CellArray     *cell_array,
                        IPuzGuesses   *overlay,
                        IPuzCellCoord *open)
{
  guint index;
  for (index = 0; index < cell_array_len (cell_array); index++)
    {
      IPuzCellCoord coord = cell_array_index (cell_array, index);
      IPuzCellCellType type = ipuz_guesses_get_cell_type (overlay, coord);

      if (type == IPUZ_CELL_NORMAL)
        {
          if (ipuz_guesses_get_guess (overlay, coord) == NULL)
            {
              *open = coord;
              return TRUE;
            }
        }
    }
  return FALSE;
}

static IPuzGuesses *
word_solver_initial_overlay (IPuzCrossword *xword,
                             gint          *empty)
{
  IPuzGuesses *overlay;
  IPuzBoard *board;
  guint row, column;

  board = ipuz_crossword_get_board (xword);
  overlay = ipuz_guesses_new_from_board (board, FALSE);

  for (row = 0; row < ipuz_guesses_get_height (overlay); row++)
    {
      for (column = 0; column < ipuz_guesses_get_width (overlay); column++)
        {
          IPuzCellCoord coord = { .row = row, .column = column };
          IPuzCell *board_cell = ipuz_board_get_cell (board, coord);
          const gchar *solution;

          if (IPUZ_CELL_IS_GUESSABLE (board_cell))
            {
              solution = ipuz_cell_get_solution (board_cell);
              if (solution)
                ipuz_guesses_set_guess (overlay,
                                        coord,
                                        solution);
              else
                (*empty) ++;
            }
        }
    }

  return overlay;
}

gchar *
word_solver_get_filter (WordSolver   *solver,
                        const GArray *cells,
                        gint         *empty_count)
{
  GString *str;

  str = g_string_new (NULL);

  for (guint i = 0; i < cells->len; i++)
    {
      IPuzCellCoord coord = g_array_index (cells, IPuzCellCoord, i);
      const gchar *guess;

      guess = ipuz_guesses_get_guess (solver->overlay, coord);
      if (guess == NULL || guess[0] == '\0')
        {
          g_string_append (str, "?");
          if (empty_count)
            (*empty_count)++;
        }
      else
        {
          g_string_append (str, guess);
        }
    }
  return g_string_free (str, FALSE);
}

void
word_solver_set_word (WordSolver   *solver,
                      const GArray *cells,
                      const gchar  *guess)
{
  const gchar *ptr;
  guint i = 0;

  for (ptr = guess; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      IPuzCellCoord coord = g_array_index (cells, IPuzCellCoord, i);

      if (ptr[0] == '?')
        {
          ipuz_guesses_set_guess (solver->overlay, coord, NULL);
        }
      else
        {
          gchar buf[6];
          g_utf8_strncpy (buf, ptr, 1);
          ipuz_guesses_set_guess (solver->overlay, coord, buf);
        }
      i++;
    }
}

static gboolean
word_solver_cross_check_word (WordSolver        *solver,
                              const GArray      *cells,
                              const gchar       *filter,
                              IPuzClueDirection  opposite_dir)
{
  g_autoptr (WordList) word_list = NULL;
  const gchar *ptr;
  guint i = 0;

  word_list = word_list_new ();
  for (ptr = filter; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      /* is this a character that's been filled in by our guess? */
      if (ptr[0] == '?')
        {
          IPuzClue *clue;
          IPuzCellCoord coord;
          g_autofree gchar *filter = NULL;

          coord = g_array_index (cells, IPuzCellCoord, i);
          clue = ipuz_crossword_find_clue_by_coord (solver->xword,
                                                    opposite_dir,
                                                    coord);
          if (clue != NULL)
            {
              guint n_items;
              gint priority;

              filter = word_solver_get_filter (solver, ipuz_clue_get_cells (clue), NULL);
              word_list_set_filter (word_list, filter);
              n_items = word_list_get_n_items (word_list);

              /* If there are no words, this is an invalid path */
              if (n_items == 0)
                return FALSE;

              /* We grab priority of the first item. Since it's sorted
               * by priority, we can confirm there are answers with a
               * priority higher than our minimum priority filter. */
              priority = word_list_get_priority (word_list, 0);
              if (priority < solver->min_priority)
                return FALSE;
            }
        }
      i++;
    }

  return TRUE;
}

void
word_solver_run_helper (WordSolver *solver,
                        gint        empty)
{
  g_autofree gchar *filter = NULL;
  g_autoptr (WordList) word_list = NULL;
  IPuzClue *clue;
  const GArray *cells;
  IPuzCellCoord coord;
  gint empty_count = 0;
  IPuzClueDirection opposite_dir;

  if (g_cancellable_is_cancelled (solver->cancellable))
    return;

  /* If there are no cells left to look at, we found a solution! Add
   * the current overlay to the solutions queue. */
  if (!word_solver_first_open (solver->cell_array,
                               solver->overlay,
                               &coord))
    {
      gint len; /* int, not guint */
      g_mutex_lock (&solver->solutions_mutex);

      if (!g_cancellable_is_cancelled (solver->cancellable))
        {
          IPuzGuesses *new_overlay;
          new_overlay = ipuz_guesses_copy (solver->overlay);
          g_array_append_val (solver->solutions, new_overlay);
        }
      len = (gint) solver->solutions->len;
      if (solver->max_solutions >= 0 &&
          solver->max_solutions <= len)
        g_cancellable_cancel (solver->cancellable);
      g_mutex_unlock (&solver->solutions_mutex);

      return;
    }

  clue = ipuz_crossword_find_clue_by_coord (solver->xword,
                                            IPUZ_CLUE_DIRECTION_ACROSS,
                                            coord);
  if (clue == NULL)
    clue = ipuz_crossword_find_clue_by_coord (solver->xword,
                                              IPUZ_CLUE_DIRECTION_DOWN,
                                              coord);
  g_assert (clue);

  cells = ipuz_clue_get_cells (clue);
  if (clue->direction == IPUZ_CLUE_DIRECTION_ACROSS)
    opposite_dir = IPUZ_CLUE_DIRECTION_DOWN;
  else
    opposite_dir = IPUZ_CLUE_DIRECTION_ACROSS;
  filter = word_solver_get_filter (solver, cells, &empty_count);
  /* FIXME(cleanup): Keep this word_list around! */
  word_list = word_list_new ();
  word_list_set_filter (word_list, filter);
  if (word_list_get_n_items (word_list) == 0)
    {
      g_atomic_int_inc (&(solver->count));
      return ;
    }

  for (guint i = 0; i < word_list_get_n_items (word_list); i++)
    {
      WordIndex word_index;
      const gchar *guess;
      gint priority;

      if (g_cancellable_is_cancelled (solver->cancellable))
        return;


      guess = word_list_get_word (word_list, i);
      priority = word_list_get_priority (word_list, i);

      if (priority < solver->min_priority)
        continue;

      /* This should always succeed. We're looking up a word we got. */
      word_list_get_word_index (word_list, i, &word_index);
      if (word_array_find (solver->word_array, word_index, NULL))
        continue;

      /* Try this word */
      word_solver_set_word (solver, cells, guess);
      if (!word_solver_cross_check_word (solver, cells, filter, opposite_dir))
        {
          g_atomic_int_inc (&(solver->count));
          continue;
        }

      /* We will try this word and iterate from it. We add the word to
       * the word_array for the next iteration so we don't include
       * multiple copies of it in our solution. */
      word_array_add (solver->word_array, word_index);
      word_solver_run_helper (solver, empty-empty_count);
      word_array_remove (solver->word_array, word_index);
    }

  /* Undo what we did */
  word_solver_set_word (solver, cells, filter);
}

static void
word_solver_ready (WordSolver   *solver,
                   GAsyncResult *res,
                   gpointer      user_data)
{
  g_clear_object (&solver->task);
  if (solver->clear_on_finish)
    {
      g_array_set_size (solver->solutions, 0);
      solver->clear_on_finish = FALSE;
      solver->state = WORD_SOLVER_READY;
    }
  else
    {
      solver->state = WORD_SOLVER_COMPLETE;
    }
  g_signal_emit (solver, obj_signals [FINISHED], 0);
}

void
word_solver_execute (GTask        *task,
                     WordSolver   *solver,
                     gpointer      task_data,
                     GCancellable *cancellable)
{
  gint empty;

  empty = GPOINTER_TO_INT (task_data);

  word_solver_run_helper (solver, empty);
}

static void
solutions_clear (IPuzGuesses **solution)
{
  g_clear_pointer (solution, ipuz_guesses_unref);
}

void
word_solver_run (WordSolver    *solver,
                 IPuzCrossword *xword,
                 CellArray     *cell_array,
                 WordArray     *word_array,
                 gint           max_solutions,
                 gint           min_priority)
{
  gint empty = 0;

  g_return_if_fail (WORD_IS_SOLVER (solver));
  g_return_if_fail (solver->state != WORD_SOLVER_RUNNING);

  if (solver->state == WORD_SOLVER_COMPLETE)
    word_solver_reset (solver);

  solver->state = WORD_SOLVER_RUNNING;
  solver->count = 0;
  g_clear_pointer (&solver->solutions, g_array_unref);
  solver->solutions = g_array_new (FALSE, FALSE, sizeof (IPuzGuesses *));
  g_array_set_clear_func (solver->solutions, (GDestroyNotify) solutions_clear);

  solver->xword = g_object_ref (xword);

  g_clear_pointer (&solver->cell_array, cell_array_unref);
  solver->cell_array = cell_array_copy (cell_array);

  g_clear_pointer (&solver->word_array, word_array_unref);
  solver->word_array = word_array_copy (word_array);

  solver->max_solutions = max_solutions;
  solver->min_priority = min_priority;
  solver->overlay = word_solver_initial_overlay (xword, &empty);

  if (empty == 0)
    return;

  solver->cancellable = g_cancellable_new ();
  solver->task = g_task_new (solver, solver->cancellable,
                             (GAsyncReadyCallback) word_solver_ready,
                             NULL);
  g_task_set_task_data (solver->task, GINT_TO_POINTER (empty), NULL);
  g_task_set_return_on_cancel (solver->task, TRUE);

  g_task_run_in_thread (solver->task, (GTaskThreadFunc) word_solver_execute);
}

void
word_solver_cancel (WordSolver *solver)
{
  g_return_if_fail (WORD_IS_SOLVER (solver));
  g_return_if_fail (solver->state == WORD_SOLVER_RUNNING);

  g_cancellable_cancel (solver->cancellable);
  solver->state = WORD_SOLVER_COMPLETE;
}

void
word_solver_reset (WordSolver *solver)
{
  g_return_if_fail (WORD_IS_SOLVER (solver));

  /* Make sure we cancel it */
  if (word_solver_get_state (solver) == WORD_SOLVER_RUNNING)
    {
      solver->clear_on_finish = TRUE;
      word_solver_cancel (solver);
    }
  else
    {
      g_clear_pointer (&solver->solutions, g_array_unref);
      solver->state = WORD_SOLVER_READY;
    }
}

WordSolverState
word_solver_get_state (WordSolver *solver)
{
  g_return_val_if_fail (WORD_IS_SOLVER (solver), WORD_SOLVER_READY);

  return solver->state;
}

gint
word_solver_get_count (WordSolver *solver)
{
  g_return_val_if_fail (WORD_IS_SOLVER (solver), 0);

  return g_atomic_int_get (&(solver->count));
}

guint
word_solver_get_n_solutions (WordSolver *solver)
{
  guint len;
  g_return_val_if_fail (WORD_IS_SOLVER (solver), 0);

  g_mutex_lock (&solver->solutions_mutex);
  len = solver->solutions->len;
  g_mutex_unlock (&solver->solutions_mutex);

  return len;
}

IPuzGuesses *
word_solver_get_solution (WordSolver *solver,
                          guint       index)
{
  IPuzGuesses *guesses = NULL;

  g_return_val_if_fail (WORD_IS_SOLVER (solver), NULL);

  g_mutex_lock (&solver->solutions_mutex);
  if (index < solver->solutions->len)
    guesses = g_array_index (solver->solutions, IPuzGuesses *, index);
  g_mutex_unlock (&solver->solutions_mutex);

  return guesses;
}
