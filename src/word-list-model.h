/* word-list.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS



#define WORD_TYPE_LIST_MODEL (word_list_model_get_type())
G_DECLARE_FINAL_TYPE (WordListModel, word_list_model, WORD, LIST_MODEL, GObject);


WordListModel *word_list_model_new           (void);
void           word_list_model_set_filter    (WordListModel *list_model,
                                              const char    *filter);
void           word_list_model_set_threshold (WordListModel *list_model,
                                              gint           threshold);



struct _WordListModelRow
{
  GObject parent_object;
  const gchar *word;
  gint priority;
};

#define WORD_TYPE_LIST_MODEL_ROW (word_list_model_row_get_type())
G_DECLARE_FINAL_TYPE (WordListModelRow, word_list_model_row, WORD, LIST_MODEL_ROW, GObject);


WordListModelRow     *word_list_model_row_new          (const gchar      *word,
                                                        gint              priority);
const gchar          *word_list_model_row_get_word     (WordListModelRow *word_list_row);
gint                  word_list_model_row_get_priority (WordListModelRow *word_list_row);




G_END_DECLS
