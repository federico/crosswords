/* picker-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "picker-list.h"
#include "puzzle-button.h"
#include "puzzle-downloader.h"
#include "puzzle-set.h"
#include "puzzle-picker.h"

#define LIST_ROW_USER_DATA "puzzle"

struct _PickerList
{
  PuzzlePicker parent_instance;

  gboolean show_progress;
  GCancellable *downloader_cancellable;
  PuzzleDownloader *downloader;
  gulong network_monitor_signal;

  /* Bound widgets */
  GtkWidget *toast_overlay;
  GtkWidget *header_label;
  GtkWidget *picker_button;
  GtkWidget *url_link_button;
  GtkWidget *list_box;
  GtkWidget *subheader_label;
  GtkWidget *puzzle_list;
};


static void picker_list_init                (PickerList       *self);
static void picker_list_class_init          (PickerListClass  *klass);
static void picker_list_dispose             (GObject          *object);
static void picker_list_load                (PuzzlePicker     *picker);
static void picker_list_update_list         (PickerList       *picker_list);
static void picker_list_update_state        (PuzzlePicker     *picker);
static void picker_list_downloader_finished (PickerList       *picker_list,
                                             const gchar      *uri,
                                             gboolean          delete_when_done);
static void picker_button_clicked_cb        (PickerList       *picker_list);


G_DEFINE_TYPE (PickerList, picker_list, PUZZLE_TYPE_PICKER);


static void
picker_list_init (PickerList *self)
{
  GtkLayoutManager *box_layout;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->show_progress = TRUE;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 16);
}

static void
picker_list_class_init (PickerListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  PuzzlePickerClass *picker_class = PUZZLE_PICKER_CLASS (klass);

  object_class->dispose = picker_list_dispose;
  picker_class->load = picker_list_load;
  picker_class->update_state = picker_list_update_state;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/picker-list.ui");
  gtk_widget_class_bind_template_child (widget_class, PickerList, toast_overlay);
  gtk_widget_class_bind_template_child (widget_class, PickerList, header_label);
  gtk_widget_class_bind_template_child (widget_class, PickerList, list_box);
  gtk_widget_class_bind_template_child (widget_class, PickerList, subheader_label);
  gtk_widget_class_bind_template_child (widget_class, PickerList, picker_button);
  gtk_widget_class_bind_template_child (widget_class, PickerList, url_link_button);
  gtk_widget_class_bind_template_child (widget_class, PickerList, puzzle_list);

  gtk_widget_class_bind_template_callback (widget_class, picker_button_clicked_cb);

  gtk_widget_class_set_layout_manager_type (widget_class,
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
picker_list_dispose (GObject *object)
{
  PickerList *picker_list;
  GtkWidget *child;

  picker_list = PICKER_LIST (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_signal_handler (&picker_list->network_monitor_signal,
                          g_network_monitor_get_default ());

  g_clear_object (&picker_list->downloader);

  G_OBJECT_CLASS (picker_list_parent_class)->dispose (object);
}

static void
set_label_face (GtkWidget   *label,
                const gchar *face)
{
  PangoFontDescription *font_desc;
  PangoAttribute *attr;
  PangoAttrList *attrs;

  font_desc = pango_font_description_new ();
  attrs = pango_attr_list_new ();
  pango_font_description_set_family (font_desc, face);
  attr = pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);
  pango_attr_list_insert (attrs, attr);
  gtk_label_set_attributes (GTK_LABEL (label), attrs);
  pango_attr_list_unref (attrs);
}


static void
picker_list_load_headers (PickerList *self)
{
  g_autofree gchar *header = NULL;
  g_autofree gchar *header_face = NULL;
  g_autofree gchar *subheader = NULL;
  g_autofree gchar *subheader_face = NULL;
  g_autofree gchar *url = NULL;
  GKeyFile *key_file;

  key_file = puzzle_picker_get_key_file (PUZZLE_PICKER (self));

  header = g_key_file_get_locale_string (key_file, "Picker List", "Header", NULL, NULL);
  header_face = g_key_file_get_string (key_file, "Picker List", "HeaderFace", NULL);
  subheader = g_key_file_get_locale_string (key_file, "Picker List", "SubHeader", NULL, NULL);
  subheader_face = g_key_file_get_string (key_file, "Picker List", "SubHeaderFace", NULL);
  url = g_key_file_get_string (key_file, "Picker List", "URL", NULL);

  gtk_label_set_text (GTK_LABEL (self->header_label), header);
  gtk_label_set_text (GTK_LABEL (self->subheader_label), subheader);

  /* set the URL */
  if (url && g_uri_is_valid (url, G_URI_FLAGS_NONE, NULL))
    {
      gtk_link_button_set_uri (GTK_LINK_BUTTON (self->url_link_button), url);
      gtk_button_set_label (GTK_BUTTON (self->url_link_button), url);
    }
  gtk_widget_set_visible (self->url_link_button, (url != NULL));

  /* Set the label style */
  /* FIXME(CSS): set some of this from CSS */
  if (header_face)
    set_label_face (self->header_label, header_face);
  if (subheader_face)
    set_label_face (self->subheader_label, subheader_face);
}

static void
network_available_cb (GNetworkMonitor *monitor,
                      gboolean         network_available,
                      PickerList      *picker_list)
{
  if (network_available)
    {
      gtk_widget_set_tooltip_text (picker_list->picker_button,
                                   NULL);
      gtk_widget_set_sensitive (picker_list->picker_button,
                                TRUE);
    }
  else
    {
      gtk_widget_set_tooltip_text (picker_list->picker_button,
                                   _("Network access is required to download a puzzle"));
      gtk_widget_set_sensitive (picker_list->picker_button,
                                FALSE);
    }
}

static void
picker_list_load_button (PickerList *self)
{
  GKeyFile *key_file;
  gboolean use_button = FALSE;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *button_label = NULL;
  g_autofree gchar *button_type = NULL;

  key_file = puzzle_picker_get_key_file (PUZZLE_PICKER (self));

  /* Default the button to false, and show it if we set everything up correctly */
  gtk_widget_set_visible (self->picker_button, FALSE);
  use_button = g_key_file_get_boolean (key_file, "Picker List", "UseButton", &error);
  if (error && error->code == G_KEY_FILE_ERROR_INVALID_VALUE)
    {
      g_warning ("Unable to parse UseButton in %s/puzzle.config\n",
                 puzzle_picker_get_id (PUZZLE_PICKER (self)));
      return;
    }

  if (! use_button)
    return;

  button_label = g_key_file_get_locale_string (key_file, "Picker Button", "Label", NULL, &error);
  if (error)
    {
      g_warning ("Mising [Picker Button]::Label key in %s/puzzle.config\n%s",
                 puzzle_picker_get_id (PUZZLE_PICKER (self)),
                 error->message);
      return;
    }
  gtk_button_set_label (GTK_BUTTON (self->picker_button), button_label);

  button_type = g_key_file_get_string (key_file, "Picker Button", "Type", &error);
  if (error)
    {
      g_warning ("Mising [Picker Button]::Type key in %s/puzzle.config\n%s",
                 puzzle_picker_get_id (PUZZLE_PICKER (self)),
                 error->message);
      return;
    }

  g_clear_object (&self->downloader);
  if (! g_strcmp0 (button_type, "file"))
    self->downloader = puzzle_downloader_new_filechooser ();
  else if (! g_strcmp0 (button_type, "downloader"))
    self->downloader = puzzle_downloader_new_from_key_file (key_file);
  else
    {
      g_warning ("Unknown downloader type:%s in %s/puzzle.config",
                 button_type,
                 puzzle_picker_get_id (PUZZLE_PICKER (self)));
      return;
    }

  /* Everything loaded fine; show the button and listen for changes */
  if (self->downloader)
    {
      /* listen to network changes if we require network */
      if (puzzle_downloader_get_requires_network (self->downloader))
        {
          GNetworkMonitor *monitor;

          monitor = g_network_monitor_get_default ();
          self->network_monitor_signal =
            g_signal_connect (monitor,
                              "network-changed",
                              G_CALLBACK (network_available_cb),
                              self);
          network_available_cb (monitor,
                                g_network_monitor_get_network_available (monitor),
                                self);
        }

      g_signal_connect_swapped (self->downloader, "finished",
                                G_CALLBACK (picker_list_downloader_finished),
                                self);
      gtk_widget_set_visible (self->picker_button, TRUE);
    }
}

static void
clear_button_clicked_cb (GtkButton    *button,
                         PuzzlePicker *picker)
{
  IPuzPuzzle *puzzle;
  IPuzGuesses *guesses;
  gfloat percent;

  puzzle = g_object_get_data (G_OBJECT (button),
                              LIST_ROW_USER_DATA);
  g_assert (puzzle != NULL);

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));

  /* Only clear the puzzle if it has changes */
  percent = ipuz_guesses_get_percent (guesses);
  if (percent > 0)
    {
      puzzle_picker_clear_puzzle (picker, puzzle,
                                  _("Clear this puzzle?"),
                                  _("If you clear this puzzle, all your progress will be lost."),
                                  TRUE, FALSE);
    }
}

static void
remove_response_cb (PuzzlePicker *picker,
                    gint          response_id,
                    GtkWindow    *dialog)
{
 IPuzPuzzle *puzzle;

  puzzle = g_object_get_data (G_OBJECT (dialog),
                              LIST_ROW_USER_DATA);

  if (response_id == GTK_RESPONSE_APPLY)
    puzzle_picker_remove_puzzle (picker, puzzle);

  gtk_window_destroy (dialog);
}

static void
remove_button_clicked_cb (GtkButton    *button,
                          PuzzlePicker *picker)
{
  GtkWindow *parent_window;
  GtkWidget *dialog;
  IPuzPuzzle *puzzle;
  IPuzGuesses *guesses;
  const gchar *secondary_text;

  parent_window = (GtkWindow *) gtk_widget_get_root (GTK_WIDGET (picker));
  puzzle = g_object_get_data (G_OBJECT (button),
                              LIST_ROW_USER_DATA);
  g_assert (puzzle != NULL);

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
  if (ipuz_guesses_get_percent (guesses) > 0.0)
    secondary_text = _("If you remove this puzzle, you won't be able to play it again and all your progress will be lost.");
  else
    secondary_text = _("If you remove this puzzle, you won't be able to play it again.");

  dialog = gtk_message_dialog_new (parent_window,
                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                   GTK_MESSAGE_WARNING,
                                   GTK_BUTTONS_NONE,
                                   _("Remove puzzle"));
  g_object_set_data (G_OBJECT (dialog), LIST_ROW_USER_DATA, puzzle);
  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
					    "%s", secondary_text);

  gtk_dialog_add_buttons (GTK_DIALOG (dialog),
                          _("_Cancel"),
                          GTK_RESPONSE_CANCEL,
                          _("_Remove"),
                          GTK_RESPONSE_APPLY,
                          NULL);

  g_signal_connect_swapped (dialog, "response",
                            G_CALLBACK (remove_response_cb),
                            picker);

  gtk_widget_show (dialog);
}

static void
row_activated_cb (GtkListBoxRow *list_row,
                  PuzzlePicker  *picker)
{
  IPuzPuzzle *puzzle;

  puzzle = g_object_get_data (G_OBJECT (list_row),
                              LIST_ROW_USER_DATA);
  g_assert (puzzle != NULL);

  puzzle_picker_puzzle_selected (picker, puzzle);
}

static void
row_widget_set_subtitle (AdwActionRow *row,
                         IPuzPuzzle   *puzzle)
{
  g_autofree gchar *subtitle = NULL;
  IPuzGuesses *guesses;
  gfloat percent;
  gboolean won;

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
  percent = ipuz_guesses_get_percent (guesses);
  won = ipuz_crossword_game_won (IPUZ_CROSSWORD (puzzle));

  if (won)
    {
      gtk_widget_add_css_class (GTK_WIDGET (row), "puzzle-won");
      subtitle = g_strdup (_("<b>Puzzle won</b>"));
    }
  else
    {
      gtk_widget_remove_css_class (GTK_WIDGET (row), "puzzle-won");
      subtitle = g_strdup_printf (_("%d%% filled in"), (gint)(percent * 100));
    }

  adw_action_row_set_subtitle (row, subtitle);
}

static GtkWidget *
row_widget_new (PuzzlePicker *picker,
                guint         n)
{
  IPuzPuzzle *puzzle;
  GtkWidget *row;
  GtkWidget *box;
  GtkWidget *button;
  GtkStyleContext *context;
  gboolean readonly;
  const gchar *puzzle_name;

  g_autofree gchar *title = NULL;

  puzzle_name = puzzle_picker_get_puzzle_name (picker, n);
  puzzle = puzzle_picker_get_puzzle (picker, n);
  readonly = puzzle_picker_get_readonly (picker, n);

  g_object_get (puzzle,
                "title", &title,
                NULL);

  row = (GtkWidget *) g_object_new (ADW_TYPE_ACTION_ROW,
                                    "title", title?title:puzzle_name,
                                    "activatable", TRUE,
                                    NULL);

  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  context = gtk_widget_get_style_context (GTK_WIDGET (box));
  gtk_style_context_add_class (context, "linked");

  button = gtk_button_new ();
  g_object_set_data (G_OBJECT (button),
                     LIST_ROW_USER_DATA, (gpointer) puzzle);
  gtk_button_set_icon_name (GTK_BUTTON (button), "view-refresh-symbolic");
  gtk_box_append (GTK_BOX (box), button);
  gtk_widget_set_valign (box, GTK_ALIGN_CENTER);
  g_signal_connect (button, "clicked",
                    G_CALLBACK (clear_button_clicked_cb),
                    picker);

  if (! readonly)
    {
      button = gtk_button_new ();
      g_object_set_data (G_OBJECT (button),
                         LIST_ROW_USER_DATA, (gpointer) puzzle);
      gtk_button_set_icon_name (GTK_BUTTON (button), "edit-delete-symbolic");
      gtk_box_append (GTK_BOX (box), button);
      g_signal_connect (button, "clicked",
                        G_CALLBACK (remove_button_clicked_cb),
                        picker);
    }

  adw_action_row_add_suffix (ADW_ACTION_ROW (row), box);
  g_object_set_data (G_OBJECT (row),
                     LIST_ROW_USER_DATA, puzzle);

  g_signal_connect (row, "activated",
                    G_CALLBACK (row_activated_cb), picker);
  return row;
}


static void
picker_list_load_list (PickerList *self)
{
  guint i;
  guint n_puzzles;
  GtkListBoxRow *row;


  n_puzzles = puzzle_picker_get_n_puzzles (PUZZLE_PICKER (self));
  gtk_widget_set_visible (self->list_box, n_puzzles > 0);

  /* Clear the existing list box */
  while ((row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (self->puzzle_list), 0)) != NULL)
    {
        gtk_list_box_remove (GTK_LIST_BOX (self->puzzle_list),
                             GTK_WIDGET (row));
    }


  for (i = 0; i < n_puzzles; i++)
    {
      GtkWidget *row_widget;

      row_widget = row_widget_new (PUZZLE_PICKER (self), i);
      gtk_list_box_append (GTK_LIST_BOX (self->puzzle_list), row_widget);
    }
}


static void
picker_list_load (PuzzlePicker *picker)
{
  GKeyFile *key_file;
  PickerList *self = PICKER_LIST (picker);

  PUZZLE_PICKER_CLASS (picker_list_parent_class)->load (picker);

  key_file = puzzle_picker_get_key_file (PUZZLE_PICKER (picker));
  self->show_progress = g_key_file_get_boolean (key_file,
                                                "Picker List", "ShowProgress",
                                                NULL);
  picker_list_load_headers (self);
  picker_list_load_button (self);
  picker_list_load_list (self);
  picker_list_update_list (self);

}


static void
picker_list_update_list (PickerList *picker_list)
{
  IPuzPuzzle *puzzle;
  guint i = 0;
  GtkListBoxRow *row;

  row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (picker_list->puzzle_list), i);

  while (row)
    {
      puzzle = g_object_get_data (G_OBJECT (row), LIST_ROW_USER_DATA);
      row_widget_set_subtitle (ADW_ACTION_ROW (row), puzzle);
      row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (picker_list->puzzle_list), i++);
    }
}


static void
picker_list_update_state (PuzzlePicker *picker)
{
  picker_list_update_list (PICKER_LIST (picker));
  PUZZLE_PICKER_CLASS (picker_list_parent_class)->update_state (picker);
}

static void
picker_list_downloader_finished (PickerList  *picker_list,
                                 const gchar *uri,
                                 gboolean     delete_when_done)
{
  g_autoptr(GError) error = NULL;

  g_assert (PICKER_IS_LIST (picker_list));

  /* if URI is NULL, then the operation was canceled */
  if (uri)
    {
      puzzle_picker_load_uri (PUZZLE_PICKER (picker_list),
                              uri, delete_when_done,
                              &error);
      /* FIXME(error): Put this in a dialog */
      if (error)
        {
          g_warning ("error loading file from downloader:%s\n",
                     error->message);
        }
    }

  gtk_widget_set_sensitive (picker_list->picker_button, TRUE);
  g_clear_object (&picker_list->downloader_cancellable);
}

static void
picker_button_clicked_cb (PickerList *picker_list)
{
  GtkWindow *parent_window;
  g_autoptr (GError) error = NULL;

  g_assert (PICKER_IS_LIST (picker_list));
  g_assert (picker_list->downloader != NULL);
  g_assert (picker_list->downloader_cancellable == NULL);

  parent_window = (GtkWindow *) gtk_widget_get_root (GTK_WIDGET (picker_list));
  picker_list->downloader_cancellable = g_cancellable_new ();
  puzzle_downloader_run_async (picker_list->downloader,
                               parent_window,
                               picker_list->downloader_cancellable,
                               &error);
  if (error)
    {
      /* FIXME(error): put up a dialog */
      g_warning ("Could not run downloader: %s\n",
                 error->message);
      return;
    }
  else
    {
      gtk_widget_set_sensitive (picker_list->picker_button, FALSE);
    }

}

/* Public functions */

/* This is used from command line or dbus calls to load uris. It's *
 * different from puzzle_picker->load_uri which is internally driven,
 * and requires a .ipuz file. */
void
picker_list_import_uri (PickerList  *picker_list,
                        const gchar *uri)
{
  GtkWindow *parent_window;
  g_autoptr (GError) error = NULL;

  g_assert (PICKER_IS_LIST (picker_list));
  g_assert (picker_list->downloader != NULL);
  /* FIXME: We can only launch these one at a time. */
  if (picker_list->downloader_cancellable)
    {
      g_warning ("can't import multiple uris simultaneously\n");
      return;
    }

  parent_window = (GtkWindow *) gtk_widget_get_root (GTK_WIDGET (picker_list));
  picker_list->downloader_cancellable = g_cancellable_new ();
  puzzle_downloader_add_import_target (picker_list->downloader, uri);
  puzzle_downloader_run_async (picker_list->downloader,
                               parent_window,
                               picker_list->downloader_cancellable,
                               &error);
  if (error)
    {
      /* FIXME(error): put up a dialog */
      g_warning ("Could not run downloader: %s\n",
                 error->message);
      return;
    }
  else
    {
      gtk_widget_set_sensitive (picker_list->picker_button, FALSE);
    }
}
