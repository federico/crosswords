/* play-clue-row.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-layout.h"

G_BEGIN_DECLS


#define PLAY_TYPE_CLUE_ROW (play_clue_row_get_type())

G_DECLARE_FINAL_TYPE (PlayClueRow, play_clue_row, PLAY, CLUE_ROW, GtkWidget);

GtkWidget *play_clue_row_new            (GtkSizeGroup *size_group);
void       play_clue_row_set_standalone (PlayClueRow  *clue_row,
                                         gboolean      standalone);

void       play_clue_row_update         (PlayClueRow  *clue_row,
                                         XwordState   *state,
                                         IPuzClue     *clue,
                                         IPuzClueId    clue_id,
                                         gboolean      showenumerations,
                                         ZoomLevel     zoom_level);
IPuzClueId play_clue_row_get_clue_id    (PlayClueRow  *clue_row);


G_END_DECLS
