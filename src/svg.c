/* svg.c - Generate SVG documents from crosswords
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "svg.h"

typedef struct
{
  guint document_width;
  guint document_height;
  guint border_size;
  guint cell_size;
} Geometry;

/* appends src to dest and consumes src */
static void
append (GString *dest, GString *src)
{
  g_string_append_len (dest, src->str, src->len);
  g_string_free (src, TRUE);
}

static GString *
xml_preamble (void)
{
  return g_string_new ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
}

static GString *
svg_toplevel (Geometry *g)
{
  GString *s = g_string_new ("");
  g_string_append_printf (s,
                          "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"%u\" height=\"%u\" viewBox=\"0 0 %u %u\">\n",
                          g->document_width, g->document_height,
                          g->document_width, g->document_height);
  return s;
}

static GString *
svg_toplevel_end (void)
{
  return g_string_new ("</svg>\n");
}

static GString *
stylesheet (void)
{
  return g_string_new ("<style>\n"
                       ".border { fill: black; }\n"
                       ".enumeration { fill: #888888; }\n"
                       "</style>\n"
                       );
}

static GString *
definitions (Geometry *g)
{
  GString *s = g_string_new ("");

  g_string_append (s, "<defs>\n");

  g_string_append_printf (s,
                          "<rect id=\"intersection\" class=\"border\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->border_size, g->border_size);
  g_string_append_printf (s,
                          "<rect id=\"border_horizontal\" class=\"border\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->cell_size, g->border_size);
  g_string_append_printf (s,
                          "<rect id=\"border_vertical\" class=\"border\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->border_size, g->cell_size);
  g_string_append_printf (s,
                          "<rect id=\"cell\" class=\"cell\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->cell_size, g->cell_size);

  g_string_append (s, "</defs>\n");

  return s;

}

static GString *
overlay_definitions (Geometry *g)
{
  GString *s = g_string_new ("");
  double half_border = (double) g->border_size / 2.0;
  double double_border = (double) g->border_size * 2.0;

  g_string_append (s, "<defs>\n");

  /* Each overlay has its anchor point at the LayoutItem's top-left corner.  However, an
   * overlay may overshoot its LayoutItem (e.g. a thick barred border).  The overlay
   * should still be placed as if it were a normal LayoutItem, without adjusting for its
   * specific size.
   */

  g_string_append_printf (s,
                          "<rect id=\"barred_horizontal\" class=\"border\" x=\"0\" y=\"%g\" width=\"%u\" height=\"%g\"/>\n",
                          -half_border,
                          g->cell_size,
                          double_border);

  g_string_append_printf (s,
                          "<rect id=\"barred_vertical\" class=\"border\" x=\"%g\" y=\"0\" width=\"%g\" height=\"%u\"/>\n",
                          -half_border,
                          double_border,
                          g->cell_size);

  g_string_append_printf (s,
                          "<rect id=\"enumeration_space_horizontal\" class=\"enumeration\" x=\"0\" y=\"%g\" width=\"%u\" height=\"%g\"/>\n",
                          -half_border,
                          g->cell_size,
                          double_border);

  g_string_append_printf (s,
                          "<rect id=\"enumeration_space_vertical\" class=\"enumeration\" x=\"%g\" y=\"0\" width=\"%g\" height=\"%u\"/>\n",
                          -half_border,
                          double_border,
                          g->cell_size);

  /* This is slightly confusing. enumeration_dash_horizontal is
   * actually a vertical dash, but it goes across the horizontal
   * line */
  g_string_append_printf (s,
                          "<rect id=\"enumeration_dash_horizontal\" class=\"enumeration\" x=\"%g\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          -half_border,
                          g->border_size,
                          g->border_size * 5);

  g_string_append_printf (s,
                          "<rect id=\"enumeration_dash_vertical\" class=\"enumeration\" x=\"0\" y=\"%g\" width=\"%u\" height=\"%u\"/>\n",
                          -half_border,
                          g->border_size * 5,
                          g->border_size);

  /* same as the dash; this cuts across a horizontal line */
  g_string_append_printf (s,
                          "<text id=\"enumeration_apostrophe_horizontal\" class=\"enumeration\" font-size=\"%gpx\" x=\"%g\" y=\"%g\" text-anchor=\"middle\">'</text>\n",
                          g->cell_size*2/3.0,
                          g->cell_size*.3,
                          g->cell_size/2.0 - double_border);

  g_string_append_printf (s,
                          "<text id=\"enumeration_apostrophe_vertical\" class=\"enumeration\" font-size=\"%gpx\" x=\"%g\" y=\"%g\" text-anchor=\"middle\">'</text>\n",
                          g->cell_size*2/3.0,
                          half_border,
                          g->cell_size*2/3.0);

  /* same as the dash; this cuts across a horizontal line */
  g_string_append_printf (s,
                          "<text id=\"enumeration_period_horizontal\" class=\"enumeration\" font-size=\"%gpx\" x=\"%g\" y=\"%g\" text-anchor=\"middle\">.</text>\n",
                          g->cell_size*2/3.0,
                          g->cell_size/2.0 ,
                          (double) g->border_size);

  g_string_append_printf (s,
                          "<text id=\"enumeration_period_vertical\" class=\"enumeration\" font-size=\"%gpx\" x=\"%g\" y=\"%g\" text-anchor=\"middle\">.</text>\n",
                          g->cell_size*2/3.0,
                          half_border,
                          g->cell_size*5/6.0 - half_border);

  /* divided */
  g_string_append_printf (s,
                          "<rect id=\"divided_horiz\" class=\"border\" x=\"0\" y=\"%g\" width=\"%u\" height=\"%g\"/>\n",
                          g->cell_size/2.0,
                          g->cell_size,
                          half_border);
  g_string_append_printf (s,
                          "<rect id=\"divided_vert\" class=\"border\" x=\"%g\" y=\"0\" width=\"%g\" height=\"%u\"/>\n",
                          g->cell_size/2.0,
                          half_border,
                          g->cell_size);

  g_string_append (s, "</defs>\n");

  return s;
}

static GString *
background (Geometry *g)
{
  return g_string_new ("");
  /*  return g_string_new ("<rect x=\"0\" y=\"0\" width=\"100\%\" height=\"100\%\" fill=\"white\"/>"); */
}

static GString *
instance (const char *id, guint xpos, guint ypos, guint width, guint height)
{
  GString *s = g_string_new ("");

  g_string_append_printf (s,
                          "<use href=\"#%s\" x=\"%u\" y=\"%u\" width=\"%u\" height=\"%u\"/>\n",
                          id,
                          xpos, ypos,
                          width, height);

  return s;

}

static GString *
positioned_instance (const char *id, double xpos, double ypos)
{
  GString *s = g_string_new ("");

  g_string_append_printf (s,
                          "<use href=\"#%s\" x=\"%g\" y=\"%g\"/>\n",
                          id,
                          xpos, ypos);

  return s;

}

static GString *
cell (LayoutCell *item, guint xpos, guint ypos, guint width, guint height)
{
  if (item->bg_color.alpha == 0.0 || item->cell_type == IPUZ_CELL_NULL)
    {
      return g_string_new ("");
    }
  else
    {
      GString *s = g_string_new ("");
      guint r = (guint) (item->bg_color.red * 255.0 + 0.5);
      guint g = (guint) (item->bg_color.green * 255.0 + 0.5);
      guint b = (guint) (item->bg_color.blue * 255.0 + 0.5);
      guint a = (guint) (item->bg_color.alpha * 255.0 + 0.5);
      g_string_append_printf (s,
                              "<use href=\"#cell\" x=\"%u\" y=\"%u\" width=\"%u\" height=\"%u\" fill=\"#%02x%02x%02x%02x\"/>\n",
                              xpos, ypos,
                              width, height,
                              r, g, b, a);
      return s;
    }
}

static GString *
objects (GridLayout *layout, Geometry *g)
{
  GString *s = g_string_new ("");

  guint grid_row, grid_column;

  guint ypos = 0;

  for (grid_row = 0; grid_row < layout->grid_rows; grid_row++)
    {
      guint xpos = 0;

      for (grid_column = 0; grid_column < layout->grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItemKind kind = grid_layout_get_kind (layout, grid_coord);

          switch (kind) {
          case LAYOUT_ITEM_KIND_INTERSECTION: {
            LayoutIntersection i = grid_layout_get_intersection (layout, grid_coord);
            if (i.filled)
              {
                append (s, instance ("intersection", xpos, ypos, g->border_size, g->border_size));
              }
            xpos += g->border_size;
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL: {
            LayoutBorderHorizontal i = grid_layout_get_border_horizontal (layout, grid_coord);
            if (i.filled)
              {
                append (s, instance ("border_horizontal", xpos, ypos, g->cell_size, g->border_size));
              }
            xpos += g->cell_size;
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_VERTICAL: {
            LayoutBorderVertical i = grid_layout_get_border_vertical (layout, grid_coord);
            if (i.filled)
              {
                append (s, instance ("border_vertical", xpos, ypos, g->border_size, g->cell_size));
              }
            xpos += g->border_size;
            break;
          }

          case LAYOUT_ITEM_KIND_CELL: {
            LayoutCell i = grid_layout_get_cell (layout, grid_coord);
            append (s, cell (&i, xpos, ypos, g->cell_size, g->cell_size));
            xpos += g->cell_size;
            break;
          }

          default:
            g_assert_not_reached ();
          }
        }

      if (grid_row % 2 == 0)
        {
          ypos += g->border_size;
        }
      else
        {
          ypos += g->cell_size;
        }
    }

  return s;
}

static GString *
overlays (GridLayout *layout, Geometry *g)
{
  GString *s = g_string_new ("");
  guint i;

  for (i = 0; i < layout->overlays->len; i++)
    {
      LayoutOverlay overlay = g_array_index (layout->overlays, LayoutOverlay, i);
      LayoutItemKind item_kind;
      const char *id;
      double xpos = 0.0;
      double ypos = 0.0;

      item_kind = grid_layout_get_kind (layout, overlay.coord);

      switch (overlay.kind)
        {
        case LAYOUT_OVERLAY_KIND_BARRED:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "barred_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "barred_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
          /* FIXME(enumeration): Use a space separator for apostrophes
           * for now */
        case LAYOUT_OVERLAY_KIND_ENUMERATION_APOSTROPHE:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_apostrophe_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_apostrophe_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
        case LAYOUT_OVERLAY_KIND_ENUMERATION_SPACE:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_space_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_space_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
        case LAYOUT_OVERLAY_KIND_ENUMERATION_PERIOD:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_period_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_period_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
        case LAYOUT_OVERLAY_KIND_ENUMERATION_DASH:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_dash_vertical";
              xpos = g->border_size * -2.0;
              ypos = g->border_size + g->cell_size / 2;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_dash_horizontal";
              xpos = g->border_size + g->cell_size / 2;
              ypos = g->border_size * -2.0;
            }
          else
            continue;
          break;
        default:
          continue;
        }

      xpos += overlay.coord.column / 2 * (g->border_size + g->cell_size);
      ypos += overlay.coord.row / 2 * (g->border_size + g->cell_size);

      append (s, positioned_instance (id, xpos, ypos));
    }

  return s;
}


static GString *
divided (GridLayout *layout, Geometry *g)
{
  GString *s = g_string_new ("");
  guint row, column;
  double xpos = 0.0;
  double ypos = 0.0;

  for (row = 0; row < layout->grid_rows; row++)
    {
      for (column = 0; column < layout->grid_columns; column++)
        {
          IPuzStyleDivided divided = IPUZ_STYLE_DIVIDED_NONE;
          LayoutItemKind kind;
          GridCoord coord = {
            .row = row,
            .column = column,
          };
          gchar *id = NULL;

          kind = grid_layout_get_kind (layout, coord);
          if (kind == LAYOUT_ITEM_KIND_CELL)
            {
              LayoutCell cell = grid_layout_get_cell (layout, coord);
              divided = cell.divided;
            }
          else if (kind == LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL)
            {
              LayoutClueBlockCell cell = grid_layout_get_clue_block_cell (layout, coord);
              divided = cell.divided;
            }
          else
            continue;

          xpos = coord.column / 2 * (g->border_size + g->cell_size) + g->border_size;
          ypos = coord.row / 2 * (g->border_size + g->cell_size) + g->border_size;

          if (divided == IPUZ_STYLE_DIVIDED_HORIZ)
            id = "divided_horiz";
          else if (divided == IPUZ_STYLE_DIVIDED_VERT)
            id = "divided_vert";
          /* FIXME: handle the rest */

          if (id)
            append (s, positioned_instance (id, xpos, ypos));
        }
    }

  return s;
}

static Geometry
compute_geometry (GridLayout *layout, LayoutConfig *config)
{
  guint horizontal_lines = layout->grid_rows - layout->board_rows;
  guint vertical_lines = layout->grid_columns - layout->board_columns;
  guint board_rows = layout->board_rows;
  guint board_columns = layout->board_columns;
  guint cell_size = 3 * config->base_size;

  Geometry geom = {
    .document_width = vertical_lines * config->border_size + board_columns * cell_size,
    .document_height = horizontal_lines * config->border_size + board_rows * cell_size,
    .border_size = config->border_size,
    .cell_size = cell_size,
  };

  return geom;
}

GString *
svg_from_layout (GridLayout *layout, LayoutConfig *config)
{
  Geometry geom = compute_geometry (layout, config);
  GString *doc = g_string_new ("");

  append (doc, xml_preamble ());
  append (doc, svg_toplevel (&geom));
  append (doc, stylesheet ());
  append (doc, definitions (&geom));
  append (doc, background (&geom));
  append (doc, objects (layout, &geom));
  append (doc, svg_toplevel_end ());

  return doc;
}

GString *
svg_overlays_from_layout (GridLayout *layout, LayoutConfig *config)
{
  Geometry geom = compute_geometry (layout, config);
  GString *doc = g_string_new ("");

  append (doc, xml_preamble ());
  append (doc, svg_toplevel (&geom));
  append (doc, stylesheet ());
  append (doc, overlay_definitions (&geom));
  append (doc, overlays (layout, &geom));
  append (doc, divided (layout, &geom));
  append (doc, svg_toplevel_end ());

  return doc;
}
