/* puzzle-set-resource.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-misc.h"
#include "puzzle-set-resource.h"
#include "play-xword.h"
#include "picker-grid.h"
#include "picker-list.h"


enum
{
  PROP_0,
  PROP_PICKER_GTYPE,
  PROP_KEY_FILE,
  PROP_RESOURCE,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };

struct _PuzzleSetResource
{
  PuzzleSet parent;

  GResource *resource;
  GKeyFile *key_file;
  GType picker_gtype;
  gchar *resource_prefix;

  GtkWidget *xword;
  GtkWidget *picker;

  guint idle_handler;
};

static void         puzzle_set_resource_init               (PuzzleSetResource      *self);
static void         puzzle_set_resource_class_init         (PuzzleSetResourceClass *klass);
static void         puzzle_set_resource_set_property       (GObject                *object,
                                                            guint                   prop_id,
                                                            const GValue           *value,
                                                            GParamSpec             *pspec);
static void         puzzle_set_resource_get_property       (GObject                *object,
                                                            guint                   prop_id,
                                                            GValue                 *value,
                                                            GParamSpec             *pspec);
static void         puzzle_set_resource_dispose            (GObject                *object);
static void         puzzle_set_resource_real_puzzles_start (PuzzleSet              *puzzle_set);
static void         puzzle_set_resource_real_change_phase  (PuzzleSet              *puzzle_set,
                                                            PuzzlePhase             phase);
static void         puzzle_set_resource_real_puzzles_done  (PuzzleSet              *puzzle_set);
static GtkWidget   *puzzle_set_resource_real_get_widget    (PuzzleSet              *puzzle_set,
                                                            PuzzlePhase             phase);
static IPuzPuzzle  *puzzle_set_resource_real_get_puzzle    (PuzzleSet              *puzzle_set,
                                                            PuzzlePhase             phase);
static const gchar *puzzle_set_resource_real_get_title     (PuzzleSet              *puzzle_set,
                                                            PuzzlePhase             phase);
static const gchar *puzzle_set_resource_real_get_uri       (PuzzleSet              *puzzle_set,
                                                            PuzzlePhase             phase);
static void         puzzle_set_resource_real_load_uri      (PuzzleSet              *puzzle_set,
                                                            const gchar            *uri);
static void         puzzle_set_resource_save               (PuzzleSetResource      *resource);

G_DEFINE_TYPE (PuzzleSetResource, puzzle_set_resource, PUZZLE_TYPE_SET);


static void
puzzle_set_resource_init(PuzzleSetResource *self)
{
}

static void
puzzle_set_resource_class_init (PuzzleSetResourceClass *klass)
{
  GObjectClass *object_class;
  PuzzleSetClass *puzzle_set_class;

  object_class = (GObjectClass *) klass;
  puzzle_set_class = (PuzzleSetClass *) klass;

  object_class->set_property = puzzle_set_resource_set_property;
  object_class->get_property = puzzle_set_resource_get_property;
  object_class->dispose = puzzle_set_resource_dispose;

  /* Navigation signals */
  puzzle_set_class->puzzles_start = puzzle_set_resource_real_puzzles_start;
  puzzle_set_class->change_phase = puzzle_set_resource_real_change_phase;
  puzzle_set_class->puzzles_done = puzzle_set_resource_real_puzzles_done;
  puzzle_set_class->get_widget = puzzle_set_resource_real_get_widget;
  puzzle_set_class->get_puzzle = puzzle_set_resource_real_get_puzzle;
  puzzle_set_class->get_title = puzzle_set_resource_real_get_title;
  puzzle_set_class->get_uri = puzzle_set_resource_real_get_uri;
  puzzle_set_class->load_uri = puzzle_set_resource_real_load_uri;

  obj_props[PROP_RESOURCE] =
    g_param_spec_boxed ("resource",
                        "Resource",
                        "Resource that the puzzle set is loaded from",
                        G_TYPE_RESOURCE,
                        G_PARAM_READWRITE);

  obj_props[PROP_KEY_FILE] =
    g_param_spec_boxed ("key-file",
                        "Key File",
                        "puzzle.config from the resource",
                        G_TYPE_KEY_FILE,
                        G_PARAM_READWRITE);

  obj_props[PROP_PICKER_GTYPE] =
    g_param_spec_gtype ("picker-gtype",
                        "Picker GType",
                        "Type of Picker we load",
                        /* PUZZLE_TYPE_PICKER, */
                        GTK_TYPE_WIDGET, /* In the future, we'll have a shared base class */
                        G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
puzzle_set_resource_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  PuzzleSetResource *self;

  self = (PuzzleSetResource *) object;

  switch (prop_id)
    {
    case PROP_RESOURCE:
      g_clear_pointer (&self->resource, g_resource_unref);
      self->resource = (GResource *) g_value_dup_boxed (value);
      break;
    case PROP_KEY_FILE:
      g_clear_pointer (&self->key_file, g_key_file_unref);
      self->key_file = (GKeyFile *) g_value_dup_boxed (value);
      break;
    case PROP_PICKER_GTYPE:
      self->picker_gtype = g_value_get_gtype (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_resource_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  PuzzleSetResource *self;

  self = (PuzzleSetResource *) object;

  switch (prop_id)
    {
    case PROP_RESOURCE:
      g_value_set_boxed (value, self->resource);
      break;
    case PROP_KEY_FILE:
      g_value_set_boxed (value, self->key_file);
      break;
    case PROP_PICKER_GTYPE:
      g_value_set_gtype (value, self->picker_gtype);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static
void puzzle_set_resource_dispose (GObject *object)
{
  PuzzleSetResource *self;

  self = (PuzzleSetResource *) object;

  g_clear_pointer (&self->resource, g_resource_unref);
  g_clear_pointer (&self->key_file, g_key_file_unref);
  g_clear_object (&self->picker);
  g_clear_object (&self->xword);

  G_OBJECT_CLASS (puzzle_set_resource_parent_class)->dispose (object);
}


static void
select_puzzle_cb (GObject           *picker,
                  IPuzPuzzle        *puzzle,
                  PuzzleSetResource *puzzle_set_resource)
{
  if (puzzle)
    {
      play_xword_load_puzzle (PLAY_XWORD (puzzle_set_resource->xword), puzzle);
      puzzle_set_change_phase (PUZZLE_SET (puzzle_set_resource), PUZZLE_PHASE_GAME);
    }
}

static void
user_idle_cb (PuzzleSetResource *self)
{
  puzzle_set_resource_save (self);
}

static void
puzzle_set_resource_real_puzzles_start (PuzzleSet *puzzle_set)
{
  PuzzleSetResource *self;

  self = (PuzzleSetResource *) puzzle_set;

  self->xword = play_xword_new ();
  self->idle_handler = g_signal_connect_swapped (self->xword, "user-idle", G_CALLBACK (user_idle_cb), self);
  g_object_ref_sink (self->xword);


  self->picker = (GtkWidget *) g_object_new (self->picker_gtype,
                                             "resource", self->resource,
                                             "key-file", self->key_file,
                                             NULL);
  g_signal_connect (G_OBJECT (self->picker),
                    "puzzle-selected",
                    G_CALLBACK (select_puzzle_cb),
                    self);
  g_object_ref_sink (self->picker);

  PUZZLE_SET_CLASS (puzzle_set_resource_parent_class)->puzzles_start (puzzle_set);
}

static void
puzzle_set_resource_real_change_phase (PuzzleSet   *puzzle_set,
                                       PuzzlePhase  phase)
{
  PuzzleSetResource *self = PUZZLE_SET_RESOURCE (puzzle_set);

  if (self->picker)
    {
      puzzle_set_resource_save (self);
      puzzle_picker_update_state (PUZZLE_PICKER (self->picker));
    }

  PUZZLE_SET_CLASS (puzzle_set_resource_parent_class)->change_phase (puzzle_set, phase);

  if (phase != PUZZLE_PHASE_GAME)
    puzzle_picker_clear_selected (PUZZLE_PICKER (self->picker));
}

static void
puzzle_set_resource_real_puzzles_done (PuzzleSet *puzzle_set)
{
  PuzzleSetResource *self = PUZZLE_SET_RESOURCE (puzzle_set);

  puzzle_set_resource_save (self);

  g_clear_object (&self->picker);
  g_clear_object (&self->xword);

  PUZZLE_SET_CLASS (puzzle_set_resource_parent_class)->puzzles_done (puzzle_set);
}

static GtkWidget *
puzzle_set_resource_real_get_widget (PuzzleSet   *puzzle_set,
                                     PuzzlePhase  phase)
{
  PuzzleSetResource *self;

  g_return_val_if_fail (PUZZLE_IS_SET_RESOURCE (puzzle_set), NULL);

  self = PUZZLE_SET_RESOURCE (puzzle_set);

  if (phase == PUZZLE_PHASE_PICKER)
    return self->picker;
  else if (phase == PUZZLE_PHASE_GAME)
    return self->xword;

  return NULL;
}

static IPuzPuzzle *
puzzle_set_resource_real_get_puzzle (PuzzleSet   *puzzle_set,
                                     PuzzlePhase  phase)
{
  PuzzleSetResource *self;

  g_return_val_if_fail (PUZZLE_IS_SET_RESOURCE (puzzle_set), NULL);
  g_return_val_if_fail (phase == PUZZLE_PHASE_GAME, NULL);

  self = PUZZLE_SET_RESOURCE (puzzle_set);

  return play_xword_get_puzzle (PLAY_XWORD (self->xword));
}

static const gchar *
puzzle_set_resource_real_get_title (PuzzleSet   *puzzle_set,
                                    PuzzlePhase  phase)
{
  PuzzleSetResource *self;
  const char *title = NULL;

  g_return_val_if_fail (PUZZLE_IS_SET_RESOURCE (puzzle_set), NULL);

  self = PUZZLE_SET_RESOURCE (puzzle_set);

  if (phase == PUZZLE_PHASE_PICKER)
    title = puzzle_picker_get_title (PUZZLE_PICKER (self->picker));
  else if (phase == PUZZLE_PHASE_GAME)
    title = play_xword_get_title (PLAY_XWORD (self->xword));

  if (title == NULL)
    title = PUZZLE_SET_CLASS (puzzle_set_resource_parent_class)->get_title (puzzle_set, phase);

  return title;
}

static const gchar *
puzzle_set_resource_real_get_uri (PuzzleSet   *puzzle_set,
                                  PuzzlePhase  phase)
{
 PuzzleSetResource *self;

 g_return_val_if_fail (PUZZLE_IS_SET_RESOURCE (puzzle_set), NULL);

 self = PUZZLE_SET_RESOURCE (puzzle_set);

 return puzzle_picker_get_uri (PUZZLE_PICKER (self->picker));
}

static void
puzzle_set_resource_real_load_uri (PuzzleSet   *puzzle_set,
                                   const gchar *uri)
{
  PuzzleSetResource *self;

  g_return_if_fail (PUZZLE_IS_SET_RESOURCE (puzzle_set));

  /* We only support loading the uri in the special-cased "uri"
   * id. Put in a guard. If this fails, something weird has happened
   * where we load the puzzle sets */
  g_return_if_fail (g_strcmp0 ("uri", puzzle_set_get_id (puzzle_set)) == 0);

  self = PUZZLE_SET_RESOURCE (puzzle_set);

  g_return_if_fail (PICKER_IS_LIST (self->picker));

  picker_list_import_uri (PICKER_LIST (self->picker), uri);
}

static void
puzzle_set_resource_save (PuzzleSetResource *self)
{
  if (self->picker)
    {
      if (puzzle_set_get_phase (PUZZLE_SET (self)) == PUZZLE_PHASE_GAME)
        {
          IPuzGuesses *guesses;

          guesses = play_xword_get_guesses (PLAY_XWORD (self->xword));
          puzzle_picker_update_selected_guesses (PUZZLE_PICKER (self->picker), guesses);
        }

      puzzle_picker_save_state (PUZZLE_PICKER (self->picker));
    }
}

/* Public functions */

/* Returns a newly allocated string containing the location of the config file
 */
static gchar *
find_puzzle_config (PuzzleSetResource *self)
{
  gchar **children;
  g_autofree gchar *prefix = NULL;
  g_autoptr (GError) error = NULL;


  children = g_resource_enumerate_children (self->resource,
                                            PUZZLE_SET_PREFIX,
                                            0, &error);
  if (error)
    {
      g_warning ("Trouble reading resource: %s", error->message);
      return FALSE;
    }

  /* We only support the puzzle_config being in the firstdirectory for
   * now. At some point I could imagine haveing multiple games in a
   * resource, or more complicated layouts. */
  if (children && children[0] != NULL)
    prefix = g_build_filename (PUZZLE_SET_PREFIX, children[0], NULL);
  g_strfreev (children);

  return g_build_filename (prefix, "puzzle.config", NULL);
}

static gboolean
puzzle_set_load_from_resource (PuzzleSetResource *self)
{
  g_autoptr (GKeyFile) key_file = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *config_file = NULL;
  g_autofree gchar *short_name = NULL;
  g_autofree gchar *long_name = NULL;
  g_autofree gchar *locale = NULL;
  g_autofree gchar *picker = NULL;
  g_autofree gchar *id = NULL;
  gboolean disabled = FALSE;

  GType picker_type;
  g_autoptr (GBytes) bytes = NULL;


  config_file = find_puzzle_config (self);
  if (config_file == NULL)
    return FALSE;

  bytes = g_resource_lookup_data (self->resource, config_file, 0,  &error);
  if (error)
    {
      g_warning ("unable to find %s in resource%s", config_file, error->message);
      return FALSE;
    }

  key_file = g_key_file_new ();
  if (! g_key_file_load_from_bytes (key_file, bytes, G_KEY_FILE_NONE, &error))
    {
      g_warning ("corrupt puzzle.config%s", error->message);
      return FALSE;
    }

  short_name = g_key_file_get_locale_string (key_file, "Puzzle Set",
                                             "ShortName", NULL, &error);
  if (error && error->code == G_KEY_FILE_ERROR_GROUP_NOT_FOUND)
    {
      g_warning ("Missing [Puzzle Set] section");
      return FALSE;
    }

  id = g_key_file_get_string (key_file, "Puzzle Set", "ID", NULL);
  if (id == NULL)
    {
      g_warning ("%s doesn't have an ID key", config_file);
      return FALSE;
    };

  long_name = g_key_file_get_locale_string (key_file, "Puzzle Set", "LongName",
                                            NULL, NULL);
  picker = g_key_file_get_string (key_file, "Puzzle Set", "Picker", NULL);

  locale = g_key_file_get_string (key_file, "Puzzle Set", "Locale", NULL);
  if (locale == NULL && g_strcmp0 (id, "uri"))
    {
      g_warning ("%s doesn't have a locale", config_file);
      return FALSE;
    }

  
  /* Is this an enabled Puzzle Set? We will hide it if Disabled is
   * TRUE, or if the try exec test fails. */
  disabled = g_key_file_get_boolean (key_file, "Puzzle Set", "Disabled", NULL);
  if (!disabled)
    {
      g_autofree gchar *try_exec = NULL;

      try_exec = g_key_file_get_string (key_file, "Puzzle Set", "TryExec", NULL);
      if (try_exec != NULL)
        {
          g_autofree gchar *program = NULL;

          program = g_find_program_in_path (try_exec);
          if (program == NULL)
            disabled = TRUE;
        }
    }

  if (g_strcmp0 ("grid", picker) == 0)
    picker_type = PICKER_TYPE_GRID;
  else if (g_strcmp0 ("list", picker) == 0)
    picker_type = PICKER_TYPE_LIST;
  else
    {
      g_warning ("unknown picker type %s", picker);
      return FALSE;
    }

  g_object_set (G_OBJECT (self),
                "id", id,
                "short-name", short_name,
                "long-name", long_name,
                "locale", locale,
                "key-file", key_file,
                "picker-gtype", picker_type,
                "disabled", disabled,
                NULL);

  return TRUE;
}

/* Can return NULL if resource can't be loaded. */
PuzzleSet *
puzzle_set_resource_new (GResource *resource)
{
  PuzzleSet *puzzle_set;

  puzzle_set = g_object_new (PUZZLE_TYPE_SET_RESOURCE,
                             "set-type", PUZZLE_SET_TYPE_RESOURCE,
                             "resource", resource,
                             NULL);

  if (puzzle_set_load_from_resource (PUZZLE_SET_RESOURCE (puzzle_set)))
    return puzzle_set;

  g_object_unref (G_OBJECT (puzzle_set));
  return NULL;
}
