/* gen-word-model.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include "gen-word-list.h"

G_BEGIN_DECLS

typedef struct Charset Charset;


typedef struct
{
  GArray *words;
  GArray *letters;
  GArray *word_index;
  Charset *charset;
  gint threshold;
  gint min_length;
  gint max_length;
  gssize letter_list_offset;
  gssize letter_index_offset;
} GenWordList;


GenWordList *gen_word_list_new               (gint           min_length,
                                              gint           max_length,
                                              gint           threshold);
void         gen_word_list_free              (GenWordList   *word_list);
void         gen_word_list_add_test_word     (GenWordList   *word_list,
                                              const char    *word,
                                              glong          priority);
gboolean     gen_word_list_parse             (GenWordList   *word_list,
                                              GInputStream  *stream);
void         gen_word_list_sort              (GenWordList   *word_list);
void         gen_word_list_build_charset     (GenWordList   *word_list);
void         gen_word_list_calculate_offsets (GenWordList   *word_list);
void         gen_word_list_write_to_stream   (GenWordList   *word_list,
                                              GOutputStream *stream);
void         gen_word_list_write             (GenWordList   *word_list,
                                              const char    *output_filename);

/* For debugging */
void         gen_word_list_dump              (GenWordList   *word_list);


G_END_DECLS
