/* xword-state.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

#include "crosswords-quirks.h"
#include "play-state-misc.h"

G_BEGIN_DECLS

#define XWORD_STATE_BLOCK_CHARS ".#"
#define XWORD_STATE_SKIP_CHARS  "?"

#define XWORD_STATE_HAS_CURSOR(state) (state->clue.direction!=IPUZ_CLUE_DIRECTION_NONE)

typedef enum
{
  XWORD_CMD_KIND_NONE,
  XWORD_CMD_KIND_UP,
  XWORD_CMD_KIND_DOWN,
  XWORD_CMD_KIND_LEFT,
  XWORD_CMD_KIND_RIGHT,
  XWORD_CMD_KIND_FORWARD,
  XWORD_CMD_KIND_BACK,
  XWORD_CMD_KIND_FORWARD_CLUE,
  XWORD_CMD_KIND_BACK_CLUE,
  XWORD_CMD_KIND_BACKSPACE,
  XWORD_CMD_KIND_DELETE,
  XWORD_CMD_KIND_SWITCH,
} XwordCmdKind;

typedef enum
{
  XWORD_STATE_SOLVE,       /* @ Solve a crossword @ */
  XWORD_STATE_BROWSE,      /* @ Browse through a board without modifying it @ */
  XWORD_STATE_EDIT,        /* @ Edit the grid @ */
  XWORD_STATE_EDIT_BROWSE, /* @ Browse through an editable board without modifying it @ */
  XWORD_STATE_SELECT,      /* @ Select cells @ */
  XWORD_STATE_VIEW,        /* @ Display a board with no interaction @ */
} XwordStateMode;


typedef enum
{
  XWORD_REVEAL_NONE,         /* @ Reveal nothing. Display the crossword as normal @ */
  XWORD_REVEAL_ERRORS_BOARD, /* @ Reveal all incorrect guesses on the board @ */
  XWORD_REVEAL_ERRORS_CLUE,  /* @ Reveal all incorrect guesses in the current clue @ */
  XWORD_REVEAL_ERRORS_CELL,  /* @ Reveal all incorrect guesses in the current cell @ */
  XWORD_REVEAL_ALL,          /* @ Reveal all unsolved letters. Effectively give up @ */
} XwordRevealMode;

typedef enum
{
  XWORD_SELECTION_SINGLE,            /* @ Select individual cells, only @ */
  XWORD_SELECTION_TYPE_LETTERS_ONLY, /* @ Only lets the user select guessed letters @ */
  XWORD_SELECTION_TYPE_BLANKS_ONLY,  /* @ Only lets the user select empty spaces @ */
} XwordSelectionMode;

typedef struct
{
  XwordStateMode mode;
  IPuzCrossword *xword;

  /* Current keyboard state */
  IPuzCellCoord cursor;
  IPuzClueId clue; /* Current selected direction and clue ID, or NONE */

  /* Modifiers to the current display and behavior */
  XwordRevealMode reveal_mode;
  gboolean skip_filled;
  XwordSelectionMode selection_mode;
  CellArray *cell_array;
  CrosswordsQuirks *quirks;
} XwordState;


XwordState *xword_state_new             (IPuzCrossword     *xword,
                                         CrosswordsQuirks  *quirks,
                                         XwordStateMode     mode) G_GNUC_WARN_UNUSED_RESULT;
void        xword_state_free            (XwordState        *state);
XwordState *xword_state_dehydrate       (XwordState        *state);
XwordState *xword_state_hydrate         (XwordState        *state,
                                         IPuzCrossword     *xword,
                                         CrosswordsQuirks  *quirk);
XwordState *xword_state_replace         (XwordState        *old_state,
                                         XwordState        *new_state) G_GNUC_WARN_UNUSED_RESULT;

XwordState *xword_state_change_mode     (XwordState        *state,
                                         XwordStateMode     mode) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_clue_selected   (XwordState        *state,
                                         IPuzClue          *clue) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_cell_selected   (XwordState        *state,
                                         IPuzCellCoord      coord) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_do_command      (XwordState        *state,
                                         XwordCmdKind       kind) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_guess           (XwordState        *state,
                                         const gchar       *guess) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_guess_at_cell   (XwordState        *state,
                                         const gchar       *guess,
                                         IPuzCellCoord      coord) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_guess_word      (XwordState        *state,
                                         IPuzCellCoord      coord,
                                         IPuzClueDirection  direction,
                                         const gchar       *word) G_GNUC_WARN_UNUSED_RESULT;
XwordState *xword_state_set_reveal_mode (XwordState        *state,
                                         XwordRevealMode    reveal_mode) G_GNUC_WARN_UNUSED_RESULT;

void        xword_state_assert_equal    (const XwordState  *a,
                                         const XwordState  *b);

void        xword_state_print           (const XwordState  *state,
                                         gboolean           print_members);


G_END_DECLS
