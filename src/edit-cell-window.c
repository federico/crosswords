/* edit-cell-window.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <libipuz/libipuz.h>
#include <glib/gi18n.h>

#include "edit-cell.h"
#include "edit-cell-window.h"
#include "play-cell.h"

static void edit_cell_window_init       (EditCellWindow *self);
static void edit_cell_window_class_init (EditCellWindowClass *klass);

static GdkRGBA
rgba_transparent (void)
{
  GdkRGBA rgba = {
    .red = 0.0,
    .green = 0.0,
    .blue = 0.0,
    .alpha = 0.0,
  };

  return rgba;
}

struct _EditCellWindow
{
  GtkDialog parent_instance;
  IPuzCrossword *xword;
  GtkWidget *cell;
  GtkWidget *edit;
};

static LayoutCell
empty_cell_layout (void)
{
  LayoutCell layout = {
    .cell_type = IPUZ_CELL_NORMAL,
    .text_color_set = FALSE,
    .bg_color = rgba_transparent (),
    .text_color = rgba_transparent (),
  };

  return layout;
}

static void
cell_changed_cb (EditCell       *edit,
                 EditCellWindow *self)
{
  IPuzCell *puz_cell;
  IPuzCellCoord coord = { .row = 0, .column = 0 };
  LayoutCell layout = empty_cell_layout ();

  puz_cell = ipuz_crossword_get_cell (self->xword, coord);
  play_cell_load_state (PLAY_CELL (self->cell), puz_cell, &layout);
}

static void
edit_cell_window_init (EditCellWindow *self)
{
  GtkWidget *box;
  GInputStream *stream;
  IPuzCell *puz_cell;
  IPuzCellCoord coord = { .row = 0, .column = 0 };

  stream = g_resources_open_stream ("/org/gnome/Crosswords/crosswords/one-cell.ipuz", 0, NULL);
  self->xword = (IPuzCrossword *) ipuz_puzzle_new_from_stream (stream, NULL, NULL);
  g_input_stream_close (G_INPUT_STREAM (stream), NULL, NULL);
  g_assert (self->xword != NULL);


  gtk_dialog_add_buttons (GTK_DIALOG (self),
                          "_Cancel", GTK_RESPONSE_CANCEL,
                          "_OK", GTK_RESPONSE_OK,
                          NULL);

  box = gtk_dialog_get_content_area (GTK_DIALOG (self));
  gtk_box_set_spacing (GTK_BOX (box), 16);
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box), GTK_ORIENTATION_VERTICAL);

  LayoutConfig config = {
    .border_size = 4,
    .base_size = 24,
  };

  self->cell = play_cell_new (coord, config);

  /* FIXME: note that this cell has no borders.  Maybe we should stick it inside a 1x1 PlayGrid/PlayState, etc.? */

  gtk_widget_set_halign (GTK_WIDGET (self->cell), GTK_ALIGN_CENTER);
  gtk_widget_set_hexpand (GTK_WIDGET (self->cell), TRUE);
  gtk_box_prepend (GTK_BOX (box), self->cell);

  self->edit = edit_cell_new ();
  gtk_widget_set_halign (GTK_WIDGET (self->edit), GTK_ALIGN_CENTER);
  gtk_widget_set_hexpand (GTK_WIDGET (self->edit), FALSE);
  g_signal_connect (G_OBJECT (self->edit), "cell-changed", G_CALLBACK (cell_changed_cb), self);
  gtk_box_append (GTK_BOX (box), self->edit);

  puz_cell = ipuz_crossword_get_cell (self->xword, coord);
  LayoutCell layout = empty_cell_layout ();
  play_cell_load_state (PLAY_CELL (self->cell), puz_cell, &layout);
  edit_cell_load_state (EDIT_CELL (self->edit), self->xword);
}

static void
edit_cell_window_class_init (EditCellWindowClass *klass)
{

}


GtkWidget *
edit_cell_window_new (void)
{
  return (GtkWidget *) g_object_new (EDIT_TYPE_CELL_WINDOW,
                                     "use-header-bar", TRUE,
                                     "modal", TRUE,
                                     "title", _("Cell Editor"),
                                     NULL);
}

G_DEFINE_TYPE (EditCellWindow, edit_cell_window, GTK_TYPE_DIALOG);
