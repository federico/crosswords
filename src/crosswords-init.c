/* crosswords-init.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib/gi18n.h>
#include <adwaita.h>

#include "crosswords-config.h"
#include "crosswords-init.h"
#include "crosswords-enums.h"

#include "play-cell.h"
#include "play-clue-list.h"
#include "play-clue-row.h"
#include "play-grid.h"
#include "picker-grid.h"
#include "puzzle-button.h"
#include "puzzle-set.h"
#include "puzzle-set-resource.h"
#include "play-window.h"
#include "play-xword.h"
#include "play-xword-column.h"


static gboolean inside_flatpak = FALSE;


static void
crosswords_init_adw (void)
{
  if (g_getenv ("CROSSWORD_FORCE_DARK"))
    adw_style_manager_set_color_scheme (adw_style_manager_get_default(), ADW_COLOR_SCHEME_FORCE_DARK);
}

static void
crosswords_init_icon_themes (void)
{
  GtkIconTheme *icon_theme;

  icon_theme = gtk_icon_theme_get_for_display (gdk_display_get_default ());
  gtk_icon_theme_add_resource_path (icon_theme, "/org/gnome/Crosswords/icons");
}

static void
crosswords_init_i18n (void)
{
  const gchar *localedir = NULL;

  /* Set up gettext translations */
  localedir = g_getenv ("CROSSWORDS_LOCALEDIR");
  if (localedir == NULL)
    localedir = LOCALEDIR;

  bindtextdomain (GETTEXT_PACKAGE, localedir);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
}

static void
crosswords_init_types (void)
{
  g_type_ensure (PLAY_TYPE_CELL);
  g_type_ensure (PLAY_TYPE_CLUE_ROW);
  g_type_ensure (PLAY_TYPE_CLUE_LIST);
  g_type_ensure (PLAY_TYPE_GRID);
  g_type_ensure (PLAY_TYPE_WINDOW);
  g_type_ensure (PLAY_TYPE_XWORD);
  g_type_ensure (PLAY_TYPE_XWORD_COLUMN);
  g_type_ensure (PICKER_TYPE_GRID);
  g_type_ensure (PUZZLE_TYPE_BUTTON);
  g_type_ensure (PUZZLE_TYPE_SET);
  g_type_ensure (PUZZLE_TYPE_SET_RESOURCE);
  g_type_ensure (QUIRKS_TYPE_GUESS_ADVANCE);
}

static void
extend_env_var (const gchar *envvar,
                const gchar *addition)
{
  const gchar *current = NULL;
  g_autofree gchar *new_envvar = NULL;

  current = g_getenv (envvar);
  if (current)
    new_envvar = g_strdup_printf ("%s:%s", current, addition);
  else
    new_envvar = g_strdup (addition);

  g_setenv (envvar, new_envvar, TRUE);
}


/* Set up the environment variables */
static void
crosswords_init_environment (void)
{
  extend_env_var ("PATH", LIBEXECDIR);

  /* FIXME: see Issue #53 "downloader environments should be cleaned up for flatpaks" */
  if (crosswords_inside_flatpak ())
    {
      GDir *extensions_dir;
      const gchar *name;

      extensions_dir = g_dir_open (EXTENSIONSDIR, 0, NULL);
      g_return_if_fail (extensions_dir != NULL);
      while ((name = g_dir_read_name (extensions_dir)) != NULL)
        {
          g_autofree gchar *file_name = NULL;
          g_autofree gchar *path = NULL;
          g_autofree gchar *pythonpath = NULL;

          if (g_strcmp0 (name, "bin") == 0)
            continue;
          if (g_strcmp0 (name, "lib") == 0)
            continue;
          if (g_str_has_prefix (name, "."))
            continue;

          file_name = g_build_filename (EXTENSIONSDIR, name, NULL);
          if (!g_file_test (file_name, G_FILE_TEST_IS_DIR))
            continue;

          path = g_build_filename (file_name, "bin", NULL);
          if (g_file_test (path, G_FILE_TEST_IS_DIR))
            extend_env_var ("PATH", path);

          pythonpath = g_build_filename (file_name, "lib", PYTHON_VERSION, "site-packages", NULL);
          if (g_file_test (path, G_FILE_TEST_IS_DIR))
            extend_env_var ("PYTHONPATH", pythonpath);
        }
    }
}

void
crosswords_init (void)
{
  static gsize guard = 0;

  if (!g_once_init_enter (&guard))
    return;

  crosswords_init_adw ();
  crosswords_init_icon_themes ();
  crosswords_init_i18n ();
  crosswords_init_types ();
  crosswords_init_environment ();
  puzzle_sets_init ();

  g_once_init_leave (&guard, 1);
}


/**
 * crosswords_inside_flatpak:
 * @void:
 *
 * Let's us know if we're curently running within a flatpak. While we
 * don't want to do anything dramatically diferent in that situation,
 * we do want to look in a different location for extensions and
 * plugins than our normal path etc. This triggers setting appropriate
 * environment variables and puzzle path directories.
 *
 * Returns: %TRUE, if we're running within a flatpak
 **/
gboolean
crosswords_inside_flatpak (void)
{
  static gsize guard = 0;

  if (!g_once_init_enter (&guard))
    return inside_flatpak;

  /* This magic file is the best way to confirm we're in a flatpak */
  if (g_file_test ("/.flatpak-info", G_FILE_TEST_IS_REGULAR))
    inside_flatpak = TRUE;

  g_once_init_leave (&guard, 1);

  return inside_flatpak;
}
