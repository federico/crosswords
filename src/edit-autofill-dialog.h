/* edit-autofill-dialog.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

G_BEGIN_DECLS


#define EDIT_TYPE_AUTOFILL_DIALOG (edit_autofill_dialog_get_type())
G_DECLARE_FINAL_TYPE (EditAutofillDialog, edit_autofill_dialog, EDIT, AUTOFILL_DIALOG, GtkDialog);

GtkWidget   *edit_autofill_dialog_new       (IPuzPuzzle *puzzle);
IPuzGuesses *edit_autofill_dialog_get_guess (EditAutofillDialog *autofill_dialog);


G_END_DECLS
