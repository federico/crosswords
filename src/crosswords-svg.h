/* crosswords-svg.h - A simple widget to display an SVG with a stylesheet
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <librsvg/rsvg.h>

G_BEGIN_DECLS

#define CROSSWORDS_TYPE_SVG (crosswords_svg_get_type ())
G_DECLARE_FINAL_TYPE (CrosswordsSvg, crosswords_svg, CROSSWORDS, SVG, GtkWidget)

GtkWidget *crosswords_svg_new (void);

void crosswords_svg_set_document (CrosswordsSvg *csvg,
                                  RsvgHandle    *handle);

G_END_DECLS
