/* edit-metadata.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-metadata.h"


struct _EditMetadata
{
  GtkWidget    parent_instance;

  PuzzleStack *puzzle_stack;
  gulong       changed_id;

  /* Template widgets */
  GtkWidget   *title_entry;
  GtkWidget   *copyright_entry;
  GtkWidget   *url_entry;
  GtkWidget   *publisher_entry;
  GtkWidget   *publication_entry;
  GtkWidget   *author_entry;
  GtkWidget   *editor_entry;
  GtkWidget   *date_entry;
  GtkWidget   *difficulty_entry;
  GtkWidget   *notes_text_view;
};


static void edit_metadata_init              (EditMetadata      *self);
static void edit_metadata_class_init        (EditMetadataClass *klass);
static void edit_metadata_dispose           (GObject           *object);


G_DEFINE_TYPE (EditMetadata, edit_metadata, GTK_TYPE_WIDGET);


static void
edit_metadata_init (EditMetadata *self)
{
  GtkLayoutManager *box_layout;

  gtk_widget_init_template (GTK_WIDGET (self));

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_HORIZONTAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 25);
  gtk_widget_set_hexpand (GTK_WIDGET (self), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (self), TRUE);
}

static void
edit_metadata_class_init (EditMetadataClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_metadata_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-metadata.ui");
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, title_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, copyright_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, url_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, publisher_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, publication_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, author_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, editor_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, date_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, difficulty_entry);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, notes_text_view);

  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_commit_changes);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_metadata_dispose (GObject *object)
{
  EditMetadata *self;
  GtkWidget *child;

  self = EDIT_METADATA (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);

  G_OBJECT_CLASS (edit_metadata_parent_class)->dispose (object);
}


static void
copy_property_to_entry (IPuzPuzzle  *puzzle,
                        const gchar *property,
                        GtkWidget   *entry)
{
  g_autofree gchar *value = NULL;
  
  if (puzzle)
    g_object_get (G_OBJECT (puzzle),
                  property, &value,
                  NULL);

  gtk_editable_set_text (GTK_EDITABLE (entry), value?value:"");
}

static void
copy_puzzle_to_ui (IPuzPuzzle   *puzzle,
                   EditMetadata *self)
{
  g_assert (EDIT_IS_METADATA (self));

  copy_property_to_entry (puzzle, "title", self->title_entry);
  copy_property_to_entry (puzzle, "copyright", self->copyright_entry);
  copy_property_to_entry (puzzle, "url", self->url_entry);
  copy_property_to_entry (puzzle, "publisher", self->publisher_entry);
  copy_property_to_entry (puzzle, "publication", self->publication_entry);
  copy_property_to_entry (puzzle, "author", self->author_entry);
  copy_property_to_entry (puzzle, "editor", self->editor_entry);
  copy_property_to_entry (puzzle, "date", self->date_entry);
  copy_property_to_entry (puzzle, "difficulty", self->difficulty_entry);
}

static gboolean
copy_entry_to_property (GtkWidget   *entry,
                        IPuzPuzzle  *puzzle,
                        const gchar *property)
{
  const gchar *entry_text;
  g_autofree gchar *value = NULL;

  if (puzzle == NULL)
    return FALSE;
  
  entry_text = gtk_editable_get_text (GTK_EDITABLE (entry));
  g_object_get (G_OBJECT (puzzle),
                property, &value,
                NULL);

  /* value can be null, but the entry is only ever an empty string. We
   * don't support saving empty strings. */
  if (g_strcmp0 (entry_text, value?value:""))
    {
      g_object_set (G_OBJECT (puzzle),
                    property, entry_text,
                    NULL);
      return TRUE;
    }

  return FALSE;
}

static gboolean
copy_ui_to_puzzle (EditMetadata *self,
                   IPuzPuzzle   *puzzle)
{
  gboolean push_change = FALSE;

  g_assert (EDIT_IS_METADATA (self));
  
  push_change = copy_entry_to_property (self->title_entry, puzzle, "title") || push_change;
  push_change = copy_entry_to_property (self->copyright_entry, puzzle, "copyright") || push_change;
  push_change = copy_entry_to_property (self->url_entry, puzzle, "url") || push_change;
  push_change = copy_entry_to_property (self->publisher_entry, puzzle, "publisher") || push_change;
  push_change = copy_entry_to_property (self->publication_entry, puzzle, "publication") || push_change;
  push_change = copy_entry_to_property (self->author_entry, puzzle, "author") || push_change;
  push_change = copy_entry_to_property (self->editor_entry, puzzle, "editor") || push_change;
  push_change = copy_entry_to_property (self->date_entry, puzzle, "date") || push_change;
  push_change = copy_entry_to_property (self->difficulty_entry, puzzle, "difficulty") || push_change;

  return push_change;
}

static void
edit_metadata_puzzle_stack_changed (EditMetadata          *self,
                                    gboolean               new_frame,
                                    PuzzleStackChangeType  change_type,
                                    PuzzleStack           *puzzle_stack)
{
  IPuzPuzzle *puzzle;

  g_assert (EDIT_IS_METADATA (self));

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  copy_puzzle_to_ui (puzzle, self);
}

/* Public Functions */

void
edit_metadata_set_puzzle_stack (EditMetadata *edit_metadata,
                                PuzzleStack  *puzzle_stack)
{
  g_return_if_fail (EDIT_IS_METADATA (edit_metadata));

  if (puzzle_stack)
    g_object_ref (puzzle_stack);

  g_clear_signal_handler (&edit_metadata->changed_id,
                          edit_metadata->puzzle_stack);
  g_clear_object (&edit_metadata->puzzle_stack);

  edit_metadata->puzzle_stack = puzzle_stack;
  edit_metadata->changed_id = g_signal_connect_swapped (edit_metadata->puzzle_stack,
                                                        "changed",
                                                        G_CALLBACK (edit_metadata_puzzle_stack_changed),
                                                        edit_metadata);

  /* Force a changed signal to get the inital setup done */
  edit_metadata_puzzle_stack_changed (edit_metadata, TRUE, STACK_CHANGE_PUZZLE, puzzle_stack);
}

void
edit_metadata_commit_changes (EditMetadata *edit_metadata)
{
  IPuzPuzzle *puzzle;

  g_return_if_fail (EDIT_IS_METADATA (edit_metadata));
  puzzle = puzzle_stack_get_puzzle (edit_metadata->puzzle_stack);
  if (copy_ui_to_puzzle (edit_metadata, puzzle))
    {
      puzzle_stack_push_change (edit_metadata->puzzle_stack,
                                STACK_CHANGE_METADATA,
                                puzzle);
    }
}
