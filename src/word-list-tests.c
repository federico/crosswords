#include <glib.h>
#include <locale.h>
#include "gen-word-list.h"
#include "word-list.h"

static void
dump_bytes (GBytes *bytes)
{
  GError *error = NULL;
  GSubprocess *sub = g_subprocess_new ((G_SUBPROCESS_FLAGS_STDIN_PIPE
                                        | G_SUBPROCESS_FLAGS_STDOUT_PIPE
                                        | G_SUBPROCESS_FLAGS_STDERR_MERGE),
                                       &error,
                                       "od",
                                       /* hex offsets */
                                       "-A",
                                       "x",
                                       /* hex bytes, 1 byte per integer, add printout */
                                       "-t",
                                       "x1z",
                                       /* no duplicate suppression */
                                       "-v",
                                       NULL);
  GBytes *stdout_buf = NULL;
  gpointer stdout_data;
  gsize stdout_size;

  if (!sub)
    {
      g_error ("Could not spawn od(1): %s", error->message);
      return; /* unreachable */
    }

  if (!g_subprocess_communicate (sub,
                                 bytes, /* stdin */
                                 NULL, /* cancellable */
                                 &stdout_buf,
                                 NULL, /* stderr_buf */
                                 &error))
    {
      g_error ("Error while running od(1): %s", error->message);
      return; /* unreachable */
    }

  stdout_data = g_bytes_unref_to_data (stdout_buf, &stdout_size);

  fwrite (stdout_data, 1, stdout_size, stdout);

  g_free (stdout_data);
  g_object_unref (sub);
}

static void
loads_default (void)
{
  WordList *wl = word_list_new ();
  word_list_set_filter (wl, "A??");
  g_assert_cmpint (word_list_get_n_items (wl), ==, 164);
  g_object_unref (wl);
}

static void
ascii_roundtrip (void)
{
  GenWordList *gen = gen_word_list_new (3, 5, 0);

  gen_word_list_add_test_word (gen, "GHIJK", 3);
  gen_word_list_add_test_word (gen, "DEF", 1);
  gen_word_list_add_test_word (gen, "ABC", 2);

  gen_word_list_sort (gen);
  gen_word_list_build_charset (gen);
  gen_word_list_calculate_offsets (gen);

  GOutputStream *stream = g_memory_output_stream_new_resizable ();
  gen_word_list_write_to_stream (gen, stream);
  g_output_stream_close (stream, NULL, NULL);
  gen_word_list_free (gen);

  GBytes *bytes = g_memory_output_stream_steal_as_bytes (G_MEMORY_OUTPUT_STREAM (stream));
  g_object_unref (stream);

  dump_bytes (bytes);

  WordList *word_list = word_list_new_from_bytes (bytes);

  word_list_set_filter (word_list, "???");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ABC");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "DEF");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 2);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex abc;
  WordIndex def;
  g_assert_true (word_list_get_word_index (word_list, 0, &abc));
  g_assert_true (word_list_get_word_index (word_list, 1, &def));

  word_list_set_filter (word_list, "????");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 0);

  word_list_set_filter (word_list, "?????");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "GHIJK");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 3);
  WordIndex ghijk;
  g_assert_true (word_list_get_word_index (word_list, 0, &ghijk));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, abc), ==, "ABC");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, def), ==, "DEF");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ghijk), ==, "GHIJK");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, abc), ==, 2);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, def), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ghijk), ==, 3);

  g_object_unref (word_list);
  g_bytes_unref (bytes);
}

static void
utf8_roundtrip (void)
{
  GenWordList *gen = gen_word_list_new (3, 5, 0);

  gen_word_list_add_test_word (gen, "AÑO", 1);
  gen_word_list_add_test_word (gen, "ÉSE", 1);

  gen_word_list_add_test_word (gen, "HOLA", 2);
  gen_word_list_add_test_word (gen, "PÂTÉ", 2);
  gen_word_list_add_test_word (gen, "ZULU", 2);

  gen_word_list_sort (gen);
  gen_word_list_build_charset (gen);
  gen_word_list_calculate_offsets (gen);

  GOutputStream *stream = g_memory_output_stream_new_resizable ();
  gen_word_list_write_to_stream (gen, stream);
  g_output_stream_close (stream, NULL, NULL);
  gen_word_list_free (gen);

  GBytes *bytes = g_memory_output_stream_steal_as_bytes (G_MEMORY_OUTPUT_STREAM (stream));
  g_object_unref (stream);

  dump_bytes (bytes);

  WordList *word_list = word_list_new_from_bytes (bytes);

  word_list_set_filter (word_list, "???");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "ÉSE");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 1);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex ano;
  WordIndex ese;
  g_assert_true (word_list_get_word_index (word_list, 0, &ano));
  g_assert_true (word_list_get_word_index (word_list, 1, &ese));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, ano), ==, "AÑO");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ese), ==, "ÉSE");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, ano), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ese), ==, 1);

  word_list_set_filter (word_list, "A?O");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");

  word_list_set_filter (word_list, "??E");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ÉSE");

  word_list_set_filter (word_list, "????");
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 3);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "HOLA");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "PÂTÉ");
  g_assert_cmpstr (word_list_get_word (word_list, 2), ==, "ZULU");

  g_object_unref (word_list);
  g_bytes_unref (bytes);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/word_list/loads_default", loads_default);

  g_test_add_func ("/word_list/ascii_roundtrip", ascii_roundtrip);
  g_test_add_func ("/word_list/utf8_roundtrip", utf8_roundtrip);

  return g_test_run ();
}
