/* puzzle-set-list.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "puzzle-set-resource.h"
#include "crosswords-misc.h"

static GListModel *global_puzzle_sets;

static void
crosswords_load_puzzle_sets_from_dir (const gchar *dir_name,
                                      GListModel  *list)
{
  const char *name;
  GDir *data_dir;

  data_dir = g_dir_open (dir_name, 0, NULL);
  /* Ignore directories that don't exist. This is fine, because we're
   * walking through $XDG_DATA_DIRS */
  if (data_dir == NULL)
    return;

  while ((name = g_dir_read_name (data_dir)) != NULL)
    {
      g_autoptr (GError) err = NULL;
      g_autofree gchar *file_name = NULL;
      GResource *resource;
      PuzzleSet *puzzle_set;

      file_name = g_build_filename (dir_name, name, NULL);
      /* Recurse to subdirectories */
      /* FIXME(cleanup): Should we look for loops? */
      if (g_file_test (file_name, G_FILE_TEST_IS_DIR))
        {
          crosswords_load_puzzle_sets_from_dir (file_name, list);
          continue;
        }

      if (! g_str_has_suffix (name, ".gresource"))
        continue;

      resource = g_resource_load (file_name, &err);
      if (err)
        {
          g_warning ("Failed to load resource %s\nError %s", file_name, err->message);
          continue;
        }

      puzzle_set = puzzle_set_resource_new (resource);
      g_resource_unref (resource);

      /* Can we create a meaningful puzzle_set from this resource? */
      if (puzzle_set == NULL)
        continue;

      g_list_store_append (G_LIST_STORE (list), puzzle_set);
    }
  g_dir_close (data_dir);
}

gint
crosswords_list_store_sort (gconstpointer a,
                            gconstpointer b,
                            gpointer      user_data)
{
  g_autofree gchar *a_short_name = NULL;
  g_autofree gchar *b_short_name = NULL;
  g_autofree gchar *a_id = NULL;
  g_autofree gchar *b_id = NULL;

  g_object_get (G_OBJECT (a),
                "short-name", &a_short_name,
                "id", &a_id,
                NULL);

  g_object_get (G_OBJECT (b),
                "short-name", &b_short_name,
                "id", &b_id,
                NULL);

  /* Always make sure the "uri" resource is sorted last */
  if (g_strcmp0 (a_id, "uri") == 0)
    return 1;
  if (g_strcmp0 (b_id, "uri") == 0)
    return -1;

  return g_strcmp0 (a_short_name, b_short_name);
}


void
puzzle_set_list_init (void)
{
  const gchar *const *datadirs;
  const gchar *PUZZLE_SET_PATH = NULL;

  if (global_puzzle_sets != NULL)
    return;

  global_puzzle_sets = (GListModel *) g_list_store_new (PUZZLE_TYPE_SET);
  
  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/puzzle-sets", NULL);
      crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/extensions/puzzle-sets", NULL);
      crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  if (g_get_user_data_dir ())
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (g_get_user_data_dir (), "crosswords/puzzle-sets", NULL);
      crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  /* We also look in $PUZZLE_SET_PATH for puzzles. This is mostly for
   * running out of the builddir with a local install. It is set in
   * the 'run' script in this case.  */
  PUZZLE_SET_PATH = g_getenv ("PUZZLE_SET_PATH");
  if (PUZZLE_SET_PATH && PUZZLE_SET_PATH[0])
    {
      g_auto (GStrv) puzzle_set_paths = NULL;

      puzzle_set_paths = g_strsplit (PUZZLE_SET_PATH, ":", -1);
      for (guint i = 0; puzzle_set_paths[i]; i++)
        crosswords_load_puzzle_sets_from_dir (puzzle_set_paths[i], global_puzzle_sets);
    }

  if (g_list_model_get_n_items (global_puzzle_sets) == 0)
    {
      /* We should always have the "uri" puzzle-set at a minimum, and
       * we won't work without it. This implies a broken
       * installation. Erroring out. */
      g_warning (_("No puzzle sets were found. Use $PUZZLE_SET_PATH to set a directory to search."));
      g_warning (_("When running out of a local build, use the `run` script to set the environment correctly."));
      g_assert_not_reached ();
    }

  g_list_store_sort (G_LIST_STORE (global_puzzle_sets), crosswords_list_store_sort, NULL);
}

GListModel *
puzzle_set_list_get (void)
{
  if (global_puzzle_sets == NULL)
    /* this will assert on failure */
    puzzle_set_list_init ();

  return global_puzzle_sets;
}
