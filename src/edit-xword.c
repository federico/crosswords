/* edit-xword.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "crosswords-enums.h"
#include "edit-xword.h"
#include "play-grid.h"
#include "word-list.h"
#include "word-list-model.h"


struct _EditXword
{
  GtkWidget      parent_instance;

  PuzzleStack   *puzzle_stack;
  gulong         changed_id;

  XwordState    *state;
  WordListModel *across_list_model;
  WordListModel *down_list_model;

  GSettings     *settings;

  /* Template widgets */
  GtkWidget     *edit_grid;
  GtkWidget     *across_word_list_view;
  GtkWidget     *down_word_list_view;
};

static void edit_xword_init                     (EditXword       *self);
static void edit_xword_class_init               (EditXwordClass  *klass);
static void edit_xword_dispose                  (GObject         *object);
static void edit_xword_activate_clipboard_copy  (EditXword       *self,
                                                 const char      *name,
                                                 GVariant        *parameter);
static void edit_xword_activate_clipboard_paste (EditXword       *self,
                                                 const char      *name,
                                                 GVariant        *parameter);

static void symmetry_changed_cb                 (EditXword       *xword);
static void grid_guess_cb                       (PlayGrid        *grid,
                                                 gchar           *guess,
                                                 EditXword       *xword);
static void grid_guess_at_cell_cb               (PlayGrid        *grid,
                                                 gchar           *guess,
                                                 guint            row,
                                                 guint            column,
                                                 EditXword       *xword);
static void grid_do_command_cb                  (PlayGrid        *grid,
                                                 XwordCmdKind     kind,
                                                 EditXword       *xword);
static void grid_cell_selected_cb               (PlayGrid        *grid,
                                                 guint            row,
                                                 guint            column,
                                                 EditXword       *xword);
static void across_word_activate_cb             (EditXword       *xword,
                                                 guint            position);
static void down_word_activate_cb               (EditXword       *xword,
                                                 guint            position);
static void edit_xword_update_state             (EditXword       *xword,
                                                 XwordState       *state);
static void edit_xword_update_states            (EditXword       *xword);


G_DEFINE_TYPE (EditXword, edit_xword, GTK_TYPE_WIDGET);


static void
setup_listitem_cb (GtkListItemFactory *factory,
                   GtkListItem        *list_item)
{
  GtkWidget *label;

  label = gtk_label_new (NULL);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_list_item_set_child (list_item, label);
}

static void
bind_listitem_cb (GtkListItemFactory *factory,
                  GtkListItem        *list_item)
{
  GtkWidget *label;
  WordListModelRow *row;
  g_autofree gchar *text = NULL;

  label = gtk_list_item_get_child (list_item);
  row = (WordListModelRow *) gtk_list_item_get_item (list_item);
  text = g_strdup_printf ("%s — (%d)",
                          word_list_model_row_get_word (row),
                          word_list_model_row_get_priority (row));
  gtk_label_set_text (GTK_LABEL (label), text);
}

static void
edit_xword_init (EditXword *self)
{
  g_autoptr (GtkListItemFactory) across_factory = NULL;
  g_autoptr (GtkListItemFactory) down_factory = NULL;

  self->across_list_model = word_list_model_new ();
  self->down_list_model = word_list_model_new ();

  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("org.gnome.Crosswords.Editor");
  g_signal_connect_object (self->settings,
                           "changed::symmetry",
                           G_CALLBACK (symmetry_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
  symmetry_changed_cb (self);

  across_factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (across_factory, "setup", G_CALLBACK (setup_listitem_cb), NULL);
  g_signal_connect (across_factory, "bind", G_CALLBACK (bind_listitem_cb), NULL);

  down_factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (down_factory, "setup", G_CALLBACK (setup_listitem_cb), NULL);
  g_signal_connect (down_factory, "bind", G_CALLBACK (bind_listitem_cb), NULL);

  g_object_set (self->across_word_list_view,
                "model", gtk_no_selection_new (G_LIST_MODEL (self->across_list_model)),
                "factory", across_factory,
                NULL);
  g_object_set (self->down_word_list_view,
                "model", gtk_no_selection_new (G_LIST_MODEL (self->down_list_model)),
                "factory", down_factory,
                NULL);
}

static void
edit_xword_class_init (EditXwordClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = edit_xword_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-xword.ui");

  gtk_widget_class_bind_template_callback (widget_class, grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, across_word_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, down_word_activate_cb);

  gtk_widget_class_bind_template_child (widget_class, EditXword, edit_grid);
  gtk_widget_class_bind_template_child (widget_class, EditXword, across_word_list_view);
  gtk_widget_class_bind_template_child (widget_class, EditXword, down_word_list_view);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_c, GDK_CONTROL_MASK, "clipboard.copy", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_v, GDK_CONTROL_MASK, "clipboard.paste", NULL);

  gtk_widget_class_install_action (widget_class, "clipboard.copy", NULL,
                                   (GtkWidgetActionActivateFunc) edit_xword_activate_clipboard_copy);
  gtk_widget_class_install_action (widget_class, "clipboard.paste", NULL,
                                   (GtkWidgetActionActivateFunc) edit_xword_activate_clipboard_paste);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_xword_dispose (GObject *object)
{
  EditXword *self;
  GtkWidget *child;

  self = EDIT_XWORD (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);
  g_clear_pointer (&self->state, xword_state_free);

  G_OBJECT_CLASS (edit_xword_parent_class)->dispose (object);
}

static void
edit_xword_activate_clipboard_copy (EditXword  *self,
                                    const char *name,
                                    GVariant   *parameter)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  if (self->state == NULL)
    return;

  /* If direction is NONE, we're probably on a block */
  if (self->state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->state->xword,
                                              self->state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
paste_received (GObject      *clipboard,
                GAsyncResult *result,
                gpointer      data)
{
  EditXword *self;
  g_autofree gchar *text = NULL;
  g_autofree gchar *upper = NULL;
  IPuzClueDirection direction;

  self = EDIT_XWORD (data);
  text = gdk_clipboard_read_text_finish (GDK_CLIPBOARD (clipboard), result, NULL);
  if (text == NULL || self->state == NULL)
    {
      /* Strange text to get. */
      gtk_widget_error_bell (GTK_WIDGET (self));
      g_object_unref (self);
      return;
    }

  direction = self->state->clue.direction;
  upper = g_utf8_strup (text, -1);
  /* If we're on a BLOCK, arbitrarily paste across. */
  if (direction == IPUZ_CLUE_DIRECTION_NONE)
    direction = IPUZ_CLUE_DIRECTION_ACROSS;

  self->state = xword_state_replace (self->state,
                                     xword_state_guess_word (self->state,
                                                             self->state->cursor,
                                                             direction, upper));
  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID, IPUZ_PUZZLE (self->state->xword));
  g_object_unref (self);
}

static void
edit_xword_activate_clipboard_paste (EditXword  *self,
                                     const char *name,
                                     GVariant   *parameter)
{
  GdkClipboard *clipboard;

  if (self->state == NULL)
    return;

  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_read_text_async (clipboard, NULL, paste_received, g_object_ref (self));

}

/* Called when a new frame is pushed on the stack */
static void
edit_xword_puzzle_stack_changed_new (EditXword   *self,
                                     PuzzleStack *puzzle_stack)
{
  IPuzPuzzle *puzzle;
  PuzzleStackChangeType change_type;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  change_type = puzzle_stack_get_change_type (puzzle_stack);

  if (puzzle == NULL)
    {
      g_clear_pointer (&self->state, xword_state_free);
      /* FIXME: set some sort of unloaded visual state */
    }
  else /* We have a puzzle to work with */
    {
      /* We always create a new state and reset the state when we get a new puzzle */
      if (change_type == STACK_CHANGE_PUZZLE)
        {
          self->state = xword_state_replace (self->state, xword_state_new (IPUZ_CROSSWORD (puzzle), NULL, XWORD_STATE_EDIT));
        }
      else
        {
          /* We may have a similar looking puzzle, but it's a new-to-us puzzle
           * so we update the state as best as we can */
          if (self->state->xword != IPUZ_CROSSWORD (puzzle))
            {
              self->state = xword_state_replace (self->state, xword_state_dehydrate (self->state));
              self->state = xword_state_replace (self->state, xword_state_hydrate (self->state,
                                                                                   IPUZ_CROSSWORD (puzzle),
                                                                                   NULL));
            }
        }
    }

  if (self->state)
    {
      XwordState *dehydrated_state;
      dehydrated_state = xword_state_dehydrate (self->state);
      puzzle_stack_set_data (puzzle_stack,
                             "edit-xword",
                             dehydrated_state,
                             (GDestroyNotify) xword_state_free);
    }
}

static void
edit_xword_puzzle_stack_changed_load (EditXword   *self,
                                      PuzzleStack *puzzle_stack)
{
  IPuzPuzzle *puzzle;
  XwordState *dehydrated_state;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  dehydrated_state = puzzle_stack_get_data (puzzle_stack, "edit-xword");

  if (dehydrated_state)
    {
      self->state = xword_state_replace (self->state, xword_state_hydrate (dehydrated_state,
                                                                           IPUZ_CROSSWORD (puzzle),
                                                                           NULL));
    }
  else
    {
      if (self->state)
        g_clear_pointer (&self->state, xword_state_free);
    }
}

static void
edit_xword_puzzle_stack_changed (EditXword             *self,
                                 gboolean               new_frame,
                                 PuzzleStackChangeType  change_type,
                                 PuzzleStack           *puzzle_stack)
{
  IPuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  if (new_frame)
    {
      edit_xword_puzzle_stack_changed_new (self, puzzle_stack);
    }
  else
    {
      edit_xword_puzzle_stack_changed_load (self, puzzle_stack);
    }

  /* Update the current summetry UI */
  /* FIXME: we need to store this. I don't think this is right */
  if (puzzle)
    {
      IPuzSymmetry symmetry;
      /* Match the symmetry to the natural symmetry in the puzzle */
      symmetry = ipuz_crossword_get_symmetry (IPUZ_CROSSWORD (puzzle));
      switch (symmetry)
        {
        case IPUZ_SYMMETRY_NONE:
          g_settings_set_string (self->settings, "symmetry", "none");
          break;
        case IPUZ_SYMMETRY_HALF:
          g_settings_set_string (self->settings, "symmetry", "half");
          break;
        case IPUZ_SYMMETRY_QUARTER:
          g_settings_set_string (self->settings, "symmetry", "quarter");
          break;
        default:
          g_assert_not_reached ();
        }
    }

  /* Is this right? */
  if (change_type == STACK_CHANGE_PUZZLE ||
      change_type == STACK_CHANGE_GRID)
    edit_xword_update_states (self);
}

static void
symmetry_changed_cb (EditXword *xword)
{
  g_autofree gchar *symmetry_str = NULL;
  IPuzSymmetry new_symmetry;

  if (xword->state == NULL || xword->state->xword == NULL)
    return;

  symmetry_str = g_settings_get_string (xword->settings, "symmetry");
  if (g_strcmp0 (symmetry_str, "none") == 0)
    new_symmetry = IPUZ_SYMMETRY_NONE;
  else if (g_strcmp0 (symmetry_str, "half") == 0)
    new_symmetry = IPUZ_SYMMETRY_HALF;
  else if (g_strcmp0 (symmetry_str, "quarter") == 0)
    new_symmetry = IPUZ_SYMMETRY_QUARTER;
  else
    g_assert_not_reached ();

  /* FIXME: warn on destructive changes */
  if (ipuz_crossword_get_symmetry (xword->state->xword) != new_symmetry)
    ipuz_crossword_set_symmetry (xword->state->xword, new_symmetry);
}


static void
grid_guess_cb (PlayGrid  *grid,
               gchar     *guess,
               EditXword *xword)
{
  xword->state = xword_state_replace (xword->state, xword_state_guess (xword->state, guess));

  puzzle_stack_push_change (xword->puzzle_stack, STACK_CHANGE_GRID, IPUZ_PUZZLE (xword->state->xword));
}

static void
grid_guess_at_cell_cb (PlayGrid  *grid,
                       gchar     *guess,
                       guint      row,
                       guint      column,
                       EditXword *xword)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column
  };

  xword->state = xword_state_replace (xword->state, xword_state_guess_at_cell (xword->state, guess, coord));

  puzzle_stack_push_change (xword->puzzle_stack, STACK_CHANGE_GRID, IPUZ_PUZZLE (xword->state->xword));
}


static void
grid_do_command_cb (PlayGrid        *grid,
                    XwordCmdKind     kind,
                    EditXword       *xword)
{
  xword->state = xword_state_replace (xword->state,
                                      xword_state_do_command (xword->state, kind));

  /* We don't need to push a change for just a focus change */
  edit_xword_update_states (xword);
}

static void
grid_cell_selected_cb (PlayGrid *grid,
                       guint     row,
                       guint     column,
                       EditXword *xword)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  xword->state = xword_state_replace (xword->state, xword_state_cell_selected (xword->state, coord));

  edit_xword_update_states (xword);
}

static void
set_focused_cell (EditXword *xword)
{
  if (XWORD_STATE_HAS_CURSOR (xword->state))
    {
      play_grid_focus_cell (PLAY_GRID (xword->edit_grid), xword->state->cursor);
    }
}

static void
word_activate (EditXword         *xword,
               guint              position,
               IPuzClueDirection  direction)
{
  GListModel *model;
  WordListModelRow *row;
  const gchar *word;
  IPuzClue *clue;
  IPuzCellCoord coord;

  if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
    model = G_LIST_MODEL (xword->across_list_model);
  else
    model = G_LIST_MODEL (xword->down_list_model);

  row = g_list_model_get_item (model, position);
  word = word_list_model_row_get_word (row);

  /* We want to paste the word from the start of the clue. We do that
   * by selecting that position and setting up the direction
   * correctly. It's a little convoluted, but means we can paste
   * unmodified. */
  clue = ipuz_crossword_find_clue_by_coord (xword->state->xword,
                                            direction,
                                            xword->state->cursor);
  g_assert (clue);
  ipuz_clue_get_first_cell (clue, &coord);

  xword->state = xword_state_replace (xword->state,
                                      xword_state_cell_selected (xword->state, coord));
  if (xword->state->clue.direction != direction)
    xword->state = xword_state_replace (xword->state,
                                        xword_state_cell_selected (xword->state, coord));

  xword->state = xword_state_replace
    (xword->state,
     xword_state_guess_word (xword->state,
                             xword->state->cursor,
                             direction, word));

  puzzle_stack_push_change (xword->puzzle_stack, STACK_CHANGE_GRID, IPUZ_PUZZLE (xword->state->xword));
}

static void
across_word_activate_cb (EditXword *xword,
                         guint      position)
{
  word_activate (xword, position, IPUZ_CLUE_DIRECTION_ACROSS);
}

static void
down_word_activate_cb (EditXword *xword,
                       guint      position)
{
  word_activate (xword, position, IPUZ_CLUE_DIRECTION_DOWN);
}

static void
edit_xword_update_state (EditXword  *xword,
                         XwordState *state)
{
  IPuzClue *across_clue;
  IPuzClue *down_clue;
  g_autofree gchar *across_clue_string = NULL;
  g_autofree gchar *down_clue_string = NULL;

  if (! XWORD_STATE_HAS_CURSOR (state))
    {
      word_list_model_set_filter (xword->across_list_model, NULL);
      word_list_model_set_filter (xword->down_list_model, NULL);
      return;
    }

  across_clue = ipuz_crossword_find_clue_by_coord (state->xword,
                                                   IPUZ_CLUE_DIRECTION_ACROSS,
                                                   state->cursor);
  down_clue = ipuz_crossword_find_clue_by_coord (state->xword,
                                                 IPUZ_CLUE_DIRECTION_DOWN,
                                                 state->cursor);

  if (across_clue)
    {
      IPuzClueId clue_id = ipuz_crossword_get_clue_id (state->xword, across_clue);
      across_clue_string = ipuz_crossword_get_clue_string_by_id (state->xword, clue_id);
    }

  if (down_clue)
    {
      IPuzClueId clue_id = ipuz_crossword_get_clue_id (state->xword, down_clue);
      down_clue_string = ipuz_crossword_get_clue_string_by_id (state->xword, clue_id);
    }

  word_list_model_set_filter (xword->across_list_model, across_clue_string);
  word_list_model_set_filter (xword->down_list_model, down_clue_string);
}

static void
edit_xword_update_states (EditXword *xword)
{
  if (xword->state == NULL)
    return;
  play_grid_update_state (PLAY_GRID (xword->edit_grid), xword->state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  edit_xword_update_state (xword, xword->state);
  set_focused_cell (xword);
}

/* Public methods */

void
edit_xword_set_puzzle_stack (EditXword   *xword,
                             PuzzleStack *puzzle_stack)
{
  g_return_if_fail (EDIT_IS_XWORD (xword));

  if (puzzle_stack)
    g_object_ref (puzzle_stack);

  g_clear_signal_handler (&xword->changed_id,
                          xword->puzzle_stack);
  g_clear_object (&xword->puzzle_stack);

  xword->puzzle_stack = puzzle_stack;
  xword->changed_id = g_signal_connect_swapped (xword->puzzle_stack,
                                                "changed",
                                                G_CALLBACK (edit_xword_puzzle_stack_changed),
                                                xword);

  /* Force a changed signal to get the inital setup done */
  edit_xword_puzzle_stack_changed (xword, TRUE, STACK_CHANGE_PUZZLE, puzzle_stack);
}
