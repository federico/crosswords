/* edit-cell.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "play-cell.h"
#include "play-state.h"

G_BEGIN_DECLS

#define EDIT_TYPE_CELL (edit_cell_get_type())

G_DECLARE_FINAL_TYPE (EditCell, edit_cell, EDIT, CELL, GtkWidget)


GtkWidget *edit_cell_new        (void);
void       edit_cell_load_state (EditCell      *edit,
                                 IPuzCrossword *xword);

G_END_DECLS
