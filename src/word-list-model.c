/* word-list-model.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <json-glib/json-glib.h>

#include "word-list.h"
#include "word-list-model.h"

struct _WordListModel
{
  GObject parent_object;
  WordList *word_list;
  GArray *obj_list;
};


static void     word_list_model_init            (WordListModel       *self);
static void     word_list_model_class_init      (WordListModelClass  *klass);
static void     word_list_model_list_model_init (GListModelInterface *iface);
static void     word_list_model_dispose         (GObject             *object);
static GType    word_list_model_get_item_type   (GListModel          *list);
static guint    word_list_model_get_n_items     (GListModel          *list);
static gpointer word_list_model_get_item        (GListModel          *list,
                                                 guint                position);

static void     word_list_model_row_init       (WordListModelRow       *self);
static void     word_list_model_row_class_init (WordListModelRowClass  *klass);


G_DEFINE_TYPE_WITH_CODE (WordListModel, word_list_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                word_list_model_list_model_init));

G_DEFINE_TYPE (WordListModelRow, word_list_model_row, G_TYPE_OBJECT);



static void
obj_list_clear (WordListModelRow **data)
{
  if (*data != NULL)
    {
      g_object_unref (G_OBJECT (*data));
      *data = NULL;
    }
}

static void
word_list_model_init (WordListModel *self)
{
  self->word_list = word_list_new ();
  self->obj_list = g_array_new (FALSE, TRUE, sizeof (WordListModelRow *));
  g_array_set_clear_func (self->obj_list, (GDestroyNotify ) obj_list_clear);
}

static void
word_list_model_class_init (WordListModelClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_list_model_dispose;
}

static void
word_list_model_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = word_list_model_get_item_type;
  iface->get_n_items = word_list_model_get_n_items;
  iface->get_item = word_list_model_get_item;
}

static void
word_list_model_dispose (GObject *object)
{
  WordListModel *self;

  self = WORD_LIST_MODEL (object);

  g_clear_object (&self->word_list);
  g_clear_pointer (&self->obj_list, g_array_unref);

  G_OBJECT_CLASS (word_list_model_parent_class)->dispose (object);
}


static GType
word_list_model_get_item_type (GListModel *model)
{
  return WORD_TYPE_LIST_MODEL_ROW;
}

static guint
word_list_model_get_n_items (GListModel *model)
{
  WordListModel *list_model;

  list_model = WORD_LIST_MODEL (model);

  return list_model->obj_list->len;
}

static gpointer
word_list_model_get_item (GListModel *model,
                          guint       position)
{
  WordListModel *list_model;
  WordListModelRow *row;
  const gchar *word;
  gint priority;

  list_model = WORD_LIST_MODEL (model);

  row  = g_array_index (list_model->obj_list, WordListModelRow *, position);
  word = word_list_get_word (list_model->word_list, position);
  priority = word_list_get_priority (list_model->word_list, position);

  if (row == NULL)
    {
      WordListModelRow **row_ptr = &(g_array_index (list_model->obj_list, WordListModelRow *, position));
      row = word_list_model_row_new (word, priority);
      *row_ptr = row;
    }
  else
    {
      row->word = word;
      row->priority = priority;
    }

  return g_object_ref (row);
}


WordListModel *
word_list_model_new (void)
{
  return (WordListModel *) g_object_new (WORD_TYPE_LIST_MODEL, NULL);
}


void
word_list_model_set_filter (WordListModel *list_model,
                            const char    *filter)
{
  gint old_len;

  g_return_if_fail (WORD_IS_LIST_MODEL (list_model));

  old_len = word_list_get_n_items (list_model->word_list);
  word_list_set_filter (list_model->word_list, filter);

  g_array_set_size (list_model->obj_list, word_list_get_n_items (list_model->word_list));
  g_list_model_items_changed (G_LIST_MODEL (list_model),
                              0, old_len,
                              word_list_get_n_items (list_model->word_list));

}

void word_list_model_set_threshold (WordListModel *list_model,
                                    gint           threshold)
{

}


/* RowModel */

static void
word_list_model_row_init (WordListModelRow *self)
{

}

static void
word_list_model_row_class_init (WordListModelRowClass *klass)
{

}

const gchar *
word_list_model_row_get_word (WordListModelRow *row)
{
  return row->word;
}

gint
word_list_model_row_get_priority (WordListModelRow *row)
{
  return row->priority;
}

WordListModelRow *
word_list_model_row_new (const gchar *word,
                         gint         priority)
{
  WordListModelRow *row;

  row = g_object_new (WORD_TYPE_LIST_MODEL_ROW, NULL);
  row->word = word;
  row->priority = priority;

  return row;
}
