/* puzzle-button-icon.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "puzzle-button-icon.h"


struct _PuzzleButtonIcon
{
  GObject parent_instance;

  GdkTexture *thumb;
  GdkTexture *stencil;
};

struct _PuzzleButtonIconClass
{
  GObjectClass parent_class;
};


static void              puzzle_button_icon_init           (PuzzleButtonIcon      *self);
static void              puzzle_button_icon_class_init     (PuzzleButtonIconClass *klass);
static void              puzzle_button_icon_paintable_init (GdkPaintableInterface *iface);
static void              puzzle_button_icon_dispose        (GObject               *object);
static void              puzzle_button_icon_snapshot       (GdkPaintable          *paintable,
                                                            GtkSnapshot           *snapshot,
                                                            double                 width,
                                                            double                 height);
static GdkPaintableFlags puzzle_button_icon_get_flags      (GdkPaintable          *paintable);


G_DEFINE_TYPE_WITH_CODE (PuzzleButtonIcon, puzzle_button_icon, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE,
                                                puzzle_button_icon_paintable_init));

static void
puzzle_button_icon_init (PuzzleButtonIcon *self)
{
  /* pass */
}

static void
puzzle_button_icon_class_init (PuzzleButtonIconClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = puzzle_button_icon_dispose;
}

static void
puzzle_button_icon_dispose (GObject *object)
{
  PuzzleButtonIcon *self = PUZZLE_BUTTON_ICON (object);

  g_clear_object (&self->thumb);
  g_clear_object (&self->stencil);

  G_OBJECT_CLASS (puzzle_button_icon_parent_class)->dispose (object);
}

static void
puzzle_button_icon_paintable_init (GdkPaintableInterface *iface)
{
  iface->snapshot = puzzle_button_icon_snapshot;
  iface->get_flags = puzzle_button_icon_get_flags;
}

static void
puzzle_button_icon_snapshot (GdkPaintable *paintable,
                             GtkSnapshot  *snapshot,
                             double        width,
                             double        height)
{
  PuzzleButtonIcon *self = PUZZLE_BUTTON_ICON (paintable);
  gint stencil_width, stencil_height;

  stencil_width = gdk_paintable_get_intrinsic_width (GDK_PAINTABLE (self->stencil));
  stencil_height = gdk_paintable_get_intrinsic_height (GDK_PAINTABLE (self->stencil));

  gtk_snapshot_save (snapshot);
  gtk_snapshot_push_opacity (snapshot, 0.25);
  gdk_paintable_snapshot (GDK_PAINTABLE (self->thumb), snapshot, width, height);
  gtk_snapshot_pop (snapshot);
  gtk_snapshot_translate (snapshot,
                          &GRAPHENE_POINT_INIT (width - stencil_width, height - stencil_height));
  gdk_paintable_snapshot (GDK_PAINTABLE (self->stencil),
                          snapshot, stencil_width, stencil_height);
  gtk_snapshot_restore (snapshot);
}

static GdkPaintableFlags
puzzle_button_icon_get_flags (GdkPaintable *paintable)
{
  return GDK_PAINTABLE_STATIC_SIZE | GDK_PAINTABLE_STATIC_CONTENTS;
}

/* Public functions */
GdkPaintable *
puzzle_button_icon_new (GdkPixbuf *thumb)
{
  PuzzleButtonIcon *self;

  self = g_object_new (PUZZLE_TYPE_BUTTON_ICON, NULL);

  self->thumb = gdk_texture_new_for_pixbuf (thumb);
  self->stencil = gdk_texture_new_from_resource ("/org/gnome/Crosswords/icons/scalable/apps/crossword-solved.svg");

  return GDK_PAINTABLE (self);
}
