/* edit-basics.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "basic-templates.h"
#include "crosswords-misc.h"
#include "edit-basics.h"

/* This is used to increment the "My Crossword-%u" title string */
static guint instance_count = 0;

enum {
  ACTIVATED,
  N_SIGNALS
};
static guint obj_signals[N_SIGNALS] = { 0 };

struct _EditBasics
{
  GtkWidget parent_instance;

  PuzzleStack *puzzle_stack;
  gulong changed_id;

  BasicType type;
  BasicSize size;
  guint custom_width;
  guint custom_height;
  gboolean locked;

  GtkWidget *basic_vbox;
  GtkWidget *unlock_button;

  GtkWidget *type_row;
  GtkWidget *template_grid_view;

  /* Dialog */
  GtkWidget *basics_size_dialog;
  GtkWidget *basics_size_row;
  GtkWidget *basics_size_width_spin;
  GtkWidget *basics_size_height_spin;

};


static void edit_basics_init                 (EditBasics            *self);
static void edit_basics_class_init           (EditBasicsClass       *klass);
static void edit_basics_dispose              (GObject               *object);
static void edit_basics_update_presets       (EditBasics            *edit_basics);
static void edit_basics_lock                 (EditBasics            *edit_basics);
static void edit_basics_unlock               (EditBasics            *edit_basics);
static void edit_basics_puzzle_stack_changed (EditBasics            *edit_basics,
                                              gboolean               new_frame,
                                              PuzzleStackChangeType  type,
                                              PuzzleStack           *puzzle_stack);
static void unlock_clicked_cb                (EditBasics            *edit_basics);
static void type_selected_cb                 (EditBasics            *edit_basics,
                                              GParamSpec            *pspec,
                                              AdwComboRow           *action_row);
static void size_dialog_response_cb          (EditBasics            *edit_basics,
                                              gint                   response_id,
                                              GtkDialog             *dialog);
static void size_selected_cb                 (EditBasics            *edit_basics,
                                              GParamSpec            *pspec,
                                              AdwComboRow           *action_row);
static void template_activated_cb            (GtkWidget             *edit_basics,
                                              guint                  position,
                                              GtkFlowBoxChild       *child);


G_DEFINE_TYPE (EditBasics, edit_basics, GTK_TYPE_WIDGET);



static void
setup_listitem_cb (GtkListItemFactory *factory,
                   GtkListItem        *list_item)
{
  GtkWidget *image;

  image = gtk_image_new ();
  gtk_widget_set_hexpand (image, FALSE);
  gtk_widget_set_size_request (image, 120, 120);

  gtk_list_item_set_child (list_item, image);
}

static void
bind_listitem_cb (GtkListItemFactory *factory,
                  GtkListItem        *list_item)
{
  GtkWidget *image;
  BasicTemplateRow *row;

  image = gtk_list_item_get_child (list_item);
  row = (BasicTemplateRow *) gtk_list_item_get_item (list_item);
  gtk_image_set_from_pixbuf (GTK_IMAGE (image),
                             basic_template_row_get_pixbuf (row));
}

static void
edit_basics_init (EditBasics *self)
{
  GtkLayoutManager *box_layout;
  g_autoptr (GtkListItemFactory) template_factory = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->locked = FALSE;
  self->type = BASIC_TYPE_STANDARD;
  self->size = BASIC_SIZE_LARGE;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
  adw_combo_row_set_selected (ADW_COMBO_ROW (self->basics_size_row), BASIC_SIZE_LARGE);
  /* Set up the template grid view */
  template_factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (template_factory, "setup", G_CALLBACK (setup_listitem_cb), NULL);
  g_signal_connect (template_factory, "bind", G_CALLBACK (bind_listitem_cb), NULL);
  gtk_grid_view_set_factory (GTK_GRID_VIEW (self->template_grid_view),
                             template_factory);
                             
  edit_basics_update_presets (self);
}

static void
edit_basics_class_init (EditBasicsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_basics_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-basics.ui");

  gtk_widget_class_bind_template_child (widget_class, EditBasics, basic_vbox);
  gtk_widget_class_bind_template_child (widget_class, EditBasics, unlock_button);

  gtk_widget_class_bind_template_child (widget_class, EditBasics, type_row);
  gtk_widget_class_bind_template_child (widget_class, EditBasics, template_grid_view);

  gtk_widget_class_bind_template_child (widget_class, EditBasics, basics_size_row);
  gtk_widget_class_bind_template_child (widget_class, EditBasics, basics_size_dialog);
  gtk_widget_class_bind_template_child (widget_class, EditBasics, basics_size_width_spin);
  gtk_widget_class_bind_template_child (widget_class, EditBasics, basics_size_height_spin);

  gtk_widget_class_bind_template_callback (widget_class, unlock_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, type_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, size_dialog_response_cb);
  gtk_widget_class_bind_template_callback (widget_class, size_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, template_activated_cb);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  obj_signals [ACTIVATED] =
    g_signal_new ("activated",
                  EDIT_TYPE_BASICS,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}


static void
edit_basics_dispose (GObject *object)
{
  EditBasics *self;
  GtkWidget *child;

  self = EDIT_BASICS (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);

  G_OBJECT_CLASS (edit_basics_parent_class)->dispose (object);
}

static void
edit_basics_lock (EditBasics *edit_basics)
{
  if (edit_basics->locked)
    return;

  gtk_widget_set_sensitive (edit_basics->basic_vbox, FALSE);
  gtk_widget_set_visible (edit_basics->unlock_button, TRUE);
  gtk_widget_grab_focus (edit_basics->unlock_button);
  edit_basics->locked = TRUE;

  /* FIXME: Update the UI to match the current puzzle */
}

static void
edit_basics_unlock (EditBasics *edit_basics)
{
  if (! edit_basics->locked)
    return;

  gtk_widget_set_sensitive (edit_basics->basic_vbox, TRUE);
  gtk_widget_set_visible (edit_basics->unlock_button, FALSE);
  gtk_widget_grab_focus (edit_basics->type_row);
  edit_basics->locked = FALSE;
}

/* This behaves very differently than other PuzzleStack::changed
 * callbacks.
 */
static void
edit_basics_puzzle_stack_changed (EditBasics            *edit_basics,
                                  gboolean               new_frame,
                                  PuzzleStackChangeType  change_type,
                                  PuzzleStack           *puzzle_stack)
{
  IPuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  if (puzzle == NULL)
    edit_basics_unlock (edit_basics);
  else
    edit_basics_lock (edit_basics);
}



static void
edit_basics_update_presets (EditBasics *edit_basics)
{
  GListModel *model;

  if (edit_basics->size == BASIC_SIZE_CUSTOM)
    model = basic_template_create_custom_model (edit_basics->custom_width, edit_basics->custom_height);
  else
    model = basic_template_get_model (edit_basics->type,
                                      edit_basics->size);
  gtk_grid_view_set_model (GTK_GRID_VIEW (edit_basics->template_grid_view),
                           GTK_SELECTION_MODEL (model));
  gtk_selection_model_select_item (GTK_SELECTION_MODEL (model),
                                   0, TRUE);
}


static void
unlock_response_cb (EditBasics *edit_basics,
                    gint        response_id,
                    GtkWindow  *dialog)
{
  if (response_id == GTK_RESPONSE_APPLY)
    puzzle_stack_push_change (edit_basics->puzzle_stack, STACK_CHANGE_PUZZLE, NULL);

  gtk_window_destroy (dialog);
}

static void
unlock_clicked_cb (EditBasics *edit_basics)
{
  GtkWidget *dialog;
  GtkWindow *parent_window;
  GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;

  parent_window = GTK_WINDOW (gtk_widget_get_root (GTK_WIDGET (edit_basics)));
  dialog = gtk_message_dialog_new (parent_window,
                                   flags,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_NONE,
                                   _("You have made changes to your puzzle. Clearing the puzzle will allow you to create a new puzzle but will overwrite any changes you've made.\n\nDo you want to clear this puzzle and start a new one?"));

  gtk_dialog_add_buttons (GTK_DIALOG (dialog),
                          _("_Cancel"),
                          GTK_RESPONSE_CANCEL,
                          _("C_lear"),
                          GTK_RESPONSE_APPLY,
                          NULL);

  g_signal_connect_swapped (dialog, "response",
                            G_CALLBACK (unlock_response_cb),
                            edit_basics);
  gtk_widget_show (dialog);

}

/* FIXME(gtk): This is super unreliable due to:
 * https://gitlab.gnome.org/GNOME/gtk/-/issues/2877
 * I don't know what to do about it.
 */
static void
type_selected_cb (EditBasics  *edit_basics,
                  GParamSpec  *pspec,
                  AdwComboRow *combo_row)
{
  BasicType new_type = adw_combo_row_get_selected (combo_row);
  if (edit_basics->type != new_type)
    {
      edit_basics->type = new_type;
      edit_basics_update_presets (edit_basics);
    }
}

static void
size_dialog_response_cb (EditBasics *edit_basics,
                         gint        response_id,
                         GtkDialog  *dialog)
{
  if (response_id == GTK_RESPONSE_APPLY)
    {
      guint new_width, new_height;

      new_width = (guint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (edit_basics->basics_size_width_spin));
      new_height = (guint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (edit_basics->basics_size_height_spin));

      if (new_width == SMALL_SIZE &&
          new_height == SMALL_SIZE)
        {
          edit_basics->size = BASIC_SIZE_SMALL;
        }
      else if (new_width == MEDIUM_SIZE &&
               new_height == MEDIUM_SIZE)
        {
          edit_basics->size = BASIC_SIZE_MEDIUM;
        }
      else if (new_width == LARGE_SIZE &&
               new_height == LARGE_SIZE)
        {
          edit_basics->size = BASIC_SIZE_LARGE;

        }
      else if (new_width == X_LARGE_SIZE &&
               new_height == X_LARGE_SIZE)
        {
          edit_basics->size = BASIC_SIZE_X_LARGE;
        }
      else
        {
          edit_basics->size = BASIC_SIZE_CUSTOM;
          edit_basics->custom_width = new_width;
          edit_basics->custom_height = new_height;
        }

      edit_basics_update_presets (edit_basics);
    }
  gtk_widget_hide (GTK_WIDGET (dialog));
}

static void
size_selected_cb  (EditBasics  *edit_basics,
                   GParamSpec  *pspec,
                   AdwComboRow *action_row)
{
  BasicSize new_size = adw_combo_row_get_selected (action_row);
  if (new_size == BASIC_SIZE_CUSTOM)
    {
      GtkWindow *window;

      window = (GtkWindow *) gtk_widget_get_root (GTK_WIDGET (edit_basics));
      gtk_window_set_transient_for (GTK_WINDOW (edit_basics->basics_size_dialog),
                                    window);

      gtk_widget_show (edit_basics->basics_size_dialog);
      return;
    }
  if (edit_basics->size != new_size)
    {
      edit_basics->size = new_size;
      edit_basics_update_presets (edit_basics);
    }
}

static void
template_activated_cb (GtkWidget       *edit_basics,
                       guint            position,
                       GtkFlowBoxChild *child)
{
  g_signal_emit (edit_basics, obj_signals [ACTIVATED], 0);
}

/* Public methods */

void
edit_basics_lock_and_write (EditBasics *edit_basics)
{
  g_return_if_fail (EDIT_IS_BASICS (edit_basics));
  g_return_if_fail (PUZZLE_IS_STACK (edit_basics->puzzle_stack));

  if (! edit_basics->locked)
    {
      GtkSelectionModel *model;
      BasicTemplateRow *row;
      IPuzPuzzle *template;
      g_autoptr (IPuzPuzzle) puzzle = NULL;

      model = gtk_grid_view_get_model (GTK_GRID_VIEW (edit_basics->template_grid_view));
      row = gtk_single_selection_get_selected_item (GTK_SINGLE_SELECTION (model));
      template = basic_template_row_get_template (row);

      if (template)
        {
          puzzle = ipuz_puzzle_deep_copy (template);
          /* Make sure we have clues on the new puzzle */
          ipuz_crossword_calculate_clues (IPUZ_CROSSWORD (puzzle));
        }

      if (puzzle)
        {
          g_autofree gchar *title_str = NULL;

          if (instance_count == 0)
            title_str = g_strdup ("My Crossword");
          else
            title_str = g_strdup_printf ("My Crossword-%u", instance_count);
          instance_count++;

          g_object_set (puzzle,
                        "showenumerations", (edit_basics->type == BASIC_TYPE_CRYPTIC),
                        "title", title_str,
                        NULL);
        }

      edit_basics_lock (edit_basics);

      if (puzzle)
        puzzle_stack_push_change (edit_basics->puzzle_stack, STACK_CHANGE_PUZZLE, puzzle);
    }
}

void
edit_basics_set_puzzle_stack (EditBasics  *edit_basics,
                              PuzzleStack *puzzle_stack)
{
  g_return_if_fail (EDIT_IS_BASICS (edit_basics));

  if (puzzle_stack)
    g_object_ref (puzzle_stack);

  g_clear_signal_handler (&edit_basics->changed_id,
                          edit_basics->puzzle_stack);
  g_clear_object (&edit_basics->puzzle_stack);
  edit_basics->puzzle_stack = puzzle_stack;

  edit_basics->changed_id = g_signal_connect_swapped (edit_basics->puzzle_stack,
                                                      "changed",
                                                      G_CALLBACK (edit_basics_puzzle_stack_changed),
                                                      edit_basics);

  /* Force a changed signal to get the inital setup done */
  edit_basics_puzzle_stack_changed (edit_basics, TRUE, STACK_CHANGE_PUZZLE, puzzle_stack);
}
