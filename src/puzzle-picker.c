/* puzzle-set.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>

#include "crosswords-misc.h"
#include "puzzle-set.h"
#include "puzzle-picker.h"


enum
{
  PROP_0,
  PROP_KEY_FILE,
  PROP_ID,
  PROP_DIRECTORY,
  PROP_RESOURCE,
  N_PROPS
};

enum {
  PUZZLE_SELECTED,
  N_SIGNALS
};

enum {
  RESPONSE_CANCEL,
  RESPONSE_VIEW,
  RESPONSE_CLEAR,
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


#define DOMAIN_DOWNLOADS     "downloads"
#define DOMAIN_SAVED_PUZZLES "saved-puzzles"


typedef struct _PuzzleInfo
{
  IPuzPuzzle *puzzle;
  gchar *puzzle_name;
  gchar *last_saved_checksum;
  gboolean readonly;  /* TRUE, when puzzle is loaded from the resource file */
  GError *loading_error;
  gchar *uri;
} PuzzleInfo;

typedef struct
{
  gchar *id;
  GKeyFile *key_file;
  GResource *resource;
  GArray *puzzles;
  PuzzleInfo *selected;
  GHashTable *skip_puzzles;

  gchar *directory;

  /* Clear dialog information */
  IPuzPuzzle *clear_puzzle;
  gboolean clear_play_when_done;
} PuzzlePickerPrivate;


static void         puzzle_picker_init               (PuzzlePicker       *self);
static void         puzzle_picker_class_init         (PuzzlePickerClass  *klass);
static void         puzzle_picker_set_property       (GObject            *object,
                                                      guint               prop_id,
                                                      const GValue       *value,
                                                      GParamSpec         *pspec);
static void         puzzle_picker_get_property       (GObject            *object,
                                                      guint               prop_id,
                                                      GValue             *value,
                                                      GParamSpec         *pspec);
static void         puzzle_picker_dispose            (GObject            *object);
static void         puzzle_picker_finalize           (GObject            *object);
static void         puzzle_picker_load_maybe         (PuzzlePicker       *picker);
static void         puzzle_picker_real_load          (PuzzlePicker       *picker);
static const gchar *puzzle_picker_real_get_title     (PuzzlePicker       *picker);
static const gchar *puzzle_picker_real_get_uri       (PuzzlePicker       *picker);
static void         puzzle_picker_real_save_state    (PuzzlePicker       *picker);
static void         puzzle_picker_real_update_state  (PuzzlePicker       *picker);
static void         puzzle_picker_real_load_uri      (PuzzlePicker       *picker,
                                                      const gchar        *uri,
                                                      gboolean            delete_when_done,
                                                      GError            **error);
static void         puzzle_picker_real_clear_puzzle  (PuzzlePicker       *picker,
                                                      IPuzPuzzle         *puzzle,
                                                      const gchar        *primary,
                                                      const gchar        *secondary,
                                                      gboolean            play_when_done,
                                                      gboolean            allow_view);

static void         puzzle_picker_delete_puzzle_name (const gchar        *id,
                                                      const gchar        *puzzle_name);
static PuzzleInfo  *puzzle_picker_find_puzzle_info   (PuzzlePicker       *picker,
                                                      IPuzPuzzle         *puzzle);
static gchar       *build_file_path                  (const gchar        *domain,
                                                      const gchar        *id,
                                                      const gchar        *puzzle_name);


G_DEFINE_TYPE_WITH_PRIVATE (PuzzlePicker, puzzle_picker, GTK_TYPE_WIDGET);


static void
puzzle_info_clear (PuzzleInfo *info)
{
  g_clear_object (&info->puzzle);
  g_clear_pointer (&info->puzzle_name, g_free);
  g_clear_pointer (&info->last_saved_checksum, g_free);
  g_clear_pointer (&info->uri, g_free);
  g_clear_pointer (&info->loading_error, g_error_free);
}

static void
puzzle_picker_init (PuzzlePicker *self)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (self));

  /* We Have two important structures within the PuzzlePicker:
   * - puzzles: which contains all the puzzles for the
   * - skip_puzzles: which contains puzzles that we don't want to display
   *   but that we can't/won't remove.
   *
   * We reload the puzzles list every time we hit load, but once we run
   * across a file we can't handle, we put it in the skip_puzzle list
   * and never try to load it again.
   */
  priv->puzzles = g_array_new (FALSE, TRUE, sizeof (PuzzleInfo));
  g_array_set_clear_func (priv->puzzles, (GDestroyNotify) puzzle_info_clear);

  priv->skip_puzzles = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
}

static void
puzzle_picker_class_init (PuzzlePickerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = puzzle_picker_get_property;
  object_class->set_property = puzzle_picker_set_property;
  object_class->dispose = puzzle_picker_dispose;
  object_class->finalize = puzzle_picker_finalize;
  klass->load = puzzle_picker_real_load;
  klass->get_title = puzzle_picker_real_get_title;
  klass->get_uri = puzzle_picker_real_get_uri;
  klass->save_state = puzzle_picker_real_save_state;
  klass->update_state = puzzle_picker_real_update_state;
  klass->load_uri = puzzle_picker_real_load_uri;
  klass->clear_puzzle = puzzle_picker_real_clear_puzzle;

  obj_props[PROP_KEY_FILE] =
    g_param_spec_boxed ("key-file",
                        "Key File",
                        "picker.config from the resource",
                        G_TYPE_KEY_FILE,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  obj_props[PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "ID of the puzzle-set. Loaded from key-file.",
                         NULL,
                         G_PARAM_READABLE);

  obj_props[PROP_RESOURCE] =
    g_param_spec_boxed ("resource",
                        "Resource",
                        "Resource that the picker set is loaded from",
                        G_TYPE_RESOURCE,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  obj_props[PROP_DIRECTORY] =
    g_param_spec_string ("directory",
                         "Directory",
                         "The root directory to load all puzzles from",
                         NULL,
                         G_PARAM_READABLE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  obj_signals[PUZZLE_SELECTED] =
    g_signal_new ("puzzle-selected",
                  PUZZLE_TYPE_PICKER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_PUZZLE);
}

static void
puzzle_picker_set_property (GObject           *object,
                            guint              prop_id,
                            const GValue      *value,
                            GParamSpec        *pspec)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  switch (prop_id)
    {
    case PROP_KEY_FILE:
      g_clear_pointer (&priv->key_file, g_key_file_unref);
      priv->key_file = (GKeyFile *) g_value_dup_boxed (value);
      puzzle_picker_load_maybe (PUZZLE_PICKER (object));
      break;
    case PROP_RESOURCE:
      g_clear_pointer (&priv->resource, g_resource_unref);
      priv->resource = (GResource *) g_value_dup_boxed (value);
      puzzle_picker_load_maybe (PUZZLE_PICKER (object));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }

}

static void
puzzle_picker_get_property (GObject           *object,
                            guint              prop_id,
                            GValue            *value,
                            GParamSpec        *pspec)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  switch (prop_id)
    {
    case PROP_KEY_FILE:
      g_value_set_boxed (value, priv->key_file);
      break;
    case PROP_ID:
      g_value_set_string (value, priv->id);
      break;
    case PROP_RESOURCE:
      g_value_set_boxed (value, priv->resource);
      break;
    case PROP_DIRECTORY:
      g_value_set_string (value, priv->directory);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }

}

static void
puzzle_picker_dispose (GObject *object)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  g_clear_pointer (&priv->puzzles, g_array_unref);
  g_clear_pointer (&priv->skip_puzzles, g_hash_table_unref);
  g_clear_pointer (&priv->resource, g_resource_unref);
  g_clear_pointer (&priv->key_file, g_key_file_unref);

  G_OBJECT_CLASS (puzzle_picker_parent_class)->dispose (object);
}

static void
puzzle_picker_finalize (GObject *object)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->directory, g_free);

  G_OBJECT_CLASS (puzzle_picker_parent_class)->dispose (object);
}

static IPuzPuzzle *
directory_load_puzzle (const gchar  *id,
                       const gchar  *puzzle_name,
                       GError      **error)
{
  IPuzPuzzle *puzzle;
  g_autoptr (IPuzGuesses) guesses = NULL;
  g_autofree gchar *file_path = NULL;

  file_path = build_file_path (DOMAIN_DOWNLOADS, id, puzzle_name);
  puzzle = ipuz_puzzle_new_from_file (file_path, error);
  if (*error)
    {
      /* FIXME: Warn the user */
      return NULL;
    }

  /* We silently discard errors loading guesses. Maybe we should warn?
   * We can't recover, though. */
  guesses = crosswords_load_guesses (id, puzzle_name);
  if (guesses)
    {
      /* Are the guesses valid, or has the xword changed? */
      if (! ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle), guesses))
        {
          ipuz_guesses_unref (guesses);
          guesses = NULL;
        }
    }

  if (guesses == NULL)
    {
      guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle)), FALSE);
      /* We don't need to check to see if this is valid */
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle), guesses);
    }

  return puzzle;
}

static void
load_puzzles_from_directory (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;
  g_autoptr (GDir) dir = NULL;
  const gchar *file;

  priv = puzzle_picker_get_instance_private (picker);

  g_clear_pointer (&priv->directory, g_free);
  priv->directory = g_build_filename (g_get_user_data_dir (),
                                      "gnome-crosswords",
                                      DOMAIN_DOWNLOADS,
                                      priv->id,
                                      NULL);

  if (g_mkdir_with_parents (priv->directory, 0755))
    {
      g_warning ("Unable to create directory %s. Please check your permissions.\n", priv->directory);
      return;
    }

  dir = g_dir_open (priv->directory, 0, NULL);
  while ((file = g_dir_read_name (dir)) != NULL)
    {
      PuzzleInfo info = {0, };
      IPuzGuesses *guesses;

      info.uri = build_file_path (DOMAIN_DOWNLOADS, priv->id, file);
      info.puzzle_name = g_strdup (file);
      info.readonly = FALSE;
      info.puzzle = directory_load_puzzle (priv->id, file, &(info.loading_error));

      /* While this next section may seem a little counterintuitive,
       * we add the file to priv->puzzles even though it failed to
       * load. We'll clean it up at the end and let the user know what
       * happened in a larger error message in
       * check_puzzles_for_errors() rather than trying to handle it
       * here..
       */
      if (info.puzzle && info.loading_error == NULL)
        {
          guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (info.puzzle));
          info.last_saved_checksum = ipuz_guesses_get_checksum (guesses, NULL);
        }
      g_array_append_val (priv->puzzles, info);
    }
}


static IPuzPuzzle *
resource_load_puzzle (GResource   *resource,
                      const gchar *id,
                      const gchar *file,
                      GError      **error)
{
  IPuzPuzzle *puzzle;
  g_autoptr (IPuzGuesses) guesses = NULL;
  g_autofree gchar *file_path = g_build_filename (PUZZLE_SET_PREFIX, id, file, NULL);

  puzzle = xwd_load_puzzle_from_resource (resource, file_path);
  guesses = crosswords_load_guesses (id, file);
  if (guesses)
    {
      /* Are the guesses valid, or has the xword changed? */
      if (! ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle), guesses))
        {
          ipuz_guesses_unref (guesses);
          guesses = NULL;
        }
    }

  if (guesses == NULL)
    {
      guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle)), FALSE);
      /* We don't need to check to see if this is valid */
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle), guesses);
    }

  return puzzle;
}

static void
load_puzzles_from_resources (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;
  guint puzzle_id = 1;

  priv = puzzle_picker_get_instance_private (picker);

  while (TRUE)
    {
      PuzzleInfo info = {0, };
      g_autofree gchar *section = NULL;
      IPuzGuesses *guesses;

      section = g_strdup_printf ("Puzzle%u", puzzle_id);
      info.puzzle_name = g_key_file_get_string (priv->key_file, section, "PuzzleName", NULL);
      if (!info.puzzle_name)
        break;

      /* Is this puzzle one we want to skip? */
      if (g_hash_table_lookup (priv->skip_puzzles, info.puzzle_name) == NULL)
        {
          info.puzzle = resource_load_puzzle (priv->resource, priv->id, info.puzzle_name, &info.loading_error);
          info.readonly = TRUE;
          guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (info.puzzle));
          info.last_saved_checksum = ipuz_guesses_get_checksum (guesses, NULL);
          g_array_append_val (priv->puzzles, info);
        }
      else
        {
          g_free (info.puzzle_name);
        }

      puzzle_id++;
    }
}

/* we check to see if any of the puzzles encountered couldn't be
   loaded. If so, we move them to the skip_puzzle list.
 */
static void
check_puzzles_for_errors (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;
  guint n_broken_files = 0;
  guint n_broken_resources = 0;
  PuzzleInfo *info;
  guint i = 0;
  g_autofree gchar *message = NULL;

  GtkDialogFlags flags;
  GtkWidget *dialog;
  GtkRoot *root;

  priv = puzzle_picker_get_instance_private (picker);


  /* Count how many problem files we have. */
  for (i = 0; i < priv->puzzles->len; i++)
    {
      info = &(g_array_index (priv->puzzles, PuzzleInfo, i));
      if (info->puzzle == NULL ||
          info->loading_error)
        {
          if (info->readonly)
            n_broken_resources ++;
          else
            n_broken_files ++;
        }
    }

  /* All fine. No need to clean anything up or  */
  if (n_broken_resources == 0 &&
      n_broken_files == 0)
    return;

  /* Generate the error and delete the message */
  if (n_broken_files > 0)
    {
      i = 0;
      while (i < priv->puzzles->len)
        {
          info = &(g_array_index (priv->puzzles, PuzzleInfo, i));
          /* Puzzle problems. We need to remove this node */
          if (info->puzzle == NULL || info->loading_error)
            {
              if (info->readonly)
                {
                  g_hash_table_insert (priv->skip_puzzles, g_strdup (info->puzzle_name), GINT_TO_POINTER (1));
                }
              else
                {
                  /* Generate a special case error_message */
                  if (n_broken_files == 1 && n_broken_resources == 0)
                    {
                      if (info->loading_error)
                        message = g_strdup_printf (_("Puzzle %s couldn't be loaded.\n<i>Error: %s</i>"), info->puzzle_name, info->loading_error->message);
                      else
                        message = g_strdup_printf (_("Puzzle %s couldn't be loaded."), info->puzzle_name);
                    }
                  puzzle_picker_delete_puzzle_name (priv->id, info->puzzle_name);
                }
              g_array_remove_index_fast (priv->puzzles, i);
            }
          else
            i++;
        }
    }

  if (n_broken_resources > 0 &&
      n_broken_files == 0)
    message = g_strdup_printf (_("Puzzle set %s contains invalid puzzle files."), priv->id);
  else if (n_broken_resources == 0 && n_broken_files > 1)
    message = g_strdup_printf (_("Multiple files couldn't be loaded. Cleaning them up."));

  if (message == NULL) /* Both broken_files and broken_resources. Rare */
    message = g_strdup_printf (_("Puzzle set %s contains invalid puzzle files and some files couldn't be loaded. Cleaning them up."), priv->id);


  root = gtk_widget_get_root (GTK_WIDGET (picker));
  flags = GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;
  dialog = gtk_message_dialog_new (GTK_WINDOW (root),
                                   flags,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_CLOSE,
                                   _("Error loading puzzle"));
  g_object_set (dialog,
                "secondary-text", message,
                "secondary-use-markup", TRUE,
                NULL);
  g_signal_connect (dialog, "response",
                    G_CALLBACK (gtk_window_destroy),
                    NULL);
  gtk_widget_show (dialog);
}
/* We need a valid resource and keyfile before we can load
 * ourselves. We only call load after both are set.
 *
 */
static void
puzzle_picker_load_maybe (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);


  if (priv->key_file && priv->resource)
    puzzle_picker_load (picker);
}


static void
puzzle_picker_real_load (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);

  /* clear old data */
  g_clear_pointer (&priv->id, g_free);
  g_array_set_size (priv->puzzles, 0);

  priv->id = g_key_file_get_string (priv->key_file, "Puzzle Set", "ID", NULL);
  if (priv->id == NULL)
    {
      g_warning (_("Loading a resource without an ID set."));
      return;
    }

  /* We really don't expect to have puzzles both in the directory or
   * in the resource, but it could happen. */
  load_puzzles_from_directory (picker);
  load_puzzles_from_resources (picker);
  check_puzzles_for_errors (picker);
}

static const gchar *
puzzle_picker_real_get_title (PuzzlePicker *picker)
{
  /* Maybe customize later, but for now use the ShortName */
  return NULL;
}

static const gchar *
puzzle_picker_real_get_uri (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);
  if (priv->selected)
    return priv->selected->uri;

  return NULL;
}

static void
puzzle_picker_real_save_state (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);

  if (priv->puzzles == NULL)
    return;

  for (guint i = 0; i < priv->puzzles->len; i++)
    {
      PuzzleInfo *info;
      IPuzGuesses *guesses;
      gchar *checksum = NULL;

      info = &(g_array_index (priv->puzzles, PuzzleInfo, i));
      guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (info->puzzle));

      if (guesses == NULL)
        continue;

      checksum = ipuz_guesses_get_checksum (guesses, NULL);
      if (g_strcmp0 (checksum, info->last_saved_checksum))
        {
          crosswords_save_guesses (guesses, priv->id, info->puzzle_name);
          g_free (info->last_saved_checksum);
          info->last_saved_checksum = checksum;
        }
      else
        {
          g_free (checksum);
        }
    }
}

static void
puzzle_picker_real_update_state (PuzzlePicker *picker)
{
  /* Pass */
}



/* This function will try to delete the temporary file. It has a check
 * to stop something weird happening, but we count on the flatpak
 * setup as our primary security mechanism. We only delete files if
 * they're in the tmp dir */
static void
delete_file (const gchar *uri)
{
  g_autoptr(GFile) file = NULL;
  g_autoptr(GFile) tmp_dir = NULL;

  file = g_file_new_for_uri (uri);
  tmp_dir = g_file_new_for_path (g_get_tmp_dir ());

  if (g_file_has_prefix (file, tmp_dir))
    {
      /* Best effort. No need to check for an error */
      g_file_delete (file, NULL, NULL);
    }
  else
    {
      g_warning ("trying to delete %s, which is not a temp file\n", uri);
    }
}

/* This is a temporary struct to pass two pieces of data into a callback
 */
typedef struct
{
  PuzzlePicker *picker;
  gchar *dest_puzzle_name;
  gchar *uri;
  gboolean delete_when_done;
} PickerDeleteTuple;

static void
copy_ready_cb (GFile             *file,
               GAsyncResult      *res,
               PickerDeleteTuple *tuple)
{
  PuzzlePickerPrivate *priv;
  g_autofree gchar *puzzle_name = NULL;

  priv = puzzle_picker_get_instance_private (tuple->picker);

  /* Finish the copy and load the new file */
  g_file_copy_finish (file, res, NULL);
  puzzle_picker_load (tuple->picker);

  /* go looking for the puzzle. puzzle_picker_load() will clear the
   * file away if it wasn't loadable. */
  puzzle_name = g_file_get_basename (file);
  for (guint i = 0; i < priv->puzzles->len; i++)
    {
      PuzzleInfo *info;

      info = &(g_array_index (priv->puzzles, PuzzleInfo, i));

      if (g_strcmp0 (tuple->dest_puzzle_name, info->puzzle_name) == 0)
        {
          puzzle_picker_puzzle_selected (tuple->picker, info->puzzle);
          break;
        }
    }

  /* This is safe to do only because we know this is a one-shot call
   * and we aren't called multiple times */
  if (tuple->delete_when_done)
    delete_file (tuple->uri);

  /* FIXME(cleanup): This should probably be put in _new() / _free() functions */
  g_object_unref (tuple->picker);
  g_free (tuple->uri);
  g_free (tuple->dest_puzzle_name);
  g_free (tuple);
}

/* This is used for the temporary files in /tmp. Before we copy the
 * file into the final, we want to give it a new name based on the
 * title of the puzzle. This will help dedup it, as well as catch
 * malformed files early.
 *
 * As we put the uri in /tmp ourselves, we know this should be fast
 * and local */
static gchar *
build_basename_from_puzzle (const gchar  *uri,
                            GError      **error)
{
  g_autofree gchar *filename = NULL;
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  g_autofree gchar *uniqueid = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *date = NULL;
  g_autofree gchar *escaped = NULL;


  filename = g_filename_from_uri (uri, NULL, NULL);
  puzzle = ipuz_puzzle_new_from_file (filename, error);
  if (puzzle == NULL)
    return NULL;

  /* Find a string to name the file */
  g_object_get (puzzle,
                "uniqueid", &uniqueid,
                "title", &title,
                "date", &date,
                NULL);

  if (uniqueid)
    escaped = g_uri_escape_string (uniqueid, NULL, TRUE);
  else if (title)
    escaped = g_uri_escape_string (title, NULL, TRUE);
  else if (date)
    escaped = g_uri_escape_string (date, NULL, TRUE);
  else /* No obvious identifier. Just have to call it puzzle */
    escaped = g_strdup ("puzzle");

  return g_strdup_printf ("%s.ipuz", escaped);
}

static void
puzzle_picker_real_load_uri (PuzzlePicker  *picker,
                             const gchar   *uri,
                             gboolean       delete_when_done,
                             GError       **error)
{
  PuzzlePickerPrivate *priv;
  PickerDeleteTuple *tuple;
  g_autoptr (GFile) source = NULL;
  g_autoptr (GFile) destination = NULL;
  gchar *basename = NULL;

  g_return_if_fail (uri != NULL);

  priv = puzzle_picker_get_instance_private (picker);
  source = g_file_new_for_uri (uri);

  if (delete_when_done)
    {
      basename = build_basename_from_puzzle (uri, error);
      if (basename == NULL)
        {
          /* We leak the file to help with debugging */
          g_warning ("%s is not a valid puzzle", uri);
          return;
        }
    }
  else
    {
      basename = g_file_get_basename (source);
    }
  destination = g_file_new_build_filename (priv->directory, basename,
                                           NULL);

  tuple = g_new (PickerDeleteTuple, 1);
  tuple->picker = g_object_ref (picker);
  tuple->uri = g_strdup (uri);
  tuple->delete_when_done = delete_when_done;
  tuple->dest_puzzle_name = basename;

  g_file_copy_async (source,
                     destination,
                     G_FILE_COPY_NONE,
                     G_PRIORITY_DEFAULT,
                     NULL,
                     NULL, NULL,
                     (GAsyncReadyCallback) copy_ready_cb,
                     tuple);
}

static void
restart_puzzle_dialog_response (GtkDialog    *dialog,
                                gint          response_id,
                                PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);
  g_return_if_fail (IPUZ_IS_PUZZLE (priv->clear_puzzle));

  if (response_id == RESPONSE_CLEAR)
    {
      IPuzGuesses *guesses;
      IPuzBoard *board;

      board = ipuz_crossword_get_board (IPUZ_CROSSWORD (priv->clear_puzzle));
      guesses = ipuz_guesses_new_from_board (board, FALSE);
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (priv->clear_puzzle), guesses);
      puzzle_picker_save_state (picker);
      puzzle_picker_update_state (picker);

      if (priv->clear_play_when_done)
        puzzle_picker_puzzle_selected (picker, priv->clear_puzzle);
    }
  else if (response_id == RESPONSE_VIEW)
    {
      priv->clear_play_when_done = FALSE;
      puzzle_picker_puzzle_selected (picker, priv->clear_puzzle);
    }
  else if (response_id == RESPONSE_CANCEL)
    {
      priv->clear_puzzle = NULL;
      priv->clear_play_when_done = FALSE;
    }

  gtk_window_destroy (GTK_WINDOW (dialog));
}

static void
puzzle_picker_real_clear_puzzle (PuzzlePicker *picker,
                                 IPuzPuzzle   *puzzle,
                                 const gchar  *primary,
                                 const gchar  *secondary,
                                 gboolean      play_when_done,
                                 gboolean      allow_view)
{
  PuzzlePickerPrivate *priv;
  GtkWidget *dialog;
  GtkDialogFlags flags;
  GtkRoot *root;

  priv = puzzle_picker_get_instance_private (picker);

  flags = GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;

  if (primary == NULL)
    primary = _("Clear puzzle?");
  root = gtk_widget_get_root (GTK_WIDGET (picker));
  dialog = gtk_message_dialog_new (GTK_WINDOW (root),
                                   flags,
                                   GTK_MESSAGE_QUESTION,
                                   GTK_BUTTONS_NONE,
                                   "%s", primary);
  gtk_dialog_add_button (GTK_DIALOG (dialog),
                         _("_Cancel"), RESPONSE_CANCEL);
  if (allow_view)
    gtk_dialog_add_button (GTK_DIALOG (dialog),
                           _("_View"), RESPONSE_VIEW);
  gtk_dialog_add_button (GTK_DIALOG (dialog),
                         _("C_lear"), RESPONSE_CLEAR);
  g_object_set (dialog,
                "secondary-use-markup", TRUE,
                "secondary-text", secondary,
                NULL);

  priv->clear_puzzle = puzzle;
  priv->clear_play_when_done = play_when_done;

  g_signal_connect (dialog, "response",
                    G_CALLBACK (restart_puzzle_dialog_response),
                    picker);

  gtk_widget_show (dialog);
}


/* Public Methods */

void
puzzle_picker_load (PuzzlePicker *picker)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->load (picker);
}


const gchar *
puzzle_picker_get_title (PuzzlePicker *picker)
{
  PuzzlePickerClass *klass;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  return (klass)->get_title (picker);
}

const gchar *
puzzle_picker_get_uri (PuzzlePicker  *picker)
{
  PuzzlePickerClass *klass;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  return (klass)->get_uri (picker);
}

void
puzzle_picker_save_state (PuzzlePicker *picker)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->save_state (picker);
}

void
puzzle_picker_update_state (PuzzlePicker *picker)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->update_state (picker);
}

void
puzzle_picker_load_uri (PuzzlePicker  *picker,
                        const gchar   *uri,
                        gboolean       delete_when_done,
                        GError       **error)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));
  g_return_if_fail (uri != NULL);

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->load_uri (picker, uri, delete_when_done, error);
}

void
puzzle_picker_clear_puzzle (PuzzlePicker *picker,
                            IPuzPuzzle   *puzzle,
                            const gchar  *primary,
                            const gchar  *secondary,
                            gboolean      play_when_done,
                            gboolean      allow_view)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));
  g_return_if_fail (IPUZ_PUZZLE (puzzle));

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->clear_puzzle (picker, puzzle, primary, secondary, play_when_done, allow_view);
}


void
puzzle_picker_puzzle_selected (PuzzlePicker *picker,
                               IPuzPuzzle   *puzzle)
{
  PuzzlePickerPrivate *priv;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  priv = puzzle_picker_get_instance_private (picker);
  priv->selected = puzzle_picker_find_puzzle_info (picker, puzzle);
  g_signal_emit (picker, obj_signals[PUZZLE_SELECTED], 0, puzzle);
}

void
puzzle_picker_clear_selected (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  priv = puzzle_picker_get_instance_private (picker);
  priv->selected = NULL;
}

void
puzzle_picker_update_selected_guesses (PuzzlePicker *picker,
                                       IPuzGuesses  *guesses)
{
  PuzzlePickerPrivate *priv;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  priv = puzzle_picker_get_instance_private (picker);

  if (priv->selected == NULL)
    return;

  if (! ipuz_crossword_set_guesses (IPUZ_CROSSWORD (priv->selected->puzzle),
                                    guesses))
    g_warning ("Mismatch between guesses and crossword");
}

const gchar *
puzzle_picker_get_id (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  priv = puzzle_picker_get_instance_private (picker);

  return priv->id;
}

GKeyFile *
puzzle_picker_get_key_file (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  priv = puzzle_picker_get_instance_private (picker);

  return priv->key_file;
}

const char *
puzzle_picker_get_directory (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  priv = puzzle_picker_get_instance_private (picker);

  return priv->directory;
}

GResource *
puzzle_picker_get_resource (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  priv = puzzle_picker_get_instance_private (picker);

  return priv->resource;
}

guint
puzzle_picker_get_n_puzzles (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), 0);

  priv = puzzle_picker_get_instance_private (picker);
  if (priv->puzzles)
    return priv->puzzles->len;

  return 0;
}



PuzzleInfo *
puzzle_picker_get_info (PuzzlePicker *picker,
                        guint         n)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);

  if (priv->puzzles)
    {
      if (n < priv->puzzles->len)
        return &(g_array_index (priv->puzzles, PuzzleInfo, n));
    }

  return NULL;
}

IPuzPuzzle *
puzzle_picker_get_puzzle (PuzzlePicker *picker,
                          guint         n)
{
  PuzzleInfo *info;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), 0);

  info = puzzle_picker_get_info (picker, n);

  if (info)
    return info->puzzle;

  return NULL;
}

const gchar *
puzzle_picker_get_puzzle_name (PuzzlePicker *picker,
                               guint         n)
{
  PuzzleInfo *info;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), 0);

  info = puzzle_picker_get_info (picker, n);

  if (info)
    return info->puzzle_name;

  return NULL;
}

const gchar *
puzzle_picker_get_last_saved_checksum (PuzzlePicker *picker,
                                       guint         n)
{
  PuzzleInfo *info;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), 0);

  info = puzzle_picker_get_info (picker, n);

  if (info)
    return info->last_saved_checksum;

  return NULL;
}

gboolean
puzzle_picker_get_readonly (PuzzlePicker *picker,
                            guint         n)
{
  PuzzleInfo *info;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), TRUE);

  info = puzzle_picker_get_info (picker, n);

  if (info)
    return info->readonly;

  return TRUE;
}

static PuzzleInfo *
puzzle_picker_find_puzzle_info (PuzzlePicker *picker,
                                IPuzPuzzle   *puzzle)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);


  for (guint i = 0; i < priv->puzzles->len; i++)
    {
      PuzzleInfo *info;

      info = &(g_array_index (priv->puzzles, PuzzleInfo, i));
      if (info->puzzle == puzzle)
        return info;
    }

  return NULL;
}

static void
puzzle_picker_delete_puzzle_name (const gchar *id,
                                  const gchar *puzzle_name)
{
  g_autoptr(GFile) file = NULL;
  g_autoptr(GError) error = NULL;

  /* Delete the main puzzle */
  file = g_file_new_build_filename (g_get_user_data_dir (),
                                    "gnome-crosswords",
                                    DOMAIN_DOWNLOADS,
                                    id, puzzle_name,
                                    NULL);
  if (!g_file_delete (file, NULL, &error))
    {
      g_warning ("Failed to delete %s: %s",
                 g_file_peek_path (file), error->message);
    }

  /* Maybe delete the saved file, if it exists */
  crosswords_discard_guesses (id, puzzle_name);
}

void
puzzle_picker_remove_puzzle (PuzzlePicker *picker,
                             IPuzPuzzle   *puzzle)
{
  PuzzlePickerPrivate *priv;
  PuzzleInfo *info;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));
  g_return_if_fail (IPUZ_IS_PUZZLE (puzzle));

  priv = puzzle_picker_get_instance_private (picker);

  info = puzzle_picker_find_puzzle_info (picker, puzzle);
  g_assert (info != NULL);

  puzzle_picker_delete_puzzle_name (priv->id, info->puzzle_name);

  /* Reload ourself */
  puzzle_picker_load (picker);
}


/* Functions */
static gchar *
build_file_path (const gchar *domain,
                 const gchar *id,
                 const gchar *puzzle_name)
{
  return g_build_filename (g_get_user_data_dir (),
                           "gnome-crosswords",
                           domain, id, puzzle_name,
                           NULL);
}

void
crosswords_save_guesses (IPuzGuesses *guesses,
                         const gchar *id,
                         const gchar *puzzle_name)
{
  g_autofree gchar *pathname = NULL;
  g_autofree gchar *filename = NULL;
  g_autofree gchar *savename = NULL;

  /* FIXME(error): I'm not sure how best to handle errors in this function. */
  g_return_if_fail (guesses != NULL);
  g_return_if_fail (id != NULL);
  g_return_if_fail (puzzle_name != NULL);

  pathname = g_build_filename (g_get_user_data_dir (),
                               "gnome-crosswords",
                               DOMAIN_SAVED_PUZZLES,
                               id, NULL);
  g_mkdir_with_parents (pathname,  0755);

  savename = g_strdup_printf ("%s.saved", puzzle_name);
  filename = g_build_filename (pathname, savename, NULL);

  ipuz_guesses_save_to_file (guesses, filename, NULL);
}

IPuzGuesses *
crosswords_load_guesses (const gchar *id,
                         const gchar *puzzle_name)
{
  g_autofree gchar *pathname = NULL;
  g_autofree gchar *saved_file_name = NULL;

  g_return_val_if_fail (id != NULL, NULL);
  g_return_val_if_fail (puzzle_name != NULL, NULL);

  saved_file_name = g_strdup_printf ("%s.saved", puzzle_name);

  pathname = g_build_filename (g_get_user_data_dir (),
                               "gnome-crosswords",
                               DOMAIN_SAVED_PUZZLES,
                               id, saved_file_name,
                               NULL);

  return ipuz_guesses_new_from_file (pathname, NULL);
}

void
crosswords_discard_guesses (const gchar *id,
                            const gchar *puzzle_name)
{
  g_autoptr(GFile) saved_file = NULL;
  g_autofree gchar *save_puzzle_name = NULL;

  g_return_if_fail (id != NULL);
  g_return_if_fail (puzzle_name != NULL);

  save_puzzle_name = g_strdup_printf ("%s.saved", puzzle_name);
  saved_file = g_file_new_build_filename (g_get_user_data_dir (),
                                          "gnome-crosswords",
                                          DOMAIN_SAVED_PUZZLES,
                                          id,
                                          save_puzzle_name,
                                          NULL);

  g_file_delete (saved_file, NULL, NULL);
}
