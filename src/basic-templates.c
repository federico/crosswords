/* basic-templates.c
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "basic-templates.h"
#include "crosswords-misc.h"


static GListModel *small_standard_model = NULL;
static GListModel *small_cryptic_model = NULL;
static GListModel *medium_standard_model = NULL;
static GListModel *medium_cryptic_model = NULL;
static GListModel *large_standard_model = NULL;
static GListModel *large_cryptic_model = NULL;
static GListModel *x_large_standard_model = NULL;
static GListModel *x_large_cryptic_model = NULL;


struct _BasicTemplateRow
{
  GObject parent_object;
  GdkPixbuf *pixbuf;
  IPuzPuzzle *template;
};


static void              basic_template_row_init       (BasicTemplateRow      *self);
static void              basic_template_row_class_init (BasicTemplateRowClass *klass);
static void              basic_template_row_dispose    (GObject               *object);
static BasicTemplateRow *basic_template_row_new        (IPuzPuzzle            *template,
                                                        GdkPixbuf             *pixbuf);


G_DEFINE_TYPE (BasicTemplateRow, basic_template_row, G_TYPE_OBJECT);


static GListStore *
create_model (void)
{
  GListStore *model;

  model = g_list_store_new (BASIC_TYPE_TEMPLATE_ROW);

  return model;
}

static void
append_resource (GListStore  *store,
                 const gchar *resource_path)
{
  g_autoptr (BasicTemplateRow) row = NULL;
  g_autoptr (IPuzPuzzle) template = NULL;
  g_autoptr (GdkPixbuf) pixbuf = NULL;

  g_return_if_fail (G_IS_LIST_STORE (store));
  template = xwd_load_puzzle_from_resource (NULL, resource_path);
  if (template == NULL)
    {
      g_warning ("Unable to load resource %s", resource_path);
      return;
    }
  pixbuf = xwd_thumbnail_puzzle (template);
  row = basic_template_row_new (template, pixbuf);

  g_list_store_append (store, row);
}

static void
basic_template_load (void)
{
  static gsize guard = 0;
  GListStore *store;

  if (!g_once_init_enter (&guard))
    return;

  /* Small standard */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-standard-1.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-standard-2.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-standard-3.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-standard-4.ipuz");
  small_standard_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* Small cryptic */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-cryptic-1.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/small-cryptic-2.ipuz");
  small_cryptic_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* Medium standard */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/medium-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/medium-standard-1.ipuz");
  medium_standard_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* Medium cryptic */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/medium-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/medium-cryptic-1.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/medium-cryptic-2.ipuz");
  medium_cryptic_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* Large standard */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-standard-1.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-standard-2.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-standard-3.ipuz");
  large_standard_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* Large cryptic */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-cryptic-1.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-cryptic-2.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-cryptic-3.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/large-cryptic-4.ipuz");
  large_cryptic_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* X-Large standard */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/x-large-blank.ipuz");
  x_large_standard_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  /* X-Large cryptic */
  store = create_model ();
  append_resource (store, "/org/gnome/Crosswords/crosswords/x-large-blank.ipuz");
  append_resource (store, "/org/gnome/Crosswords/crosswords/x-large-cryptic-1.ipuz");
  x_large_cryptic_model = (GListModel *) gtk_single_selection_new (G_LIST_MODEL (store));

  g_once_init_leave (&guard, 1);
}

GListModel *
basic_template_get_model (BasicType type,
                          BasicSize size)
{
  basic_template_load ();

  g_assert (type == BASIC_TYPE_STANDARD || type == BASIC_TYPE_CRYPTIC);

  switch (size)
    {
    case BASIC_SIZE_SMALL:
      if (type == BASIC_TYPE_STANDARD)
        return small_standard_model;
      else
        return small_cryptic_model;
    case BASIC_SIZE_MEDIUM:
      if (type == BASIC_TYPE_STANDARD)
        return medium_standard_model;
      else
        return medium_cryptic_model;
    case BASIC_SIZE_LARGE:
      if (type == BASIC_TYPE_STANDARD)
        return large_standard_model;
      else
        return large_cryptic_model;
    case BASIC_SIZE_X_LARGE:
      if (type == BASIC_TYPE_STANDARD)
        return x_large_standard_model;
      else
        return x_large_cryptic_model;
    case BASIC_SIZE_CUSTOM:
    default:
      g_assert_not_reached ();
    }
  return NULL;
}



static void
basic_template_row_init (BasicTemplateRow *self)
{

}

static void
basic_template_row_class_init (BasicTemplateRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = basic_template_row_dispose;
}

static void
basic_template_row_dispose (GObject *object)
{
  BasicTemplateRow *self = BASIC_TEMPLATE_ROW (object);

  g_clear_object (&self->pixbuf);
  g_clear_object (&self->template);
}


static BasicTemplateRow *
basic_template_row_new (IPuzPuzzle *template,
                        GdkPixbuf  *pixbuf)
{
  BasicTemplateRow *row;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (template), NULL);
  g_return_val_if_fail (GDK_IS_PIXBUF (pixbuf), NULL);

  row = g_object_new (BASIC_TYPE_TEMPLATE_ROW, NULL);

  row->template = g_object_ref (template);
  row->pixbuf = g_object_ref (pixbuf);

  return row;
}

/* Public methods */

GListModel *
basic_template_create_custom_model (guint width,
                                    guint height)
{
  GListModel *custom_model;
  g_autoptr (IPuzPuzzle) template = NULL;
  g_autoptr (GdkPixbuf) pixbuf = NULL;
  g_autoptr (BasicTemplateRow) row = NULL;

  custom_model = (GListModel *) create_model ();

  template = (IPuzPuzzle *) ipuz_crossword_new ();
  ipuz_crossword_set_size (IPUZ_CROSSWORD (template),
                           width, height);
  pixbuf = xwd_thumbnail_puzzle (template);
  row = basic_template_row_new (template, pixbuf);

  g_list_store_append (G_LIST_STORE (custom_model), row);

  return G_LIST_MODEL (gtk_single_selection_new (custom_model));
}

GdkPixbuf *
basic_template_row_get_pixbuf (BasicTemplateRow *template_row)
{
  g_return_val_if_fail (BASIC_IS_TEMPLATE_ROW (template_row), NULL);

  return template_row->pixbuf;
}

IPuzPuzzle *
basic_template_row_get_template (BasicTemplateRow *template_row)
{
  g_return_val_if_fail (BASIC_IS_TEMPLATE_ROW (template_row), NULL);

  return template_row->template;
}
