/* crosswords-app.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <adwaita.h>
#include "crosswords-app.h"
#include "crosswords-credits.h"
#include "crosswords-init.h"
#include "play-window.h"


struct _CrosswordsApp
{
  AdwApplication parent;
};


static void crosswords_app_init          (CrosswordsApp       *app);
static void crosswords_app_class_init    (CrosswordsAppClass  *klass);
static void crosswords_app_activate      (GApplication        *app);
static void crosswords_app_open          (GApplication        *app,
                                          GFile              **files,
                                          int                  n_files,
                                          const char          *hint);
static void crosswords_app_actions_quit  (GSimpleAction       *action,
                                          GVariant            *param,
                                          gpointer             user_data);
static void crosswords_app_actions_about (GSimpleAction       *action,
                                          GVariant            *param,
                                          gpointer             user_data);
static void crosswords_app_startup       (GApplication        *app);


G_DEFINE_TYPE(CrosswordsApp, crosswords_app, ADW_TYPE_APPLICATION);

static void
crosswords_app_init (CrosswordsApp *app)
{
  /* Nothing to do*/
}

static void
crosswords_app_activate (GApplication *app)
{
  GtkWindow *window;
  g_autoptr (GSettings) settings = NULL;

  g_assert (CROSSWORDS_IS_APP (app));

  settings = g_settings_new ("org.gnome.Crosswords");
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (PLAY_TYPE_WINDOW,
                           "maximized", g_settings_get_boolean (settings, "is-maximized"),
                           "application", app,
                           NULL);

  gtk_window_present (window);
}

static void
crosswords_app_open (GApplication  *app,
                     GFile        **files,
                     int            n_files,
                     const char    *hint)
{
  g_assert (CROSSWORDS_IS_APP (app));

  /* Do we want to dedup files? Or worry about it opening the same file
   * twice? Probably best to just do what we say and make sure we can handle
   * multiple instances of the same file at the same time.
   */
  for (int i = 0; i < n_files; i++)
    {
      PlayWindow *window;
      g_autoptr (GSettings) settings = NULL;
      g_autofree gchar *uri = NULL;

      uri = g_file_get_uri (files[i]);

      settings = g_settings_new ("org.gnome.Crosswords");
      window = g_object_new (PLAY_TYPE_WINDOW,
                             "maximized", g_settings_get_boolean (settings, "is-maximized"),
                             "application", app,
                             NULL);
      play_window_load_uri (window, uri);
      gtk_window_present (GTK_WINDOW (window));
    }
}


static void
crosswords_app_startup (GApplication *app)
{
  static const GActionEntry actions[] = {
    { "quit", crosswords_app_actions_quit, NULL, NULL, NULL, {0,0,0}},
    { "about", crosswords_app_actions_about, NULL, NULL, NULL, {0,0,0}},
  };
  static const gchar *quit_accels[] = { "<Primary>Q", NULL };

  G_APPLICATION_CLASS (crosswords_app_parent_class)->startup (app);


  crosswords_init ();

  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.quit", quit_accels);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   actions,
                                   G_N_ELEMENTS (actions),
                                   app);
}

static void
crosswords_app_window_added (GtkApplication *application,
                             GtkWindow      *window)
{
  g_assert (CROSSWORDS_IS_APP (application));
  g_assert (GTK_IS_WINDOW (window));

#ifdef DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (window), "devel");
#endif

  GTK_APPLICATION_CLASS (crosswords_app_parent_class)->window_added (application, window);
}

static void
crosswords_app_class_init (CrosswordsAppClass *klass)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (klass);
  GtkApplicationClass *gtk_application_class = GTK_APPLICATION_CLASS (klass);

  application_class->activate = crosswords_app_activate;
  application_class->open = crosswords_app_open;
  application_class->startup = crosswords_app_startup;

  gtk_application_class->window_added = crosswords_app_window_added;

}

/* Actions */

static void
crosswords_app_actions_quit (GSimpleAction *action,
                               GVariant      *param,
                               gpointer       user_data)
{
  GtkApplication *app;
  GList *app_list;

  app = GTK_APPLICATION (user_data);

  for (app_list = gtk_application_get_windows (app);
       app_list;
       app_list = app_list->next)
    {
      play_window_save_state (PLAY_WINDOW (app_list->data));
    }

  g_application_quit (G_APPLICATION (app));
}

static void
crosswords_app_actions_about (GSimpleAction *action,
                                GVariant      *param,
                                gpointer       user_data)
{
  CrosswordsApp *app;
  AdwAboutWindow *dialog;
  GtkWindow *window;

  app = CROSSWORDS_APP (user_data);
  g_assert (CROSSWORDS_IS_APP (app));

  dialog = ADW_ABOUT_WINDOW (adw_about_window_new ());
  adw_about_window_set_application_name (dialog, _("Crosswords"));
  adw_about_window_set_developer_name (dialog, "Jonathan Blandford");
  adw_about_window_set_application_icon (dialog, PACKAGE_ICON_NAME);
  adw_about_window_set_developers (dialog, crosswords_authors);

  /* Translators: Put your name here so that it will be included in the about dialog */
  adw_about_window_set_translator_credits (dialog, _("translator-credits"));
  adw_about_window_set_designers (dialog, crosswords_designers);
  adw_about_window_add_credit_section (dialog, _("Puzzles by"), crosswords_puzzles);
  adw_about_window_set_version (dialog, PACKAGE_VERSION);
  adw_about_window_set_copyright (dialog, "© 2021-2022 Jonathan Blandford");
  adw_about_window_set_license_type (dialog, GTK_LICENSE_GPL_3_0);
  adw_about_window_set_website (dialog, PACKAGE_WEBSITE);
  adw_about_window_set_issue_url (dialog, ISSUE_WEBSITE);
  adw_about_window_set_release_notes (dialog, _(crosswords_release_notes));
  /* The default size is a bit small for all the items */
  gtk_window_set_default_size (GTK_WINDOW (dialog), 515, 640);

  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);

  gtk_window_present (GTK_WINDOW (dialog));
}

/* Public methods */

CrosswordsApp *
crosswords_app_new (void)
{
  return g_object_new (CROSSWORDS_TYPE_APP,
                       "application-id", APP_ID,
                       "flags", G_APPLICATION_HANDLES_OPEN,
                       "register-session", TRUE,
                       NULL);
}
