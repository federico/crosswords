#pragma once

#include "xword-state.h"

G_GNUC_WARN_UNUSED_RESULT XwordState *load_state (const char *filename, XwordStateMode mode);
