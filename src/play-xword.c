/* play-xword.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "play-clue-list.h"
#include "play-clue-row.h"
#include "play-grid.h"
#include "play-xword.h"
#include "play-xword-column.h"
#include "puzzle-stack.h"
#include "word-list.h"


#define XWORD_IN_WON_STATE(xword) ((xword->state)&&(xword->state->mode==XWORD_STATE_BROWSE))
#define DIRECTION_IS_VISIBLE(dir) (dir != IPUZ_CLUE_DIRECTION_NONE && dir != IPUZ_CLUE_DIRECTION_HIDDEN)
#define ACROSS_DOWN(primary_dir,secondary_dir) ((primary_dir == IPUZ_CLUE_DIRECTION_ACROSS && \
                                                 secondary_dir == IPUZ_CLUE_DIRECTION_DOWN) || \
                                                (primary_dir == IPUZ_CLUE_DIRECTION_DOWN && \
                                                 secondary_dir == IPUZ_CLUE_DIRECTION_ACROSS))


enum
{
  PROP_0,
  PROP_PUZZLE,
  PROP_MODE,
  PROP_COMPACT_MODE,
  N_PROPS,
};

enum
{
  GAME_WON,
  REVEAL_CANCELED,
  USER_IDLE,
  LAST_SIGNAL
};


static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint play_xword_signals[LAST_SIGNAL] = { 0 };


struct _PlayXword
{
  GtkWidget   parent_instance;

  PlayXwordMode  mode;
  gchar         *title;
  XwordState    *state;
  gboolean       compact_mode;

  /* undo stack */
  PuzzleStack   *puzzle_stack;
  gulong         changed_id;

  /* Toasts for messaging */
  AdwToast      *game_won_toast;
  AdwToast      *warning_toast;
  AdwToast      *hint_toast;

  LayoutConfig   layout_config;
  GSettings     *settings;
  ZoomLevel      zoom_level;

  XwordRevealMode reveal_mode;
  guint          autosave_idle_handler;
  guint          across_toggle_handler;
  guint          down_toggle_handler;

  /* Hint */
  WordList      *word_list;
  gchar         *filter;
  GArray        *filter_rand_array;
  guint          filter_offset;

  /* Template widgets */
  GtkWidget     *xword_column;
  GtkWidget     *intro_label;
  GtkWidget     *play_grid;
  GtkWidget     *notes_label;
  GtkWidget     *clue_label;
  GtkWidget     *primary_clues;
  GtkWidget     *secondary_clues;
  GtkWidget     *action_bar;
  GtkWidget     *across_toggle;
  GtkWidget     *down_toggle;
  GtkWidget     *toast_overlay;
  GtkSizeGroup  *spacer_size_group;
};


static void play_xword_init                     (PlayXword             *self);
static void play_xword_class_init               (PlayXwordClass        *klass);
static void play_xword_set_property             (GObject               *object,
                                                 guint                  prop_id,
                                                 const GValue          *value,
                                                 GParamSpec            *pspec);
static void play_xword_get_property             (GObject               *object,
                                                 guint                  prop_id,
                                                 GValue                *value,
                                                 GParamSpec            *pspec);
static void play_xword_dispose                  (GObject               *object);
static void play_xword_activate_clipboard_copy  (PlayXword             *self,
                                                 const char            *name,
                                                 GVariant              *parameter);
static void play_xword_activate_clipboard_paste (PlayXword             *self,
                                                 const char            *name,
                                                 GVariant              *parameter);
static void play_xword_actions_undo             (PlayXword             *self,
                                                 const gchar           *action_name,
                                                 GVariant              *param);
static void play_xword_actions_redo             (PlayXword             *self,
                                                 const gchar           *action_name,
                                                 GVariant              *param);

static void play_xword_post_change              (PlayXword             *xword);
static void play_xword_set_filter               (PlayXword             *xword,
                                                 const gchar           *filter);
static void play_xword_update_won               (PlayXword             *xword);
static void play_xword_update_state             (PlayXword             *xword);
static void play_xword_update_states            (PlayXword             *xword);
static void direction_toggled_cb                (GtkWidget             *toggle,
                                                 PlayXword             *xword);
static void zoom_changed_cb                     (PlayXword             *xword);
static void grid_guess_cb                       (PlayGrid              *grid,
                                                 gchar                 *guess,
                                                 PlayXword             *xword);
static void grid_guess_at_cell_cb               (PlayGrid              *grid,
                                                 gchar                 *guess,
                                                 guint                  row,
                                                 guint                  column,
                                                 PlayXword             *xword);
static void update_autosave_idle_handler        (PlayXword             *xword);
static void cell_selected_cb                    (PlayGrid              *grid,
                                                 guint                  row,
                                                 guint                  column,
                                                 PlayXword             *xword);
static void grid_do_command_cb                  (PlayGrid              *grid,
                                                 XwordCmdKind           kind,
                                                 PlayXword             *xword);
static void clue_selected_cb                    (PlayXword             *xword,
                                                 IPuzClueDirection      direction,
                                                 gint                   index,
                                                 PlayClueList          *clue);
static void play_xword_puzzle_stack_changed     (PlayXword             *self,
                                                 gboolean               new_frame,
                                                 PuzzleStackChangeType  change_type,
                                                 PuzzleStack           *puzzle_stack);


G_DEFINE_TYPE (PlayXword, play_xword, GTK_TYPE_WIDGET);


static void
play_xword_init (PlayXword *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->mode = PLAY_XWORD_MODE_NONE;
  self->compact_mode = FALSE;

  /* Set up the toggles */
  gtk_toggle_button_set_group (GTK_TOGGLE_BUTTON (self->across_toggle),
                               GTK_TOGGLE_BUTTON (self->down_toggle));
  self->across_toggle_handler =
    g_signal_connect (self->across_toggle, "toggled",
                      G_CALLBACK (direction_toggled_cb), self);
  self->down_toggle_handler =
    g_signal_connect (self->down_toggle, "toggled",
                      G_CALLBACK (direction_toggled_cb), self);

  /* Toasts */
  self->game_won_toast = adw_toast_new ("");
  adw_toast_set_priority (self->game_won_toast, ADW_TOAST_PRIORITY_HIGH);
  adw_toast_set_button_label (self->game_won_toast, _("_Select another?"));
  adw_toast_set_action_name (self->game_won_toast, "win.go-prev");
  adw_toast_set_timeout (self->game_won_toast, 0);

  self->warning_toast = adw_toast_new (_("You have filled in the puzzle incorrectly"));
  adw_toast_set_button_label (self->warning_toast, _("_Reveal mistakes?"));
  adw_toast_set_action_name (self->warning_toast, "xword.reveal");

  self->hint_toast = adw_toast_new ("");

  self->settings = g_settings_new ("org.gnome.Crosswords");
  self->zoom_level = ZOOM_UNSET;

  self->word_list = word_list_new ();
  self->filter_rand_array = g_array_new (FALSE, FALSE, sizeof (gushort));

  g_signal_connect_object (self->settings,
                           "changed::zoom-level",
                           G_CALLBACK (zoom_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
  zoom_changed_cb (self);

  /* set up the size group with the headers with a blank label*/
  play_clue_list_set_header_size_group (PLAY_CLUE_LIST (self->primary_clues),
                                        self->spacer_size_group);
  play_clue_list_set_header_size_group (PLAY_CLUE_LIST (self->secondary_clues),
                                        self->spacer_size_group);
}

static void
play_xword_class_init (PlayXwordClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = play_xword_set_property;
  object_class->get_property = play_xword_get_property;
  object_class->dispose = play_xword_dispose;

  play_xword_signals [GAME_WON] =
    g_signal_new ("game-won",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  play_xword_signals [REVEAL_CANCELED] =
    g_signal_new ("reveal-canceled",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
  play_xword_signals [USER_IDLE] =
    g_signal_new ("user-idle",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_PUZZLE] = g_param_spec_object ("puzzle",
                                                "Puzzle",
                                                "Currently loaded IPuz File",
                                                IPUZ_TYPE_PUZZLE,
                                                G_PARAM_READABLE);
  obj_props[PROP_MODE] = g_param_spec_enum ("mode",
                                            "Mode",
                                            "Game won effect mode",
                                            PLAY_TYPE_XWORD_MODE,
                                            PLAY_XWORD_MODE_NONE,
                                            G_PARAM_READABLE);
  obj_props[PROP_COMPACT_MODE] = g_param_spec_boolean ("compact-mode",
                                                       "Compact Mode",
                                                       "Compact Mode",
                                                       FALSE,
                                                       G_PARAM_READABLE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/play-xword.ui");
  gtk_widget_class_bind_template_child (widget_class, PlayXword, xword_column);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, intro_label);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, play_grid);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, notes_label);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, clue_label);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, primary_clues);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, secondary_clues);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, action_bar);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, across_toggle);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, down_toggle);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, toast_overlay);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, spacer_size_group);

  gtk_widget_class_bind_template_callback (widget_class, play_xword_update_state);
  gtk_widget_class_bind_template_callback (widget_class, grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_selected_cb);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_c, GDK_CONTROL_MASK, "clipboard.copy", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_v, GDK_CONTROL_MASK, "clipboard.paste", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK, "win.undo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_y, GDK_CONTROL_MASK, "win.redo", NULL);

  gtk_widget_class_install_action (widget_class, "clipboard.copy", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_activate_clipboard_copy);
  gtk_widget_class_install_action (widget_class, "clipboard.paste", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_activate_clipboard_paste);
  gtk_widget_class_install_action (widget_class, "win.undo", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_actions_undo);
  gtk_widget_class_install_action (widget_class, "win.redo", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_actions_redo);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}


static void
play_xword_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  switch (prop_id)
    {
    case PROP_PUZZLE:
      play_xword_load_puzzle (PLAY_XWORD (object),
                              IPUZ_PUZZLE (g_value_get_object (value)));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
play_xword_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PlayXword *self;

  g_return_if_fail (object != NULL);

  self = PLAY_XWORD (object);

  switch (prop_id)
    {
    case PROP_PUZZLE:
      if (self->puzzle_stack)
        g_value_set_object (value, puzzle_stack_get_puzzle (self->puzzle_stack));
      else
        g_value_set_object (value, NULL);
      break;
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    case PROP_COMPACT_MODE:
      g_value_set_boolean (value, play_xword_column_get_clue_visible (PLAY_XWORD_COLUMN (self->xword_column)));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static void
play_xword_dispose (GObject *object)
{
  PlayXword *self;
  GtkWidget *child;

  g_return_if_fail (object != NULL);

  self = PLAY_XWORD (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->title, g_free);
  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);
  g_clear_pointer (&self->state, xword_state_free);
  g_clear_object (&self->settings);
  g_clear_object (&self->game_won_toast);
  g_clear_object (&self->warning_toast);
  g_clear_object (&self->hint_toast);
  g_clear_object (&self->word_list);
  g_clear_pointer (&self->filter, g_free);
  if (self->filter_rand_array)
    {
      g_array_free (self->filter_rand_array, TRUE);
      self->filter_rand_array = NULL;
    }

  if (self->autosave_idle_handler)
    {
      g_source_remove (self->autosave_idle_handler);
      self->autosave_idle_handler = 0;
    }

  G_OBJECT_CLASS (play_xword_parent_class)->dispose (object);
}

static void
play_xword_activate_clipboard_copy (PlayXword  *self,
                                    const char *name,
                                    GVariant   *parameter)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  if (self->state == NULL)
    return;

  if (XWORD_IN_WON_STATE (self))
    return;

  /* If direction is NONE, we're probably on a block */
  if (self->state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_guess_string_by_id (self->state->xword,
                                               self->state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
paste_received (GObject      *clipboard,
                GAsyncResult *result,
                gpointer      data)
{
  PlayXword *self;
  g_autofree gchar *text = NULL;
  g_autofree gchar *upper = NULL;
  IPuzClueDirection direction;

  self = PLAY_XWORD (data);

  if (XWORD_IN_WON_STATE (self))
    return;

  text = gdk_clipboard_read_text_finish (GDK_CLIPBOARD (clipboard), result, NULL);
  if (text == NULL || self->state == NULL)
    {
      /* Strange text to get. */
      gtk_widget_error_bell (GTK_WIDGET (self));
      g_object_unref (self);
      return;
    }

  direction = self->state->clue.direction;
  upper = g_utf8_strup (text, -1);
  /* If we're on a BLOCK, arbitrarily paste across. */
  if (direction == IPUZ_CLUE_DIRECTION_NONE)
    direction = IPUZ_CLUE_DIRECTION_ACROSS;

  self->state = xword_state_replace (self->state,
                                     xword_state_guess_word (self->state,
                                                             self->state->cursor,
                                                             direction, upper));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword));
  g_object_unref (self);
}

static void
play_xword_activate_clipboard_paste (PlayXword  *self,
                                     const char *name,
                                     GVariant   *parameter)
{
  GdkClipboard *clipboard;

  if (self->state == NULL)
    return;

  if (XWORD_IN_WON_STATE (self))
    return;

  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_read_text_async (clipboard, NULL, paste_received, g_object_ref (self));

}

static void
play_xword_actions_undo (PlayXword   *self,
                         const gchar *action_name,
                         GVariant    *param)
{
  g_return_if_fail (PLAY_IS_XWORD (self));
  g_return_if_fail (PUZZLE_IS_STACK (self->puzzle_stack));

  if (XWORD_IN_WON_STATE (self))
    return;

  puzzle_stack_undo (self->puzzle_stack);
}

static void
play_xword_actions_redo (PlayXword   *self,
                         const gchar *action_name,
                         GVariant    *param)
{
  g_return_if_fail (PLAY_IS_XWORD (self));
  g_return_if_fail (PUZZLE_IS_STACK (self->puzzle_stack));

  if (XWORD_IN_WON_STATE (self))
    return;

  puzzle_stack_redo (self->puzzle_stack);
}

static void
play_xword_post_change (PlayXword *xword)
{
  g_signal_emit (xword, play_xword_signals[REVEAL_CANCELED], 0);
  update_autosave_idle_handler (xword);
  play_xword_update_states (xword);
}


static void
direction_toggled_cb (GtkWidget *toggle,
                      PlayXword *xword)
{
  if (xword->state == NULL)
    return;

  if (toggle == xword->across_toggle &&
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)) &&
      xword->state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
    {
      grid_do_command_cb (PLAY_GRID (xword->play_grid), XWORD_CMD_KIND_SWITCH, xword);
      if (xword->state->clue.direction != IPUZ_CLUE_DIRECTION_ACROSS)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xword->down_toggle), TRUE);
    }
  else if (toggle == xword->down_toggle &&
           gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)) &&
           xword->state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
    {
      grid_do_command_cb (PLAY_GRID (xword->play_grid), XWORD_CMD_KIND_SWITCH, xword);
      if (xword->state->clue.direction != IPUZ_CLUE_DIRECTION_DOWN)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (xword->across_toggle), TRUE);
    }
}

static void
zoom_changed_cb (PlayXword *xword)
{
  ZoomLevel new_zoom_level;
  g_autofree gchar *zoom_level_str = NULL;
  IPuzPuzzleKind kind = IPUZ_PUZZLE_CROSSWORD;

  if (xword->state)
    g_object_get (xword->state->xword,
                  "puzzle-kind", &kind,
                  NULL);

  zoom_level_str = g_settings_get_string (xword->settings, "zoom-level");
  new_zoom_level = zoom_level_from_string (zoom_level_str);

  /* Same zoom_level as before */
  if (xword->zoom_level == new_zoom_level)
    return;

  xword->zoom_level = new_zoom_level;
  play_xword_column_set_skip_next_anim (PLAY_XWORD_COLUMN (xword->xword_column), TRUE);
  xword->layout_config = layout_config_at_zoom_level (kind, xword->zoom_level);
  play_xword_update_states (xword);
}


static gboolean
update_autosave_idle_cb (PlayXword *xword)
{
  g_signal_emit (xword, play_xword_signals[USER_IDLE], 0);

  xword->autosave_idle_handler = 0;
  return G_SOURCE_REMOVE;
}

static void
update_autosave_idle_handler (PlayXword *xword)
{
  if (xword->autosave_idle_handler)
    g_source_remove (xword->autosave_idle_handler);

  xword->autosave_idle_handler =
    g_timeout_add_seconds (1, G_SOURCE_FUNC (update_autosave_idle_cb), xword);
}


static void
grid_guess_cb (PlayGrid  *grid,
               gchar     *guess,
               PlayXword *xword)
{
  xword->state = xword_state_replace (xword->state, xword_state_guess (xword->state, guess));

  puzzle_stack_push_change (xword->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (xword->state->xword));
}

static void
grid_guess_at_cell_cb (PlayGrid  *grid,
                       gchar     *guess,
                       guint      row,
                       guint      column,
                       PlayXword *xword)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column
  };

  xword->state = xword_state_replace (xword->state, xword_state_guess_at_cell (xword->state, guess, coord));

  puzzle_stack_push_change (xword->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (xword->state->xword));
}

static void
grid_do_command_cb (PlayGrid        *grid,
                    XwordCmdKind     kind,
                    PlayXword       *xword)
{
  if (kind == XWORD_CMD_KIND_BACKSPACE ||
      kind == XWORD_CMD_KIND_DELETE)
    {
      g_autofree gchar *old_hash = NULL;
      g_autofree gchar *new_hash = NULL;

      /* only push a change if the board actually changes. Otherwise,
         we assume they deleted a blank space */
      old_hash = ipuz_guesses_get_checksum (puzzle_stack_get_guesses (xword->puzzle_stack), NULL);
      xword->state = xword_state_replace (xword->state, xword_state_do_command (xword->state, kind));
      new_hash = ipuz_guesses_get_checksum (ipuz_crossword_get_guesses (IPUZ_CROSSWORD (xword->state->xword)), NULL);

      if (g_strcmp0 (old_hash, new_hash) != 0)
        puzzle_stack_push_change (xword->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (xword->state->xword));
      else
        play_xword_post_change (xword);
    }
  else
    {
      xword->state = xword_state_replace (xword->state, xword_state_do_command (xword->state, kind));
      play_xword_post_change (xword);
    }
}

static void
cell_selected_cb (PlayGrid *grid,
                  guint row,
                  guint column,
                  PlayXword *xword)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  xword->state = xword_state_replace (xword->state, xword_state_cell_selected (xword->state, coord));

  play_xword_post_change (xword);
}

static void
clue_selected_cb (PlayXword         *xword,
                  IPuzClueDirection  direction,
                  gint               index,
                  PlayClueList      *clue_list)
{
  IPuzPuzzle *puzzle;
  IPuzClue *clue;
  IPuzClueId clue_id = {
    .direction = direction,
    .index = index
  };

  puzzle = puzzle_stack_get_puzzle (xword->puzzle_stack);
  clue = ipuz_crossword_get_clue_by_id (IPUZ_CROSSWORD (puzzle), clue_id);
  xword->state = xword_state_replace (xword->state, xword_state_clue_selected (xword->state, clue));

  play_xword_post_change (xword);
}


/* Shuffle the list of words so we don't go in pure alphabetical
 * order */
static void
shuffle_array (GArray *arr)
{
  if (arr->len == 0)
    return;

  for (guint i = 0; i < arr->len - 1; i++)
    {
      guint j = i + rand() / (RAND_MAX / (arr->len - i) + 1);
      gushort swap = g_array_index (arr, gushort, j);
      g_array_index (arr, gushort, j) = g_array_index (arr, gushort, i);
      g_array_index (arr, gushort, i) = swap;
    }
}

static void
play_xword_set_filter (PlayXword   *xword,
                       const gchar *filter)
{
  gboolean is_filter = FALSE;
  guint n_items;

  if (g_strcmp0 (filter, xword->filter) == 0)
    return;

  g_clear_pointer (&xword->filter, g_free);

  for (const gchar *p = filter; p[0] != '\0'; p = g_utf8_next_char (p))
    {
      if (p[0] == '?')
        {
          is_filter = TRUE;
          break;
        }
    }

  if (! is_filter)
    return;

  xword->filter = g_strdup (filter);
  word_list_set_filter (xword->word_list, xword->filter);
  xword->filter_offset = 0;

  n_items = word_list_get_n_items (xword->word_list);
  g_array_set_size (xword->filter_rand_array, n_items);

  for (guint i = 0; i < n_items; i++)
    g_array_index (xword->filter_rand_array, gushort, i) = (gushort) i;

  shuffle_array (xword->filter_rand_array);
}

/* This can be called multiple times when you win. It may be a problem
 * in the future.
 */
static void
play_xword_update_won (PlayXword *xword)
{
  IPuzPuzzle *puzzle;
  IPuzGuesses *guesses;

  g_return_if_fail (xword->puzzle_stack != NULL);

  puzzle = puzzle_stack_get_puzzle (xword->puzzle_stack);
  if (ipuz_crossword_game_won (IPUZ_CROSSWORD (puzzle)))
    {
      /* Lock down the state so you can't edit it */
      xword->state = xword_state_replace (xword->state, xword_state_change_mode (xword->state, XWORD_STATE_BROWSE));

      adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (xword->toast_overlay),
                                   g_object_ref (xword->game_won_toast));
      adw_toast_dismiss (xword->warning_toast);
      return;
    }

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
  if (ipuz_guesses_get_percent (guesses) >= 1.0)
    {
      adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (xword->toast_overlay),
                                   g_object_ref (xword->warning_toast));
    }
}

static void
set_focused_cell (PlayXword *xword)
{
  if (XWORD_STATE_HAS_CURSOR (xword->state))
    {
      play_grid_focus_cell (PLAY_GRID (xword->play_grid), xword->state->cursor);
    }
}

static void
update_caption (PlayXword *xword)
{
  IPuzPuzzle *puzzle;
  g_autofree gchar *intro = NULL;
  g_autofree gchar *notes = NULL;
  g_autofree gchar *explanation = NULL;

  puzzle = puzzle_stack_get_puzzle (xword->puzzle_stack);
  if (puzzle)
    g_object_get (puzzle,
                  "intro", &intro,
                  "notes", &notes,
                  "explanation", &explanation,
                  NULL);

  if (intro && *intro)
    {
      gtk_label_set_markup (GTK_LABEL (xword->intro_label), intro);
      set_zoom_level_css_class (xword->intro_label, "caption-", xword->zoom_level);
    }
  else
    {
      gtk_label_set_markup (GTK_LABEL (xword->intro_label), "");
    }

  if (notes && *notes)
    {
      gtk_widget_show (xword->notes_label);
      gtk_label_set_markup (GTK_LABEL (xword->notes_label), notes);
      set_zoom_level_css_class (xword->notes_label, "caption-", xword->zoom_level);
    }
  else
    {
      gtk_widget_hide (xword->notes_label);
    }

  if (explanation && *explanation)
    {
      g_autofree gchar *new_title = NULL;

      new_title = g_strdup_printf ("%s\n%s", _("Congratulations — you solved the puzzle!"), explanation);
      adw_toast_set_title (xword->game_won_toast, new_title);
    }
  else
    {
      adw_toast_set_title (xword->game_won_toast, _("Congratulations — you solved the puzzle!"));
    }
}

static void
update_clue_row (PlayXword *xword)
{
  IPuzClue *clue;
  gboolean showenumerations;

  g_assert (xword);
  g_assert (xword->state);

  IPuzClueId clue_id = { .direction = xword->state->clue.direction,
                         .index = xword->state->clue.index };

  g_object_get (G_OBJECT (xword->state->xword),
                "showenumerations", &showenumerations,
                NULL);

  clue = ipuz_crossword_get_clue_by_id (xword->state->xword,
                                        xword->state->clue);

  play_clue_row_update (PLAY_CLUE_ROW (xword->clue_label),
                        xword->state, clue,
                        clue_id, showenumerations,
                        xword->zoom_level);

}

static void
update_directions (PlayXword *self)
{
  guint n_clue_sets;
  IPuzClueDirection primary_dir = IPUZ_CLUE_DIRECTION_NONE;
  IPuzClueDirection secondary_dir = IPUZ_CLUE_DIRECTION_NONE;
  gboolean primary_visible;
  gboolean secondary_visible;

  n_clue_sets = ipuz_crossword_get_n_clue_sets (self->state->xword);
  /* FIXME(cluesets): We don't support tertiary clues yet. For now,
   * just manage the two */
  if (n_clue_sets > 2)
    {
      g_warning ("More than two sets of clues not supported");
      n_clue_sets = 2;
    }
  primary_visible = play_xword_column_get_primary_visible (PLAY_XWORD_COLUMN (self->xword_column));
  secondary_visible = play_xword_column_get_secondary_visible (PLAY_XWORD_COLUMN (self->xword_column));
  if (n_clue_sets > 0)
    primary_dir = ipuz_crossword_clue_set_get_dir (self->state->xword, 0);
  if (n_clue_sets > 1)
    secondary_dir = ipuz_crossword_clue_set_get_dir (self->state->xword, 1);

  /* First, update the visibility of the clue widgets */
  if (DIRECTION_IS_VISIBLE (primary_dir))
    gtk_widget_show (self->primary_clues);
  else
    {
      gtk_widget_hide (self->primary_clues);
      primary_visible = FALSE;
    }

  if (DIRECTION_IS_VISIBLE (secondary_dir))
    gtk_widget_show (self->secondary_clues);
  else
    {
      gtk_widget_hide (self->secondary_clues);
      secondary_visible = FALSE;
    }

  /* Special case when only one column of rows is visible and we're
   * across/down. Change the direction to the current clue
   * direction. */
  if (ACROSS_DOWN (primary_dir, secondary_dir) &&
      (primary_visible && !secondary_visible))
    {
      g_object_set (self->primary_clues, "direction", self->state->clue.direction, NULL);
    }
  else
    {
      if (DIRECTION_IS_VISIBLE (primary_dir))
        g_object_set (self->primary_clues, "direction", primary_dir, NULL);
      if (DIRECTION_IS_VISIBLE (secondary_dir))
        g_object_set (self->secondary_clues, "direction", secondary_dir, NULL);
    }

  /* Update the Across/Down toggles. We only do this when we're across/down xword */
  if (ACROSS_DOWN (primary_dir, secondary_dir))
    {
      gtk_widget_show (self->action_bar);
      g_signal_handler_block (self->across_toggle, self->across_toggle_handler);
      g_signal_handler_block (self->down_toggle, self->down_toggle_handler);

      if (self->state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->across_toggle), TRUE);
      else if  (self->state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->down_toggle), TRUE);

      g_signal_handler_unblock (self->across_toggle, self->across_toggle_handler);
      g_signal_handler_unblock (self->down_toggle, self->down_toggle_handler);
    }
  else
    {
      gtk_widget_hide (self->action_bar);
    }
}

/* Notifies the window when we're in compact mode so we can update the
 * main UI to adjust */
static void
update_compact_mode (PlayXword *self)
{
  gboolean primary_visible;
  gboolean secondary_visible;

  primary_visible = play_xword_column_get_primary_visible (PLAY_XWORD_COLUMN (self->xword_column));
  secondary_visible = play_xword_column_get_secondary_visible (PLAY_XWORD_COLUMN (self->xword_column));

  if (primary_visible || secondary_visible)
    self->compact_mode = FALSE;
  else
    self->compact_mode = TRUE;
  g_object_notify_by_pspec (G_OBJECT (self),
                            obj_props[PROP_COMPACT_MODE]);
}

/* WARNING: This function is triggered by both state changes (safe),
 * and changes in the visibility of elements in the
 * PlayXwordColumn. That happens in the allocation callback. Making
 * changes to widths of widgets could cause it to loop.
 */
static void
play_xword_update_state (PlayXword *self)
{
  if (self->state == NULL)
    return;

  update_directions (self);
  play_clue_list_update (PLAY_CLUE_LIST (self->primary_clues), self->state, self->zoom_level);
  play_clue_list_update (PLAY_CLUE_LIST (self->secondary_clues), self->state, self->zoom_level);
  update_clue_row (self);
  update_caption (self);
  update_compact_mode (self);
}

static void
play_xword_update_states (PlayXword *xword)
{
  g_assert (PLAY_IS_XWORD (xword));

  if (xword->state == NULL)
    return;

  play_grid_update_state (PLAY_GRID (xword->play_grid), xword->state, xword->layout_config);
  play_xword_update_state (xword);
  set_focused_cell (xword);
}

/* Called when a new frame is pushed on the stack */
static void
play_xword_puzzle_stack_changed_new (PlayXword   *self,
                                     PuzzleStack *puzzle_stack)
{
  if (self->state)
    {
      XwordState *dehydrated_state;
      dehydrated_state = xword_state_dehydrate (self->state);
      puzzle_stack_set_data (puzzle_stack,
                             "play-xword",
                             dehydrated_state,
                             (GDestroyNotify) xword_state_free);
    }
}

static void
play_xword_puzzle_stack_changed_load (PlayXword   *self,
                                      PuzzleStack *puzzle_stack)
{
  IPuzPuzzle *puzzle;
  XwordState *dehydrated_state;
  g_autoptr (CrosswordsQuirks) quirks = NULL;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  dehydrated_state = puzzle_stack_get_data (puzzle_stack, "play-xword");
  quirks = crosswords_quirks_new (puzzle);

  g_assert (dehydrated_state);
  self->state = xword_state_replace (self->state, xword_state_hydrate (dehydrated_state,
                                                                       IPUZ_CROSSWORD (puzzle),
                                                                       quirks));
}

static void
play_xword_puzzle_stack_changed (PlayXword             *self,
                                 gboolean               new_frame,
                                 PuzzleStackChangeType  change_type,
                                 PuzzleStack           *puzzle_stack)
{
  if (new_frame)
    play_xword_puzzle_stack_changed_new (self, puzzle_stack);
  else
    play_xword_puzzle_stack_changed_load (self, puzzle_stack);

  /* We made a guess. Let's see if we won or filled in the grid. */
  play_xword_update_won (self);
  play_xword_post_change (self);
}


/* Public functions */

GtkWidget *
play_xword_new (void)
{
  return (GtkWidget *) g_object_new (PLAY_TYPE_XWORD, NULL);

}

void
play_xword_set_mode (PlayXword     *xword,
                     PlayXwordMode  mode)
{
  g_return_if_fail (PLAY_IS_XWORD (xword));

  xword->mode = mode;
}


void
play_xword_load_puzzle (PlayXword  *xword,
                        IPuzPuzzle *puzzle)
{
  g_return_if_fail (PLAY_IS_XWORD (xword));

  /* Reset us to a good position */

  if (puzzle)
    g_object_ref (G_OBJECT (puzzle));

  g_clear_signal_handler (&xword->changed_id,
                          xword->puzzle_stack);
  g_clear_object (&xword->puzzle_stack);
  g_clear_pointer (&xword->state, xword_state_free);
  adw_toast_dismiss (xword->game_won_toast);
  adw_toast_dismiss (xword->warning_toast);

  /* Set the puzzle */
  xword->puzzle_stack = puzzle_stack_new (PUZZLE_STACK_PLAY, puzzle);
  xword->changed_id = g_signal_connect_swapped (xword->puzzle_stack,
                                                "changed",
                                                G_CALLBACK (play_xword_puzzle_stack_changed),
                                                xword);
  g_object_notify_by_pspec (G_OBJECT (xword), obj_props[PROP_PUZZLE]);

  g_clear_pointer (&xword->title, g_free);
  if (puzzle)
    {
      IPuzPuzzleKind kind;
      g_autoptr (CrosswordsQuirks) quirks;

      quirks = crosswords_quirks_new (puzzle);
      g_object_get (puzzle,
                    "title", &xword->title,
                    "puzzle-kind", &kind,
                    NULL);
      xword->state = xword_state_new (IPUZ_CROSSWORD (puzzle), quirks, XWORD_STATE_SOLVE);
      xword->layout_config = layout_config_at_zoom_level (kind, xword->zoom_level);
      /* Force a change signal to get everything set up */
      play_xword_puzzle_stack_changed (xword, TRUE, STACK_CHANGE_GUESS, xword->puzzle_stack);
      play_xword_column_set_skip_next_anim (PLAY_XWORD_COLUMN (xword->xword_column), TRUE);
    }

  play_xword_update_won (xword);
}

IPuzPuzzle *
play_xword_get_puzzle (PlayXword *xword)
{
  g_return_val_if_fail (PLAY_IS_XWORD (xword), NULL);

  if (xword->puzzle_stack)
    return puzzle_stack_get_puzzle (xword->puzzle_stack);
  return NULL;
}

IPuzGuesses *
play_xword_get_guesses (PlayXword *xword)
{
  g_return_val_if_fail (PLAY_IS_XWORD (xword), NULL);

  if (xword->puzzle_stack)
    return puzzle_stack_get_guesses (xword->puzzle_stack);
  return NULL;
}

const gchar *
play_xword_get_title (PlayXword *xword)
{
  g_return_val_if_fail (PLAY_IS_XWORD (xword), NULL);

  return xword->title;
}

void
play_xword_show_hint (PlayXword *xword)
{
  g_autofree gchar *guess_string = NULL;
  g_autofree gchar *hint = NULL;

  g_return_if_fail (PLAY_IS_XWORD (xword));

  if (! XWORD_STATE_HAS_CURSOR (xword->state))
    {
      word_list_set_filter (xword->word_list, NULL);
      return;
    }

  guess_string = ipuz_crossword_get_guess_string_by_id (xword->state->xword,
                                                        xword->state->clue);
  play_xword_set_filter (xword, guess_string);

  if (xword->filter)
    {
      guint n_hints;

      n_hints = word_list_get_n_items (xword->word_list);

      if (n_hints == 0)
        hint = g_strdup (_("No words found"));
      else
        {
          guint j = g_array_index (xword->filter_rand_array, gushort, xword->filter_offset);
          if (n_hints == 1)
            hint = g_strdup_printf (_("Suggestion: %s"),
                                    word_list_get_word (xword->word_list, j));
          else
            hint = g_strdup_printf (_("Suggestion (%u/%u): %s"),
                                    xword->filter_offset + 1,
                                    n_hints,
                                    word_list_get_word (xword->word_list, j));
          xword->filter_offset++;

          /* Wrap around to 0 */
          if (xword->filter_offset == n_hints)
            xword->filter_offset = 0;
        }
    }
  else
    {
      hint = g_strdup (_("No words found"));
    }

  adw_toast_set_title (ADW_TOAST (xword->hint_toast), hint);
  adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (xword->toast_overlay),
                               g_object_ref (xword->hint_toast));
}

void
play_xword_set_reveal_mode (PlayXword       *xword,
                            XwordRevealMode  reveal_mode)
{
  g_return_if_fail (PLAY_IS_XWORD (xword));

  if (xword->reveal_mode == reveal_mode)
    return;

  xword->reveal_mode = reveal_mode;
  xword->state = xword_state_replace (xword->state, xword_state_set_reveal_mode (xword->state, xword->reveal_mode));
  play_xword_update_states (xword);
}

static void
play_xword_clear_dialog_response_cb (PlayXword *xword,
                                     gint       response,
                                     GtkDialog *dialog)
{
  if (response == GTK_RESPONSE_OK)
    {
      g_autoptr (IPuzPuzzle) puzzle;
      g_autoptr (IPuzGuesses) guesses = NULL;

      /* This is a little wacky; we implement clearing the dialog by
       * just clearing the guesses on the current puzzle, and
       * reloading it. The expectation that load_puzzle() will set
       * everything up correctly and get is in a valid state. Since we
       * destroy the existing puzzle set and it's the only ref to the
       * puzzle, we have to keep a ref around for load_puzzle to
       * work. */
      puzzle = play_xword_get_puzzle (xword);
      g_object_ref (puzzle);
      guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle)), FALSE);
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle), guesses);
      play_xword_load_puzzle (xword, puzzle);
    }
  gtk_window_destroy (GTK_WINDOW (dialog));
}

void
play_xword_clear_puzzle (PlayXword *xword)
{
  IPuzGuesses *guesses;
  gfloat percentage = 0.0;

  g_return_if_fail (PLAY_IS_XWORD (xword));

  if (xword->puzzle_stack == NULL)
    return;

  guesses = puzzle_stack_get_guesses (xword->puzzle_stack);
  percentage = ipuz_guesses_get_percent (guesses);

  if (percentage > 0.0)
    {
      GtkRoot *root;
      GtkDialog *dialog;
      GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;

      root = gtk_widget_get_root (GTK_WIDGET (xword));
      dialog = (GtkDialog *) gtk_message_dialog_new (GTK_WINDOW (root),
                                                     flags,
                                                     GTK_MESSAGE_QUESTION,
                                                     GTK_BUTTONS_NONE,
                                                     "Clear this puzzle?");
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                                "If you clear this puzzle, all your progress will be lost.");
      gtk_dialog_add_buttons (dialog,
                              _("_Cancel"),
                              GTK_RESPONSE_CANCEL,
                              _("C_lear"),
                              GTK_RESPONSE_OK,
                              NULL);
      g_signal_connect_swapped (dialog, "response", G_CALLBACK (play_xword_clear_dialog_response_cb), xword);
      gtk_widget_show (GTK_WIDGET (dialog));
    }
}
