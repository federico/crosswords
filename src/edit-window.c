/* edit-window.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-app.h"
#include "edit-autofill-dialog.h"
#include "edit-basics.h"
#include "edit-clues.h"
#include "edit-metadata.h"
#include "edit-save-changes-dialog.h"
#include "edit-window.h"
#include "edit-xword.h"
#include "play-grid.h"
#include "puzzle-stack.h"
#include "word-list.h"
#include "word-solver.h"


typedef enum
{
  EDIT_STAGE_BASIC,    /* Set the crossword type and basic grid */
  EDIT_STAGE_GRID,     /* Fill in the grid with letters */
  EDIT_STAGE_CLUES,    /* Add clues to the puzzle */
  EDIT_STAGE_STYLE,    /* Customize the puzzle through any style changes*/
  EDIT_STAGE_METADATA, /* Set metadata for the puzzle */
} EditWindowStage;

struct _EditWindow
{
  AdwApplicationWindow  parent_instance;

  PuzzleStack *puzzle_stack;
  gulong stack_changed_id;
  gulong changed_id;
  gchar *uri;

  EditWindowStage stage;

  GtkWidget *main_stack;
  GtkWidget *hero_grid;
  GtkWidget *edit_basics;
  GtkWidget *edit_xword;
  GtkWidget *edit_clues;
  GtkWidget *edit_metadata;

  /* Template widgets */
  GtkWidget *modified_label;
  GtkWidget *subtitle_label;
  GtkWidget *edit_menu_button;
  GtkWidget *symmetry_box;
  GtkWidget *autofill_button;
};


static void     edit_window_init                      (EditWindow       *self);
static void     edit_window_class_init                (EditWindowClass  *klass);
static void     edit_window_dispose                   (GObject          *object);
static void     edit_window_finalize                  (GObject          *object);
static gboolean edit_window_close_request             (GtkWindow        *window);
static void     edit_window_stack_changed_cb          (EditWindow       *self,
                                                       GParamSpec       *pspec,
                                                       GtkWidget        *stack);
static void     edit_window_basics_activated_cb       (EditWindow       *self,
                                                       EditBasics       *basics);
static void     edit_window_actions_show_primary_menu (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_undo              (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_redo              (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_prev_stage        (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_next_stage        (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_open              (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_save              (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_save_as           (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_preview           (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_show_help_overlay (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_close             (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_actions_autofill          (EditWindow       *self,
                                                       const gchar      *action_name,
                                                       GVariant         *param);
static void     edit_window_commit_changes            (EditWindow       *self);
static void     edit_window_update                    (EditWindow       *self);
static void     edit_window_get_title_and_path        (EditWindow       *self,
                                                       gchar           **title,
                                                       gchar           **path);
static void     edit_window_set_puzzle_stack          (EditWindow       *self,
                                                       PuzzleStack      *puzzle_stack);


static EditWindowStage stage_from_stage_name (const char      *stage_name);
static const char     *stage_to_stage_name   (EditWindowStage  stage);



G_DEFINE_TYPE (EditWindow, edit_window, ADW_TYPE_APPLICATION_WINDOW);


static void
edit_window_init (EditWindow *self)
{
  g_autoptr (PuzzleStack) puzzle_stack = NULL;
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  g_autoptr (GSettings) settings = NULL;
  g_autoptr (GSimpleActionGroup) group = NULL;
  g_autoptr (GAction) action = NULL;
  XwordState *state;
  GMenu *menu;
  GtkApplication *app;

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_add_css_class (GTK_WIDGET (self), "edit");

  /* We need to keep this handler id around so we can block it at times. */
  self->stack_changed_id = g_signal_handler_find (self->main_stack,
                                                  G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA,
                                                  0, 0, NULL,
                                                  edit_window_stack_changed_cb,
                                                  self);

  /* Menus */
  app = GTK_APPLICATION (EDIT_APP_DEFAULT);
  menu = gtk_application_get_menu_by_id (GTK_APPLICATION (app), "edit-menu");
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (self->edit_menu_button), G_MENU_MODEL (menu));

  /* Settings */
  settings = g_settings_new ("org.gnome.Crosswords.Editor");
  group = g_simple_action_group_new ();
  action = g_settings_create_action (settings, "symmetry");
  g_action_map_add_action (G_ACTION_MAP (group), action);
  gtk_widget_insert_action_group (GTK_WIDGET (self), "win", G_ACTION_GROUP (group));

  /* Load the hero section */
  puzzle = xwd_load_puzzle_from_resource (NULL, "/org/gnome/Crosswords/crosswords/edit-hero.ipuz");
  g_assert (puzzle != NULL);

  state = xword_state_new (IPUZ_CROSSWORD (puzzle), NULL, XWORD_STATE_VIEW);
  play_grid_update_state (PLAY_GRID (self->hero_grid), state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  xword_state_free (state);

  /* Set up the initial UI to be the basic page */
  self->stage = EDIT_STAGE_BASIC;

  /* Set up an initial PuzzleStack */
  puzzle_stack = puzzle_stack_new (PUZZLE_STACK_EDIT, NULL);
  edit_window_set_puzzle_stack (self, puzzle_stack);
}

static void
edit_window_class_init (EditWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkWindowClass *window_class = GTK_WINDOW_CLASS (klass);

  object_class->dispose = edit_window_dispose;
  object_class->finalize = edit_window_finalize;
  window_class->close_request = edit_window_close_request;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-window.ui");

  gtk_widget_class_bind_template_child (widget_class, EditWindow, main_stack);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, hero_grid);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_basics);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_xword);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_clues);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_metadata);

  gtk_widget_class_bind_template_child (widget_class, EditWindow, modified_label);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, subtitle_label);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_menu_button);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, symmetry_box);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, autofill_button);

  gtk_widget_class_bind_template_callback (widget_class, edit_window_stack_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_window_basics_activated_cb);

  /* Non-visible actions */
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F10, 0, "win.show-primary-menu", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK, "win.undo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_y, GDK_CONTROL_MASK, "win.redo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Page_Up, GDK_CONTROL_MASK, "win.prev-stage", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Page_Down, GDK_CONTROL_MASK, "win.next-stage", NULL);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_o, GDK_CONTROL_MASK, "win.open", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK, "win.save", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK | GDK_SHIFT_MASK, "win.save-as", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_question, GDK_CONTROL_MASK, "win.show-help-overlay", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_w, GDK_CONTROL_MASK, "win.close", NULL);


  gtk_widget_class_install_action (widget_class, "win.show-primary-menu", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_show_primary_menu);
  gtk_widget_class_install_action (widget_class, "win.undo", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_undo);
  gtk_widget_class_install_action (widget_class, "win.redo", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_redo);
  gtk_widget_class_install_action (widget_class, "win.prev-stage", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_prev_stage);
  gtk_widget_class_install_action (widget_class, "win.next-stage", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_next_stage);

  gtk_widget_class_install_action (widget_class, "win.open", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_open);
  gtk_widget_class_install_action (widget_class, "win.save", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_save);
  gtk_widget_class_install_action (widget_class, "win.save-as", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_save_as);
  gtk_widget_class_install_action (widget_class, "win.autofill", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_autofill);
  gtk_widget_class_install_action (widget_class, "win.preview", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_preview);
  gtk_widget_class_install_action (widget_class, "win.show-help-overlay", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_show_help_overlay);
  gtk_widget_class_install_action (widget_class, "win.close", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_close);
}

static void
edit_window_dispose (GObject *object)
{
  EditWindow *self;

  self = EDIT_WINDOW (object);

  g_clear_object (&self->puzzle_stack);

  G_OBJECT_CLASS (edit_window_parent_class)->dispose (object);
}


static void
edit_window_finalize (GObject *object)
{
  EditWindow *self;

  self = EDIT_WINDOW (object);

  g_clear_pointer (&self->uri, g_free);

  G_OBJECT_CLASS (edit_window_parent_class)->finalize (object);
}

static gboolean
edit_window_close_request (GtkWindow *window)
{
  GList *save_info_list = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *path = NULL;
  EditWindow *self;

  self = EDIT_WINDOW (window);
  g_assert (EDIT_IS_WINDOW (self));

  if (edit_window_get_unsaved_changes (self, &title, &path))
    {
      /* FIXME(uri): We are converting our URI to a path and back to a
       * URI. We should keep a URI throughout the dialog and convert
       * it to a path just to present it. */
      save_info_list = edit_save_info_list_add (save_info_list,
                                                GTK_WIDGET (self),
                                                title, path);
    }

  if (save_info_list == NULL)
    return FALSE;

  edit_save_changes_dialog_run (GTK_WINDOW (self),
                                save_info_list,
                                NULL);

  return TRUE;
}

static void
update_stack (EditWindow *self)
{
  const gchar *new_stage_name;

  /* set the new stage */
  new_stage_name = gtk_stack_get_visible_child_name (GTK_STACK (self->main_stack));
  self->stage = stage_from_stage_name (new_stage_name);
  edit_window_update (self);

}

/* This callback should only run when instigated by the user or by the
 * basics page. When we programmatically set the page, we need to
 * block it from running. We keep stack_changed_id around for that
 * reason.
 */
static void
edit_window_stack_changed_cb (EditWindow *self,
                              GParamSpec *pspec,
                              GtkWidget  *stack)
{
  gboolean maybe_new_puzzle = FALSE;

  /* If we were on the First page, potentially write a new puzzle */
  if (self->stage == EDIT_STAGE_BASIC)
    maybe_new_puzzle = TRUE;

  edit_window_commit_changes (self);
  update_stack (self);

  if (maybe_new_puzzle)
    edit_basics_lock_and_write (EDIT_BASICS (self->edit_basics));
}

static void
edit_window_basics_activated_cb (EditWindow *self,
                                 EditBasics *basics)
{
  gtk_stack_set_visible_child_name (GTK_STACK (self->main_stack),
                                    stage_to_stage_name (EDIT_STAGE_GRID));
}

/* Actions */

static void
edit_window_actions_show_primary_menu (EditWindow  *self,
                                       const gchar *action_name,
                                       GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  /* FIXME(mystery): Why no keyboard focus? */
  gtk_menu_button_popup (GTK_MENU_BUTTON (self->edit_menu_button));
}

static
void edit_window_actions_undo (EditWindow  *self,
                               const gchar *action_name,
                               GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (puzzle_stack_can_undo (self->puzzle_stack))
    puzzle_stack_undo (self->puzzle_stack);
  else
    g_warning ("win.undo called on a puzzle that can't be undone\n");
}

static void
edit_window_actions_redo (EditWindow  *self,
                          const gchar *action_name,
                          GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (puzzle_stack_can_redo (self->puzzle_stack))
    puzzle_stack_redo (self->puzzle_stack);
  else
    g_warning ("win.redo called on a puzzle that can't be redone\n");
}

static void
edit_window_actions_prev_stage (EditWindow  *self,
                                const gchar *action_name,
                                GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->stage != EDIT_STAGE_BASIC)
    gtk_stack_set_visible_child_name (GTK_STACK (self->main_stack),
                                      stage_to_stage_name (self->stage - 1));
}

static
void edit_window_actions_next_stage (EditWindow  *self,
                                     const gchar *action_name,
                                     GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->stage != EDIT_STAGE_METADATA)
    gtk_stack_set_visible_child_name (GTK_STACK (self->main_stack),
                                      stage_to_stage_name (self->stage + 1));
}


static void
edit_window_open_response_cb (EditWindow           *self,
                              gint                  response_id,
                              GtkFileChooserNative *native)
{
  g_autoptr(GFile) file = NULL;

  g_assert (EDIT_IS_WINDOW (self));
  g_assert (GTK_IS_FILE_CHOOSER_NATIVE (native));

  if (response_id == GTK_RESPONSE_ACCEPT)
    file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (native));

  gtk_native_dialog_destroy (GTK_NATIVE_DIALOG (native));

  if (file)
    {
      g_autofree gchar *uri = g_file_get_uri (file);

      edit_window_load_uri (self, uri);
    }
}

static void
edit_window_actions_open (EditWindow  *self,
                          const gchar *action_name,
                          GVariant    *param)
{
  g_autoptr(GtkFileFilter) filter = NULL;
  GtkFileChooserNative *native;

  g_assert (EDIT_IS_WINDOW (self));

  native = gtk_file_chooser_native_new (_("Select a Puzzle"),
                                        GTK_WINDOW (self),
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        _("Open"),
                                        _("Cancel"));
  gtk_native_dialog_set_modal (GTK_NATIVE_DIALOG (native), TRUE);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("IPuz Files"));
  gtk_file_filter_add_pattern (filter, "*.ipuz");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (native), g_object_ref (filter));

  g_signal_connect_object (native,
                           "response",
                           G_CALLBACK (edit_window_open_response_cb),
                           self,
                           G_CONNECT_SWAPPED);

  gtk_native_dialog_show (GTK_NATIVE_DIALOG (native));

}

static void
edit_window_actions_save (EditWindow  *self,
                          const gchar *action_name,
                          GVariant    *param)
{
  IPuzPuzzle *puzzle;
  g_autoptr (GError) error = NULL;
  g_autoptr (GFile) file = NULL;
  g_autofree char *buffer = NULL;
  gsize len;

  g_assert (EDIT_IS_WINDOW (self));

  if (self->uri == NULL)
    {
      edit_window_actions_save_as (self, action_name, param);
      return;
    }

  edit_window_commit_changes (self);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  if (puzzle == NULL)
    return;

  buffer = ipuz_puzzle_save_to_data (puzzle,
                                     &len);

  /* FIXME(error): We should handle errors better */
  file = g_file_new_for_uri (self->uri);
  if (! g_file_replace_contents (file, buffer,len,
                                 NULL,
                                 TRUE,
                                 G_FILE_CREATE_REPLACE_DESTINATION,
                                 NULL, NULL,
                                 &error))
    {
      g_print ("error saving file: %s\n", error->message);
    }
  else
    {
      puzzle_stack_set_saved (self->puzzle_stack);
      edit_window_update (self);
    }
}

static void
editor_window_save_as_response (EditWindow           *self,
                                gint                  response_id,
                                GtkFileChooserNative *native)
{
  g_assert (EDIT_IS_WINDOW (self));
  g_assert (GTK_IS_FILE_CHOOSER_NATIVE (native));

  if (response_id == GTK_RESPONSE_ACCEPT)
    {
      g_autoptr(GFile) dest = NULL;

      if ((dest = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (native))))
        {
          g_clear_pointer (&self->uri, g_free);
          self->uri = g_file_get_uri (dest);
          if (self->uri)
            edit_window_actions_save (self, NULL, NULL);
        }
    }

  gtk_native_dialog_destroy (GTK_NATIVE_DIALOG (native));
}

static void
edit_window_actions_save_as (EditWindow  *self,
                             const gchar *action_name,
                             GVariant    *param)
{
  GtkFileChooserNative *native;

  g_assert (EDIT_IS_WINDOW (self));

  native = gtk_file_chooser_native_new (_("Save As"),
                                        GTK_WINDOW (self),
                                        GTK_FILE_CHOOSER_ACTION_SAVE,
                                        _("Save"),
                                        _("Cancel"));

  g_signal_connect_object (native,
                           "response",
                           G_CALLBACK (editor_window_save_as_response),
                           self,
                           G_CONNECT_SWAPPED);

  gtk_native_dialog_show (GTK_NATIVE_DIALOG (native));
}


static void
edit_window_actions_preview (EditWindow  *self,
                             const gchar *action_name,
                             GVariant    *param)
{
  IPuzPuzzle *puzzle;

  g_assert (EDIT_IS_WINDOW (self));

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  if (puzzle == NULL)
    return;

  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle));
}

/* FIXME(mystery): Why is this needed? GtkApplication should do this
 * automatically */
static void
edit_window_actions_show_help_overlay (EditWindow  *self,
                           const gchar *action_name,
                           GVariant    *param)
{
  g_autoptr(GtkBuilder) builder = NULL;
  GObject *help_overlay;

  g_assert (EDIT_IS_WINDOW (self));

  builder = gtk_builder_new_from_resource ("/org/gnome/Crosswords/Editor/gtk/help-overlay.ui");
  help_overlay = gtk_builder_get_object (builder, "help_overlay");

  if (GTK_IS_SHORTCUTS_WINDOW (help_overlay))
    {
      gtk_window_set_transient_for (GTK_WINDOW (help_overlay), GTK_WINDOW (self));
      gtk_window_present (GTK_WINDOW (help_overlay));
    }
}

static void
edit_window_actions_close (EditWindow  *self,
                           const gchar *action_name,
                           GVariant    *param)
{
  GList *save_info_list = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *path = NULL;

  g_assert (EDIT_IS_WINDOW (self));

  if (edit_window_get_unsaved_changes (self, &title, &path))
    {
      /* FIXME(uri): We are converting our URI to a path and back to a
       * URI. We should keep a URI throughout the dialog and convert
       * it to a path just to present it. */
      save_info_list = edit_save_info_list_add (save_info_list,
                                                GTK_WIDGET (self),
                                                title, path);
    }

  if (save_info_list == NULL)
    {
      gtk_window_destroy (GTK_WINDOW (self));
    }
  else
    {
      edit_save_changes_dialog_run (GTK_WINDOW (self),
                                    save_info_list,
                                    NULL);
    }
}


/* this is the inverse of word_solver_initial_overlay */
static void
guesses_to_solution (IPuzCrossword *xword,
                     IPuzGuesses   *guess)
{
  IPuzBoard *board;
  guint row, column;

  board = ipuz_crossword_get_board (xword);
  for (row = 0; row < ipuz_guesses_get_height (guess); row++)
    {
      for (column = 0; column < ipuz_guesses_get_width (guess); column++)
        {
          IPuzCellCoord coord = { .row = row, .column = column };
          const gchar *solution;

          solution = ipuz_guesses_get_guess (guess, coord);

          if (solution && solution[0] != '\0')
            {
              IPuzCell *cell = ipuz_board_get_cell (board, coord);
              ipuz_cell_set_solution (cell, solution);
            }
        }
    }

}

static void
autofill_response_cb (EditAutofillDialog *dialog,
                      gint                response_id,
                      EditWindow         *window)
{
  if (response_id == GTK_RESPONSE_OK)
    {
      IPuzPuzzle *puzzle;
      IPuzGuesses *guesses;

      puzzle = puzzle_stack_get_puzzle (window->puzzle_stack);
      guesses = edit_autofill_dialog_get_guess (dialog);
      guesses_to_solution (IPUZ_CROSSWORD (puzzle), guesses);
      puzzle_stack_push_change (window->puzzle_stack, STACK_CHANGE_GRID, puzzle);
    }

  gtk_window_destroy (GTK_WINDOW (dialog));
}

static void
edit_window_actions_autofill (EditWindow  *self,
                              const gchar *action_name,
                              GVariant    *param)
{
  IPuzPuzzle *puzzle;
  GtkWidget *autofill_dialog;

  g_assert (EDIT_IS_WINDOW (self));

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  autofill_dialog = edit_autofill_dialog_new (puzzle);
  gtk_window_set_transient_for (GTK_WINDOW (autofill_dialog),
                                GTK_WINDOW (self));
  g_signal_connect (autofill_dialog, "response", G_CALLBACK (autofill_response_cb), self);
  gtk_widget_show (autofill_dialog);
}


static void
edit_window_commit_changes (EditWindow *self)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->stage == EDIT_STAGE_CLUES)
    edit_clues_commit_changes (EDIT_CLUES (self->edit_clues));
  else if (self->stage == EDIT_STAGE_METADATA)
    edit_metadata_commit_changes (EDIT_METADATA (self->edit_metadata));
}

static void
edit_window_update (EditWindow *self)
{
  g_autofree gchar *title = NULL;
  g_autofree gchar *path = NULL;
  gboolean show_grid = FALSE;

  if (self->stage == EDIT_STAGE_GRID)
    show_grid = TRUE;


  edit_window_get_title_and_path (self, &title, &path);

  if (title && title[0])
    {
      g_autofree gchar *label = NULL;

      if (path)
        {
          label = g_strdup_printf ("%s — %s", title, path);
        }

      gtk_label_set_label (GTK_LABEL (self->subtitle_label),
                           label?label:title);
    }
  else if (path)
    {
      gtk_label_set_label (GTK_LABEL (self->subtitle_label), path);
    }
  else
    {
      gtk_label_set_label (GTK_LABEL (self->subtitle_label), "");
    }

  gtk_widget_set_visible (self->modified_label,
                          puzzle_stack_get_unsaved_changes (self->puzzle_stack));
  gtk_widget_action_set_enabled (GTK_WIDGET (self), "win.symmetry", show_grid);
  gtk_widget_action_set_enabled (GTK_WIDGET (self), "win.autofill", show_grid);
  gtk_widget_set_visible (self->autofill_button, show_grid);
  gtk_widget_set_visible (self->symmetry_box, show_grid);
}

static void
edit_window_puzzle_stack_changed (EditWindow            *self,
                                  gboolean               new_frame,
                                  PuzzleStackChangeType  change_type,
                                  PuzzleStack           *puzzle_stack)
{
  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "win.undo",
                                 puzzle_stack_can_undo (puzzle_stack));
  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "win.redo",
                                 puzzle_stack_can_redo (puzzle_stack));
  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "win.preview",
                                 (puzzle_stack_get_puzzle (puzzle_stack) != NULL));
  if (new_frame)
    {
      /* FIXME(save): Do we need to reset the save uri here; either by
       * clearing it or saving it? I'm not sure what the right
       * behavior is. */
      puzzle_stack_set_data (self->puzzle_stack,
                             "edit-window",
                             (gpointer) stage_to_stage_name (self->stage),
                             NULL);

    }
  else
    {
      const gchar *stage_name;

      stage_name = puzzle_stack_get_data (self->puzzle_stack,
                                          "edit-window");
      g_assert (stage_name != NULL);
      g_signal_handler_block (self->main_stack, self->stack_changed_id);
      gtk_stack_set_visible_child_name (GTK_STACK (self->main_stack),
                                        stage_name);
      g_signal_handler_unblock (self->main_stack, self->stack_changed_id);
      update_stack (self);
    }

  edit_window_update (self);
}

static void
edit_window_get_title_and_path (EditWindow  *self,
                                gchar      **title,
                                gchar      **path)
{
  IPuzPuzzle *puzzle;

  g_return_if_fail (EDIT_IS_WINDOW (self));
  g_return_if_fail (title != NULL);
  g_return_if_fail (path != NULL);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  if (puzzle == NULL)
    return;

  g_object_get (puzzle,
                "title", title,
                NULL);

  if (self->uri)
    *path = g_filename_from_uri (self->uri, NULL, NULL);
}

static void
edit_window_set_puzzle_stack (EditWindow  *self,
                              PuzzleStack *puzzle_stack)
{
  g_return_if_fail (PUZZLE_IS_STACK (puzzle_stack));

  if (self->puzzle_stack)
    g_clear_signal_handler (&self->changed_id,
                            self->puzzle_stack);

  g_object_ref (puzzle_stack);
  g_clear_object (&self->puzzle_stack);
  self->puzzle_stack = puzzle_stack;

  edit_basics_set_puzzle_stack (EDIT_BASICS (self->edit_basics), self->puzzle_stack);
  edit_xword_set_puzzle_stack (EDIT_XWORD (self->edit_xword), self->puzzle_stack);
  edit_clues_set_puzzle_stack (EDIT_CLUES (self->edit_clues), self->puzzle_stack);
  edit_metadata_set_puzzle_stack (EDIT_METADATA (self->edit_metadata), self->puzzle_stack);

  self->changed_id = g_signal_connect_swapped (self->puzzle_stack,
                                               "changed",
                                               G_CALLBACK (edit_window_puzzle_stack_changed),
                                               self);

  /* Force a changed signal to get the inital setup done */
  edit_window_puzzle_stack_changed (self, TRUE, STACK_CHANGE_PUZZLE, puzzle_stack);

  edit_window_update (self);
}

struct stage_name_table {
  const char *stage_name;
  EditWindowStage stage;
};

static const struct stage_name_table stage_names[] = {
  { "basic",    EDIT_STAGE_BASIC },
  { "grid",     EDIT_STAGE_GRID },
  { "clues",    EDIT_STAGE_CLUES },
  { "style",    EDIT_STAGE_STYLE },
  { "metadata", EDIT_STAGE_METADATA },
};

static EditWindowStage
stage_from_stage_name (const char *stage_name)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (stage_names); i++)
    {
      if (g_strcmp0 (stage_name, stage_names[i].stage_name) == 0)
        return stage_names[i].stage;
    }

  g_assert_not_reached ();
}

static const char *
stage_to_stage_name (EditWindowStage stage)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (stage_names); i++)
    {
      if (stage == stage_names[i].stage)
        return stage_names[i].stage_name;
    }

  g_assert_not_reached ();
}

static gchar *
get_temp_file_for_title (const gchar *title)
{
  const gchar *dir = NULL;
  gchar *filename;
  gchar *path;
  guint i = 1;

  dir = g_get_user_special_dir (G_USER_DIRECTORY_DOCUMENTS);
  filename = g_strdup_printf ("%s.ipuz", title);
  path = g_build_filename (dir, filename, NULL);
  g_free (filename);

  while (g_file_test (path, G_FILE_TEST_EXISTS))
    {
      g_free (path);

      filename = g_strdup_printf ("%s(%u).ipuz", title, i++);
      path = g_build_filename (dir, filename, NULL);
      g_free (filename);
    }

  return path;
}

/* Public methods */

/**
 * edit_window_get_unsaved_changes:
 * @self: A `EditWindow`
 * @title: location to store the title of the puzzle
 * @path: location to store the path of the puzzle
 *
 * Returns %TRUE if there are unsaved changes in @self. @title and
 * @path will be set to the title in that case. They will always be
 * set, either to the existing values or predictive values.
 *
 * Returns: %TRUE, if there are unsaved changes
 **/
gboolean
edit_window_get_unsaved_changes (EditWindow  *self,
                                 gchar      **title,
                                 gchar      **path)
{
  g_return_val_if_fail (EDIT_IS_WINDOW (self), FALSE);
  g_return_val_if_fail (title != NULL, FALSE);
  g_return_val_if_fail (path != NULL, FALSE);

  if (puzzle_stack_get_unsaved_changes (self->puzzle_stack))
    {
      edit_window_get_title_and_path (self, title, path);

      if (*title == NULL)
        *title = g_strdup (_("Untitled Puzzle"));
      if (*path == NULL)
        *path = get_temp_file_for_title (*title);
      return TRUE;
    }

  return FALSE;
}

void
edit_window_save (EditWindow  *edit_window,
                  const gchar *uri)
{
  g_return_if_fail (EDIT_IS_WINDOW (edit_window));
  g_return_if_fail (uri != NULL);


  g_clear_pointer (&edit_window->uri, g_free);
  edit_window->uri = g_strdup (uri);
  edit_window_actions_save (edit_window, NULL, NULL);
}


void
edit_window_load_uri (EditWindow  *edit_window,
                      const gchar *uri)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileInputStream) stream = NULL;

  g_return_if_fail (EDIT_IS_WINDOW (edit_window));

  file = g_file_new_for_uri (uri);
  stream = g_file_read (file, NULL, NULL);

  if (stream)
    {
      puzzle = ipuz_puzzle_new_from_stream (G_INPUT_STREAM (stream), NULL, NULL);
      g_input_stream_close (G_INPUT_STREAM (stream), NULL, NULL);
    }

  if (puzzle)
    {
      g_autoptr (PuzzleStack) puzzle_stack = NULL;

      g_clear_pointer (&edit_window->uri, g_free);
      edit_window->uri = g_strdup (uri);
      puzzle_stack = puzzle_stack_new (PUZZLE_STACK_EDIT, puzzle);
      edit_window_set_puzzle_stack (edit_window, puzzle_stack);
    }
  else
    {
      /* FIXME(dialog): Catch and warn for errors here */
    }
}
