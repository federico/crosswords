/* puzzle-downloader.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "puzzle-downloader.h"
#include "puzzle-downloader-dialog.h"

#define MASTER_SCHEMA "org.gnome.Crosswords.puzzle-sets"
#define PUZZLE_SET_PATH_PREFIX "/org/gnome/Crosswords/puzzle-sets/"

enum {
  FINISHED,
  N_SIGNALS
};


static guint obj_signals[N_SIGNALS] = { 0 };


/* This is a huge struct. I could turn it into a union, but it's not worth it */
struct _PuzzleDownloader
{
  GObject parent_instance;

  /* Common fields for all downloaders */
  DownloaderType type;
  GtkWindow *parent_window;
  gchar *id;
  gboolean convert_from_puz;
  gboolean requires_network;

  /* process info */
  gchar *command_string;
  GSubprocess *subprocess;
  GCancellable *wait_cancellable;

  /* cancel dialog */
  guint cancel_timeout;
  GtkWindow *cancel_dialog;

  /* File info */
  gchar *temp_file; /* The file that's being converted */

  /* User set values */
  GSettings *settings;
  gint number_value;
  GDateTime *date_value;
  gchar *string_value;

  /* File Dialog values */
  gchar *import_target_uri;

  /* Shared dialog values */
  gchar *header;
  gchar *primary_text;
  gchar *secondary_text;
  gchar *link_uri;
  gchar *link_text;

  /* Date fields */
  GDate *lower_date;
  GDate *upper_date;
  GDate *default_date;

  /* Number fields */
  int lower_number;
  int upper_number;
  int default_number;

  /* URI fields */
  gchar *default_domain;
};


static void   puzzle_downloader_init           (PuzzleDownloader      *self);
static void   puzzle_downloader_class_init     (PuzzleDownloaderClass *klass);
static void   puzzle_downloader_dispose        (GObject               *object);
static void   downloader_cancel_timeout_add    (PuzzleDownloader      *downloader);
static void   downloader_cancel_timeout_remove (PuzzleDownloader      *downloader);
static void   downloader_show_error_dialog     (PuzzleDownloader      *downloader,
                                                const gchar           *secondary_text);
static void   convert_puz_to_ipuz              (PuzzleDownloader      *downloader);
static gchar *stdout_to_temp_file              (GSubprocess           *subprocess);


G_DEFINE_TYPE (PuzzleDownloader, puzzle_downloader, G_TYPE_OBJECT);


static void
puzzle_downloader_init (PuzzleDownloader *self)
{
  self->lower_number = -1;
  self->upper_number = -1;
  self->default_number = -1;
}

static void
puzzle_downloader_class_init (PuzzleDownloaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = puzzle_downloader_dispose;

  /**
   * PuzzleDownloader::finished:
   * @downloader: the `PuzzleDownloader`
   * @uri: The URI of the file to be copied over
   * @delete_when_done: %TRUE, if the file is a temporary file
   *
   * Emitted when the downloader has either completed getting the
   * puzzle. If @uri is NULL than the user canceled the download. If
   * @delete_when_done is %TRUE, then the file should deleted after
   * it's been copied.
   */
  obj_signals[FINISHED] =
    g_signal_new ("finished",
                  PUZZLE_TYPE_DOWNLOADER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_STRING,
                  G_TYPE_BOOLEAN);
}

static void
puzzle_downloader_dispose (GObject *object)
{
  PuzzleDownloader *downloader;

  downloader = PUZZLE_DOWNLOADER (object);

  g_clear_pointer (&downloader->id, g_free);
  g_clear_pointer (&downloader->command_string, g_free);
  if (downloader->subprocess)
    {
      g_cancellable_cancel (downloader->wait_cancellable);
      g_clear_object (&downloader->wait_cancellable);
      g_subprocess_force_exit (downloader->subprocess);
    }

  g_clear_pointer (&downloader->temp_file, g_free);

  g_clear_object (&downloader->subprocess);
  g_clear_object (&downloader->settings);
  g_clear_pointer (&downloader->date_value, g_date_time_unref);
  g_clear_pointer (&downloader->string_value, g_free);

  g_clear_pointer (&downloader->header, g_free);
  g_clear_pointer (&downloader->primary_text, g_free);
  g_clear_pointer (&downloader->secondary_text, g_free);
  g_clear_pointer (&downloader->link_uri, g_free);
  g_clear_pointer (&downloader->link_text, g_free);

  g_clear_pointer (&downloader->import_target_uri, g_free);

  g_clear_pointer (&downloader->lower_date, g_date_free);
  g_clear_pointer (&downloader->upper_date, g_date_free);
  g_clear_pointer (&downloader->default_date, g_date_free);

  g_clear_pointer (&downloader->default_domain, g_free);

  downloader_cancel_timeout_remove (downloader);
  g_clear_object (&downloader->cancel_dialog);

  G_OBJECT_CLASS (puzzle_downloader_parent_class)->dispose (object);
}

/*
 * Cancel dialog
 */

static void
cancel_dialog_response (PuzzleDownloader *downloader,
                        int               response_id)
{
  if (downloader->type == DOWNLOADER_TYPE_FILE)
    {
      /* Clean up the partially downloaded file */
      if (downloader->temp_file)
        {
          unlink (downloader->temp_file);
          g_clear_pointer (&downloader->temp_file, g_free);
        }
      g_cancellable_cancel (downloader->wait_cancellable);
    }
  else
    {
      g_subprocess_force_exit (downloader->subprocess);
    }
}

static gboolean
downloader_cancel_timeout_cb (PuzzleDownloader *downloader)
{
  GtkDialogFlags flags;
  GtkWidget *message_area, *box, *spinner, *label;

  g_assert (PUZZLE_IS_DOWNLOADER (downloader));
  g_assert (downloader->cancel_dialog == NULL);

  flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;

  downloader->cancel_dialog = (GtkWindow *)
    gtk_message_dialog_new (downloader->parent_window,
                            flags,
                            GTK_MESSAGE_QUESTION,
                            GTK_BUTTONS_CANCEL,
                            NULL);

  message_area = gtk_message_dialog_get_message_area (GTK_MESSAGE_DIALOG (downloader->cancel_dialog));

  /* Simulate a message area. This is a little hokey, but it works
   * until GtkMessageDialog changes substantially. We use the primary
   * message as the secondary message, and add our own primary
   * messsage */
  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 10);

  spinner = gtk_spinner_new ();
  gtk_spinner_start (GTK_SPINNER (spinner));
  gtk_box_prepend (GTK_BOX (box), spinner);
  label = g_object_new (GTK_TYPE_LABEL,
                        "halign", GTK_ALIGN_CENTER,
                        "valign", GTK_ALIGN_START,
                        "wrap", TRUE,
                        "max-width-chars", 60,
                        "label", _("Cancel downloading the puzzle"),
                        NULL);
  gtk_widget_add_css_class (label, "title-2");
  gtk_box_append (GTK_BOX (box), label);

  gtk_box_prepend (GTK_BOX (message_area), box);

  g_signal_connect_swapped (downloader->cancel_dialog,
                            "response",
                            G_CALLBACK (cancel_dialog_response),
                            downloader);

  gtk_widget_show (GTK_WIDGET (downloader->cancel_dialog));

  downloader->cancel_timeout = 0;
  return G_SOURCE_REMOVE;
}

static void
downloader_cancel_timeout_add (PuzzleDownloader *downloader)
{
  g_assert (downloader->cancel_timeout == 0);

  downloader->cancel_timeout = g_timeout_add (2000,
                                              (GSourceFunc) downloader_cancel_timeout_cb,
                                              downloader);
}

static void
downloader_cancel_timeout_remove (PuzzleDownloader *downloader)
{
  if (downloader->cancel_timeout)
    {
      g_source_remove (downloader->cancel_timeout);
      downloader->cancel_timeout = 0;
    }
}

static void
downloader_show_error_dialog (PuzzleDownloader *downloader,
                              const gchar      *secondary_text)
{
  GtkDialogFlags flags;
  GtkWidget *dialog;

  g_assert (PUZZLE_IS_DOWNLOADER (downloader));

  flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;

  dialog = gtk_message_dialog_new (downloader->parent_window,
                                   flags,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_CANCEL,
                                   _("Problem downloading puzzle"));
  gtk_message_dialog_format_secondary_markup (GTK_MESSAGE_DIALOG (dialog),
                                              "%s", secondary_text);

  g_signal_connect (dialog, "response",
                            G_CALLBACK (gtk_window_destroy),
                            NULL);
  gtk_widget_show (dialog);
}

/*
 * Convertor
 */


void
convertor_finished_cb (GSubprocess      *subprocess,
                       GAsyncResult     *res,
                       PuzzleDownloader *downloader)
{
  g_autofree gchar *uri = NULL;
  g_autofree gchar *temp_file = NULL;

  /* The command returned. Let's get the file and save the setting */
  if (g_subprocess_get_successful (subprocess))
    temp_file = stdout_to_temp_file (subprocess);
  else
    downloader_show_error_dialog (downloader,
                                  _("Downloaded file is not a valid crossword."));

  /* Clear subprocess info so we can run another command */
  g_clear_object (&downloader->subprocess);
  if (downloader->temp_file)
    {
      unlink (downloader->temp_file);
      g_clear_pointer (&downloader->temp_file, g_free);
    }

  if (temp_file)
    uri = g_filename_to_uri (temp_file, NULL, NULL);

  g_signal_emit (downloader, obj_signals [FINISHED], 0, uri, TRUE);
}

/* Kicks off a convertor. We expect the potentially slow bits to be
 * done by now, and while we do this asyncronously we don't use the
 * cancel dialog.
 */
static void
convert_puz_to_ipuz (PuzzleDownloader *downloader)
{
  g_autoptr (GError) error = NULL;

  downloader->subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE,
                                             &error,
                                             "ipuz-convertor",
                                             "-i",
                                             downloader->temp_file,
                                             NULL);
  if (error)
    {
      /* FIXME(dialog): Put this in a dialog */
      g_warning ("Can't run the convertor: %s", error->message);
      return;
    }
  g_subprocess_wait_async (downloader->subprocess,
                           NULL,
                           (GAsyncReadyCallback) convertor_finished_cb,
                           downloader);
}


/*
 * Downloader command
 */

/* All this work for a > operator... :-S */
static gchar *
stdout_to_temp_file (GSubprocess *subprocess)
{
  gchar *temp_file = NULL;
  GInputStream *stdout_pipe;
  g_autoptr (GIOChannel) channel = NULL;
  g_autoptr (GError) error = NULL;
  gint fd;
  gsize bytes_read = 0;

  fd = g_file_open_tmp ("crosswords-XXXXXX",
                        &temp_file,
                        &error);
  if (fd == -1)
    {
      g_warning ("Could not create a temp_file: %s\n", error->message);
      return NULL;
    }

  /* FIXME(windows): write the windows version of this code. Should we
   * use g_file_new_tmp() instead? */
  channel = g_io_channel_unix_new (fd);
  g_io_channel_set_close_on_unref (channel, TRUE);

  /* Set the encoding to be NULL, to allow us to write binary data */
  g_io_channel_set_encoding (channel, NULL, NULL);
  stdout_pipe = g_subprocess_get_stdout_pipe (subprocess);

  do
    {
      gchar buf[4096];
      if (g_input_stream_read_all (stdout_pipe,
                                   buf, 4096,
                                   &bytes_read,
                                   NULL, &error))
        {
          GIOStatus result;

          result = g_io_channel_write_chars (channel,
                                             (gchar *) buf,
                                             bytes_read,
                                             NULL, &error);
          if (result != G_IO_STATUS_NORMAL)
            {
              g_warning ("unable to write to temp file: %s\n", temp_file);
              return NULL;
            }
        }
      else
        {
          g_warning ("unable to read from downloader: %s\n", error->message);
          return NULL;
        }
    }
  while (bytes_read > 0);

  return temp_file;
}

void
command_finished_cb (GSubprocess      *subprocess,
                     GAsyncResult     *res,
                     PuzzleDownloader *downloader)
{
  gchar *temp_file = NULL;

  /* The command reutrned. Let's get the file and save the setting */
  if (g_subprocess_get_successful (subprocess))
    {
      temp_file = stdout_to_temp_file (subprocess);
    }
  else
    {
      g_autofree gchar *secondary_text = NULL;

      secondary_text = g_strdup_printf (_("Unable to download a puzzle from \"%s.\""),
                                        downloader->header);
      downloader_show_error_dialog (downloader,
                                    secondary_text);
    }


  /* Clear subprocess info so we can run another command */
  downloader_cancel_timeout_remove (downloader);
  g_clear_pointer (&downloader->cancel_dialog, gtk_window_destroy);
  g_clear_object (&downloader->subprocess);

  /* If we successfully run the subcommand and captured it's stdout,
   * we either load the file, or potentially convert it. */
  if (temp_file)
    {
      if (downloader->convert_from_puz)
        {
          g_assert (downloader->temp_file == NULL);
          downloader->temp_file = temp_file;
          convert_puz_to_ipuz (downloader);
        }
      else
        {
          g_autofree gchar *uri = NULL;

          uri = g_filename_to_uri (temp_file, NULL, NULL);
          g_signal_emit (downloader, obj_signals [FINISHED], 0, uri, TRUE);
          g_free (temp_file);
        }
    }
  else
    {
      /* We didn't successfully get a file. This will cancel the
       * download */
      g_signal_emit (downloader, obj_signals[FINISHED], 0, NULL, FALSE);
    }
}

static gchar **
generate_argv (PuzzleDownloader *downloader)
{
  gchar **argv;

  argv = g_strsplit (downloader->command_string, " ", -1);

  /* FIXME(bug): dedup multi-spaces */
  for (int i = 0; argv[i] != NULL; i++)
    {
      if (g_strcmp0 (argv[i], "%N") == 0)
        {
          gint value;
          g_free (argv[i]);
          value = g_settings_get_int (downloader->settings, "saved-number-value");
          argv[i] = g_strdup_printf ("%d", value);
        }
      else if (g_strcmp0 (argv[i], "%D") == 0)
        {
          gchar *date_string = NULL;

          date_string = g_date_time_format_iso8601 (downloader->date_value);
          if (date_string == NULL)
            {
              g_warning ("Couldn't generate an ISO8601 date string. This should never happen.");
              g_free (date_string);
              continue;
            }

          g_free (argv[i]);
          argv[i] = date_string;
        }
      else if (g_strcmp0 (argv[i], "%U") == 0 ||
               g_strcmp0 (argv[0], "%S") == 0)
        {
          g_free (argv[i]);
          argv[i] = g_strdup (downloader->string_value);
        }
    }

  return argv;
}


static void
run_command (PuzzleDownloader  *downloader,
             GCancellable      *cancellable,
             GError           **error)
{
  g_autoptr (GError) tmp_error = NULL;
  gchar **argv;

  g_return_if_fail (downloader->command_string != NULL);

  /* If we've started a download, don't start a second one.*/
  if (downloader->subprocess)
    return;

  argv = generate_argv (downloader);
  if (argv == NULL)
    {
      /* FIXME(error): set a GError here */
      g_warning ("Invalid command in downloader:%s:", downloader->command_string);
      return;
    }
  downloader->subprocess = g_subprocess_newv ((const char * const*)argv,
                                              G_SUBPROCESS_FLAGS_STDOUT_PIPE,
                                              &tmp_error);
  g_strfreev (argv);

  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return;
    }

  downloader_cancel_timeout_add (downloader);

  downloader->wait_cancellable = g_cancellable_new ();
  g_subprocess_wait_async (downloader->subprocess,
                           downloader->wait_cancellable,
                           (GAsyncReadyCallback) command_finished_cb,
                           downloader);
}

/*
 * Input Dialogs
 */

static void
file_dialog_copy_ready_cb (GFile            *source,
                           GAsyncResult     *res,
                           PuzzleDownloader *downloader)
{

  downloader_cancel_timeout_remove (downloader);
  g_clear_pointer (&downloader->cancel_dialog, gtk_window_destroy);

  if (downloader->convert_from_puz)
    {
      convert_puz_to_ipuz (downloader);
    }
  else
    {
      g_autofree gchar *uri = NULL;

      uri = g_filename_to_uri (downloader->temp_file, NULL, NULL);
      g_signal_emit (downloader, obj_signals [FINISHED], 0, uri, TRUE);
      g_clear_pointer (&downloader->temp_file, g_free);
    }
}

static void
start_file_import (PuzzleDownloader *downloader)
{
  g_autoptr(GFile) source = NULL;
  g_autoptr (GFile) destination = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *basename = NULL;
  g_autoptr (GFileIOStream) stream = NULL;
  GFileCopyFlags flags;

  g_assert (downloader->import_target_uri != NULL);

  source = g_file_new_for_uri (downloader->import_target_uri);
  /* We can short circuit the download/conversion step if the file is
   * local and it's a .ipuz file. */
  /* FIXME(mime): It would be nice to do mime-sniffing here for puz
   * files, at least */
  /* FIXME(gio): g_file_query_filesystem_info might be a better
   * way to find out if it's local */
  if (strcmp (g_uri_peek_scheme (downloader->import_target_uri), "file") == 0)
    {
      g_autofree char *filename = NULL;
      filename = g_filename_from_uri (downloader->import_target_uri, NULL, NULL);
      if (filename != NULL && g_str_has_suffix (filename, ".ipuz"))
        {
          g_signal_emit (downloader, obj_signals [FINISHED], 0, downloader->import_target_uri, FALSE);
          return;
        }
    }

  /* The file is a remote file, or a .puz/jpz file. In either case, We
   * copy the file to a local temporary location to process it */
  destination = g_file_new_tmp ("crosswords-XXXXXX",
                                &stream, &error);
  if (destination == NULL)
    {
      g_warning ("Unable to create a tmp file: %s", error->message);
      return;
    }
  /* FIXME(mime): we really need to add sniffing */
  basename = g_file_get_basename (source);
  if (g_str_has_suffix (basename, ".puz") ||
      g_str_has_suffix (basename, ".jpz") ||
      g_str_has_suffix (basename, ".xml"))
    {
      downloader->convert_from_puz = TRUE;
      downloader->temp_file = g_file_get_path (destination);
    }
  else
    {
      downloader->convert_from_puz = FALSE;
    }

  downloader_cancel_timeout_add (downloader);
  downloader->wait_cancellable = g_cancellable_new ();

  flags = G_FILE_COPY_OVERWRITE;

  /* I think this might be wrong. Should I be writing to the stream?*/
  g_file_copy_async (source,
                     destination,
                     flags,
                     G_PRIORITY_DEFAULT,
                     downloader->wait_cancellable,
                     NULL, NULL,
                     (GAsyncReadyCallback) file_dialog_copy_ready_cb,
                     downloader);
}

static void
on_file_dialog_response (GtkNativeDialog  *dialog,
                         int               response,
                         PuzzleDownloader *downloader)
{
  g_autoptr(GFile) source = NULL;

  if (response == GTK_RESPONSE_ACCEPT)
    source = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (dialog));
  gtk_native_dialog_destroy (dialog);
  g_object_unref (dialog);

  /* This only exists when response == ACCEPT */
  if (source)
    {
      if (downloader->import_target_uri != NULL)
        g_warning ("This should never happen. We missed a case somewhere\n");
      downloader->import_target_uri = g_file_get_uri (source);
      start_file_import (downloader);
    }
  else /* User canceled opening a file */
    {
        g_signal_emit (downloader, obj_signals [FINISHED], 0, NULL, FALSE);
    }
}

static void
run_file_dialog (PuzzleDownloader *downloader)
{
  GtkFileChooserNative *native;
  GtkFileFilter *filter = NULL;

  native = gtk_file_chooser_native_new (_("Select a Puzzle"),
                                        downloader->parent_window,
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        _("_Open"),
                                        _("_Cancel"));
  gtk_native_dialog_set_modal (GTK_NATIVE_DIALOG (native), TRUE);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("All Puzzles"));
  gtk_file_filter_add_pattern (filter, "*.ipuz");
  gtk_file_filter_add_pattern (filter, "*.puz");
  gtk_file_filter_add_pattern (filter, "*.xml");
  gtk_file_filter_add_pattern (filter, "*.jpz");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (native), filter);
  g_object_unref (filter);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("IPuz Puzzles"));
  gtk_file_filter_add_pattern (filter, "*.ipuz");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (native), filter);
  g_object_unref (filter);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("AcrossLite Puzzles"));
  gtk_file_filter_add_pattern (filter, "*.puz");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (native), filter);
  g_object_unref (filter);


  g_signal_connect (native, "response",
                    G_CALLBACK (on_file_dialog_response),
                    downloader);

  gtk_native_dialog_show (GTK_NATIVE_DIALOG (native));
}

static void
on_input_dialog_response (PuzzleDownloaderDialog *dialog,
                          int                     response,
                          PuzzleDownloader       *downloader)
{
  if (response == GTK_RESPONSE_ACCEPT)
    {
      if (downloader->type == DOWNLOADER_TYPE_NUMBER)
        {
          g_settings_set_int (downloader->settings, "saved-number-value",
                              puzzle_downloader_dialog_get_number (dialog));
        }
      /* FIXME(downloader): support these types */
      else if (downloader->type == DOWNLOADER_TYPE_DATE)
        downloader->date_value = puzzle_downloader_dialog_get_date (dialog);
      else if (downloader->type == DOWNLOADER_TYPE_URL)
        downloader->string_value = puzzle_downloader_dialog_get_url (dialog);
      else if (downloader->type == DOWNLOADER_TYPE_ENTRY)
        downloader->string_value = puzzle_downloader_dialog_get_entry (dialog);

      run_command (downloader, NULL, NULL);
    }
  else
    {
      g_signal_emit (downloader, obj_signals [FINISHED], 0, NULL, FALSE);
    }

  gtk_window_destroy (GTK_WINDOW (dialog));

}

static void
run_input_dialog (PuzzleDownloader *downloader)
{
  PuzzleDownloaderDialog *dialog;

  dialog = (PuzzleDownloaderDialog *) puzzle_downloader_dialog_new
    (downloader->type,
     downloader->parent_window,
     downloader->primary_text,
     downloader->secondary_text,
     downloader->link_uri,
     downloader->link_text);

  if (downloader->type == DOWNLOADER_TYPE_NUMBER)
    {
      gint value;

      value = g_settings_get_int (downloader->settings, "saved-number-value");
      if (value == -1)
        {
          if  (downloader->default_number == -1)
            value = downloader->lower_number;
          else
            value = downloader->default_number;
        }

      puzzle_downloader_dialog_set_number_limits (dialog,
                                                  (downloader->lower_number < 0)?0:downloader->lower_number,
                                                  (downloader->upper_number < 0)?G_MAXINT:downloader->upper_number,
                                                  value);
    }
  /* FIXME(donwloader): Add the other types */

  g_signal_connect (dialog, "response",
                    G_CALLBACK (on_input_dialog_response),
                    downloader);
  gtk_widget_show (GTK_WIDGET (dialog));
}

/* Public methods */

PuzzleDownloader *
puzzle_downloader_new_filechooser ()
{
  PuzzleDownloader *downloader;

  downloader = (PuzzleDownloader *) g_object_new (PUZZLE_TYPE_DOWNLOADER, NULL);
  downloader->type = DOWNLOADER_TYPE_FILE;
  /* We know the filechooser has an ID of "uri" */
  downloader->id = g_strdup ("uri");
  return downloader;
}

PuzzleDownloader *
puzzle_downloader_new_from_key_file (GKeyFile *key_file)
{
  g_autoptr (PuzzleDownloader) downloader = NULL;
  g_autofree gchar *date_string = NULL;
  g_autofree gchar *type_string = NULL;
  g_autofree gchar *path = NULL;

  g_return_val_if_fail (key_file != NULL, NULL);

  downloader = (PuzzleDownloader *) g_object_new (PUZZLE_TYPE_DOWNLOADER, NULL);

  /* Now that we have the ID, we can set up the gsettings. */
  downloader->id = g_key_file_get_string (key_file, "Puzzle Set", "ID", NULL);
  path = g_strconcat (PUZZLE_SET_PATH_PREFIX, downloader->id, "/", NULL);
  downloader->settings = g_settings_new_with_path (MASTER_SCHEMA, path);


  type_string = g_key_file_get_string (key_file, "Downloader", "Type", NULL);
  if (type_string == NULL)
    {
      g_warning ("downloader missing a Type string\n");
      return NULL;
    }
  if (g_strcmp0 (type_string, "auto") == 0)
    downloader->type = DOWNLOADER_TYPE_AUTO;
  else if (g_strcmp0 (type_string, "date") == 0)
    downloader->type = DOWNLOADER_TYPE_DATE;
  else if (g_strcmp0 (type_string, "number") == 0)
    downloader->type = DOWNLOADER_TYPE_NUMBER;
  else if (g_strcmp0 (type_string, "url") == 0)
    downloader->type = DOWNLOADER_TYPE_URL;
  else if (g_strcmp0 (type_string, "entry") == 0)
    downloader->type = DOWNLOADER_TYPE_ENTRY;
  else /* possibly a type string we don't support. Fail gracefully */
    return NULL;

  downloader->header = g_key_file_get_string (key_file, "Picker List", "Header", NULL);
  downloader->primary_text = g_key_file_get_string (key_file, "Downloader", "PrimaryText", NULL);
  downloader->secondary_text = g_key_file_get_string (key_file, "Downloader", "SecondaryText", NULL);
  downloader->link_uri = g_key_file_get_string (key_file, "Downloader", "LinkUri", NULL);
  downloader->link_text = g_key_file_get_string (key_file, "Downloader", "LinkText", NULL);
  downloader->command_string = g_key_file_get_string (key_file, "Downloader", "Command", NULL);
  /* FIXME(validation): We should check all these strings */

  /* Default these to FALSE */
  downloader->convert_from_puz = g_key_file_get_boolean (key_file, "Downloader", "ConvertPuzToIpuz", NULL);
  downloader->requires_network = g_key_file_get_boolean (key_file, "Downloader", "RequiresNetwork", NULL);

  if (downloader->type == DOWNLOADER_TYPE_NUMBER)
    {
      GError *error = NULL;

      downloader->lower_number = g_key_file_get_int64 (key_file, "Downloader", "LowerValue", &error);
      if (error)
        {
          downloader->lower_number = -1;
          g_clear_pointer (&error, g_error_free);
        }
      downloader->upper_number = g_key_file_get_int64 (key_file, "Downloader", "UpperValue", NULL);
      if (error)
        {
          downloader->upper_number = -1;
          g_clear_pointer (&error, g_error_free);
        }
      downloader->default_number = g_key_file_get_int64 (key_file, "Downloader", "DefaultValue", NULL);
      if (error)
        {
          downloader->default_number = -1;
          g_clear_pointer (&error, g_error_free);
        }
    }

  if (downloader->command_string == NULL)
    {
      g_warning ("missing command field for downloader\n");
      return NULL;
    }

  if (downloader->type == DOWNLOADER_TYPE_DATE)
    {
      date_string = g_key_file_get_string (key_file, "Downloader", "LowerValue", NULL);
      if (date_string)
        {
          GDate *date;
          date = g_date_new ();
          g_date_set_parse (date, date_string);

          if (! g_date_valid (date))
            {
              g_warning ("invalid date string while parsing a downloader: %s\n", date_string);
              g_clear_pointer (&date, g_date_free);
            }

          downloader->lower_date = date;
        }
    }

  return g_object_ref (downloader);
}

void
puzzle_downloader_add_import_target (PuzzleDownloader *downloader,
                                     const gchar      *uri)
{
  g_return_if_fail (PUZZLE_IS_DOWNLOADER (downloader));
  g_return_if_fail (downloader->type == DOWNLOADER_TYPE_FILE);
  g_return_if_fail (uri != NULL);
  g_return_if_fail (downloader->import_target_uri == NULL);
  
  downloader->import_target_uri = g_strdup (uri);
}

gboolean
puzzle_downloader_get_requires_network (PuzzleDownloader *downloader)
{
  g_return_val_if_fail (PUZZLE_IS_DOWNLOADER (downloader), FALSE);

  return downloader->requires_network;
}


void
puzzle_downloader_run_async (PuzzleDownloader  *downloader,
                             GtkWindow         *parent_window,
                             GCancellable      *cancellable,
                             GError           **error)
{
  g_return_if_fail (PUZZLE_IS_DOWNLOADER (downloader));
  g_return_if_fail (GTK_IS_WINDOW (parent_window));

  downloader->parent_window = parent_window;

  if (downloader->type == DOWNLOADER_TYPE_FILE)
    {
      if (downloader->import_target_uri)
        start_file_import (downloader);
      else
        run_file_dialog (downloader);
    }
  else if (downloader->type == DOWNLOADER_TYPE_AUTO)
    {
      run_command (downloader, cancellable, error);
    }
  else
    {
      run_input_dialog (downloader);
    }
}
