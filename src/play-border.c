/* play-border.c - Border items for PlayGrid.
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "play-border.h"
#include "play-style.h"

static void play_border_init           (PlayBorder       *self);
static void play_border_class_init     (PlayBorderClass  *klass);
static void play_border_measure        (GtkWidget        *widget,
                                        GtkOrientation    orientation,
                                        int               for_size,
                                        int              *minimum,
                                        int              *natural,
                                        int              *minimum_baseline,
                                        int              *natural_baseline);
static void play_border_snapshot       (GtkWidget        *widget,
                                        GtkSnapshot      *snapshot);

struct _PlayBorder {
  GtkWidget parent_instance;

  PlayBorderKind kind;
  LayoutConfig config;

  gboolean filled;
  LayoutItemBorderStyle css_class;
  gboolean bg_color_set;
  GdkRGBA bg_color;
};

G_DEFINE_TYPE (PlayBorder, play_border, GTK_TYPE_WIDGET);

static void
play_border_init (PlayBorder *self)
{
  self->css_class = LAYOUT_BORDER_STYLE_NORMAL;
}

static void
play_border_class_init (PlayBorderClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  widget_class->measure = play_border_measure;
  widget_class->snapshot = play_border_snapshot;

  gtk_widget_class_set_css_name (widget_class, "play-border");
}

static void
play_border_measure (GtkWidget        *widget,
                     GtkOrientation    orientation,
                     int               for_size,
                     int              *minimum,
                     int              *natural,
                     int              *minimum_baseline,
                     int              *natural_baseline)
{
  PlayBorder *self = PLAY_BORDER (widget);
  int width, height;

  /* If we are an intersection, we want border_size * border_size.
   *
   * If we are a horizontal or vertical border, we want to be as wide or as tall as cells,
   * and border_size tall/wide, respectively.
   *
   * However, we hope that the parent grid widget will adjust the dimension that depends
   * on the cell size, and leave the other dimension alone.
   */

  switch (self->kind)
    {
    case PLAY_BORDER_KIND_INTERSECTION:
      width = self->config.border_size;
      height = self->config.border_size;
      break;

    case PLAY_BORDER_KIND_HORIZONTAL:
      width = 3 * self->config.base_size;
      height = self->config.border_size;
      break;

    case PLAY_BORDER_KIND_VERTICAL:
      width = self->config.border_size;
      height = 3 * self->config.base_size;
      break;

    default:
      g_assert_not_reached ();
      return;
    }

  switch (orientation) {
  case GTK_ORIENTATION_HORIZONTAL:
    *minimum = width;
    *natural = width;
    break;

  case GTK_ORIENTATION_VERTICAL:
    *minimum = height;
    *natural = height;
    break;

  default:
    g_assert_not_reached();
  }
}

static void
play_border_snapshot (GtkWidget   *widget,
                      GtkSnapshot *snapshot)
{
  PlayBorder *self = PLAY_BORDER (widget);
  int width, height;

  width = gtk_widget_get_width (widget);
  height = gtk_widget_get_height (widget);

  if (self->filled)
    {
      GdkRGBA color;
      if (self->bg_color_set)
        {
          color = self->bg_color;
        }
      else
        {
          GtkStyleContext *context;

          context = gtk_widget_get_style_context (widget);
          gtk_style_context_get_color (context, &color);
        }
      gtk_snapshot_append_color (snapshot, &color,
                                 &GRAPHENE_RECT_INIT (0, 0, width, height));
    }
}

GtkWidget *
play_border_new (PlayBorderKind kind,
                 LayoutConfig   config)
{
  PlayBorder *self = g_object_new (PLAY_TYPE_BORDER, NULL);
  self->kind = kind;
  self->config = config;

  return GTK_WIDGET (self);
}

static const gchar *
css_class_to_string (LayoutItemBorderStyle css_class)
{
  switch (css_class)
    {
    case LAYOUT_BORDER_STYLE_UNSET:
      return "unset";
    case LAYOUT_BORDER_STYLE_NORMAL:
      return NULL;
    case LAYOUT_BORDER_STYLE_DARK:
      return "dark";
    case LAYOUT_BORDER_STYLE_FOCUSED:
      return "focused";
    case LAYOUT_BORDER_STYLE_SELECTED:
      return "selected";
    case LAYOUT_BORDER_STYLE_HIGHLIGHTED:
      return "highlighted";
    case LAYOUT_BORDER_STYLE_INITIAL_VAL:
      return "initial-val";
    default:
      g_assert_not_reached ();
    }
  return NULL;
}

static void
play_border_set_css_class (PlayBorder            *cell,
                           LayoutItemBorderStyle  old_css_class,
                           LayoutItemBorderStyle  new_css_class)
{
  if (old_css_class == new_css_class)
    return;

  /* Set the css classes on the widget */
  if (old_css_class != LAYOUT_BORDER_STYLE_NORMAL)
    gtk_widget_remove_css_class (GTK_WIDGET (cell), css_class_to_string (old_css_class));
  if (new_css_class != LAYOUT_BORDER_STYLE_NORMAL)
    gtk_widget_add_css_class (GTK_WIDGET (cell), css_class_to_string (new_css_class));
}

void
play_border_update_state (PlayBorder            *self,
                          gboolean               filled,
                          LayoutItemBorderStyle  css_class,
                          gboolean               bg_color_set,
                          GdkRGBA                bg_color)
{
  g_return_if_fail (PLAY_IS_BORDER (self));

  self->filled = !!filled;

  play_border_set_css_class (self, self->css_class, css_class);
  self->css_class = css_class;
  self->bg_color_set = bg_color_set;
  self->bg_color = bg_color;

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

void
play_border_update_config (PlayBorder   *border,
                           LayoutConfig  config)
{
  g_return_if_fail (PLAY_IS_BORDER (border));

  if (! layout_config_equal (&config, &border->config))
    {
      border->config = config;
      gtk_widget_queue_resize (GTK_WIDGET (border));
    }
}
