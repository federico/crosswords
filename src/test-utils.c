#include "test-utils.h"

XwordState *
load_state (const char *filename, XwordStateMode mode)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IPuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  XwordState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = xword_state_new (IPUZ_CROSSWORD (puzzle), NULL, mode);
  g_assert (state != NULL);

  g_object_unref (puzzle);
  g_free (path);

  return state;
}
