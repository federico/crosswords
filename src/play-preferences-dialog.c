/* play-preferences-dialog.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib/gi18n.h>

#include "crosswords-app.h"
#include "crosswords-quirks.h"
#include "play-preferences-dialog.h"


struct _PlayPreferencesDialog
{
  AdwPreferencesWindow parent_instance;

  GSettings *settings;

  /* Template widgets */
  GtkWidget *show_all_languages_switch;
  GtkWidget *guess_behavior_row;
  GtkWidget *switch_on_move_switch;
};


static void      play_preferences_dialog_init       (PlayPreferencesDialog      *self);
static void      play_preferences_dialog_class_init (PlayPreferencesDialogClass *klass);
static void      play_preferences_dialog_dispose    (GObject                    *object);
static gboolean  guess_behavior_row_get_mapping     (GValue                     *value,
                                                     GVariant                   *variant,
                                                     gpointer                    user_data);
static GVariant *guess_behavior_row_set_mapping     (const GValue               *value,
                                                     const GVariantType         *expected_type,
                                                     gpointer                    user_data);


G_DEFINE_TYPE (PlayPreferencesDialog, play_preferences_dialog, ADW_TYPE_PREFERENCES_WINDOW);


static void
play_preferences_dialog_class_init (PlayPreferencesDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = play_preferences_dialog_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/play-preferences-dialog.ui");
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, show_all_languages_switch);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, guess_behavior_row);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, switch_on_move_switch);
}

static void
play_preferences_dialog_init (PlayPreferencesDialog *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

  self->settings = g_settings_new ("org.gnome.Crosswords");
  g_settings_bind (self->settings, "show-all-languages",
                   self->show_all_languages_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind_with_mapping (self->settings, "guess-advance-type",
                                self->guess_behavior_row, "selected",
                                G_SETTINGS_BIND_DEFAULT,
                                guess_behavior_row_get_mapping,
                                guess_behavior_row_set_mapping,
                                NULL, NULL);
  g_settings_bind (self->settings, "switch-on-move",
                   self->switch_on_move_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
}

static void
play_preferences_dialog_dispose (GObject *object)
{
  PlayPreferencesDialog *self;

  self = PLAY_PREFERENCES_DIALOG (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (play_preferences_dialog_parent_class)->dispose (object);
}

static gboolean
guess_behavior_row_get_mapping (GValue   *value,
                                GVariant *variant,
                                gpointer  user_data)
{
  const gchar *advance;

  advance = g_variant_get_string (variant, NULL);

  if (! g_strcmp0 (advance, "adjacent"))
    g_value_set_uint (value, (guint) QUIRKS_GUESS_ADVANCE_ADJACENT);
  else if (! g_strcmp0 (advance, "open"))
    g_value_set_uint (value, (guint) QUIRKS_GUESS_ADVANCE_OPEN);
  else if (! g_strcmp0 (advance, "open-in-clue"))
    g_value_set_uint (value, (guint) QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE);
  else
    {
      g_warning ("Unknown advance type: %s", advance);
      return FALSE;
    }

  return TRUE;
}

static GVariant *
guess_behavior_row_set_mapping (const GValue       *value,
                                const GVariantType *expected_type,
                                gpointer            user_data)
{
  GVariant *retval = NULL;

  /* FIXME(enum): Maybe we should use the enum nick for this */
  switch (g_value_get_uint (value))
    {
    case QUIRKS_GUESS_ADVANCE_ADJACENT:
      retval = g_variant_new_string ("adjacent");
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN:
      retval = g_variant_new_string ("open");
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE:
      retval = g_variant_new_string ("open-in-clue");
      break;
    default:
      g_assert_not_reached ();
    }

  return retval;
}

/* Public functions */

GtkWidget *
play_preferences_dialog_new (GtkWindow *parent_window)
{
  g_return_val_if_fail (GTK_IS_WINDOW (parent_window), NULL);

  return g_object_new (PLAY_TYPE_PREFERENCES_DIALOG,
                       "application", CROSSWORDS_APP_DEFAULT,
                       "transient-for", parent_window,
                       NULL);
}
