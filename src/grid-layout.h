/* layout.h - Grid layout for crosswords
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "xword-state.h"

G_BEGIN_DECLS

/* This is the default base size in pixels used to determine the
 * layout of a cell. See docs/cell-content.png for more info. */
#define DEFAULT_CELL_BASE_SIZE 12
#define MIN_CELL_BASE_SIZE 6
#define MAX_CELL_BASE_SIZE 480


typedef enum
{
  LAYOUT_ITEM_KIND_INTERSECTION,      /* @ Corresponds to LayoutIntersection @ */
  LAYOUT_ITEM_KIND_BORDER_HORIZONTAL, /* @ Corresponds to LayoutBorderHorizontal @*/
  LAYOUT_ITEM_KIND_BORDER_VERTICAL,   /* @ Corresponds to LayoutBorderVertical @ */
  LAYOUT_ITEM_KIND_CELL,              /* @ Corresponds to LayoutCell @ */
  LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL,   /* @ Corresponds to LayoutClueBlockCell @ */
} LayoutItemKind;

typedef enum
{
  LAYOUT_OVERLAY_KIND_BARRED,                 /* @ Extra-thick border @ */
  LAYOUT_OVERLAY_KIND_ENUMERATION_SPACE,      /* @ Thick bar to indicate word break @ */
  LAYOUT_OVERLAY_KIND_ENUMERATION_DASH,       /* @ Dash to indicate word separation style @ */
  LAYOUT_OVERLAY_KIND_ENUMERATION_APOSTROPHE, /* @ Apostrophe to indicate word separation style @ */
  LAYOUT_OVERLAY_KIND_ENUMERATION_PERIOD,     /* @ Period to indicate word separation style @ */
} LayoutOverlayKind;

typedef enum
{
  LAYOUT_ITEM_STYLE_UNSET,         /* @ Not used @ */
  LAYOUT_ITEM_STYLE_NULL,          /* @ Null cell @ */
  LAYOUT_ITEM_STYLE_NORMAL,        /* @ Normal cell @ */
  LAYOUT_ITEM_STYLE_FOCUSED,       /* @ Normal cell with the cursor on it @ */
  LAYOUT_ITEM_STYLE_BLOCK,         /* @ Block cell @ */
  LAYOUT_ITEM_STYLE_FOCUSED_BLOCK, /* @ Block cell with the cursor on it @ */
  LAYOUT_ITEM_STYLE_ERROR,         /* @ Normal cell indicating an error @ */
  LAYOUT_ITEM_STYLE_SELECTED,      /* @ Cell in a clue that has the cursor @*/
  LAYOUT_ITEM_STYLE_HIGHLIGHTED,   /* @ Cell marked as highlighted by the puzzle @*/
  LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED, /* @ a selected cell marked as highlighted @*/
  LAYOUT_ITEM_STYLE_INITIAL_VAL,   /* @ Cell with a preset-value @ */
  LAYOUT_ITEM_STYLE_CLUE_BLOCK,    /* @ Block a clue in it (for an arrowword) @ */
} LayoutItemCellStyle;

#define LAYOUT_ITEM_CSS_GUESSABLE(css_type) ((css_type==LAYOUT_ITEM_STYLE_NORMAL)||\
                                             (css_type==LAYOUT_ITEM_STYLE_FOCUSED)||\
                                             (css_type==LAYOUT_ITEM_STYLE_SELECTED)||\
                                             (css_type==LAYOUT_ITEM_STYLE_HIGHLIGHTED)||\
                                             (css_type==LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED))

typedef enum
{
  LAYOUT_BORDER_STYLE_UNSET,         /* @ Not used @ */
  LAYOUT_BORDER_STYLE_DARK,          /* @ Between two block cells, or the edge @ */
  LAYOUT_BORDER_STYLE_NORMAL,        /* @ Normal cell @ */
  LAYOUT_BORDER_STYLE_FOCUSED,       /* @ Next to the cursor @ */
  LAYOUT_BORDER_STYLE_SELECTED,      /* @ Beteween two selected cells @ */
  LAYOUT_BORDER_STYLE_HIGHLIGHTED,   /* @ Beteween two highlighted cells @ */
  LAYOUT_BORDER_STYLE_INITIAL_VAL,   /* @ Between two initial val cells @ */
} LayoutItemBorderStyle;

typedef enum
{
  LAYOUT_SPLIT_HIGHLIGHT_NONE,      /* @ cell isn't highlighted @ */
  LAYOUT_SPLIT_HIGHLIGHT_FULL,      /* @ Entire cell is highlighted @ */
  LAYOUT_SPLIT_HIGHLIGHT_PRIMARY,   /* @ Primary text area is highlighted @ */
  LAYOUT_SPLIT_HIGHLIGHT_SECONDARY, /* @ Secondary cell is highlighted @ */
} LayoutItemSplitHighlightState;
typedef enum
{
  ZOOM_XSMALL, /* @ Smallest zoom level @ */
  ZOOM_SMALL,  /* @ Smaller zoom level @ */
  ZOOM_NORMAL, /* @ Default zoom level. The base size is DEFAULT_CELL_BASE_SIZE @ */
  ZOOM_LARGE,  /* @ Larger zoom level @ */
  ZOOM_XLARGE, /* @ Even Larger zoom level @ */
  ZOOM_UNSET,
} ZoomLevel;


/* Structures */

typedef struct
{
  guint border_size; /* @ Distance between rows or columns @ */
  guint base_size;   /* @ Base size for cells. See  */
} LayoutConfig;

typedef struct
{
  gboolean filled;
  LayoutItemBorderStyle css_class;
  GdkRGBA bg_color;
  gboolean bg_color_set;
} LayoutIntersection;

typedef struct
{
  gboolean filled;
  LayoutItemBorderStyle css_class;
  GdkRGBA bg_color;
  gboolean bg_color_set;
} LayoutBorderHorizontal;

typedef struct
{
  gboolean filled;
  LayoutItemBorderStyle css_class;
  GdkRGBA bg_color;
  gboolean bg_color_set;
} LayoutBorderVertical;

typedef struct
{
  IPuzCellCellType cell_type;
  LayoutItemCellStyle css_class;
  IPuzStyleDivided divided;
  const char *main_text;
  GdkRGBA bg_color;
  gboolean bg_color_set;
  GdkRGBA text_color;
  gboolean text_color_set;

  /* Points into the GridLayout's array of strings; has the lifetime of the GridLayout */
} LayoutCell;

typedef struct
{
  IPuzCellCellType cell_type;
  LayoutItemCellStyle css_class;
  IPuzStyleDivided divided;
  const char *main_text;
  const char *secondary_text;
  IPuzArrowwordArrow top_arrow;
  IPuzArrowwordArrow bottom_arrow;
  LayoutItemSplitHighlightState split_highlight_state;
} LayoutClueBlockCell;

typedef struct
{
  /* Dimensions of the puzzle, i.e. how many cells in the board */
  guint board_rows;
  guint board_columns;

  /* Dimensions of the layout grid; see "Grid layout for crosswords" above */
  guint grid_rows;
  guint grid_columns;

  /* 2D array of LayoutItem, grid_rows * grid_columns, row major, rowstride=grid_columns */
  GArray *grid;

  /* Array of LayoutOverlay */
  GArray *overlays;

  /* Bag of strings that get displayed on the board.  For example, LayoutCell.main_text will point into this. */
  GStringChunk *strings;
} GridLayout;

/* Coordinates within the layout grid.
 *
 *    0 2 4 6
 * 0  +-+-+-+
 *    |C|C|C|
 * 2  +-+-+=+
 *    |C|C|X|
 * 4  +-+-+-+
 *    |C|C|C|
 * 6  +-+-+-+
 *
 * For example, X is at row=3, column=5; the = above it is at row=2, column=5.
 */
typedef struct
{
  guint row;
  guint column;
} GridCoord;

typedef struct
{
  LayoutOverlayKind kind;

  GridCoord coord;
} LayoutOverlay;

/* Layout Config */
LayoutConfig           layout_config_default             (IPuzPuzzleKind       kind);
LayoutConfig           layout_config_at_base_size        (guint                base_size);
LayoutConfig           layout_config_at_zoom_level       (IPuzPuzzleKind       kind,
                                                          ZoomLevel            zoom_level);
void                   layout_config_size_at_base_size   (guint                base_size,
                                                          guint                xword_width,
                                                          guint                xword_height,
                                                          gint                *width,
                                                          gint                *height);
LayoutConfig           layout_config_within_bounds       (guint                min_base_size,
                                                          guint                max_base_size,
                                                          gint                 bounds_width,
                                                          gint                 bounds_height,
                                                          guint                xword_width,
                                                          guint                xword_height,
                                                          gboolean            *valid);
gboolean               layout_config_equal               (LayoutConfig        *a,
                                                          LayoutConfig        *b);

/* Grid Layout */
GridLayout            *grid_layout_new                   (XwordState          *state);
void                   grid_layout_free                  (GridLayout          *layout);
LayoutItemKind         grid_layout_get_kind              (GridLayout          *layout,
                                                          GridCoord            coord);
LayoutIntersection     grid_layout_get_intersection      (GridLayout          *layout,
                                                          GridCoord            coord);
LayoutBorderHorizontal grid_layout_get_border_horizontal (GridLayout          *layout,
                                                          GridCoord            coord);
LayoutBorderVertical   grid_layout_get_border_vertical   (GridLayout          *layout,
                                                          GridCoord            coord);
LayoutCell             grid_layout_get_cell              (GridLayout          *layout,
                                                          GridCoord            coord);
LayoutClueBlockCell    grid_layout_get_clue_block_cell   (GridLayout          *layout,
                                                          GridCoord            coord);
void                   grid_layout_print                 (GridLayout          *layout);

/* Helper functions for tests */
gboolean               layout_cell_equal                 (const LayoutCell *a,
                                                          const LayoutCell *b);
gboolean               layout_overlay_equal              (const LayoutOverlay *a,
                                                          const LayoutOverlay *b);

/* Zoom helper-functions */
const gchar           *zoom_level_to_string              (ZoomLevel            zoom_level);
ZoomLevel              zoom_level_from_string            (const gchar         *zoom_string);
ZoomLevel              zoom_level_zoom_in                (ZoomLevel            zoom_level);
ZoomLevel              zoom_level_zoom_out               (ZoomLevel            zoom_level);




G_END_DECLS
