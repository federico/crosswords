/* edit-clue-entry.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "edit-clue-entry.h"
#include <libipuz/libipuz.h>


struct _EditClueEntry
{
  GtkWidget  parent_instance;
  IPuzClueId clue_id;
};


static void edit_clue_entry_init       (EditClueEntry      *self);
static void edit_clue_entry_class_init (EditClueEntryClass *klass);
static void edit_clue_entry_dispose    (GObject          *object);


G_DEFINE_TYPE (EditClueEntry, edit_clue_entry, GTK_TYPE_WIDGET);

static void
edit_clue_entry_init (EditClueEntry *self)
{
  self->clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;
}

static void
edit_clue_entry_class_init (EditClueEntryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = edit_clue_entry_dispose;

  gtk_widget_class_set_layout_manager_type (GTK_WIDGET_CLASS (klass),
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
edit_clue_entry_dispose (GObject *object)
{
  GtkWidget *child;

  g_return_if_fail (object != NULL);


  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_clue_entry_parent_class)->dispose (object);
}

static void
edit_clue_entry_set_clue (EditClueEntry *clue_entry,
                          IPuzClue      *clue,
                          GtkSizeGroup  *size_group)
{
  GtkWidget *clue_label;
  g_autofree gchar *clue_string = NULL;
  const gchar *clue_text;
  GtkWidget *num_label = NULL;
  g_autofree gchar *num_text = NULL;
  gboolean showenumerations = FALSE;

  g_return_if_fail (clue != NULL);

  clue_text = ipuz_clue_get_clue_text (clue);

  /* Set the clue number/label */


  if (ipuz_clue_get_number (clue) > 0)
    num_text = g_strdup_printf ("<b>%d</b> ", ipuz_clue_get_number (clue));
  else if (ipuz_clue_get_label (clue) != NULL)
    num_text = g_markup_printf_escaped ("<b>%s</b> ", ipuz_clue_get_label (clue));
  if (num_text)
    {
      num_label = gtk_label_new (num_text);
      gtk_label_set_xalign (GTK_LABEL (num_label), 0.0);
      gtk_label_set_yalign (GTK_LABEL (num_label), 0.0);
      gtk_label_set_use_markup (GTK_LABEL (num_label), TRUE);
      gtk_size_group_add_widget (size_group, num_label);
      gtk_widget_insert_before (num_label, GTK_WIDGET (clue_entry), NULL);
    }

  /* The clue label */
  if (showenumerations)
    {
      g_autoptr (IPuzEnumeration) enumeration = NULL;

      enumeration = ipuz_clue_get_enumeration (clue);
      if (enumeration)
        clue_string = g_markup_printf_escaped ("%s  (%s)",
                                               clue_text,
                                               ipuz_enumeration_get_display (enumeration));
      else
        /* We guess the enumeration */
        clue_string = g_markup_printf_escaped ("%s  (%d)",
                                               clue_text,
                                               (gint) g_utf8_strlen (clue_text, 0));
    }
  else
    {
      clue_string = g_markup_printf_escaped ("%s", clue_text);
    }

  clue_label = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (clue_label), clue_string);
  gtk_label_set_wrap (GTK_LABEL (clue_label), TRUE);
  gtk_label_set_width_chars (GTK_LABEL (clue_label), 30);
  gtk_label_set_xalign (GTK_LABEL (clue_label), 0.0);
  gtk_label_set_yalign (GTK_LABEL (clue_label), 0.0);
  gtk_widget_insert_after (clue_label, GTK_WIDGET (clue_entry), num_label);
}

/* Public Members */

GtkWidget *
edit_clue_entry_new (IPuzClue     *clue,
                     IPuzClueId    clue_id,
                     GtkSizeGroup *size_group)
{
  EditClueEntry *clue_entry;

  g_return_val_if_fail (clue != NULL, NULL);

  clue_entry = g_object_new (EDIT_TYPE_CLUE_ENTRY, NULL);
  clue_entry->clue_id = clue_id;
  edit_clue_entry_set_clue (clue_entry, clue, size_group);

  return GTK_WIDGET (clue_entry);
}

IPuzClueId
edit_clue_entry_get_clue_id (EditClueEntry *clue_entry)
{
  IPuzClueId none = { .direction = IPUZ_CLUE_DIRECTION_NONE, .index = 0 };

  g_return_val_if_fail (EDIT_IS_CLUE_ENTRY (clue_entry), none);

  return clue_entry->clue_id;
}
