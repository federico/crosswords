/* charset.h - Maintain a character set for a crossword puzzle
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef struct Charset Charset;

Charset *charset_new            (void);
void     charset_free           (Charset       *charset);
void     charset_add            (Charset       *charset,
                                 const char    *str);
gint     charset_index          (const Charset *charset,
                                 gunichar       c);
gsize    charset_num_characters (const Charset *charset);
char    *charset_serialize      (const Charset *charset);
Charset *charset_deserialize    (const char    *str);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Charset, charset_free);

G_END_DECLS
