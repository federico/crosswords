#include <glib.h>
#include <locale.h>
#include "grid-layout.h"
#include "layout-tests.h"
#include "test-utils.h"

/* Tests for a complete puzzle */
#define FOCUS_TEST_FILENAME "tests/focus.ipuz"

/* Tests for a puzzle with no clues */
#define NO_CLUES_TEST_FILENAME "tests/focus-noclues.ipuz"

/* Tests for a puzzle with no focusable cells */
#define NO_FOCUSABLE_CELLS_TEST_FILENAME "tests/focus-no-focusable-cells.ipuz"

/* Tests for a puzzle where not all cells have clues */
#define SOME_CLUES_TEST_FILENAME "tests/focus-someclues.ipuz"

static void
assert_state (const XwordState *state, gboolean has_cursor, guint row, guint column, IPuzClueDirection direction, guint clue_index)
{
  XwordState test = {
    .xword = state->xword,
    .clue.index = clue_index,
    .clue.direction = direction,
    .cursor.row = row,
    .cursor.column = column,
    .quirks = state->quirks
  };

  xword_state_assert_equal (state, &test);
}

static void
initial_state (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  xword_state_free (state);
}

static void
can_select_valid_cells (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzCellCoord coord;

  coord.row = 1;
  coord.column = 0;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  coord.row = 0;
  coord.column = 1;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  xword_state_free (state);
}

static void
selecting_cell_changes_direction (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  IPuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  xword_state_free (state);
}

G_GNUC_WARN_UNUSED_RESULT static XwordState *
assert_cell_is_not_selectable (XwordState *state, guint row, guint column, IPuzCellCellType cell_type)
{
  IPuzCellCoord old_coord = state->cursor;
  guint old_direction = state->clue.direction;
  guint old_index = state->clue.index;
  IPuzCell *cell;
  IPuzCellCoord coord = { .row = row, .column = column };

  /* Check the cell type */
  cell = ipuz_crossword_get_cell (state->xword, coord);
  g_assert (cell != NULL);
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, cell_type);

  /* And it must not be selectable, and the state must remain unchanged */
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, old_coord.row, old_coord.column, old_direction, old_index);
  return state;
}

static void
wont_select_null_cells (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  state = assert_cell_is_not_selectable (state, 1, 1, IPUZ_CELL_NULL);

  xword_state_free (state);
}

static void
wont_select_block_cells (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  state = assert_cell_is_not_selectable (state, 2, 1, IPUZ_CELL_BLOCK);

  xword_state_free (state);
}

static IPuzCellCoord
coord_for_cmd_kind (IPuzCellCoord coord, XwordCmdKind kind)
{
  switch (kind)
    {
    case XWORD_CMD_KIND_UP:
      coord.row -= 1;
      break;

    case XWORD_CMD_KIND_DOWN:
      coord.row += 1;
      break;

    case XWORD_CMD_KIND_LEFT:
      coord.column -= 1;
      break;

    case XWORD_CMD_KIND_RIGHT:
      coord.column += 1;
      break;

    default:
      g_assert_not_reached ();
      break;

    }

  return coord;
}


static void
assert_focuses_within_clue (guint start_row,
                            guint start_col,
                            guint num_cells,
                            XwordCmdKind focus_forward,
                            XwordCmdKind focus_back,
                            IPuzClueDirection direction)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzCellCoord coord = {
    .row = start_row,
    .column = start_col,
  };
  guint i;

  /* Select the starting cell and ensure we are in the correct direction */

  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  if (state->clue.direction != direction)
    {
      state = xword_state_replace (state, xword_state_cell_selected (state, coord));
    }

  gint clue_index = state->clue.index;
  assert_state (state, TRUE, coord.row, coord.column, direction, clue_index);

  /* Go forward... */
  for (i = 0; i < num_cells; i++)
    {
      state = xword_state_replace (state, xword_state_do_command (state, focus_forward));
      coord = coord_for_cmd_kind (coord, focus_forward);
      assert_state (state, TRUE, coord.row, coord.column, direction, clue_index);
    }

  /* Go back... */
  for (i = 0; i < num_cells; i++)
    {
      state = xword_state_replace (state, xword_state_do_command (state, focus_back));
      coord = coord_for_cmd_kind (coord, focus_back);
      assert_state (state, TRUE, coord.row, coord.column, direction, clue_index);
    }

  xword_state_free (state);
}

static void
focuses_within_clue_across (void)
{
  assert_focuses_within_clue (0, 0, 3, XWORD_CMD_KIND_RIGHT, XWORD_CMD_KIND_LEFT, IPUZ_CLUE_DIRECTION_ACROSS);
}

static void
focuses_within_clue_down (void)
{
  assert_focuses_within_clue (0, 0, 3, XWORD_CMD_KIND_DOWN, XWORD_CMD_KIND_UP, IPUZ_CLUE_DIRECTION_DOWN);
}

static void
do_circuit (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);

  /* Do a complete circuit along the test board; this will test all the easy
   * cases for switching direction..
   *
   * First go down.
   */

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_DOWN));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_DOWN));
  assert_state (state, TRUE, 2, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_DOWN));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_DOWN)); /* try going out of bounds */
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  /* Then go right */

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 3, 1, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 3, 2, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 3, 3, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_RIGHT)); /* try going out of bounds */
  assert_state (state, TRUE, 3, 3, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  /* Then go up */

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_UP));
  assert_state (state, TRUE, 2, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_UP));
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_UP)); /* try going out of bounds */
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  /* Then go left */

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_LEFT)); /* try going out of bounds */
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
}

static void
assert_skips_unselectable_cells (IPuzCellCoord start,
                                 IPuzClueDirection start_direction,
                                 gint start_clue_index,
                                 IPuzCellCoord end,
                                 IPuzClueDirection end_direction,
                                 gint end_clue_index,
                                 XwordCmdKind focus_forward,
                                 XwordCmdKind focus_back)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);

  state = xword_state_replace (state, xword_state_cell_selected (state, start));
  assert_state (state, TRUE, start.row, start.column, start_direction, start_clue_index);

  state = xword_state_replace (state, xword_state_do_command (state, focus_forward));
  assert_state (state, TRUE, end.row, end.column, end_direction, end_clue_index);

  state = xword_state_replace (state, xword_state_do_command (state, focus_back));
  assert_state (state, TRUE, start.row, start.column, start_direction, start_clue_index);

  xword_state_free (state);
}

static void
skips_unselectable_cells_across (void)
{
  IPuzCellCoord start = {
    .row = 1,
    .column = 0,
  };

  IPuzCellCoord end = {
    .row = 1,
    .column = 3,
  };

  assert_skips_unselectable_cells (start, IPUZ_CLUE_DIRECTION_DOWN, 0,
                                   end, IPUZ_CLUE_DIRECTION_DOWN, 1,
                                   XWORD_CMD_KIND_RIGHT,
                                   XWORD_CMD_KIND_LEFT);
}

static void
skips_unselectable_cells_down (void)
{
  IPuzCellCoord start = {
    .row = 0,
    .column = 1,
  };

  IPuzCellCoord end = {
    .row = 3,
    .column = 1,
  };

  assert_skips_unselectable_cells (start, IPUZ_CLUE_DIRECTION_ACROSS, 0,
                                   end, IPUZ_CLUE_DIRECTION_ACROSS, 1,
                                   XWORD_CMD_KIND_DOWN,
                                   XWORD_CMD_KIND_UP);
}

G_GNUC_WARN_UNUSED_RESULT static XwordState *
assert_stays_the_same_when_navigating (XwordState *state, guint row, guint column, XwordCmdKind cmd_kind)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));

  IPuzCellCoord old_coord = state->cursor;
  guint old_direction = state->clue.direction;
  guint old_index = state->clue.index;

  state = xword_state_replace (state, xword_state_do_command (state, cmd_kind));
  assert_state (state, TRUE, old_coord.row, old_coord.column, old_direction, old_index);
  return state;
}

static void
stops_at_bounds (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  state = assert_stays_the_same_when_navigating (state, 1, 0, XWORD_CMD_KIND_LEFT);
  state = assert_stays_the_same_when_navigating (state, 1, 3, XWORD_CMD_KIND_RIGHT);
  state = assert_stays_the_same_when_navigating (state, 0, 1, XWORD_CMD_KIND_UP);
  state = assert_stays_the_same_when_navigating (state, 3, 1, XWORD_CMD_KIND_DOWN);
  xword_state_free (state);
}

static void
select_clue (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzClue *clue;
  IPuzClueId clue_id = {
    .direction = IPUZ_CLUE_DIRECTION_DOWN,
    .index = 1,
  };

  clue = ipuz_crossword_get_clue_by_id (state->xword, clue_id);
  state = xword_state_replace (state, xword_state_clue_selected (state, clue));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  clue_id.direction = IPUZ_CLUE_DIRECTION_ACROSS;
  clue = ipuz_crossword_get_clue_by_id (state->xword, clue_id);
  state = xword_state_replace (state, xword_state_clue_selected (state, clue));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  xword_state_free (state);
}

static void
seleting_same_clue_leaves_cursor_unchanged (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzClue *clue;

  IPuzCellCoord coord = {
    .row = 1,
    .column = 3,
  };

  IPuzClueId clue_id = {
    .direction = IPUZ_CLUE_DIRECTION_DOWN,
    .index = 1,
  };
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  clue = ipuz_crossword_get_clue_by_id (state->xword, clue_id);
  state = xword_state_replace (state, xword_state_clue_selected (state, clue));
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  xword_state_free (state);
}

static void
no_focusable_cells_can_load (void)
{
  XwordState *state = load_state (NO_FOCUSABLE_CELLS_TEST_FILENAME, XWORD_STATE_SOLVE);
  assert_state (state, FALSE, 0, 0, IPUZ_CLUE_DIRECTION_NONE, 0);
  xword_state_free (state);
}

static void
no_clues_can_load (void)
{
  XwordState *state = load_state (NO_CLUES_TEST_FILENAME, XWORD_STATE_SOLVE);
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);
  xword_state_free (state);
}

static void
no_clues_can_select_cells (void)
{
  XwordState *state = load_state (NO_CLUES_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzCellCoord coord;

  coord.row = 0;
  coord.column = 1;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  coord.row = 1;
  coord.column = 0;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_NONE, 0);

  /* not selectable, position stays the same */
  coord.row = 1;
  coord.column = 1;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_NONE, 0);

  xword_state_free (state);
}

static void
no_clues_can_focus (void)
{
  XwordState *state = load_state (NO_CLUES_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzCellCoord coord;

  coord.row = 0;
  coord.column = 1;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_DOWN));
  assert_state (state, TRUE, 3, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  /* With direction = NONE, going forward/back does nothing since there is nowhere to go */
  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_FORWARD));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACK));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_SWITCH));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  xword_state_free (state);
}

static void
some_clues_can_load (void)
{
  XwordState *state = load_state (SOME_CLUES_TEST_FILENAME, XWORD_STATE_SOLVE);
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  xword_state_free (state);
}

static void
some_clues_can_select_cells (void)
{
  XwordState *state = load_state (SOME_CLUES_TEST_FILENAME, XWORD_STATE_SOLVE);
  IPuzCellCoord coord;

  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  /* Select a cell with no clue */
  coord.row = 3;
  coord.column = 1;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 3, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  /* Select a cell with a clue again */
  coord.row = 1;
  coord.column = 0;
  state = xword_state_replace (state, xword_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  xword_state_free (state);
}

static void
assert_guess (IPuzGuesses *guesses, guint row, guint column, const char *expected)
{
  IPuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  const char *guess = ipuz_guesses_get_guess (guesses, coord);
  g_assert_cmpstr (guess, ==, expected);
}

static void
guesses_enters_complete_guess_across (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  g_autoptr (IPuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, NULL);

  /* enter threee guesses, advancing the cursor at each... */
  state = xword_state_replace (state, xword_state_guess (state, "A"));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, "A");

  state = xword_state_replace (state, xword_state_guess (state, "B"));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 1, "B");

  state = xword_state_replace (state, xword_state_guess (state, "C"));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 2, "C");

  /* ... but upon entering the last guess of the clue, leave the cursor in place */
  state = xword_state_replace (state, xword_state_guess (state, "D"));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 3, "D");

  xword_state_free (state);
}

static void
guesses_enters_complete_guess_down (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  g_autoptr (IPuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_SWITCH));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, NULL);

  /* enter threee guesses, advancing the cursor at each... */
  state = xword_state_replace (state, xword_state_guess (state, "A"));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, "A");

  state = xword_state_replace (state, xword_state_guess (state, "B"));
  assert_state (state, TRUE, 2, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 1, 0, "B");

  state = xword_state_replace (state, xword_state_guess (state, "C"));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 2, 0, "C");

  /* ... but upon entering the last guess of the clue, leave the cursor in place */
  state = xword_state_replace (state, xword_state_guess (state, "D"));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 3, 0, "D");

  xword_state_free (state);
}

static void
guesses_backspace_across (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  g_autoptr (IPuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);
  assert_guess (guesses, 0, 0, NULL);

  state = xword_state_replace (state, xword_state_guess (state, "A"));
  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, "A");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, NULL);

  state = xword_state_replace (state, xword_state_guess (state, "A"));
  state = xword_state_replace (state, xword_state_guess (state, "B"));
  state = xword_state_replace (state, xword_state_guess (state, "C"));
  state = xword_state_replace (state, xword_state_guess (state, "D"));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 3, NULL);
  assert_guess (guesses, 0, 2, "C");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 2, NULL);
  assert_guess (guesses, 0, 1, "B");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 1, NULL);
  assert_guess (guesses, 0, 0, "A");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, NULL);
}

static void
guesses_backspace_down (void)
{
  XwordState *state = load_state (FOCUS_TEST_FILENAME, XWORD_STATE_SOLVE);
  g_autoptr (IPuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);
  assert_guess (guesses, 0, 0, NULL);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_SWITCH));

  state = xword_state_replace (state, xword_state_guess (state, "A"));
  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, "A");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, NULL);

  state = xword_state_replace (state, xword_state_guess (state, "A"));
  state = xword_state_replace (state, xword_state_guess (state, "B"));
  state = xword_state_replace (state, xword_state_guess (state, "C"));
  state = xword_state_replace (state, xword_state_guess (state, "D"));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 2, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 3, 0, NULL);
  assert_guess (guesses, 2, 0, "C");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 2, 0, NULL);
  assert_guess (guesses, 1, 0, "B");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 1, 0, NULL);
  assert_guess (guesses, 0, 0, "A");

  state = xword_state_replace (state, xword_state_do_command (state, XWORD_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, NULL);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/focus/initial_state", initial_state);
  g_test_add_func ("/focus/can_select_valid_cells", can_select_valid_cells);
  g_test_add_func ("/focus/selecting_cell_changes_direction", selecting_cell_changes_direction);
  g_test_add_func ("/focus/wont_select_null_cells", wont_select_null_cells);
  g_test_add_func ("/focus/wont_select_block_cells", wont_select_block_cells);
  g_test_add_func ("/focus/focuses_within_clue_across", focuses_within_clue_across);
  g_test_add_func ("/focus/focuses_within_clue_down", focuses_within_clue_down);
  g_test_add_func ("/focus/do_circuit", do_circuit);
  g_test_add_func ("/focus/skips_unselectable_cells_across", skips_unselectable_cells_across);
  g_test_add_func ("/focus/skips_unselectable_cells_down", skips_unselectable_cells_down);
  g_test_add_func ("/focus/stops_at_bounds", stops_at_bounds);

  g_test_add_func ("/focus/select_clue", select_clue);
  g_test_add_func ("/focus/seleting_same_clue_leaves_cursor_unchanged", seleting_same_clue_leaves_cursor_unchanged);

  g_test_add_func ("/no_clues/no_focusable_cells_can_load", no_focusable_cells_can_load);
  g_test_add_func ("/no_clues/can_load", no_clues_can_load);
  g_test_add_func ("/no_clues/can_select_cells", no_clues_can_select_cells);
  g_test_add_func ("/no_clues/can_focus", no_clues_can_focus);

  g_test_add_func ("/some_clues/can_load", some_clues_can_load);
  g_test_add_func ("/some_clues/can_select_cells", some_clues_can_select_cells);

  g_test_add_func ("/guesses/enters_complete_guess_across", guesses_enters_complete_guess_across);
  g_test_add_func ("/guesses/enters_complete_guess_down", guesses_enters_complete_guess_down);
  g_test_add_func ("/guesses/backspace_across", guesses_backspace_across);
  g_test_add_func ("/guesses/backspace_down", guesses_backspace_down);

  g_test_add_func ("/layout/sets_main_text_for_solve_mode", layout_sets_main_text_for_solve_mode);
  g_test_add_func ("/layout/sets_main_text_for_browse_mode", layout_sets_main_text_for_browse_mode);
  g_test_add_func ("/layout/sets_main_text_for_edit_mode", layout_sets_main_text_for_edit_mode);
  g_test_add_func ("/layout/sets_main_text_for_edit_browse_mode", layout_sets_main_text_for_edit_browse_mode);
  g_test_add_func ("/layout/sets_main_text_for_select_mode", layout_sets_main_text_for_select_mode);
  g_test_add_func ("/layout/sets_main_text_for_view_mode", layout_sets_main_text_for_view_mode);
  g_test_add_func ("/layout/sets_main_text_to_initial_val", layout_sets_main_text_to_initial_val);
  g_test_add_func ("/layout/loads_barred", layout_loads_barred);

  return g_test_run ();
}
