/* xword-state.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "xword-state.h"
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "crosswords-quirks.h"

#define STATE_DEHYDRATED(state) (state && state->xword == NULL)

static XwordState *  xword_state_clone                   (XwordState        *state);
static gboolean      find_first_focusable_cell           (IPuzCrossword     *xword,
                                                          IPuzCellCoord     *out_coord);
static gboolean      get_first_cell_safe                 (IPuzClue          *clue,
                                                          IPuzCellCoord     *coord);
static void          set_initial_cursor                  (XwordState        *state);
static void          xword_state_clue_selected_mut       (XwordState        *state,
                                                          IPuzClue          *clue);
static void          xword_state_cell_selected_mut       (XwordState        *state,
                                                          IPuzCellCoord      coord);
static IPuzClue *    get_next_clue                       (XwordState        *state,
                                                          IPuzClueId         prev_clue,
                                                          gint               incr);
static IPuzCellCoord coord_for_forward_back              (IPuzCellCoord      coord,
                                                          IPuzClueDirection  direction,
                                                          gint               incr);
static void          xword_state_focus_forward_back      (XwordState        *state,
                                                          gint               incr);
static void          xword_state_focus_forward_back_clue (XwordState        *state,
                                                          gint               incr);
static void          xword_state_focus_up_down           (XwordState        *state,
                                                          gint               incr);
static void          xword_state_focus_left_right        (XwordState        *state,
                                                          gint               incr);
static void          xword_state_focus_toggle            (XwordState        *state);
static void          xword_state_delete                  (XwordState        *state);
static void          xword_state_backspace               (XwordState        *state);
static void          xword_state_solve_guess             (XwordState        *state,
                                                          const gchar       *guess,
                                                          IPuzCellCoord      coord);
static void          xword_state_editable_guess          (XwordState        *state,
                                                          const gchar       *guess,
                                                          IPuzCellCoord      coord);


static XwordState *
xword_state_clone (XwordState *state)
{
  XwordState *c = g_new0 (XwordState, 1);
  *c = *state;
  if (state->xword)
    c->xword = g_object_ref (state->xword);
  if (state->cell_array)
    c->cell_array = cell_array_ref (state->cell_array);
  if (state->quirks)
    c->quirks = g_object_ref (state->quirks);

  return c;
}

static gboolean
find_first_focusable_cell (IPuzCrossword *xword,
                           IPuzCellCoord *out_coord)
{
  out_coord->row = 0;
  out_coord->column = 0;

  guint rows = ipuz_crossword_get_height (xword);
  guint columns = ipuz_crossword_get_width (xword);
  guint row, column;

  for (row = 0; row < rows; row++)
    {
      for (column = 0; column < columns; column++)
        {
          IPuzCellCoord coord = {
            .row = row,
            .column = column,
          };
          IPuzCell *cell = ipuz_crossword_get_cell (xword, coord);

          if (IPUZ_CELL_IS_NORMAL (cell))
            {
              *out_coord = coord;
              return TRUE;
            }
        }
    }

  return FALSE;
}

/* This is a bit of a safety hack against weird puzzles. See libipuz
 * Issue #14 for more information.  It's effectively
 * ipuz_clue_get_first_cell();
 */
static gboolean
get_first_cell_safe (IPuzClue      *clue,
                     IPuzCellCoord *coord)
{
  const GArray *cells;

  g_assert (coord);

  cells = ipuz_clue_get_cells (clue);
  if (cells == NULL || cells->len == 0)
    {
      coord->row = 0;
      coord->column = 0;
      return FALSE;
    }

  ipuz_clue_get_first_cell (clue, coord);
  return TRUE;
}

static void
set_initial_cursor (XwordState *state)
{
  IPuzClueId clue_id;

  if (state->mode == XWORD_STATE_VIEW)
    {
      state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
      return;
    }
  /* Get the first clue we can find, or none */
  if (ipuz_crossword_get_n_clues (state->xword, IPUZ_CLUE_DIRECTION_ACROSS) > 0)
    {
      clue_id.direction = IPUZ_CLUE_DIRECTION_ACROSS;
      clue_id.index = 0;
    }
  else if (ipuz_crossword_get_n_clues (state->xword, IPUZ_CLUE_DIRECTION_DOWN) > 0)
    {
      clue_id.direction = IPUZ_CLUE_DIRECTION_DOWN;
      clue_id.index = 0;
    }
  else
    {
      clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;
      clue_id.index = 0;
    }

  if (clue_id.direction != IPUZ_CLUE_DIRECTION_NONE)
    {
      IPuzClue *clue = ipuz_crossword_get_clue_by_id (state->xword, clue_id);
      g_assert (clue != NULL);

      /* Select the first cell in that clue */
      if (!get_first_cell_safe (clue, &state->cursor))
        {
          clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;
        }
    }
  else
    {
      find_first_focusable_cell (state->xword, &state->cursor);
    }

  state->clue = clue_id;
}


/* Public functions */

/**
 * xword_state_new:
 * @xword: An `IPuzCrossword`
 *
 * Creates a new XwordState containing @xword. By default, the initial
 * cell will be the first across clue in the puzzle.
 *
 * Returns: (transfer full) The newly allocated `XwordState`
 **/
XwordState *
xword_state_new (IPuzCrossword    *xword,
                 CrosswordsQuirks *quirks,
                 XwordStateMode    mode)
{
  XwordState *state;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (xword), NULL);

  state = g_new0 (XwordState, 1);

  state->xword = g_object_ref (xword);
  if (quirks)
    state->quirks = (CrosswordsQuirks *)g_object_ref (quirks);
  state->mode = mode;

  if (mode == XWORD_STATE_SELECT)
    state->cell_array = cell_array_new ();

  set_initial_cursor (state);

  return state;
}

/**
 * xword_state_dehydrate:
 * @state: A `XwordState`
 *
 * Creates a *dehydrated* `XwordState`.
 *
 * In general, a `XwordState` has all the information to recreate the
 * state of a board by containing cursor information as well as the
 * puzzle. Occasionally, we need to store the state of board without
 * the puzzle. In those instances, we can create a dehydrated
 * `XwordState` that just contains cursor information. It can be
 * reversed by calling xword_state_hydrate() with an appropriate puzzle
 *
 * No operations can or should be done on a dehydrated state. It
 * can be freed with xword_state_free()
 *
 * Returns: A new `XwordState` without the xword set.
 **/
XwordState *
xword_state_dehydrate (XwordState *state)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);
  g_clear_object (&state->xword);
  g_clear_object (&state->quirks);

  return state;
}

/**
 * xword_state_hydrate:
 * @state: A `XwordState`
 * @xword: An `IPuzCrossword`
 *
 * *Rehydrates* a dehydrated `XwordState`. see xword_state_dehydrate()
 for more information.
 *
 * Returns: A new `XwordState` with the xword set to @xword.
 **/
XwordState *
xword_state_hydrate (XwordState        *state,
                     IPuzCrossword    *xword,
                     CrosswordsQuirks *quirks)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (IPUZ_IS_CROSSWORD (xword), NULL);
  g_return_val_if_fail (STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);
  state->xword = g_object_ref (xword);
  if (quirks)
    state->quirks = g_object_ref (quirks);

  return state;
}

/**
 * xword_state_free:
 * @state: A `XwordState`
 *
 * Frees a `XwordState` and associated resources.
 **/
void
xword_state_free (XwordState *state)
{
  g_return_if_fail (state != NULL);

  if (state->xword)
    g_object_unref (G_OBJECT (state->xword));
  if (state->cell_array)
    cell_array_unref (state->cell_array);
  if (state->quirks)
    g_object_unref (state->quirks);

  g_free (state);
}

/**
 * xword_state_replace:
 * @old_state: old state which will be freed.
 * @new_state: new state which will replace it.
 *
 * `XwordState` is immutable, so its functions return a new
 * `XwordState` instead of changing an existing one.  This function is
 * a convenience to avoid freeing a lot of old `XwordState` by hand
 * every time one of those functions is called. You can use `state =
 * xword_state_replace (state, xword_state_foo (state, BAR)`, for
 * example.
 *
 * Returns: `new_state`.
 */
XwordState *
xword_state_replace (XwordState *old_state,
                     XwordState *new_state)
{
  if (old_state)
    xword_state_free (old_state);

  return new_state;
}

/**
 * xword_state_change_mode:
 * @state: A `XwordState`
 * @clue: A `IPuzClue`
 *
 * Updates @state to have the new mode state.
 *
 * Returns: A new `XwordState` with the new mode.
 **/
XwordState *
xword_state_change_mode (XwordState     *state,
                         XwordStateMode  mode)
{
  XwordState *new_state;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  new_state = xword_state_clone (state);

  /* No change. */
  if (mode == state->mode)
    return new_state;

  new_state->mode = mode;

  /* if the old mode was VIEW, we need to try to set the cursor. We
   * select the first available clue */
  if (state->mode == XWORD_STATE_VIEW)
    {
      set_initial_cursor (state);
    }
  else if (mode == XWORD_STATE_VIEW)
    {
      /* Unset the cursor. This indicates it has no real value */
      new_state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
    }

  return new_state;
}


static void
xword_state_clue_selected_mut (XwordState *state,
                               IPuzClue   *clue)
{
  IPuzClueId clue_id;

  clue_id = ipuz_crossword_get_clue_id (state->xword, clue);
  if (ipuz_clue_id_equal (&state->clue, &clue_id))
    return;

  if (get_first_cell_safe (clue, &state->cursor))
    state->clue = clue_id;
  else
    state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
}

/**
 * xword_state_clue_selected:
 * @state: A `XwordState`
 * @clue: A `IPuzClue`
 *
 * Updates @state to have the currently selected clue to be
 * @clue. The cursor will be set to be the first unguessed cell in
 * clue. Otherwise, it will put the cursor on the first cell.
 *
 * Returns: A new `XwordState` with the new state.  Remember to free the old one.
 **/
XwordState *
xword_state_clue_selected (XwordState *state,
                           IPuzClue   *clue)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (clue != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);
  if (state->mode != XWORD_STATE_VIEW)
    xword_state_clue_selected_mut (state, clue);

  return state;
}

static void
xword_state_cell_selected_mut (XwordState     *state,
                               IPuzCellCoord  coord)
{
  IPuzCell *cursor_cell;
  const IPuzClue *new_clue;

  cursor_cell = ipuz_board_get_cell (ipuz_crossword_get_board (state->xword), coord);
  g_return_if_fail (cursor_cell != NULL);

  if (IPUZ_CELL_IS_NULL (cursor_cell))
    return;

  /* Do nothing if you click on a block if mode != EDIT */
  if (IPUZ_CELL_IS_BLOCK (cursor_cell) && state->mode != XWORD_STATE_EDIT)
    return;

  if (state->mode == XWORD_STATE_SELECT)
    {
      /* This is only really used for the autofill dialog. Thus, we
       * only let you select a cell if it doesn't have any
       * solutions. If we ever allow broader selection for the
       * crossword, we should change this logic. */
      if (ipuz_cell_get_solution (cursor_cell) == NULL)
        cell_array_toggle (state->cell_array, coord);
      return;
    }

  /* If you click on the focus cell, we change orientation... */
  if (state->cursor.row == coord.row &&
      state->cursor.column == coord.column)
    {
      new_clue = ipuz_cell_get_clue (cursor_cell,
                                     ipuz_clue_direction_switch (state->clue.direction));

      if (new_clue)
        {
          state->clue = ipuz_crossword_get_clue_id (state->xword, new_clue);
        }

      return;
    }

  state->cursor = coord;

  if (state->clue.direction != IPUZ_CLUE_DIRECTION_NONE)
    {
      new_clue = ipuz_cell_get_clue (cursor_cell, state->clue.direction);
      if (new_clue == NULL) {
        new_clue = ipuz_cell_get_clue (cursor_cell,
                                       ipuz_clue_direction_switch (state->clue.direction));
      }
    }
  else
    {
      /* No current direction, so pick any that is available */

      new_clue = ipuz_cell_get_clue (cursor_cell, IPUZ_CLUE_DIRECTION_ACROSS);
      if (!new_clue)
        {
          new_clue = ipuz_cell_get_clue (cursor_cell, IPUZ_CLUE_DIRECTION_DOWN);
        }
    }

  state->clue = ipuz_crossword_get_clue_id (state->xword, new_clue);
}

/**
 * xword_state_cell_selected:
 * @state: A `XwordState`
 * @coord: Coordinates of the cell to be selected
 *
 * Updates @state to have a new focused cell. If the selected cell is
 * the current cell, then it will change the direction of the clue, if
 * that's an option.
 *
 * Returns: A new `XwordState` with the new state.  Remember to free the old one.
 **/
XwordState *
xword_state_cell_selected (XwordState    *state,
                           IPuzCellCoord  coord)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);
  if (state->mode != XWORD_STATE_VIEW)
    xword_state_cell_selected_mut (state, coord);

  return state;
}

/* returns the next clue logically in a puzzle. We wrap around from
 * across<->down and back. */
static IPuzClue *
get_next_clue (XwordState *state,
               IPuzClueId  prev_clue,
               gint        incr)
{
  gint clue_index;
  guint n_clues;

  g_assert (incr == -1 || incr == 1);

  /* No direction - find the first clue at whatever direction is
   * available */
  /* FIXME(cluesets): make this work with clue sets */
  if (IPUZ_CLUE_ID_IS_UNSET (prev_clue))
    {
      IPuzClueId clue_id = {
        .direction = IPUZ_CLUE_DIRECTION_ACROSS,
        .index = 0
      };
      IPuzClue *clue = ipuz_crossword_get_clue_by_id (state->xword, clue_id);

      if (clue)
        return clue;

      clue_id.direction = IPUZ_CLUE_DIRECTION_DOWN;
      return ipuz_crossword_get_clue_by_id (state->xword, clue_id);
    }

  clue_index = prev_clue.index;
  n_clues = ipuz_crossword_get_n_clues (state->xword, prev_clue.direction);

  if (clue_index == 0 && incr == -1)
    {
      IPuzClueDirection other_dir = ipuz_clue_direction_switch (prev_clue.direction);
      guint other_n_clues = ipuz_crossword_get_n_clues (state->xword, other_dir);
      IPuzClueId clue_id = {
        .direction = other_dir,
        .index = other_n_clues - 1,
      };

      if (other_n_clues > 0)
        return ipuz_crossword_get_clue_by_id (state->xword, clue_id);
      else
        return NULL;
    }
  else if ((guint) (clue_index + incr) >= n_clues)
    {
      IPuzClueId clue_id = {
        .index = 0,
      };

      clue_id.direction = ipuz_clue_direction_switch (prev_clue.direction);
      return ipuz_crossword_get_clue_by_id (state->xword, clue_id);
    }
  else
    {
      IPuzClueId clue_id = {
        .direction = prev_clue.direction,
        .index = clue_index + incr,
      };
      return ipuz_crossword_get_clue_by_id (state->xword, clue_id);
    }
}

static IPuzCellCoord
coord_for_forward_back (IPuzCellCoord     coord,
                        IPuzClueDirection direction,
                        gint              incr)
{
  g_assert (incr == -1 || incr == 1);

  if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
    coord.column += incr;
  else if (direction == IPUZ_CLUE_DIRECTION_DOWN)
    coord.row += incr;

  return coord;
}

static void
xword_state_focus_forward_back (XwordState *state,
                                gint       incr)
{
  if (state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    {
      /* If there is no direction, there's nowhere to go. */
      return;
    }

  /* Don't move past the last cell in the current clue */

  IPuzCellCoord advanced_coord = coord_for_forward_back (state->cursor, state->clue.direction, incr);
  IPuzCell *cell = ipuz_crossword_get_cell (state->xword, advanced_coord);

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      const IPuzClue *clue = ipuz_cell_get_clue (cell, state->clue.direction);
      if (clue)
        {
          IPuzClueId clue_id = ipuz_crossword_get_clue_id (state->xword, clue);
          if (ipuz_clue_id_equal (&clue_id, &state->clue))
            {
              xword_state_cell_selected_mut (state, advanced_coord);
            }
        }
    }
}

static void
xword_state_focus_forward_back_clue (XwordState *state,
                                     gint        incr)
{
  IPuzClue *next_clue = get_next_clue (state, state->clue, incr);

  if (next_clue)
    {
      xword_state_clue_selected_mut (state, next_clue);
    }
}

/* Keep in sync with left/right below */
static void
xword_state_focus_up_down (XwordState *state,
                           gint       incr)
{
  IPuzCell *cell;

  g_assert (incr == -1 || incr == 1);

  /* this will underflow when cursor_row == 0, but that should be okay.
   */
  IPuzCellCoord coord = {
    .row = state->cursor.row + incr,
    .column = state->cursor.column
  };

  cell = ipuz_crossword_get_cell (state->xword, coord);
  while (cell)
    {
      if (IPUZ_CELL_IS_NORMAL (cell) || state->mode == XWORD_STATE_EDIT)
        {
          xword_state_cell_selected_mut (state, coord);
          /* If we change directions on move, we toggle the cursor twice */
          if (crosswords_quirks_get_switch_on_move (state->quirks) &&
              state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
            xword_state_cell_selected_mut (state, coord);
          break;
        }

      coord.row += incr;
      cell = ipuz_crossword_get_cell (state->xword, coord);
    }
}
/* Keep in sync with up/down above */
static void
xword_state_focus_left_right (XwordState *state,
                              gint        incr)
{
  IPuzCell *cell;

  g_assert (incr == -1 || incr == 1);

  /* this will underflow when cursor_column == 0, but that should be okay.
   */
  IPuzCellCoord coord = {
    .row = state->cursor.row,
    .column = state->cursor.column + incr
  };

  cell = ipuz_crossword_get_cell (state->xword, coord);
  while (cell)
    {
      if (IPUZ_CELL_IS_NORMAL (cell) || state->mode == XWORD_STATE_EDIT)
        {
          xword_state_cell_selected_mut (state, coord);
          /* If we change directions on move, we toggle the cursor twice */
          if (crosswords_quirks_get_switch_on_move (state->quirks) &&
              state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
            xword_state_cell_selected_mut (state, coord);
          break;
        }

      coord.column += incr;
      cell = ipuz_crossword_get_cell (state->xword, coord);
    }
}

static void
xword_state_focus_toggle (XwordState *state)
{
  xword_state_cell_selected_mut (state, state->cursor);
}

static void
xword_state_delete (XwordState *state)
{
  IPuzCellCoord coord = {
    .row = state->cursor.row,
    .column = state->cursor.column
  };

  /* FIXME(cleanup): I don't love this behaviour. We are counting on
   * solve_guess/editable_guess to not advance the cursor when
   * done. Otherwise we go back, and then advance the cursor by
   * entering some text */
  if (state->mode == XWORD_STATE_EDIT)
    xword_state_editable_guess (state, NULL, coord);
  else
    xword_state_solve_guess (state, NULL, coord);
}

static void
xword_state_backspace (XwordState *state)
{
  xword_state_delete (state);
  xword_state_focus_forward_back (state, -1);
}


/**
 * xword_state_do_command:
 * @state: A `XwordState`
 * @kind: Command kind.
 *
 * Updates the state based on a command @kind.  Not all commands are valid in all states;
 * invalid ones will be ignored and a new state equivalent to the old one will be
 * returned.
 *
 * Returns: A new `XwordState` with the new state.  Remember to free the old one.
 **/
XwordState *
xword_state_do_command (XwordState   *state,
                        XwordCmdKind  kind)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);

  if (state->mode == XWORD_STATE_VIEW)
    return state;

  switch (kind)
    {
    case XWORD_CMD_KIND_UP:
      xword_state_focus_up_down (state, -1);
      break;
    case XWORD_CMD_KIND_RIGHT:
      xword_state_focus_left_right (state, 1);
      break;
    case XWORD_CMD_KIND_DOWN:
      xword_state_focus_up_down (state, 1);
      break;
    case XWORD_CMD_KIND_LEFT:
      xword_state_focus_left_right (state, -1);
      break;
    case XWORD_CMD_KIND_FORWARD:
      xword_state_focus_forward_back (state, 1);
      break;
    case XWORD_CMD_KIND_BACK:
      xword_state_focus_forward_back (state, -1);
      break;
    case XWORD_CMD_KIND_FORWARD_CLUE:
      xword_state_focus_forward_back_clue (state, 1);
      break;
    case XWORD_CMD_KIND_BACK_CLUE:
      xword_state_focus_forward_back_clue (state, -1);
      break;
    case XWORD_CMD_KIND_SWITCH:
      xword_state_focus_toggle (state);
      break;
    case XWORD_CMD_KIND_BACKSPACE:
      xword_state_backspace (state);
      break;
    case XWORD_CMD_KIND_DELETE:
      xword_state_delete (state);
      break;
    default:
      break;
    }

  return state;
}

/* Guess a cell, but don't advance the cursor. That's handled elsewhere */
static void
xword_state_solve_guess (XwordState    *state,
                         const gchar   *guess,
                         IPuzCellCoord  coord)
{
  IPuzGuesses *guesses;
  const gchar *cur_guess;

  guesses = ipuz_crossword_get_guesses (state->xword);
  if (guesses == NULL)
    {
      return;
    }

  cur_guess = ipuz_guesses_get_guess (guesses, coord);

  /* Handle Dutch crosswords */
  if (crosswords_quirks_get_ij_digraph (state->quirks))
    {
      if (g_strcmp0 (cur_guess, "I") == 0 &&
          g_strcmp0 (guess, "J") == 0)
        {
          ipuz_guesses_set_guess (guesses, coord, "IJ");
          return;
        }
    }

  if (g_strcmp0 (guess, cur_guess) != 0)
    {
      ipuz_guesses_set_guess (guesses, coord, guess);
      return;
    }
}

static void
xword_state_editable_guess (XwordState    *state,
                            const gchar   *guess,
                            IPuzCellCoord  coord)
{
  const gchar *cur_solution;
  IPuzCell *cell;

  cell = ipuz_crossword_get_cell (state->xword, coord);
  cur_solution = ipuz_cell_get_solution (cell);

  /* We can get NULL as a guess. Makes this a little messy */
  if (guess == NULL)
    {
      if (cur_solution == NULL || cur_solution[0] == '\0')
        return;
      ipuz_cell_set_solution (cell, NULL);
      return;
    }

  /* Check to see if we're writing a block */
  if (strstr (XWORD_STATE_BLOCK_CHARS, guess) != NULL)
    {
      if (IPUZ_CELL_IS_BLOCK (cell))
        ipuz_crossword_set_cell_type (state->xword, coord, IPUZ_CELL_NORMAL);
      else
        ipuz_crossword_set_cell_type (state->xword, coord, IPUZ_CELL_BLOCK);

      /* Change the cell type */
      ipuz_crossword_calculate_clues (state->xword);

      if (IPUZ_CELL_IS_BLOCK (cell))
        {
          state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
          state->clue.index = 0;
        }
      else
        xword_state_cell_selected_mut (state, coord);

      return;
    }

  /* We have a normal character. Write that. */
  if (g_strcmp0 (guess, cur_solution) != 0)
    {
      gboolean update_clue = IPUZ_CELL_IS_BLOCK (cell);

      ipuz_crossword_set_cell_type (state->xword, coord, IPUZ_CELL_NORMAL);
      ipuz_cell_set_solution (cell, guess);
      if (update_clue)
        {
          const IPuzClue *clue;

          ipuz_crossword_calculate_clues (state->xword);
          clue = ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_ACROSS);
          if (!clue)
            clue = ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_ACROSS);
          state->clue = ipuz_crossword_get_clue_id (state->xword, clue);
        }
    }
}

static void
guess_advance_adjacent (XwordState *state)
{
  IPuzCellCoord advanced_coord = coord_for_forward_back (state->cursor, state->clue.direction, 1);
  IPuzCell *cell = ipuz_crossword_get_cell (state->xword, advanced_coord);

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      const IPuzClue *clue = ipuz_cell_get_clue (cell, state->clue.direction);
      if (clue)
        {
          IPuzClueId clue_id = ipuz_crossword_get_clue_id (state->xword, clue);
          if (ipuz_clue_id_equal (&clue_id, &state->clue))
            {
              xword_state_focus_forward_back (state, 1);
            }
        }
    }
}


/* This will iterate through a clue until it finds an open space. If
 * it finds one, it will set the cursor to that open space and return
 * TRUE. Otherwise, it will return FALSE */
static gboolean
advance_through_clue_until_open (XwordState *state,
                                 IPuzClue   *clue,
                                 gboolean    start_at_cursor,
                                 gboolean    end_at_cursor,
                                 gboolean   *found_end_cursor)
{
  const GArray *cells;
  gboolean found_cursor = FALSE;
  IPuzGuesses *guesses;

  g_assert (clue != NULL);

  cells = ipuz_clue_get_cells (clue);
  if (cells == NULL)
    {
      g_warning ("Puzzle has a clue without cells");
      return FALSE;
    }

  guesses = ipuz_crossword_get_guesses (state->xword);
  for (guint i = 0; i < cells->len; i++)
    {
      IPuzCellCoord coord;
      const gchar *guess;

      coord = g_array_index (cells, IPuzCellCoord, i);
      if ((start_at_cursor || end_at_cursor) &&
          coord.column == state->cursor.column &&
          coord.row == state->cursor.row)
        {
          if (end_at_cursor)
            {
              if (found_end_cursor)
                *found_end_cursor = TRUE;
              return FALSE;
            }

          if (start_at_cursor)
            {
              found_cursor = TRUE;
              continue;
            }
        }

      if (start_at_cursor && !found_cursor)
        continue;

      guess = ipuz_guesses_get_guess (guesses, coord);
      if (guess == NULL)
        {
          xword_state_cell_selected_mut (state, coord);
          return TRUE;
        }
    }

  return FALSE;
}

static void
guess_advance_open (XwordState *state)
{
  IPuzClue *clue_ptr;
  gboolean start_at_cursor;
  gboolean end_at_cursor;
  gboolean found_end_cursor;
  IPuzClueId clue_id;

  clue_ptr = ipuz_crossword_get_clue_by_id (state->xword, state->clue);
  clue_id = state->clue;
  if (clue_ptr == NULL)
    return;

  start_at_cursor = TRUE;
  end_at_cursor = FALSE;
  found_end_cursor = FALSE;

  while (TRUE)
    {
      if (advance_through_clue_until_open (state, clue_ptr,
                                           start_at_cursor,
                                           end_at_cursor,
                                           &found_end_cursor))
        return;

      if (found_end_cursor)
        return;
      start_at_cursor = FALSE;
      end_at_cursor = TRUE;
      clue_ptr = get_next_clue (state, clue_id, 1);
      clue_id = ipuz_crossword_get_clue_id (state->xword, clue_ptr);
    }
}

static void
guess_advance_open_in_clue (XwordState *state)
{
  IPuzClue *clue;
  gboolean found_end_cursor = FALSE;

  clue = ipuz_crossword_get_clue_by_id (state->xword, state->clue);
  if (clue == NULL)
    return;

  /* First go to the end if possible. */
  if (advance_through_clue_until_open (state, clue,
                                       TRUE, FALSE, NULL))
    return;

  /* Then, try to find an open space earlier in the clue */
  advance_through_clue_until_open (state, clue,
                                   FALSE, TRUE, &found_end_cursor);
  if (! found_end_cursor)
    return;

  /* We didn't find anything open, so we just go to the next cell */
  guess_advance_adjacent (state);
}

static void
guess_advance (XwordState *state)
{

  /* FIXME: #101 "Entering a letter in crossword-editor should advance
   * to the next letter"
   * We should make this a preference and write this */
  if (state->mode == XWORD_STATE_EDIT)
    return;

  if (state->mode != XWORD_STATE_SOLVE)
    return;

  switch (crosswords_quirks_get_guess_advance (state->quirks))
    {
    case QUIRKS_GUESS_ADVANCE_ADJACENT:
      guess_advance_adjacent (state);
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN:
      guess_advance_open (state);
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE:
      guess_advance_open_in_clue (state);
      break;
    default:
      g_assert_not_reached ();
    }
}


/**
 * xword_state_guess:
 * @state: A `XwordState`
 * @guess: (nullable) A new guess
 *
 * Adds a guess to the current focused cell. If @guess is NULL or "",
 * then the cell is cleared.
 *
 * Returns: A new `XwordState` with the new state.  Remember to free the old one.
 **/
XwordState *
xword_state_guess (XwordState   *state,
                   const gchar *guess)
{
  IPuzCellCoord coord = {
    .row = state->cursor.row,
    .column = state->cursor.column
  };

  g_return_val_if_fail (state != NULL, FALSE);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);

  if (state->mode == XWORD_STATE_EDIT)
    {
      xword_state_editable_guess (state, guess, coord);
      guess_advance (state);
    }
  else if (state->mode == XWORD_STATE_SOLVE)
    {
      xword_state_solve_guess (state, guess, coord);
      guess_advance (state);
    }

  return state;
}

XwordState *
xword_state_guess_at_cell (XwordState       *state,
                           const gchar      *guess,
                           IPuzCellCoord     coord)
{
  g_return_val_if_fail (state != NULL, FALSE);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);

  if (state->mode == XWORD_STATE_VIEW)
    return state;

  if (state->mode == XWORD_STATE_EDIT)
    {
      xword_state_editable_guess (state, guess, coord);
    }
  else
    {
      xword_state_solve_guess (state, guess, coord);
    }

  return state;

}


/**
 * xword_state_guess_word:
 * @state: A `XwordState`
 * @coord: A `IPuzCellCoord` in which to start the word
 * @direction: A `IPuzClueDirection`in which to fill in the word
 * @word: A sequence of UTF-8 characters to insert into the state
 *
 * Adds @word to the current @state one character at a time. It will
 * start at @coord and will continue in direction indicated by
 * @direction. It will stop adding the clue once it hits a block.
 *
 * If the word has a "?" in it, it will skip that letter.
 *
 * Returns: A new `XwordState` with the new state.  Remember to free the old one.
 **/
XwordState *
xword_state_guess_word (XwordState        *state,
                        IPuzCellCoord      coord,
                        IPuzClueDirection  direction,
                        const gchar       *word)
{
  g_autofree gchar *normalized_word = NULL;
  const gchar *ptr;
  const gchar *end;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (word != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);

  if (!(state->mode == XWORD_STATE_EDIT ||
        state->mode == XWORD_STATE_SOLVE))
    return state;

  normalized_word = g_utf8_normalize (word, -1, G_NORMALIZE_NFC);

  ptr = normalized_word;
  while (ptr[0])
    {
      g_autofree gchar *cluster = NULL;
      IPuzCellCoord old_cursor;

      cluster = utf8_get_next_cluster (ptr, &end);
      ptr = end;

      /* cluster being NULL means we couldn't parse some text and
       * should stop processing */
      if (cluster == NULL)
        break;

      /* We skip any characters that would set a block */
      if (strstr (XWORD_STATE_BLOCK_CHARS, cluster) != NULL)
        continue;

      old_cursor = state->cursor;
      if (strstr (XWORD_STATE_SKIP_CHARS, cluster) != NULL)
        {
          xword_state_focus_forward_back (state, 1);
        }
      else if (state->mode == XWORD_STATE_EDIT)
        {
          xword_state_editable_guess (state, cluster, state->cursor);
          xword_state_focus_forward_back (state, 1);
        }
      else if (state->mode == XWORD_STATE_SOLVE)
        {
          xword_state_solve_guess (state, cluster, state->cursor);
          xword_state_focus_forward_back (state, 1);
        }

      /* If the cursor didn't move, we hit a block or an edge */
      if (state->cursor.row == old_cursor.row &&
          state->cursor.column == old_cursor.column)
        break;
    }

  return state;
}

XwordState *
xword_state_set_reveal_mode (XwordState      *state,
                             XwordRevealMode  reveal_mode)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! STATE_DEHYDRATED (state), NULL);

  state = xword_state_clone (state);
  state->reveal_mode = reveal_mode;
  return state;
}

void
xword_state_assert_equal (const XwordState *a,
                          const XwordState *b)
{
  g_assert_cmpint (a->clue.direction, ==, b->clue.direction);
  g_assert_cmpint (a->clue.index,     ==, b->clue.index);
  g_assert_cmpint (a->cursor.row,     ==, b->cursor.row);
  g_assert_cmpint (a->cursor.column,  ==, b->cursor.column);
  g_assert_cmpint (a->reveal_mode,    ==, b->reveal_mode);
  g_assert_cmpint (a->mode,           ==, b->mode);
  g_assert (a->xword == b->xword);
  g_assert (a->quirks == b->quirks);
}

void
xword_state_print (const XwordState *state,
                   gboolean          print_members)
{
  g_return_if_fail (state != NULL);

  g_print ("XwordState (%p)\n", state);
  g_print ("\txword: (%p)\n", state->xword);
  g_print ("\tcursor: (%u, %u)\n", state->cursor.row, state->cursor.column);
  g_print ("\tclue: (");
  switch (state->clue.direction)
    {
    case IPUZ_CLUE_DIRECTION_ACROSS:
      g_print ("ACROSS");
      break;
    case IPUZ_CLUE_DIRECTION_DOWN:
      g_print ("DOWN");
      break;
    case IPUZ_CLUE_DIRECTION_NONE:
      g_print ("NONE");
      break;
    default:
      g_print ("UNKNOWN");
      break;
    }
  g_print (", %d)\n", state->clue.index);
  g_print ("\treveal_mode: ");
  switch (state->reveal_mode)
    {
    case XWORD_REVEAL_NONE:
      g_print ("NONE\n");
      break;
    case XWORD_REVEAL_ERRORS_BOARD:
      g_print ("ERRORS_BOARD\n");
      break;
    case XWORD_REVEAL_ERRORS_CLUE:
      g_print ("ERRORS_CLUE\n");
      break;
    case XWORD_REVEAL_ERRORS_CELL:
      g_print ("ERRORS_CELL\n");
      break;
    case XWORD_REVEAL_ALL:
      g_print ("ALL\n");
      break;
    }
  g_print ("\tcell_array: %p\n", state->cell_array);
  g_print ("\tquirks: %p\n", state->quirks);

  if (print_members && state->xword)
    ipuz_crossword_print (state->xword);
}
