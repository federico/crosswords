/* gen-word-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <locale.h>
#include "charset.h"
#include "word-list-index.h"
#include "word-list-misc.h"
#include "gen-word-list.h"

#include <glib/gi18n.h>
#include <glib.h>
#include <gio/gio.h>
#include <gio/gunixoutputstream.h>

typedef struct
{
  char *word;
  gboolean free_word;

  /* Number of code points in the word (3 for FOO and FÖÖ). */
  gint word_len;

  /* Number of bytes in the word, without the nul terminator (3 for FOO, 5 for FÖÖ). */
  gssize byte_len;

  /* Priority of the word; comes from the corpus. */
  guchar priority;
} ValidatedWord;


/* A single word in the word list.
 *
 * This starts out with all fields filled by parse_line() and gen_word_list_add_word(),
 * except for the index field, which gets initialized when the WordSection buckets are
 * computed after sorting all the words.
 */
typedef struct
{
  /* Index of the word within its bucket.  Only valid after gen_word_list_calculate_offsets() is called. */
  guint index;

  /* The word plus its nul terminator. */
  gchar *word;

  /* Number of code points in the word (3 for FOO and FÖÖ). */
  gint word_len;

  /* Collation key for sorting. */
  gchar *collate_key;

  /* Number of bytes in the word, without the nul terminator (3 for FOO, 5 for FÖÖ). */
  gssize byte_len;

  /* Priority of the word; comes from the corpus. */
  guchar priority;
} WordEntry;

typedef struct
{
  /* Elements are gushort, which are indices into each word-length bucket. */
  GArray *array;
  guint offset;
} FilterFragmentList;

/* This is a g_array_set_clear_func */
static void
clear_word_entry (gpointer data)
{
  WordEntry *entry = data;

  g_clear_pointer (&entry->word, g_free);
  g_clear_pointer (&entry->collate_key, g_free);
  entry->index = 0;
  entry->word_len = 0;
  entry->byte_len = 0;
  entry->priority = 0;
}

GenWordList  *
gen_word_list_new (gint min_length,
                    gint max_length,
                    gint threshold)
{
  GenWordList *word_list;

  word_list = g_new0 (GenWordList, 1);
  word_list->charset = charset_new ();

  word_list->words = g_array_new (FALSE, TRUE, sizeof (WordEntry));
  g_array_set_clear_func (word_list->words, clear_word_entry);

  word_list->word_index = g_array_new (FALSE, TRUE, sizeof (WordListSection));

  word_list->threshold = threshold;
  word_list->min_length = min_length;
  word_list->max_length = max_length;

  return word_list;
}

void
gen_word_list_free (GenWordList *word_list)
{
  g_clear_pointer (&word_list->words, g_array_unref);
  g_clear_pointer (&word_list->letters, g_array_unref);
  g_clear_pointer (&word_list->word_index, g_array_unref);

  if (word_list->charset)
    {
      charset_free (word_list->charset);
      word_list->charset = NULL;
    }

  g_free (word_list);
}

static gboolean
valid_char (gunichar c)
{
  return g_unichar_type (c) == G_UNICODE_UPPERCASE_LETTER;
}

static gboolean
valid_word (const char *word)
{
  const char *ptr;
  gsize len = 0;

  for (ptr = word; ptr[0]; ptr = g_utf8_next_char (ptr))
    {
      gunichar c;

      c = g_utf8_get_char (ptr);
      if (!valid_char (c))
        return FALSE;

      len++;
    };

  if (len == 0)
    return FALSE;
  else
    return TRUE;
}

typedef enum {
  PARSE_LINE_STATUS_OK,
  PARSE_LINE_STATUS_BAD_SYNTAX,
  PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE,
  PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE,
} ParseLineStatus;

static ParseLineStatus
validate_word (const gchar   *word,
               gboolean       free_word,
               gssize         byte_len,
               gint           word_len,
               glong          priority,
               gint           min_length,
               gint           max_length,
               guchar         threshold,
               ValidatedWord *out_validated)
{
  if (!valid_word (word))
    {
      return PARSE_LINE_STATUS_BAD_SYNTAX;
    }

  if (priority < threshold || priority > 255)
    {
      return PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE;
    }

  if (word_len < min_length || word_len > max_length)
    {
      return PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE;
    }
  else
    {
      out_validated->word = (char *) word;
      out_validated->free_word = free_word;
      out_validated->byte_len = byte_len;
      out_validated->word_len = word_len;
      out_validated->priority = priority;

      return PARSE_LINE_STATUS_OK;
    }
}

static void
validated_word_free (ValidatedWord v)
{
  if (v.free_word)
    {
      g_free (v.word);
    }
}

/* Consumes the line; then @out_validated must be freed by the caller with
 * validated_word_free().
 */
static ParseLineStatus
parse_line (ValidatedWord *out_validated,
            gchar         *line,
            gint           min_length,
            gint           max_length,
            guchar         threshold)
{
  ParseLineStatus status;
  gchar *composed;
  gssize byte_len;
  gint word_len;
  glong number;
  gchar *start_of_number;
  gchar *after_number;
  gchar *semicolon;

  line = g_strstrip (line);
  composed = g_utf8_normalize (line, -1, G_NORMALIZE_NFC);
  g_free (line);
  line = composed;

  semicolon = g_strstr_len (line, -1, ";");
  if (semicolon == NULL)
    {
      status = PARSE_LINE_STATUS_BAD_SYNTAX;
      goto err;
    }

  semicolon[0] = '\0';
  byte_len = semicolon - line;
  word_len = g_utf8_strlen (line, byte_len);

  start_of_number = semicolon + 1;
  number = strtol (start_of_number, &after_number, 10);
  if (after_number == start_of_number
      || *after_number != '\0')
    {
      status = PARSE_LINE_STATUS_BAD_SYNTAX;
      goto err;
    }

  status = validate_word (line, TRUE, byte_len, word_len,
                          number, min_length, max_length,
                          threshold, out_validated);

  if (status == PARSE_LINE_STATUS_OK)
    {
      return PARSE_LINE_STATUS_OK;
    }

 err:
  g_free (line);
  return status;
}

static void
gen_word_list_add_word (GenWordList   *word_list,
                        ValidatedWord *validated)
{
  WordEntry entry = {
    .index = G_MAXUINT, /* "uninitialized" */
    .word = g_strdup (validated->word),
    .word_len = validated->word_len,
    .byte_len = validated->byte_len,
    .collate_key = g_utf8_collate_key (validated->word, validated->byte_len),
    .priority = validated->priority,
  };

  g_array_append_val (word_list->words, entry);
}

/**
 * gen_word_list_add_test_word():
 * @word_list: The word list generator.
 * @word: Word to add; must be valid or this function will abort.
 * @priority: Priority for the added word, must be above the @word_list's threshold.
 *
 * Adds a test word to the word list.  It is a programming error to
 * try to add an invalid word, per validate_word() - this function will
 * abort if an invalid word gets passed.
 */
void
gen_word_list_add_test_word (GenWordList *word_list,
                             const char  *word,
                             glong        priority)
{
  gssize byte_len = strlen (word);
  gint word_len = g_utf8_strlen (word, byte_len);
  ValidatedWord validated;
  ParseLineStatus status;

  status = validate_word (word, FALSE, byte_len, word_len, priority,
                          word_list->min_length, word_list->max_length, word_list->threshold,
                          &validated);
  switch (status)
    {
    case PARSE_LINE_STATUS_OK:
      break;

    case PARSE_LINE_STATUS_BAD_SYNTAX:
      g_error ("bad syntax: word %s", word);
      return; /* unreachable */

    case PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE:
      g_error ("word length out of range: %s", word);
      return; /* unreachable */

    case PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE:
      g_error ("priority out of range: %ld", priority);
      return; /* unreachable */

    default:
      g_assert_not_reached ();
      return; /* unreachable */
    }

  gen_word_list_add_word (word_list, &validated);
  validated_word_free (validated);
}

gboolean
gen_word_list_parse (GenWordList *word_list,
                     GInputStream *stream)
{
  g_autoptr (GDataInputStream) data_stream = NULL;
  gchar *line;

  data_stream = g_data_input_stream_new (stream);

  line = g_data_input_stream_read_line_utf8 (data_stream, NULL, NULL, NULL);
  while (line)
    {
      ValidatedWord validated;

      if (parse_line (&validated, line, word_list->min_length, word_list->max_length, word_list->threshold) == PARSE_LINE_STATUS_OK)
        {
          gen_word_list_add_word (word_list, &validated);
          g_free ((char *) validated.word);
        }

      line = g_data_input_stream_read_line_utf8 (data_stream, NULL, NULL, NULL);
    }
  return TRUE;
}

static gint
word_array_sort (gconstpointer a,
                 gconstpointer b)
{
  WordEntry *entry_a = (WordEntry *)a;
  WordEntry *entry_b = (WordEntry *)b;

  if (entry_a->word_len != entry_b->word_len)
    return (entry_a->word_len - entry_b->word_len);
  if (entry_a->priority != entry_b->priority)
    return (entry_b->priority - entry_a->priority);
  return strcmp (entry_a->collate_key, entry_b->collate_key);
}

void
gen_word_list_sort (GenWordList *word_list)
{
  g_array_sort (word_list->words, (GCompareFunc) word_array_sort);
}

void
gen_word_list_build_charset (GenWordList *word_list)
{
  guint i;

  for (i = 0; i < word_list->words->len; i++)
    {
      WordEntry entry = g_array_index (word_list->words, WordEntry, i);
      charset_add (word_list->charset, entry.word);
    }
}

static void
gen_word_list_add_word_to_letters (GenWordList *word_list,
                                   const gchar *word,
                                   WordIndex    word_index)
{
  gint pos = 0;
  const gchar *ptr;
  FilterFragment fragment;
  fragment.length = g_utf8_strlen (word, -1);

  for (ptr = word; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragmentList *frag_list;
      gsize index;

      fragment.position = pos;
      fragment.char_index = charset_index (word_list->charset, g_utf8_get_char (ptr));

      index = word_index_fragment_index (word_list->min_length, charset_num_characters (word_list->charset), fragment);
      g_assert (index < word_list->letters->len);
      frag_list = &(g_array_index (word_list->letters, FilterFragmentList, index));

      /* if this triggers, we have more letters in a given filter
       * fragment than we can store (more than 65536). In that
       * instance, we need to cull our input or adjust the format to support wider ints */
      g_assert (word_index.index < G_MAXUINT16);
      g_array_append_val (frag_list->array, word_index.index);
      //      g_print ("adding %d for word %s at index %d\n", word_index.index, word, index);

      pos++;
    }

}

/* This is a g_array_set_clear_func */
static void
clear_filter_fragment_list (gpointer data)
{
  FilterFragmentList *list = data;

  g_clear_pointer (&list->array, g_array_unref);
}

static guint
word_entry_stride (WordEntry *entry)
{
  /* 1 byte for priority, plus UTF-8 bytes, plus nul terminator. */
  return entry->byte_len + 2;
}

void
gen_word_list_calculate_offsets (GenWordList *word_list)
{
  guint i;
  guint letters_size;
  guint section_stride = 0;
  guint section_offset = 0;

  letters_size = word_index_calculate_letters_size (word_list->min_length,
                                                    word_list->max_length,
                                                    charset_num_characters (word_list->charset));
  word_list->letters = g_array_new (FALSE, TRUE, sizeof (FilterFragmentList));
  g_array_set_clear_func (word_list->letters, clear_filter_fragment_list);
  g_array_set_size (word_list->letters, letters_size);

  for (i = 0; i < letters_size; i++)
    {
      FilterFragmentList *frag_list;
      frag_list = &(g_array_index (word_list->letters, FilterFragmentList, i));
      frag_list->array = g_array_new (FALSE, TRUE, sizeof (gushort));
      frag_list->offset = 0;
    }

  /* Find the start and end words of the current section */

  /* Compute the maximum stride for all the words in the section */

  /* Use the maximum stride as the offset betwen words */

  guint section_start_idx = 0;
  while (section_start_idx < word_list->words->len)
    {
      WordEntry *first_entry = &(g_array_index (word_list->words, WordEntry, section_start_idx));
      section_stride = word_entry_stride (first_entry);
      guint next_section_idx;

      /* Walk forwards until we reach the next section (i.e. the first word
       * with a different length).
       *
       * Along the way, find the maximum stride of all the words in the present section.
       */

      for (next_section_idx = section_start_idx + 1; next_section_idx < word_list->words->len; next_section_idx++)
        {
          WordEntry *next_entry = &(g_array_index (word_list->words, WordEntry, next_section_idx));
          if (next_entry->word_len != first_entry->word_len)
            break;

          guint next_stride = word_entry_stride (next_entry);
          section_stride = MAX (section_stride, next_stride);
        }

      /* Generate section info */

      WordListSection section = {
        .word_len = first_entry->word_len,
        .stride = section_stride,
        .count = next_section_idx - section_start_idx,
        .offset = section_offset,
      };
      g_array_append_val (word_list->word_index, section);

      section_offset += section.count * section_stride;

      /* Now assign the indexes to each word in the present section */

      gint index = 0;
      guint k;

      for (k = section_start_idx; k < next_section_idx; k++)
        {
          WordEntry *entry = &(g_array_index (word_list->words, WordEntry, k));
          entry->index = index;

          WordIndex word_index = {
            .length = entry->word_len,
            .index = index,
          };

          index++;
          gen_word_list_add_word_to_letters (word_list, entry->word, word_index);
        }

      section_start_idx = next_section_idx;
    }
}

static void
word_list_write_words (GenWordList   *word_list,
                       GOutputStream *stream)
{
  g_autoptr (GError) error = NULL;
  guint i;
  guint word_num = 0;
  guint section_num;
  gssize total_offset = 0;

  for (section_num = 0; section_num < word_list->word_index->len; section_num++)
    {
      WordListSection *section = &(g_array_index (word_list->word_index, WordListSection, section_num));
      guint i;

      for (i = 0; i < section->count; i++)
        {
          WordEntry *entry;
          gssize entry_bytes = 0;

          entry = &(g_array_index (word_list->words, WordEntry, word_num));
          entry_bytes += g_output_stream_write (stream,
                                                &(entry->priority), 1,
                                                NULL, &error);
          g_assert (error == NULL);

          entry_bytes += g_output_stream_write (stream,
                                                entry->word, entry->byte_len,
                                                NULL, &error);
          g_assert (error == NULL);

          int padding;
          for (padding = 0; padding < section->stride - entry_bytes; padding++)
            {
              g_output_stream_write (stream, "\0", 1, NULL, &error);
            }

          total_offset += section->stride;

          word_num++;
        }
    }

  /* Write the letter list to file */
  word_list->letter_list_offset = total_offset;

  for (i = 0; i < word_list->letters->len; i++)
    {
      FilterFragmentList *frag_list;

      frag_list = &(g_array_index (word_list->letters, FilterFragmentList, i));
      frag_list->offset = total_offset;
      /* FIXME(serialize): We are counting on this serializing correctly. */
      if (frag_list->array->len > 0)
        total_offset += g_output_stream_write (stream,
                                               frag_list->array->data,
                                               (frag_list->array->len) * 2,
                                               NULL, &error);
      g_assert (error == NULL);
    }

  /* write the letter_index for the letter list */
  word_list->letter_index_offset = total_offset;

  for (i = 0; i < word_list->letters->len; i++)
    {
      FilterFragmentList *frag_list;
      guchar buf[6];
      gushort len;

      frag_list = &(g_array_index (word_list->letters, FilterFragmentList , i));
      g_assert (frag_list->array->len < G_MAXUINT16);
      len = (gushort) frag_list->array->len;

      /* FIXME(serialize): This marshalling is also arch-dependent */
      /* FIXME(serialize): our offset includes the header, so is
         absolute and not relative. We fix it on the other end, but
         it's different from the documentation. We should make this
         consistent */
      memcpy (buf, (char*) &(frag_list->offset), sizeof (guint));
      memcpy (buf + 4, (char *) &(len), sizeof (gushort));

      total_offset += g_output_stream_write (stream,
                                             buf,
                                             6,
                                             NULL, &error);
    }

  /* NULL terminate the section */
  char nil = '\0';
  total_offset += g_output_stream_write (stream, &nil, 1,
                                         NULL, &error);
}

static void
word_list_write_index (GenWordList   *word_list,
                       GOutputStream *stream)
{
  GString *string;
  guint i;
  g_autofree char *charset_str = NULL;

  charset_str = charset_serialize (word_list->charset);

  string = g_string_new (NULL);
  g_string_append (string, "{\n");
  g_string_append_printf (string, "  \"charset\": \"%s\",\n", charset_str);
  g_string_append (string, "  \"filterchar\": \"?\",\n");
  g_string_append_printf (string, "  \"min-length\": %d,\n", word_list->min_length);
  g_string_append_printf (string, "  \"max-length\": %d,\n", word_list->max_length);
  g_string_append_printf (string, "  \"threshold\": %d,\n", word_list->threshold);
  g_string_append_printf (string, "  \"letter-list-offset\": %d,\n", (gint)word_list->letter_list_offset);
  g_string_append_printf (string, "  \"letter-index-offset\": %d,\n", (gint)word_list->letter_index_offset);
  g_string_append (string, "  \"words\": [\n");

  for (i = 0; i < word_list->word_index->len; i++)
    {
      WordListSection *header_section;

      header_section = &(g_array_index (word_list->word_index, WordListSection, i));
      g_string_append_printf (string, "\t\t[%d, %d, %u, %d]",
                              header_section->word_len,
                              header_section->stride,
                              header_section->offset,
                              header_section->count);
      if (i + 1 < word_list->word_index->len)
        g_string_append (string, ",\n");
      else
        g_string_append (string, "\n");
    }
  g_string_append (string, "\t]\n}\n");
  g_output_stream_write (stream,
                         string->str, string->len,
                         NULL, NULL);

  g_string_free (string, TRUE);
}

void
gen_word_list_write_to_stream (GenWordList *word_list,
                               GOutputStream *stream)
{
  word_list_write_words (word_list, stream);
  word_list_write_index (word_list, stream);
}

void
gen_word_list_write (GenWordList   *word_list,
                     const char    *output_filename)

{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GOutputStream) stream = NULL;
  g_autoptr (GError) error = NULL;

  if (output_filename != NULL)
    {
      file = g_file_new_for_commandline_arg (output_filename);
      stream = (GOutputStream *) g_file_replace (file, NULL,
                                                 FALSE, G_FILE_CREATE_REPLACE_DESTINATION,
                                                 NULL, &error);
      if (error)
        {
          g_printerr (_("Error saving file %s: %s\n"), output_filename, error->message);
          return;
        }
    }
  else
    {
      stream = g_unix_output_stream_new (1, FALSE);
    }

  g_assert (stream);

  gen_word_list_write_to_stream (word_list, stream);

  g_output_stream_close (stream, NULL, NULL);
}

#ifdef TESTING
static void
rejects_invalid_lines (void)
{
  ValidatedWord v;

  /* invalid syntax */
  g_assert_cmpint (parse_line (&v, g_strdup (""), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line (&v, g_strdup ("FOO"), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line (&v, g_strdup (";42"), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line (&v, g_strdup ("a;42"), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line (&v, g_strdup ("A;"), 1, 100, 0), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line (&v, g_strdup ("A;B"), 1, 100, 0), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line (&v, g_strdup ("A;-"), 1, 100, 0), ==, PARSE_LINE_STATUS_BAD_SYNTAX);

  /* word length out of range */
  g_assert_cmpint (parse_line (&v, g_strdup ("ABC;10"), 1, 2, 1), ==, PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE);
  g_assert_cmpint (parse_line (&v, g_strdup ("A;10"), 3, 3, 1), ==, PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE);
  g_assert_cmpint (parse_line (&v, g_strdup ("ABCD;10"), 3, 3, 1), ==, PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE);

  /* priority out of range */
  g_assert_cmpint (parse_line (&v, g_strdup ("ABCD;10"), 4, 4, 20), ==, PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE);
  g_assert_cmpint (parse_line (&v, g_strdup ("ABCD;-10"), 4, 4, 20), ==, PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE);
  g_assert_cmpint (parse_line (&v, g_strdup ("ABCD;10000000000000000000000000000000000000000000"), 4, 4, 20),
                   ==,
                   PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE);
}

static void
get_entry_works_for_ascii_words (void)
{
  ValidatedWord v;

  /* Plain old word */
  g_assert_cmpint (parse_line (&v, g_strdup ("FOOBAR;42"), 1, 100, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "FOOBAR");
  g_assert_cmpint (v.byte_len, ==, 6);
  g_assert_cmpint (v.word_len, ==, 6);
  g_assert_cmpint (v.priority, ==, 42);
  validated_word_free (v);

  /* Exact size */
  g_assert_cmpint (parse_line (&v, g_strdup ("A;42"), 1, 1, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "A");
  g_assert_cmpint (v.byte_len, ==, 1);
  g_assert_cmpint (v.word_len, ==, 1);
  g_assert_cmpint (v.priority, ==, 42);
  validated_word_free (v);

  /* Just at threshold */
  g_assert_cmpint (parse_line (&v, g_strdup ("FOO;50"), 2, 10, 50), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "FOO");
  g_assert_cmpint (v.byte_len, ==, 3);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);

  /* Max priority */
  g_assert_cmpint (parse_line (&v, g_strdup ("FOO;255"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "FOO");
  g_assert_cmpint (v.byte_len, ==, 3);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 255);
  validated_word_free (v);

  /* Carriage return or whitespace at the end, like in peter-broda-wordlist__scored.txt */
  g_assert_cmpint (parse_line (&v, g_strdup ("STY;80\r"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "STY");
  g_assert_cmpint (v.byte_len, ==, 3);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 80);
  validated_word_free (v);
}

static void
get_entry_works_for_non_ascii_words (void)
{
  ValidatedWord v;

  /* Normal, already composed */
  g_assert_cmpint (parse_line (&v, g_strdup ("AÑO;50"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "AÑO");
  g_assert_cmpint (v.byte_len, ==, 4);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);

  /* Decomposed to composed */
  g_assert_cmpint (parse_line (&v, g_strdup ("AÑO;50"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "AÑO");
  g_assert_cmpint (v.byte_len, ==, 4);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);

  /* Decomposed to composed */
  g_assert_cmpint (parse_line (&v, g_strdup ("BLÅHAJ;50"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "BLÅHAJ");
  g_assert_cmpint (v.byte_len, ==, 7);
  g_assert_cmpint (v.word_len, ==, 6);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);
}

static void
sorts_words (void)
{
  const char *input = "ÁRBOL;1\nARTES;1\nPERRO;1\n";
  g_autoptr (GInputStream) stream = g_memory_input_stream_new_from_data (input, strlen (input), NULL);

  GenWordList *word_list = gen_word_list_new (1, 10, 1);
  g_assert (gen_word_list_parse (word_list, stream));
  gen_word_list_sort (word_list);

  g_assert_cmpint (word_list->words->len, ==, 3);

  WordEntry entry;

  entry = g_array_index (word_list->words, WordEntry, 0);
  g_assert_cmpstr (entry.word, ==, "ÁRBOL");

  entry = g_array_index (word_list->words, WordEntry, 1);
  g_assert_cmpstr (entry.word, ==, "ARTES");

  entry = g_array_index (word_list->words, WordEntry, 2);
  g_assert_cmpstr (entry.word, ==, "PERRO");
}

static void
word_list_builds_charset (void)
{
  const char *input = "ÁRBOL;1\nBLÅHAJ;1\nBORLA;1\nTRALALA;1\n";
  g_autoptr (GInputStream) stream = g_memory_input_stream_new_from_data (input, strlen (input), NULL);

  GenWordList *word_list = gen_word_list_new (1, 10, 1);
  g_assert (gen_word_list_parse (word_list, stream));
  gen_word_list_sort (word_list);
  gen_word_list_build_charset (word_list);

  g_assert_cmpint (charset_num_characters (word_list->charset), ==, 10);
  g_autofree char *serialized = charset_serialize (word_list->charset);

  /* Characters are sorted by Unicode code point, not logically.  I guess that doesn't matter? */
  g_assert_cmpstr (serialized, ==, "ABHJLORTÁÅ");
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/gen_word_list/rejects_invalid_lines", rejects_invalid_lines);
  g_test_add_func ("/gen_word_list/get_entry_works_for_ascii_words", get_entry_works_for_ascii_words);
  g_test_add_func ("/gen_word_list/get_entry_works_for_non_ascii_words", get_entry_works_for_non_ascii_words);
  g_test_add_func ("/gen_word_list/sorts_words", sorts_words);
  g_test_add_func ("/gen_word_list/builds_charset", word_list_builds_charset);

  return g_test_run ();
}
#endif
