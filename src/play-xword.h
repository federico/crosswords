/* play-xword.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

G_BEGIN_DECLS


/* Determines what type of behavior we have when the game is
 * won. Currently unused */
typedef enum
{
  PLAY_XWORD_MODE_NONE,
} PlayXwordMode;

#define PLAY_TYPE_XWORD (play_xword_get_type())
G_DECLARE_FINAL_TYPE (PlayXword, play_xword, PLAY, XWORD, GtkWidget);


GtkWidget *  play_xword_new             (void);
void         play_xword_set_mode        (PlayXword       *xword,
                                         PlayXwordMode    mode);
void         play_xword_load_puzzle     (PlayXword       *xword,
                                         IPuzPuzzle      *puzzle);
IPuzPuzzle  *play_xword_get_puzzle      (PlayXword       *xword);
IPuzGuesses *play_xword_get_guesses     (PlayXword       *xword);
const gchar *play_xword_get_title       (PlayXword       *xword);
void         play_xword_show_hint       (PlayXword       *xword);
void         play_xword_set_reveal_mode (PlayXword       *xword,
                                         XwordRevealMode  reveal_mode);
void         play_xword_clear_puzzle    (PlayXword       *xword);


G_END_DECLS
