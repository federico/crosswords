/* puzzle-picker.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS


#define PUZZLE_TYPE_PICKER (puzzle_picker_get_type())
G_DECLARE_DERIVABLE_TYPE (PuzzlePicker, puzzle_picker, PUZZLE, PICKER, GtkWidget);

struct _PuzzlePickerClass
{
  GtkWidgetClass widget_class;

  /* Virtual function */
  void        (*load)         (PuzzlePicker  *picker);
  const char *(*get_title)    (PuzzlePicker  *picker);
  const char *(*get_uri)      (PuzzlePicker  *picker);
  void        (*save_state)   (PuzzlePicker  *picker);
  void        (*update_state) (PuzzlePicker  *picker);

  void        (*load_uri)     (PuzzlePicker  *picker,
                               const gchar   *uri,
                               gboolean       delete_when_done,
                               GError       **error);
  void        (*clear_puzzle) (PuzzlePicker  *picker,
                               IPuzPuzzle    *puzzle,
                               const gchar   *primary,
                               const gchar   *secondary,
                               gboolean       play_when_done,
                               gboolean       allow_view);
};

void           puzzle_picker_load                    (PuzzlePicker  *picker);
const gchar   *puzzle_picker_get_title               (PuzzlePicker  *picker);
const gchar   *puzzle_picker_get_uri                 (PuzzlePicker  *picker);
void           puzzle_picker_save_state              (PuzzlePicker  *picker);
void           puzzle_picker_update_state            (PuzzlePicker  *picker);
void           puzzle_picker_load_uri                (PuzzlePicker  *picker,
                                                      const gchar   *uri,
                                                      gboolean       delete_when_done,
                                                      GError       **error);
void           puzzle_picker_clear_puzzle            (PuzzlePicker  *picker,
                                                      IPuzPuzzle    *puzzle,
                                                      const gchar   *primary,
                                                      const gchar   *secondary,
                                                      gboolean       play_when_done,
                                                      gboolean       allow_view);
void           puzzle_picker_puzzle_selected         (PuzzlePicker  *picker,
                                                      IPuzPuzzle    *puzzle);
void           puzzle_picker_clear_selected          (PuzzlePicker  *picker);
void           puzzle_picker_update_selected_guesses (PuzzlePicker  *picker,
                                                      IPuzGuesses   *guesses);



const gchar   *puzzle_picker_get_id                  (PuzzlePicker  *picker);
GKeyFile      *puzzle_picker_get_key_file            (PuzzlePicker  *picker);
const gchar   *puzzle_picker_get_directory           (PuzzlePicker  *picker);
GResource     *puzzle_picker_get_resource            (PuzzlePicker  *picker);

/* List methods */
guint          puzzle_picker_get_n_puzzles           (PuzzlePicker  *picker);
IPuzPuzzle    *puzzle_picker_get_puzzle              (PuzzlePicker  *picker,
                                                      guint          n);
const gchar   *puzzle_picker_get_puzzle_name         (PuzzlePicker  *picker,
                                                      guint          n);
const gchar   *puzzle_picker_get_last_saved_checksum (PuzzlePicker  *picker,
                                                      guint          n);
gboolean       puzzle_picker_get_readonly            (PuzzlePicker  *picker,
                                                      guint          n);

void           puzzle_picker_remove_puzzle           (PuzzlePicker  *picker,
                                                      IPuzPuzzle    *puzzle);

/* Accessible public functions */
void           crosswords_save_guesses               (IPuzGuesses   *guesses,
                                                      const gchar   *id,
                                                      const gchar   *puzzle_name);
IPuzGuesses   *crosswords_load_guesses               (const gchar   *id,
                                                      const gchar   *puzzle_name);
void           crosswords_discard_guesses            (const gchar   *id,
                                                      const gchar   *puzzle_name);


G_END_DECLS
